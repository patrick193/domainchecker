# DOMAINCHECKER #

A project should check top level domains and show status of the domain(free or not).

## WHAT ABOUT DEPENDENCIES? ##

The project dependence on the ioncube module. This module help to protect and License the scripts. You can download it from the link below

```html
http://www.ioncube.com/php_encoder.php
```

## How can i work with it? ##

First of all you should change a configuration file. 
You have to change a data for database connection in the configuration file.
As we use a whmcs system we hatve tu create a sub domain with username. 
For example 

```html
http://admin.web-domain.com
```

Path to configuration file:

```html
$ configuration.php
``` 

Also you need change API credentials. You can do it 

```html
$ includes/domain-check/dc_const.php
```
You have to change User and password for Virturo API.


### Multiple processes  ###

What did i use:

* CURL
* curl_multi_exec function

### What did changed and improved? ###

As for code optimization 

* check and rewriting almost all loops
* multiple processing
* added one more function for prepare and check new curl chunk
* added new function for checking answer from server(Virturo API)
* added a function for three status types(free, unavailable and can not check)