<?php
// Use debugging output
DEFINE('DC_DEBUG', false);
DEFINE('DC_CAPTCHA', false);

// Possible errors
DEFINE('DC_ER_NONE',	0);
DEFINE('DC_ER_CAPTCHA',	1);

// Virturo API access credentials   
DEFINE('VIRTURO_CRED_URL',			'http://login.virturo.de/api.php');
DEFINE('VIRTURO_CRED_API_USER',		'fc2a683f9de0b0cf1b460c4957772a45654320ed');
DEFINE('VIRTURO_CRED_API_PASS',		'ccec4ab17294340adac1e2fe1805b22e3f9a65c0');
DEFINE('VIRTURO_CRED_API_CLASS',	'DomainManagement');
DEFINE('VIRTURO_CRED_API_METHOD',	'Start');

DEFINE('VIRTURO_CRED_API_MULTI_CURL',	true);	// Use multi-cURL requests for domain check
DEFINE('VIRTURO_CRED_API_LIMIT',		100);	// Maximum time execution limit for domain check
DEFINE('VIRTURO_CRED_API_CHUNK',		200);		// Maximum domains for one multi-cURL request. The list of domains is splitted into chunks and every chunk is requested separately to preserve time execution limit.

?>

