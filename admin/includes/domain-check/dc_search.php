<?php
define( 'CLIENTAREA', true );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/init.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/modules/addons/domainpricesimport/const.php');

require_once(dirname(__FILE__) . '/dc_const.php');

// Get the domains Categories
$categories = array();
$cat_default = 0;

$query = full_query( "SELECT DISTINCT ".DPI_DB_FLD_CATEGORY.", ".DPI_DB_FLD_ID." FROM ".DPI_DB_TBL_CATEGORIES." ORDER by ".DPI_DB_FLD_CATEGORY );

while ($row = mysql_fetch_array( $query )) {
	$categories[] = array( 'id' => $row[DPI_DB_FLD_ID], 'name' => $row[DPI_DB_FLD_CATEGORY] );
	
	// Default category is Europe
	if ( strcasecmp($row[DPI_DB_FLD_CATEGORY], 'Europa') == 0 )
		$cat_default = count($categories) - 1;
}

// Default Category is Europe
if ( empty($_POST['dc_cat']) ) {
	$current_cat = $categories[$cat_default]['id'];
} else {
	$current_cat = $_POST['dc_cat'];
}

// decoding query variables
$dc_url = parse_url($_POST['dc_src']);
$dc_query = htmlspecialchars_decode($dc_url['query']);
$dc_query = str_replace( 'amp;', '', $dc_query );

parse_str( $dc_query, $query_vars );

// Using Smarty to display the search form
global $whmcs;
global $_LANG;

if($_POST['dc_cur'] == 'EUR') {
	$query_vars['dc_cur'] = 'CHF';
	$dc_cur_name = $_LANG['domain_check']['EUR'];
} else {
	$query_vars['dc_cur'] = 'EUR';
	$dc_cur_name = $_LANG['domain_check']['CHF'];
}

$dc_smarty = new Smarty();
$dc_smarty->caching = 0;
$dc_smarty->compile_dir = $whmcs->get_template_compiledir_name();

$dc_smarty->assign( 'LANG', $_LANG );

$dc_smarty->assign( 'dc_dom', $_POST['dc_dom'] );
$dc_smarty->assign( 'dc_categories', $categories );
$dc_smarty->assign( 'dc_curr_cat', $current_cat );
$dc_smarty->assign( 'dc_country', $_POST['dc_country'] );
$dc_smarty->assign( 'dc_cur_name', $dc_cur_name );
$dc_smarty->assign( 'dc_curr_url', htmlspecialchars($dc_url['path'] . '?' . http_build_query($query_vars)) );
$dc_smarty->assign( 'dc_switch_curr', $query_vars['dc_cur'] );
$dc_smarty->assign( 'logged_in', $_POST['logged_in'] );
$dc_smarty->assign( 'use_captcha', $_POST['use_captcha'] );
$dc_smarty->assign( 'dc_error', $_POST['dc_error'] );
$dc_smarty->assign( 'sid', session_id() );

// if this is a request from product domain selection page - we must redirect the search results to the same page
// also we must save $_GET query variables and pass them back to the product domain page ((configureproductdomain.tpl))
$dc_smarty->assign( 'dc_is_product', $_POST['dc_is_product'] );

if ( $_POST['dc_is_product'] ) {
	$dc_smarty->assign( 'dc_target_url', htmlspecialchars($dc_url['path']) );
	
	foreach ( $query_vars as $var_key => $var_value ) {
		if ( !in_array( $var_key, array( 'dc_dom', 'dc_cat', 'dc_cur', 'btnPruefen', 'dc_captcha', 'dc_error' ) ) ) {
			$get_vars[$var_key] = $var_value;
		}
	}
	
	$dc_smarty->assign( 'dc_get_vars', $get_vars );
} else {
	$dc_smarty->assign( 'dc_target_url', 'domain_check.php' );
}

echo $dc_smarty->fetch( ROOTDIR . '/templates/' . $whmcs->get_config('Template') . '/dc_search.tpl' );
?>
