<?php

// *** Get the country and currency for a user
function dc_user_info(&$country, &$curr) {
    // *** Getting user info
    $country = '';
    $curr = '';

    // If the client is not logged in we take his location from GeoIP and currency = EUR
    // otherwise we take country and currency from user's profile
    if (!empty($_SESSION["uid"])) {
        require_once( ROOTDIR . '/includes/clientfunctions.php' );

        $client_info = getClientsDetails($_SESSION["uid"]);

        $country = $client_info['countryname'];

        $user_cur = getCurrency($_SESSION["uid"]);
        $curr = $user_cur['code'];
    } else {
        if (empty($country)) {
            require_once( dirname(__FILE__) . '/geoip/master/src/geoip.inc' );

            $gi = geoip_open(dirname(__FILE__) . '/geoip/GeoIP.dat', GEOIP_STANDARD);

            $gi_code = geoip_country_code_by_addr($gi, $_SERVER[REMOTE_ADDR]);
            $country = geoip_country_name_by_addr($gi, $_SERVER[REMOTE_ADDR]);

            geoip_close($gi);
        }

        if ($_GET['dc_cur'] != '') {

            if ($_GET['dc_cur'] == 'CHF') {
                $curr = 'CHF';
            } else {
                $curr = 'EUR';
            }
        } elseif (empty($curr)) {

            if ($gi_code == 'CH') {
                $curr = 'CHF';
            } else {
                $curr = 'EUR';
            }
        }
    }
}

// *** Prepare domain name from user's input. Make it safe and remove all extra staff
function dc_prepare_sld($domain) {
    // Process user input before doing the search
    $dc_dom = strtolower($domain);

    // - remove the protocol part if present (http://, https://, ...)
    while (strpos($dc_dom, ':') !== FALSE) {
        $domain_parts = explode(':', $dc_dom, 2);
        $dc_dom = $domain_parts[1];
    }

    // - remove invalid characters
    $dc_dom = preg_replace('/[^a-z0-9-.]/', '', $dc_dom);

    // - remove 'www.' if present
    $dc_dom = str_replace('www.', '', $dc_dom);

    return $dc_dom;
}

// *** Removes non-numeric characters from the beginning and the end of the string that contains price
function dc_trim_price($price_str) {
    $price_chars = str_split($price_str, 1);
    $allowed = array_map('strval', range(0, 9, 1)); // an array of numbers 0..9

    $start = 0;
    $end = 0;

    // searching for the first numeric character
    for ($i = 0; $i < count($price_chars); $i++)
        if (in_array($price_chars[$i], $allowed)) {
            $start = $i;
            break;
        }

    // searching for the last numeric character
    for ($i = count($price_chars) - 1; $i >= 0; $i--)
        if (in_array($price_chars[$i], $allowed)) {
            $end = $i;
            break;
        }

    return substr($price_str, $start, $end - $start + 1);
}

// *** Convert string price representation to a system suitable format
function dc_string_to_price($str) {
    $price = $str;

    // trims all characters except numbers and dots
    $price = dc_trim_price($price);

    // replacing possible decimal/thousand separator with a dot
    $price = str_replace(',', '.', $price);
    $price = str_replace(' ', '.', $price);

    // leaving only the last dot as decimal separator, assuming that all other dots are bits separators
    while (substr_count($price, '.') > 1)
        $price = preg_replace('/\./', '', $price, 1);

    // replacing dot with a system decimal separator 
    $locale_info = localeconv();

    $price = str_replace('.', $locale_info['decimal_point'], $price);

    return floatval($price);
}

// *** Get the price in a proper format
function dc_format_price($price, $currency, $with_currency = true) {
    $decimal_sep = ",";
    $thousand_sep = ".";

    return number_format($price, 2, $decimal_sep, $thousand_sep) . ($with_currency ? ' ' . $currency : '');
}

// *** Processes domain record from the database. Prepares the data for showing on the site
function dc_get_record($row, $currency) {
    $row[DPI_DB_FLD_TLD] = strtolower($row[DPI_DB_FLD_TLD]);
    $row['check_tld'] = substr($row[DPI_DB_FLD_TLD], 1); // TLD without the leading dot (as required by Virturo)

    if ($currency == 'EUR') {
        $year_price = dc_string_to_price($row[DPI_DB_FLD_YEAR_EUR]);
        $reg_price = dc_string_to_price($row[DPI_DB_FLD_REGISTER_EUR]);
        $trans_price = dc_string_to_price($row[DPI_DB_FLD_TRANSFER_EUR]);
    } else {
        $year_price = dc_string_to_price($row[DPI_DB_FLD_YEAR_CHF]);
        $reg_price = dc_string_to_price($row[DPI_DB_FLD_REGISTER_CHF]);
        $trans_price = dc_string_to_price($row[DPI_DB_FLD_TRANSFER_CHF]);
    }

    // Format price
    $row['year_price'] = dc_format_price($year_price, $currency);
    $row['reg_price'] = dc_format_price($reg_price, $currency);
    $row['trans_price'] = dc_format_price($trans_price, $currency);

    return $row;
}

// *** Class for checking domain availability using Virturo API
abstract class dcVirturoBase {

    protected $command = array(); // An array of parameters to send to Virturo
    protected $urls = array();

    function __construct() {
        $this->command['username'] = VIRTURO_CRED_API_USER;
        $this->command['password'] = VIRTURO_CRED_API_PASS;
        $this->command['class'] = VIRTURO_CRED_API_CLASS;
        $this->command['method'] = VIRTURO_CRED_API_METHOD;
    }

    // prepare the cURL connection
    function getCURL() {
        $curl_handle = curl_init();

        curl_setopt($curl_handle, CURLOPT_URL, VIRTURO_CRED_URL);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 20);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        return $curl_handle;
    }

    // get the check result status
    function get_status($response) {
//        var_dump($response);
        return strripos($response, 'UNAVAILABLE') === false;
    }

    // perform the check
    abstract function check($sld, $tlds);
}

// *** Domain check using a series of consecutive cURL requests
class dcVirturoSingle extends dcVirturoBase {

    function check($sld, $tlds) {
        set_time_limit(VIRTURO_CRED_API_LIMIT);

        $this->command['sld'] = $sld;

        $curl_handle = $this->getCURL();

        foreach ($tlds as &$tld) {
            $this->command['tld'] = $tld['check_tld'];

            curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $this->command);

            // save result for output in the template
            $tld['result'] = curl_exec($curl_handle);
            $tld['status'] = $this->get_status($tld['result']);
        }

        curl_close($curl_handle);

        return $tlds;
    }

}

// *** Domain check using a multiple cURL requests
class dcVirturoMulti extends dcVirturoBase {

    function check($sld, $tlds) {
        //error_reporting(E_ALL);
        set_time_limit(VIRTURO_CRED_API_LIMIT);


        $multi_handle = curl_multi_init();
        $handles = array();

        foreach ($this->urls as $tld) {
            curl_setopt($tld['url'], CURLOPT_POSTFIELDS, $tld['command']);
            curl_multi_add_handle($multi_handle, $tld['url']);
//            var_dump($tld['url']);        die;

            $handles[$tld['command'][DPI_DB_FLD_TLD]] = $tld['url'];
        }
        unset($tld);

        $active = null;

        do {
            $curl_result = curl_multi_exec($multi_handle, $active);
        } while ($curl_result === CURLM_CALL_MULTI_PERFORM || $active);

        foreach ($tlds as &$tld) {
//            var_dump($handles); echo "<pre>";
//            var_dump($tld[DPI_DB_FLD_TLD]);
//            die;
            preg_match("/^\.", $tld[DPI_DB_FLD_TLD]) ? $dom =$tld[DPI_DB_FLD_TLD] :$dom = str_replace(".", "", $tld[DPI_DB_FLD_TLD]);
            $tld['result'] = curl_multi_getcontent($handles[$dom]);
//            $tld['status'] = strpos($tld['result'], 'UNAVAILABLE') === false;
               if ((strpos($tld['result'], 'FAILURE') or empty($tld['result']))) {
                    $tld['status'] = 'not_checked';
                } elseif(strpos($tld['result'], 'UNAVAILABLE')) {
                    $tld['status'] = false;
                }else{
                    $tld['status'] = true;
                }
//            var_dump($tld['result']);
//            die;
            curl_multi_remove_handle($multi_handle, $handles[$tld['tld']]);
            curl_close($handles[$tld[DPI_DB_FLD_TLD]]);
        }

        curl_multi_close($multi_handle);
//        die;
        return $tlds;
    }

    function pre_check($sld, $tlds) {
        $this->command['sld'] = $sld;
        $i = 0;
        foreach ($tlds as $tld) {
            $this->command['tld'] = $tld['check_tld'];

            $curl_handle = $this->getCURL();
            $this->urls[$i]['command'] = $this->command;
            $this->urls[$i]['url'] = $curl_handle;
            $i++;
        }
    }

    // Split the multi-domain request into set of requests with $chunk_size domains at one time
    function check_chunked($sld, $tlds, $chunk_size) {
//                print_r(pcntl_fork());die;
        $count = count($tlds);
        $done = 0;
        $limit = 0;
        $tld_results = array();
        for ($done, $limit; $done < $count; $done += $limit) {

            $tld_temp = array();

            if (($count - $done) < $chunk_size) {
                $limit = $count - $done;
            } else {
                $limit = $chunk_size;
            }

            $tld_temp = $this->pre_check($sld, array_slice($tlds, $done, $limit, true));
            $tld_temp = $this->check($sld, array_slice($tlds, $done, $limit, true));

            $tld_results = array_merge($tld_results, $tld_temp);
//                         exit($i);
        }

        return $tld_results;
    }

}

?>