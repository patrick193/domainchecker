<?php
require_once(dirname(__FILE__) . '/dc_utils.php');

// *** In the case of some problems with Virturo (execution time, etc...) - display that all domains are available 
function dc_shutdown() { 
	$errors = error_get_last(); 
	
	if( $errors == null ) exit;
	
	if( $errors['type'] != 1 ) exit; // only for unrecoverable errors
	
	global $whmcs;
	global $dc_smarty;
	global $domain;
	global $tlds;

	foreach ( $tlds as &$tld ) {
		$tld['result'] = 'connection problem';
		$tld['status'] = true;
	}
	
	$template = 'dc_process.tpl';

	$dc_smarty->assign( 'domain', $domain );
	$dc_smarty->assign( 'tlds', $tlds );

	if ( defined('DC_DEBUG') && DC_DEBUG ) {
		$dc_smarty->assign( 'dc_debug', true );
		$dc_smarty->assign( 'dc_run_time', microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] );
	} else {
		$dc_smarty->assign( 'dc_debug', false );
	}

	echo $dc_smarty->fetch( ROOTDIR . '/templates/' . $whmcs->get_config('Template') . '/' . $template );
}

// If the client requires to check the single domain during the product order process - search form script is not involved
// so we have to establish connection and initialize variables ourselves
require_once(dirname(__FILE__) . '/dc_search.php');


$domain = $_POST['dc_dom'];
$dc_cur = $_POST['dc_cur'];

$tlds = array();

// If there is a dot in user query then it is TLD and we should include it into the search
if ( strpos( $domain, '.' ) !== FALSE ) {
	$domain_parts = explode( '.', $domain, 2 );
	
	$domain 		= $domain_parts[0];
	$additional_tld	= '.' . mysql_real_escape_string( strtoupper($domain_parts[1]) );
	
	$query = full_query("SELECT * FROM ".DPI_DB_TBL_DOMAINS." WHERE ".DPI_DB_FLD_TLD."='".$additional_tld."'");

	if ($query) {
		$row = mysql_fetch_array( $query );
		if ( is_array($row) )
			if ( $row[DPI_DB_FLD_CATEGORY] != $_POST['dc_cat'] ) $tlds[] = dc_get_record($row, $dc_cur);
	}
}

// Get the list of TLDs by selected Category
$query = full_query("SELECT * FROM ".DPI_DB_TBL_DOMAINS." WHERE ".DPI_DB_FLD_CATEGORY."='".mysql_real_escape_string( $_POST['dc_cat'] )."'");

while ($row = mysql_fetch_array( $query )) {
	$tlds[] = dc_get_record($row, $dc_cur);
}


// *** Check the domain availability

// If the check will fail - display that all the domains are available anyway
//register_shutdown_function('dc_shutdown');
//$s = microtime();
if ( defined('VIRTURO_CRED_API_MULTI_CURL') && VIRTURO_CRED_API_MULTI_CURL ) {
	// split the given array of TLDs into chunks and perform a series of multi-cURL request
	$dc_check = new dcVirturoMulti();
	$tlds = $dc_check->check_chunked($domain, $tlds, VIRTURO_CRED_API_CHUNK);
//	die('sddsdsdsd');
} else {
	// perform a sequence of cURL requests
	$dc_check = new dcVirturoSingle();
	
	$tlds = $dc_check->check($domain, $tlds);
}
//$e = microtime();
//var_dump($e-$s);die;

// Display the result
$template = 'dc_process.tpl';

$dc_smarty->assign( 'domain', $domain );
$dc_smarty->assign( 'tlds', $tlds );

if ( defined('DC_DEBUG') && DC_DEBUG ) {
	$dc_smarty->assign( 'dc_debug', true );
	$dc_smarty->assign( 'dc_run_time', microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] );
} else {
	$dc_smarty->assign( 'dc_debug', false );
}

echo $dc_smarty->fetch( ROOTDIR . '/templates/' . $whmcs->get_config('Template') . '/' . $template );

?>
