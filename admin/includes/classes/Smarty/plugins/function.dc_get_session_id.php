<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */ 
 
 /*
 * -------------------------------------------------------------
 * File:     function.dc_get_session_id.php
 * Type:     function
 * Name:     dc_get_session_id
 * Purpose:  outputs current session id
 * -------------------------------------------------------------
 */
 
function smarty_function_dc_get_session_id($params, &$smarty) {
	return session_id();
}
?>
