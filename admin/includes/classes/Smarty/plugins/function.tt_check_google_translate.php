<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */ 
 
 /*
 * -------------------------------------------------------------
 * File:     function.tt_check_google_translate.php
 * Type:     function
 * Name:     tt_check_google_translate
 * Purpose:  checks if a ticket reply is created using Google Translate API
 * -------------------------------------------------------------
 */

if (!defined("WHMCS"))
	die("This file cannot be accessed directly");

function smarty_function_tt_check_google_translate($params, &$smarty) {
	require_once('init.php');
	include_once( ROOTDIR . '/modules/addons/translatetickets/const.php ' );
	
	if ( !defined('TT_GOOGLE') ) return;

	$reply = $params['reply'];
	
	$smarty->assign( 'is_google_translate', ($reply['name'] === TT_GOOGLE ? 1 : 0) );

	return;
}
?>
