<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */ 
 
 /*
 * -------------------------------------------------------------
 * File:     function.dc_prepare_product_features.php
 * Type:     function
 * Name:     dc_prepare_product_features
 * Purpose:  gets the list of product features based on product description and forms a list for every product
 * -------------------------------------------------------------
 */
 
function smarty_function_dc_prepare_product_features($params, &$smarty) {
	$products = $params['products'];
	$features_tmp = array();
	
	foreach ($products as $num => $product) {
		$lines = explode("\n", $product['description']);
		
		foreach ($lines as $line) {
			$parts = explode(':', $line, 3);

			if ( count($parts) == 3 ) {
				$category = trim( $parts[0] );
				$name = trim( $parts[1] );
				$value = trim( $parts[2] );
			} elseif ( count($parts) == 2 ) {
				$category = ' ';
				$name = trim( $parts[0] );
				$value = trim( $parts[1] );
			} else {
				continue;
			}
			
			$features_tmp[$category][$name][$num] = $value;
		}
	
	}
	
	$features = array();
	
	foreach ( $features_tmp as $cat_name => $category ) {
		$feat_array	= array();
		
		foreach ( $category as $feat_name => $feature ) {
			$feat_array[] = array('feature' => $feat_name);
		}

		$cat_array	= array('cat' => $cat_name, 'features' => $feat_array);
		
		$features[] = $cat_array;
	}
	
	// setting all features for all products (default: empty)
	require_once( ROOTDIR . '/includes/domain-check/dc_utils.php' );
	$dc_cur = $smarty->get_template_vars('dc_cur');
	
	for ($i = 0; $i < count($features); $i++) {
		for ($j = 0; $j < count($features[$i]['features']); $j++) {
			$cat_name	= $features[$i]['cat'];
			$feat_name	= $features[$i]['features'][$j]['feature'];

			$values	= array();
			
			foreach ($products as $num => $product) {
				if ( isset( $features_tmp[$cat_name] ) )
					if ( isset( $features_tmp[$cat_name][$feat_name] ) )
						if ( isset( $features_tmp[$cat_name][$feat_name][$num] ) ) {
							$values[$num] = $features_tmp[$cat_name][$feat_name][$num];
							
							// if there is "|" in value - consider it for the differrent prices
							if ( strpos($values[$num], '|') !== false ) {
								$price_parts = explode('|', $values[$num]);
								
								foreach ($price_parts as $part) {
									if ( strpos($part, $dc_cur) ) {
										$values[$num] = trim($part);
									}
								}
							}
						} else {
							$values[$num] = '';
						}
			}
			
			$features[$i]['features'][$j]['values'] = $values;
		}
	}

	// Formatting prices
	$dc_prices = array();
	
	foreach ($products as $num => $product) {
		$dc_prices[$num] = dc_format_price( $product['pricing']['rawpricing']['monthly'], $dc_cur, false);
	}

	$smarty->assign( 'dc_features', $features );
	$smarty->assign( 'dc_prices', $dc_prices );
	return;
}
?>
