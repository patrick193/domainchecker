<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */ 
 
 /*
 * -------------------------------------------------------------
 * File:     function.dc_set_user_info.php
 * Type:     function
 * Name:     dc_set_user_info
 * Purpose:  sets in smarty two variables: with the country and currency of the user
 * -------------------------------------------------------------
 */
 
function smarty_function_dc_set_user_info($params, &$smarty) {
	require_once('init.php');
	require_once( ROOTDIR . '/includes/domain-check/dc_utils.php' );

	global $_LANG;
	
	$currencies = array();
	$result = select_query( "tblcurrencies", "id,code", "", "code", "ASC" );

	while ($data = mysql_fetch_assoc( $result )) {
		$currencies[] = array( 'id' => $data["id"], 'code' => $data["code"]);
	}

	dc_user_info($coutry, $curr);

	if ( isset($_SESSION['currency']) ) {
		foreach ( $currencies as $current_curr ) {
			if ( $current_curr['id'] == $_SESSION['currency'] )
				$curr = $current_curr['code'];
		}
	}

	if($curr == 'EUR') {
		$dc_cur_name	= $_LANG['domain_check']['EUR'];
		$dc_switch_curr	= 'CHF';
	} else {
		$dc_cur_name	= $_LANG['domain_check']['CHF'];
		$dc_switch_curr	= 'EUR';
	}

	foreach ( $currencies as $current_curr ) {
		if ( $current_curr['code'] == $dc_switch_curr )
			$dc_switch_curr_id = $current_curr['id'];
	}
	
	$smarty->assign( 'dc_country', $coutry );
	$smarty->assign( 'dc_cur', $curr );
	$smarty->assign( 'dc_cur_name', $dc_cur_name );
	$smarty->assign( 'dc_switch_curr', $dc_switch_curr );
	$smarty->assign( 'dc_switch_curr_id', $dc_switch_curr_id );
		
	return;
}
?>
