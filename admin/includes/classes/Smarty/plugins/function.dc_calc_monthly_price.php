<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */ 
 
 /*
 * -------------------------------------------------------------
 * File:     function.dc_calc_monthly_price.php
 * Type:     function
 * Name:     dc_calc_monthly_price
 * Purpose:  calculates a monthly price for a product based on 
 *				the product price setting (quaterly, yearly...) 
 * -------------------------------------------------------------
 */
 
function smarty_function_dc_calc_monthly_price($params, &$smarty) {
	$smarty->assign( 'price', array( 'monthly' => formatCurrency(0), 'setup' => formatCurrency(0) ) );

	$mincycle = $params['pricing']['minprice']['cycle'];
	$minprice = $params['pricing']['rawpricing'][$mincycle];
	
	if ( $mincycle == 'monthly' )
		return;
	
	switch ( $mincycle ) {
		case 'quarterly':
			$price = $minprice / 3;
			$setup = $params['pricing']['rawpricing']['qsetupfee'];
			break;
		case 'semiannually':
			$price = $minprice / 6;
			$setup = $params['pricing']['rawpricing']['ssetupfee'];
			break;
		case 'annually':
			$price = $minprice / 12;
			$setup = $params['pricing']['rawpricing']['asetupfee'];
			break;
		case 'biennially':
			$price = $minprice / 24;
			$setup = $params['pricing']['rawpricing']['bsetupfee'];
			break;
		case 'triennially':
			$price = $minprice / 36;
			$setup = $params['pricing']['rawpricing']['tsetupfee'];
			break;
		default:
			$price = -1;
	}

	$smarty->assign( 'price', array( 'monthly' => formatCurrency($price), 'setup' => formatCurrency($setup) ) );
	return;
}
?>
