<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */ 
 
 /*
 * -------------------------------------------------------------
 * File:     function.dc_is_current_page.php
 * Type:     function
 * Name:     dc_is_current_page
 * Purpose:  outputs a specific class for current menu item
 * -------------------------------------------------------------
 */
 
function smarty_function_dc_is_current_page($params, &$smarty) {
	$result = false;
	
	if ( strpos( $params['page'], '|' ) === false ) {
		$pages = array( $params['page'] );
	} else {
		$pages = explode( '|', $params['page'] );
	}
	
	foreach ($pages as $page) {
		if ( $_SERVER['REQUEST_URI'] == ( '/' . $page) ) {
			$result .= ' class="current"';
			break;
		}
	}
	
	return $result;
}
?>
