<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */ 
 
 /*
 * -------------------------------------------------------------
 * File:     function.dc_domain_check_form.php
 * Type:     function
 * Name:     dc_domain_check_form
 * Purpose:  outputs a domain check form instead of the default form
 * -------------------------------------------------------------
 */
 
function smarty_function_dc_domain_check_form($params, &$smarty) {
	define( 'DC_EMBEDDED', true );

	if ( $params['productdomain'] ) define( 'DC_PRODUCTDOMAIN', true );
	
	ob_start();
	require_once( $_SERVER['DOCUMENT_ROOT'] . '/domain_check.php' );
	
	return ob_get_clean();
}
?>
