<?php
/**
 *
 * From BAMAT domain check
 * API function that returns a list of domain categories
 * returns result in JSON format
 *
 * */

if (!defined( "WHMCS" )) {
	exit( "This file cannot be accessed directly" );
}

require(ROOTDIR."/modules/addons/domainpricesimport/const.php");
require(ROOTDIR."/includes/domain-check/dc_const.php");
require(ROOTDIR."/includes/domain-check/dc_utils.php");

$cat_result = select_query( DPI_DB_TBL_CATEGORIES, "", "", DPI_DB_FLD_CATEGORY, "ASC" );

$apiresults = array( "result" => "success", "totalresults" => mysql_num_rows( $cat_result ) );

while ($cat_data = mysql_fetch_array( $cat_result )) {
	$dom_result = select_query( DPI_DB_TBL_DOMAINS, "", DPI_DB_FLD_CATEGORY."='". $cat_data[DPI_DB_FLD_ID] . "'");
	
	$dom_prices = array();
	
	while ($dom_data = mysql_fetch_array( $dom_result )) {
		$dom_data = dc_get_record($dom_data, $curr);
		
		$new_price['name']			= $dom_data[DPI_DB_FLD_TLD];
		$new_price['description']	= $dom_data[DPI_DB_FLD_DESCRIPTION];
		$new_price['year_price']	= $dom_data['year_price'];
		$new_price['reg_price']		= $dom_data['reg_price'];
		$new_price['trans_price']	= $dom_data['trans_price'];

		$dom_prices[] = $new_price;
	}

	$name = $cat_data[DPI_DB_FLD_CATEGORY];
	
	$apiresults["categories"][] = array( "name" => $name, "prices" => $dom_prices );

	if ($name == 'Europa')
		$apiresults["default_category"] = count($apiresults["categories"]) - 1;
}

$apiresults["curr"] = $curr;

$responsetype = "json";
?>
