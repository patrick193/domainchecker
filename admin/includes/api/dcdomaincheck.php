<?php
/**
 *
 * From BAMAT domain check
 * API function that returns a list of domain categories
 * returns result in JSON format
 *
 * */

if (!defined( "WHMCS" )) {
	exit( "This file cannot be accessed directly" );
}

require(ROOTDIR."/modules/addons/domainpricesimport/const.php");
require(ROOTDIR."/includes/domain-check/dc_const.php");
require(ROOTDIR."/includes/domain-check/dc_utils.php");

// Getting the list of domains
$tlds = array();

// *** Check domains by category
if ( isset($category) ) {
	// If there is a dot in user query then it is TLD and we should include it into the search
	if ( strpos( $domain, '.' ) !== FALSE ) {
		$domain_parts = explode( '.', $domain, 2 );
		
		$domain 		= $domain_parts[0];
		$additional_tld	= '.' . mysql_real_escape_string( strtoupper($domain_parts[1]) );
		
		$query = full_query("SELECT * FROM ".DPI_DB_TBL_DOMAINS." WHERE ".DPI_DB_FLD_TLD."='".$additional_tld."'");

		if ($query) {
			$row = mysql_fetch_array( $query );
			if ( is_array($row) )
				if ( $row[DPI_DB_FLD_CATEGORY] != $category ) $tlds[] = dc_get_record($row, $curr);
		}
	}

	// Get the list of TLDs by selected Category
	$query = full_query("SELECT * FROM ".DPI_DB_TBL_DOMAINS." WHERE ".DPI_DB_FLD_CATEGORY."='".mysql_real_escape_string( $category )."'");

	while ($row = mysql_fetch_array( $query )) {
		$tlds[] = dc_get_record($row, $curr);
	}
	
} elseif (isset($domains_list)) {

	$domains_list = explode( ';', $domains_list);
	
	// *** Check domains by number of domains
	$domain_parts = explode( '.', $domains_list[0], 2 );
	$domain = $domain_parts[0];
	
	$req_tlds = array();
	
	foreach ($domains_list as $dom) {
		$domain_parts = explode( '.', $dom, 2 );
		$req_tlds[] = '.' . $domain_parts[1];
	}
	
	// Get the prices of selected domains
	$query = full_query("SELECT * FROM ".DPI_DB_TBL_DOMAINS." WHERE ".DPI_DB_FLD_TLD." IN ('".implode("', '", $req_tlds)."')");

	while ($row = mysql_fetch_array( $query )) {
		$tlds[] = dc_get_record($row, $curr);
	}
}

// *** Check the domain availability
if ( defined('VIRTURO_CRED_API_MULTI_CURL') && VIRTURO_CRED_API_MULTI_CURL ) {
	// split the given array of TLDs into chunks and perform a series of multi-cURL request
	$dc_check = new dcVirturoMulti();
	
	$tlds = $dc_check->check_chunked($domain, $tlds, VIRTURO_CRED_API_CHUNK);
	
} else {
	// perform a sequence of cURL requests
	$dc_check = new dcVirturoSingle();
	
	$tlds = $dc_check->check($domain, $tlds);
}

$apiresults = array_merge( $apiresults, array( "result" => "success", "totalresults" => mysql_num_rows( $tlds ) ) );
$apiresults['domain'] = $domain;
$apiresults['tlds'] = $tlds;

$responsetype = "json";
?>