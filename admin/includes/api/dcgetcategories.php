<?php
/**
 *
 * From BAMAT domain check
 * API function that returns a list of domain categories
 * returns result in JSON format
 *
 * */

if (!defined( "WHMCS" )) {
	exit( "This file cannot be accessed directly" );
}

require(ROOTDIR."/modules/addons/domainpricesimport/const.php");

$result = select_query( DPI_DB_TBL_CATEGORIES, "", "", DPI_DB_FLD_CATEGORY, "ASC" );

$apiresults = array( "result" => "success", "totalresults" => mysql_num_rows( $result ) );

while ($data = mysql_fetch_array( $result )) {
	$id = $data[DPI_DB_FLD_ID];
	$name = $data[DPI_DB_FLD_CATEGORY];

	$apiresults["categories"][] = array( "id" => $id, "name" => $name );
}

$responsetype = "json";
?>