<?php
/**
 * Function for getting price details for cart template
 *
 * Please refer to the documentation @ http://docs.whmcs.com/Hooks for more information
 * The code in this hook is commented out by default. Uncomment to use.
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2013
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */


if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

// For "bamatcart" order template: detailed domain prices for displaying in the cart
function bamat_domain_price_details($vars) {
	if ( empty($vars['domains']) ) return;
	
	$domains = $vars['domains'];
	
	if ( count($domains) == 0 ) return;
	
	// detect current currency
	$cur = getCurrency($_SESSION["uid"]);
	$dc_cur = $cur['code'];
	
	// get unique TLDs list of all the domains
	$tlds = array();
	$domain_prices = array();

	foreach ($domains as $domain) {
		$domain_name = $domain['domain'];
		$domain_prices[$domain_name]['type'] = $domain['type'];
	
		$parts = explode('.', $domain_name, 2);
		$domain_prices[$domain_name]['price_tld'] = '.' . strtoupper($parts[1]);
		$tlds[] = "'" . $domain_prices[$domain_name]['price_tld'] . "'";
	}
	
	$tlds = array_unique($tlds);
	
	// Get domain prices
	$prices = array();

	require_once( $_SERVER['DOCUMENT_ROOT'] . '/modules/addons/domainpricesimport/const.php');
	require_once(dirname(__FILE__) . '/../domain-check/dc_const.php');
	require_once(dirname(__FILE__) . '/../domain-check/dc_utils.php');

	$query = full_query("SELECT * FROM ".DPI_DB_TBL_DOMAINS." WHERE ".DPI_DB_FLD_TLD." IN (".implode(',', $tlds).")");

	while ($row = mysql_fetch_array( $query )) {
		$prices[ $row[DPI_DB_FLD_TLD] ] = dc_get_record($row, $dc_cur);
	}

	// transferring per-domain detailed prices to Smarty
	global $_LANG;

	foreach ($domain_prices as &$domain) {
		$year_price = $prices[ $domain['price_tld'] ]['year_price'];

		if ($domain['type'] == 'transfer') {
			$setup_price = $prices[ $domain['price_tld'] ]['trans_price'];
		} else {
			$setup_price = $prices[ $domain['price_tld'] ]['reg_price'];
		}
		
		$price_text = '';
		
		if ( $year_price > 0 ) {
			$price_text = $year_price;
		}
		
		if ( $setup_price > 0 ) {
			if ( !empty($price_text) )
				$price_text .= " + $setup_price " . $_LANG['ordersetupfee'];
		}
		
		// No prices were given, assuming that this domain is free
		if ( empty($price_text) ) $price_text = $_LANG['orderfree'];

		$domain['year_price'] = $year_price;
		$domain['setup_price'] = $setup_price;
		$domain['price_text'] = $price_text;
		
		if ($domain['type'] == 'transfer') {
			$domain['setup_price'] = $prices[ $domain['price_tld'] ]['trans_price'];
		} else {
			$domain['setup_price'] = $prices[ $domain['price_tld'] ]['reg_price'];
		}
	}
	
	global $smarty;

	// For API calls $smarty is not available
	if ( !empty($smarty) )
		$smarty->assign('domain_prices', $domain_prices);
}

add_hook("PreCalculateCartTotals", 1, "bamat_domain_price_details");
