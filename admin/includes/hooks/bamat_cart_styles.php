<?php
/**
 * Example Hook Function
 *
 * Please refer to the documentation @ http://docs.whmcs.com/Hooks for more information
 * The code in this hook is commented out by default. Uncomment to use.
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2013
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */


if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

define('TARGET_CART_TPL', 'bamatcart');
define('TARGET_TPL', 'bamat');
	
function bamat_add_cart_assets($vars) {
	$assets = '';
	
	// Add special assets for ticket view page (for rating, replies, etc...)
	if ( $vars['filename'] == 'viewticket' && $vars['template'] == TARGET_TPL ) {
		$assets .= '<script type="text/javascript" src="templates/'.TARGET_TPL.'/js/viewticket.js"></script>';
		
		return $assets;
	}
	
	// For "bamatcart" order template: add assets (css/js) into the header
	if ( $vars['carttpl'] == TARGET_CART_TPL ) {
		// JavaScript
		$assets .= '<script type="text/javascript" src="includes/jscript/jqueryui.js"></script>';
		$assets .= '<script type="text/javascript" src="templates/orderforms/'.TARGET_CART_TPL.'/js/main.js"></script>';
		$assets .= '<script type="text/javascript" src="templates/orderforms/'.TARGET_CART_TPL.'/js/bamatcart.js"></script>';

		// Styles
		$assets .= '<link rel="stylesheet" type="text/css" href="templates/orderforms/'.TARGET_CART_TPL.'/css/style.css" />';
		$assets .= '<link rel="stylesheet" type="text/css" href="templates/orderforms/'.TARGET_CART_TPL.'/css/uistyle.css" />';
	}

    return $assets;
}

add_hook("ClientAreaHeadOutput", 1, "bamat_add_cart_assets");
