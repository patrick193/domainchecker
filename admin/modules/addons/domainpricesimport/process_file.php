<?php
$error = 0;

// Check input
if ( empty($_FILES) ) {
	die();
}

// Handle upload
$uploaddir = dirname(__FILE__) . '/';
$uploadfile = $uploaddir . basename($_FILES['dpi_file_upload']['name']);

if ( !move_uploaded_file($_FILES['dpi_file_upload']['tmp_name'], $uploadfile) ) {
	die();
}

echo basename($uploadfile);
?>