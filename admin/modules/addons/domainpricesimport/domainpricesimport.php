<?php
/**
 * Domain Prices Import Addon Module
 *
 * Allows to import domain prices from Excel file into WHMCS
 *
 * @package    WHMCS
 * @author     General Internet <info@general-internet.ch>
 * @copyright  Copyright (c) General Internet 2015
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

require_once( dirname(__FILE__) . '/const.php' );

function domainpricesimport_config() {
    $configarray = array(
    "name" => "Domain Prices Import",
    "description" => "Allows to import domain prices from Excel file into WHMCS",
    "version" => "1.0",
    "author" => "General Internet",
    "language" => "english",
	);
    
	return $configarray;
}

function domainpricesimport_activate() {

    # Create DB Table for domain categories
	$query = "CREATE TABLE `". DPI_DB_TBL_CATEGORIES . "` 
				(`".DPI_DB_FLD_ID."` INT (2) NOT NULL AUTO_INCREMENT PRIMARY KEY,".				// Category ID
				 "`".DPI_DB_FLD_CATEGORY."` VARCHAR(13) COLLATE utf8_general_ci DEFAULT NULL".	// Category name
				 ") DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
    
	$result = full_query($query);

    # Create DB Table for domains
	$query = "CREATE TABLE `". DPI_DB_TBL_DOMAINS . "` 
				(`".DPI_DB_FLD_ID."` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,".					// Domain ID
				"`".DPI_DB_FLD_TLD."` VARCHAR(14) COLLATE utf8_general_ci NOT NULL,".				// Domain name
				"`".DPI_DB_FLD_CATEGORY."` INT(2) DEFAULT NULL,".									// ID of the domain category
				"`".DPI_DB_FLD_DESCRIPTION."` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL,".	// Domain description
				"`".DPI_DB_FLD_DOMAIN."` INT(10) NOT NULL,".										// ID of the domain from WHMCS tbldomainpricing
				"`".DPI_DB_FLD_REGISTER_EUR."` DECIMAL(10,2) NOT NULL,".							// Price for domain registration, EUR
				"`".DPI_DB_FLD_YEAR_EUR."` DECIMAL(10,2) NOT NULL,".								// Price for domain for a year, EUR
				"`".DPI_DB_FLD_TRANSFER_EUR."` DECIMAL(10,2) NOT NULL,".							// Price for domain transfer, EUR
				"`".DPI_DB_FLD_REGISTER_CHF."` DECIMAL(10,2) NOT NULL,".							// Price for domain registration, CHF
				"`".DPI_DB_FLD_YEAR_CHF."` DECIMAL(10,2) NOT NULL,".								// Price for domain for a year, CHF
				"`".DPI_DB_FLD_TRANSFER_CHF."` DECIMAL(10,2) NOT NULL".								// Price for domain transfer, CHF
				") DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
    
	$result = full_query($query);

    # Return Result
    return array('status'=>'success','description'=>'Module successfully activated.');
}

function domainpricesimport_deactivate() {

    # Remove custom DB tables
    $query = "DROP TABLE `".DPI_DB_TBL_CATEGORIES."`";
    $result = full_query($query);

    $query = "DROP TABLE `".DPI_DB_TBL_DOMAINS."`";
    $result = full_query($query);

    # Return Result
    return array('status'=>'success','description'=>'Module successfully deactivated.');
}

function domainpricesimport_upgrade($vars) {
}

function domainpricesimport_output($vars) {
    $modulelink = $vars['modulelink'];
    $version = $vars['version'];
    $LANG = $vars['_lang'];

	$action = $_REQUEST['action'];

	echo '<link href="../modules/addons/domainpricesimport/css/domainpricesimport.css" rel="stylesheet" type="text/css" />';

	if ( isset($_REQUEST['dpi_file_confirm']) ) {
		require_once( dirname(__FILE__) . '/doimport.php' );
	} elseif ( !empty($action) ) {
		require_once(dirname(__FILE__) . '/selection.php');
	} else {
		require_once( dirname(__FILE__) . '/upload_form.php' );
	}
}
