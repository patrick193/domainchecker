<?php
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

function dpi_get_currency_id($currency) {
	$id = 0;
	
	$query = full_query("SELECT id FROM tblcurrencies WHERE code='".$currency."'");
	
	if ($query) {
		$row = mysql_fetch_array( $query );
		if (is_array($row))
			$id = $row['id'];
	}
	
	return $id;
}

function dpi_get_domain_id($tld) {
	$id = 0;
	
	if ( empty($tld)) return $id;
	
	$query = full_query("SELECT id FROM tbldomainpricing WHERE extension='".$tld."'");
	
	if ($query) {
		$row = mysql_fetch_array( $query );
		if (is_array($row))
			$id = $row['id'];
	}

	// if there is no such domain - add it
	if ( $id == 0 ) {
		$query = full_query("SELECT * FROM tbldomainpricing ORDER BY id DESC LIMIT 1");
		
		if ($query) {
			$row = mysql_fetch_array( $query );
			if (is_array($row)) {
				$query = sprintf( "INSERT INTO `tbldomainpricing` ".
									"(`extension`, `dnsmanagement`, `emailforwarding`, `idprotection`, `eppcode`, `autoreg`, `order`) ".
									"VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%d');",
								strtolower($tld),
								$row['dnsmanagement'],
								$row['emailforwarding'],
								$row['idprotection'],
								$row['eppcode'],
								$row['autoreg'],
								intval($row['order']) + 1);
				
				$result = full_query( $query );

				$id = mysql_insert_id();
			}
		}
	}
	
	return $id;
}

// Initializing
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

date_default_timezone_set('Europe/London'); 
	
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

require_once dirname(__FILE__) . '/PHPExcel.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/domain-check/dc_utils.php';

// reading the file
$xls_data = array();
$filename = dirname(__FILE__) . '/' . $action;

$inputFileType	= PHPExcel_IOFactory::identify( $filename );
$objReader		= PHPExcel_IOFactory::createReader( $inputFileType );
$objPHPExcel	= $objReader->load( $filename );
$xls_data		= $objPHPExcel->getActiveSheet()->toArray();

$dpi_col_tld = $_REQUEST['dpi_col_tld'];
$dpi_col_cat = $_REQUEST['dpi_col_cat'];
$dpi_col_desc = $_REQUEST['dpi_col_desc'];
$dpi_col_eur_year = $_REQUEST['dpi_col_eur_year'];
$dpi_col_eur_reg = $_REQUEST['dpi_col_eur_reg'];
$dpi_col_eur_trans = $_REQUEST['dpi_col_eur_trans'];
$dpi_col_chf_year = $_REQUEST['dpi_col_chf_year'];
$dpi_col_chf_reg = $_REQUEST['dpi_col_chf_reg'];
$dpi_col_chf_trans = $_REQUEST['dpi_col_chf_trans'];

$error = '';

// Get currency ids
$eur_id = dpi_get_currency_id('EUR');
$chf_id = dpi_get_currency_id('CHF');

// Add the new data to the database
full_query( "TRUNCATE TABLE `" . DPI_DB_TBL_CATEGORIES . "`");
full_query( "TRUNCATE TABLE `" . DPI_DB_TBL_DOMAINS . "`");
full_query( "DELETE FROM `tblpricing` WHERE type = 'domainregister' OR type = 'domainrenew' OR type = 'domaintransfer'");

$categories = array();

for ( $i = 1; $i < count($xls_data); $i++ ) {
	$row = $xls_data[$i];
	
	$cat_id		= 0;
	$cat_name	= $row[$dpi_col_cat];
	
	// *** Update the catagories table
	if ( array_key_exists( strtolower($cat_name), $categories ) ) {
		$cat_id = $categories[ strtolower($cat_name) ];
	} else {
		$query = sprintf( "INSERT INTO `%s` (`%s`) VALUES ('%s');",
							DPI_DB_TBL_CATEGORIES,
							DPI_DB_FLD_CATEGORY,
							$cat_name);
							
		$result = full_query( $query );
		
		$cat_id = mysql_insert_id();
		$categories[ strtolower($cat_name) ] = $cat_id;
	}
	
	// *** Update the domains table with prices

	// Find domain in WHMCS 'tbldomainpricing'
	$domain_id = dpi_get_domain_id( $row[ $dpi_col_tld ] );

	$year_price_eur		= dc_string_to_price($row[ $dpi_col_eur_year ]);
	$reg_price_eur		= dc_string_to_price($row[ $dpi_col_eur_reg ]);
	$trans_price_eur	= dc_string_to_price($row[ $dpi_col_eur_trans ]);
		
	$year_price_chf		= dc_string_to_price($row[ $dpi_col_chf_year ]);
	$reg_price_chf		= dc_string_to_price($row[ $dpi_col_chf_reg ]);
	$trans_price_chf	= dc_string_to_price($row[ $dpi_col_chf_trans ]);
		
	$fields = array(DPI_DB_FLD_TLD,
					DPI_DB_FLD_CATEGORY,
					DPI_DB_FLD_DESCRIPTION,
					DPI_DB_FLD_DOMAIN,
					DPI_DB_FLD_REGISTER_EUR,
					DPI_DB_FLD_YEAR_EUR,
					DPI_DB_FLD_TRANSFER_EUR,
					DPI_DB_FLD_REGISTER_CHF,
					DPI_DB_FLD_YEAR_CHF,
					DPI_DB_FLD_TRANSFER_CHF);
	
	$values = array($row[ $dpi_col_tld ],
					$cat_id,
					$row[ $dpi_col_desc ],
					$domain_id,
					$reg_price_eur,
					$year_price_eur,
					$trans_price_eur,
					$reg_price_chf,
					$year_price_chf,
					$trans_price_chf);

	$query = sprintf( "INSERT INTO `%s` ". // table
						"(`%s`) ". // fields
						"VALUES ('%s');", // values
						DPI_DB_TBL_DOMAINS,
						implode( '`, `', $fields),
						implode( "', '", $values));

	$result = full_query( $query );
	
	// *** Update domains prices

	$year_price_eur		= dc_string_to_price($row[ $dpi_col_eur_year ]);
	$reg_price_eur		= dc_string_to_price($row[ $dpi_col_eur_reg ]);
	$trans_price_eur	= dc_string_to_price($row[ $dpi_col_eur_trans ]);
		
	$year_price_chf		= dc_string_to_price($row[ $dpi_col_chf_year ]);
	$reg_price_chf		= dc_string_to_price($row[ $dpi_col_chf_reg ]);
	$trans_price_chf	= dc_string_to_price($row[ $dpi_col_chf_trans ]);
	// Update WHMCS prices for EUR
	if ( $eur_id > 0 ) {
		// Update prices in WHMCS
		$result = full_query( "INSERT INTO tblpricing (type, currency, relid, msetupfee) ".
								"VALUES ('domainregister', '".$eur_id."', '".$domain_id."', '".($year_price_eur + $reg_price_eur)."')" );
		$result = full_query( "INSERT INTO tblpricing (type, currency, relid, msetupfee) ".
								"VALUES ('domaintransfer', '".$eur_id."', '".$domain_id."', '".($year_price_eur + $trans_price_eur)."')" );
		$result = full_query( "INSERT INTO tblpricing (type, currency, relid, msetupfee) ".
								"VALUES ('domainrenew', '".$eur_id."', '".$domain_id."', '".$year_price_eur."')" );
	}
	
	// Update WHMCS prices for EUR
	if ( $chf_id > 0 ) {
		// Update prices in WHMCS
		$result = full_query( "INSERT INTO tblpricing (type, currency, relid, msetupfee) ".
								"VALUES ('domainregister', '".$chf_id."', '".$domain_id."', '".($year_price_chf + $reg_price_chf)."')" );
		$result = full_query( "INSERT INTO tblpricing (type, currency, relid, msetupfee) ".
								"VALUES ('domaintransfer', '".$chf_id."', '".$domain_id."', '".($year_price_chf + $trans_price_chf)."')" );
		$result = full_query( "INSERT INTO tblpricing (type, currency, relid, msetupfee) ".
								"VALUES ('domainrenew', '".$chf_id."', '".$domain_id."', '".$year_price_chf."')" );
	}
}

?>

<?php if ( !empty($error) ) { ?>
	<div class="errorbox" id="dpi_error">
		<strong><span class="title"><?php echo $LANG['dpi_error']; ?></span></strong><br>
		<?php echo $error; ?>
	</div>
<?php } else { ?>
	<div class="successbox" id="dpi_success">
		<strong><span class="title"><?php echo $LANG['dpi_success']; ?></span></strong><br>
		<?php echo sprintf( $LANG['dpi_records_processed'], count($xls_data) -1); ?>
	</div>
<?php }

unlink( dirname(__FILE__).'/'.$action );
?>