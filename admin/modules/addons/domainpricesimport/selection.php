<?php
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");

function dpi_columns_list($title_row, $id, $default, $LANG = null) {
	$result = "<select id='$id' name='$id'>";
	$result .= "<option value='-1'>".$LANG['dpi_not_selected']."</option>";
	
	for ($i = 0; $i < count($title_row); $i++) {
		if ($title_row[$i] == $default) {
			$result .= "<option value='$i' selected='selected'>".$title_row[$i]."</option>";
		} else {
			$result .= "<option value='$i'>".$title_row[$i]."</option>";
		}
	}
	
	$result .= '</select>';
	
	return $result;
}

// Initializing
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

date_default_timezone_set('Europe/London'); 
	
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

require_once dirname(__FILE__) . '/PHPExcel.php';

// reading the file
$xls_data = array();
$filename = dirname(__FILE__) . '/' . $action;

$inputFileType	= PHPExcel_IOFactory::identify( $filename );
$objReader		= PHPExcel_IOFactory::createReader( $inputFileType );
$objPHPExcel	= $objReader->load( $filename );
$xls_data		= $objPHPExcel->getActiveSheet()->toArray();

$error = '';

if ( !( is_array($xls_data) && (count($xls_data) > 0) ) ) {
	$error = $LANG['dpi_unable_to_read'];
}

if (empty($error)) {
?>

<p><?php echo $LANG['dpi_select_desc']; ?></p>

<form id="dpi_confirm" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">

<table class="dpi_table dpi_confirm">
<thead>
	<tr>
		<th><?php echo $LANG['dpi_head_col']; ?></th>
		<th><?php echo $LANG['dpi_head_confirm_col']; ?></th>
		<th><?php echo $LANG['dpi_head_desc']; ?></th>
	</tr>
</thead>

<tbody>

	<tr>
		<td><?php echo $LANG['dpi_column_tld']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_tld', $LANG['dpi_column_tld_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_tld_desc']; ?></td>
	</tr>
	<tr>
		<td><?php echo $LANG['dpi_column_cat']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_cat', $LANG['dpi_column_cat_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_cat_desc']; ?></td>
	</tr>
	<tr>
		<td><?php echo $LANG['dpi_column_desc']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_desc', $LANG['dpi_column_desc_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_desc_desc']; ?></td>
	</tr>
	<tr>
		<td><?php echo $LANG['dpi_column_eur_year']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_eur_year', $LANG['dpi_column_eur_year_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_eur_year_desc']; ?></td>
	</tr>
	<tr>
		<td><?php echo $LANG['dpi_column_eur_reg']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_eur_reg', $LANG['dpi_column_eur_reg_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_eur_reg_desc']; ?></td>
	</tr>
	<tr>
		<td><?php echo $LANG['dpi_column_eur_trans']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_eur_trans', $LANG['dpi_column_eur_trans_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_eur_trans_desc']; ?></td>
	</tr>
	<tr>
		<td><?php echo $LANG['dpi_column_chf_year']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_chf_year', $LANG['dpi_column_chf_year_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_chf_year_desc']; ?></td>
	</tr>
	<tr>
		<td><?php echo $LANG['dpi_column_chf_reg']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_chf_reg', $LANG['dpi_column_chf_reg_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_chf_reg_desc']; ?></td>
	</tr>
	<tr>
		<td><?php echo $LANG['dpi_column_chf_trans']; ?></td>
		<td><?php echo dpi_columns_list($xls_data[0], 'dpi_col_chf_trans', $LANG['dpi_column_chf_trans_def'], $LANG); ?></td>
		<td><?php echo $LANG['dpi_column_chf_trans_desc']; ?></td>
	</tr>
	
</tbody>
</table>

<input type="hidden" value="<?php echo $_REQUEST['module']; ?>" name="module">
<input type="hidden" value="<?php echo $_REQUEST['action']; ?>" name="action">

<p><input type="submit" value="<?php echo $LANG['dpi_confirm_button']; ?>" name="dpi_file_confirm" id="dpi_file_confirm"></p>
	
</form>

<?php } ?>

<?php if ( !empty($error) ) { ?>

	<div class="errorbox" id="dpi_error">
		<strong><span class="title"><?php echo $LANG['dpi_error']; ?></span></strong><br>
		<?php echo $error; ?>
	</div>

<?php
	unlink( dirname(__FILE__).'/'.$action );
}
?>

<script type="text/javascript" src="../modules/addons/domainpricesimport/js/selection.js"></script>
