<?php

$_ADDONLANG['dpi_upload_file']				= "Please upload a file with domain prices.";
$_ADDONLANG['dpi_upload_button']			= "Upload";
$_ADDONLANG['dpi_confirm_button']			= "Confirm";

$_ADDONLANG['dpi_upload_file_desc']			= "The file should be in Excel format, first column contains column titles. The file should contain the following columns (maybe with another titles):";
$_ADDONLANG['dpi_head_col']					= "Column";
$_ADDONLANG['dpi_head_def']					= "Default Title";
$_ADDONLANG['dpi_head_desc']				= "Description";
$_ADDONLANG['dpi_column_tld']				= "TLD";
$_ADDONLANG['dpi_column_tld_def']			= "TLD";
$_ADDONLANG['dpi_column_tld_desc']			= "top level domain like '.NET', '.COM' (letter case does not matter)";
$_ADDONLANG['dpi_column_cat']				= "Category";
$_ADDONLANG['dpi_column_cat_def']			= "Kategorie";
$_ADDONLANG['dpi_column_cat_desc']			= "sorting of TLDs by part of the world or anything else";
$_ADDONLANG['dpi_column_desc']				= "Description";
$_ADDONLANG['dpi_column_desc_def']			= "Beschreibung";
$_ADDONLANG['dpi_column_desc_desc']			= "TLD description like region name or field of activity";
$_ADDONLANG['dpi_column_eur_year']			= "Year Price EUR";
$_ADDONLANG['dpi_column_eur_year_def']		= "pro Jahr EUR";
$_ADDONLANG['dpi_column_eur_year_desc']		= "domain price for a year in EUR";
$_ADDONLANG['dpi_column_eur_reg']			= "Registration Price EUR";
$_ADDONLANG['dpi_column_eur_reg_def']		= "einmalige Neuregistrierung EUR";
$_ADDONLANG['dpi_column_eur_reg_desc']		= "new domain registration price in EUR";
$_ADDONLANG['dpi_column_eur_trans']			= "Transfer Price EUR";
$_ADDONLANG['dpi_column_eur_trans_def']		= "einmaliger Transfer EUR";
$_ADDONLANG['dpi_column_eur_trans_desc']	= "transfer price for existing domain in EUR";
$_ADDONLANG['dpi_column_chf_year']			= "Year Price CHF";
$_ADDONLANG['dpi_column_chf_year_def']		= "pro Jahr CHF";
$_ADDONLANG['dpi_column_chf_year_desc']		= "domain price for a year in CHF";
$_ADDONLANG['dpi_column_chf_reg']			= "Registration Price CHF";
$_ADDONLANG['dpi_column_chf_reg_def']		= "einmalige Neuregistrierung CHF";
$_ADDONLANG['dpi_column_chf_reg_desc']		= "new domain registration price in CHF";
$_ADDONLANG['dpi_column_chf_trans']			= "Transfer Price CHF";
$_ADDONLANG['dpi_column_chf_trans_def']		= "einmaliger Transfer CHF";
$_ADDONLANG['dpi_column_chf_trans_desc']	= "transfer price for existing domain in CHF";

$_ADDONLANG['dpi_head_confirm_col']			= "Default Title";
$_ADDONLANG['dpi_select_desc']				= "Confirm the columns for import:";
$_ADDONLANG['dpi_please_wait']				= "Please wait...";
$_ADDONLANG['dpi_error']					= "An error occured. Please try <a href='%s'>again</a>.";
$_ADDONLANG['dpi_success']					= "Data successfully imported.";
$_ADDONLANG['dpi_column_missing']			= "Column is missing: '%s";
$_ADDONLANG['dpi_unable_to_read']			= "Unable to read file contents";
$_ADDONLANG['dpi_not_selected']				= "Not selected";
$_ADDONLANG['dpi_records_processed']		= "Processed %d records";
