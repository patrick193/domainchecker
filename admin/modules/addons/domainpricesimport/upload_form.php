<?php
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");
?>

<form id="dpi_upload" method="post" action="" enctype="multipart/form-data">
    <h2><?php echo $LANG['dpi_upload_file']; ?></h2> 
    <p><input type="file" name="dpi_file_upload" id="dpi_file_upload">
    <input type="submit" value="<?php echo $LANG['dpi_upload_button']; ?>" name="dpi_file_submit" id="dpi_file_submit"></p>
</form>

<div id="dpi_progress">
	<p><img src="../modules/addons/domainpricesimport/img/status.gif" alt="status" width="32" height="32"> <span><?php echo $LANG['dpi_please_wait']; ?></span></p>
</div>

<div id="dpi_desc_wrap">
	<p><?php echo $LANG['dpi_upload_file_desc']; ?></p>

	<table class="dpi_table">
	<thead>
		<tr>
			<th><?php echo $LANG['dpi_head_col']; ?></th>
			<th><?php echo $LANG['dpi_head_def']; ?></th>
			<th><?php echo $LANG['dpi_head_desc']; ?></th>
		</tr>
	</thead>

	<tbody>

		<tr>
			<td><?php echo $LANG['dpi_column_tld']; ?></td>
			<td><?php echo $LANG['dpi_column_tld_def']; ?></td>
			<td><?php echo $LANG['dpi_column_tld_desc']; ?></td>
		</tr>
		<tr>
			<td><?php echo $LANG['dpi_column_cat']; ?></td>
			<td><?php echo $LANG['dpi_column_cat_def']; ?></td>
			<td><?php echo $LANG['dpi_column_cat_desc']; ?></td>
		</tr>
		<tr>
			<td><?php echo $LANG['dpi_column_desc']; ?></td>
			<td><?php echo $LANG['dpi_column_desc_def']; ?></td>
			<td><?php echo $LANG['dpi_column_desc_desc']; ?></td>
		</tr>
		<tr>
			<td><?php echo $LANG['dpi_column_eur_year']; ?></td>
			<td><?php echo $LANG['dpi_column_eur_year_def']; ?></td>
			<td><?php echo $LANG['dpi_column_eur_year_desc']; ?></td>
		</tr>
		<tr>
			<td><?php echo $LANG['dpi_column_eur_reg']; ?></td>
			<td><?php echo $LANG['dpi_column_eur_reg_def']; ?></td>
			<td><?php echo $LANG['dpi_column_eur_reg_desc']; ?></td>
		</tr>
		<tr>
			<td><?php echo $LANG['dpi_column_eur_trans']; ?></td>
			<td><?php echo $LANG['dpi_column_eur_trans_def']; ?></td>
			<td><?php echo $LANG['dpi_column_eur_trans_desc']; ?></td>
		</tr>
		<tr>
			<td><?php echo $LANG['dpi_column_chf_year']; ?></td>
			<td><?php echo $LANG['dpi_column_chf_year_def']; ?></td>
			<td><?php echo $LANG['dpi_column_chf_year_desc']; ?></td>
		</tr>
		<tr>
			<td><?php echo $LANG['dpi_column_chf_reg']; ?></td>
			<td><?php echo $LANG['dpi_column_chf_reg_def']; ?></td>
			<td><?php echo $LANG['dpi_column_chf_reg_desc']; ?></td>
		</tr>
		<tr>
			<td><?php echo $LANG['dpi_column_chf_trans']; ?></td>
			<td><?php echo $LANG['dpi_column_chf_trans_def']; ?></td>
			<td><?php echo $LANG['dpi_column_chf_trans_desc']; ?></td>
		</tr>
		
	</tbody>
	</table>
</div>

<script type="text/javascript" src="../modules/addons/domainpricesimport/js/domain_form.js"></script>

<div id="dpi_result"></div>