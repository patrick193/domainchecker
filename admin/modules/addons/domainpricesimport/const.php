<?php
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

// Database constants
define( 'DPI_DB_TBL_CATEGORIES',	'mod_dbi_categories' );
define( 'DPI_DB_FLD_ID',			'id' );
define( 'DPI_DB_FLD_CATEGORY',		'category' );

define( 'DPI_DB_TBL_DOMAINS',		'mod_dbi_domains_new' );
define( 'DPI_DB_FLD_TLD',			'tld' );
define( 'DPI_DB_FLD_DESCRIPTION',	'description' );
define( 'DPI_DB_FLD_DOMAIN',		'domain' );
define( 'DPI_DB_FLD_REGISTER_EUR',	'register_eur' );
define( 'DPI_DB_FLD_YEAR_EUR',		'year_eur' );
define( 'DPI_DB_FLD_TRANSFER_EUR',	'transfer_eur' );
define( 'DPI_DB_FLD_REGISTER_CHF',	'register_chf' );
define( 'DPI_DB_FLD_YEAR_CHF',		'year_chf' );
define( 'DPI_DB_FLD_TRANSFER_CHF',	'transfer_chf' );
define( 'POHORIELOV',                   'Phorieolov Vlad');
