jQuery('#dpi_upload').submit( function(event) {
	event.preventDefault();

	var filename;
	
	// Preventing from submitting an empty form
	if( !jQuery('#dpi_file_upload', this).val() ) {
		alert('Please choose the file');
		event.preventDefault();
		return;
	}

	// Uploading file
	jQuery('#dpi_desc_wrap').hide();
	jQuery('#dpi_upload').slideUp();
	jQuery('#dpi_progress').slideDown();

	jQuery.ajax({
		url: '../modules/addons/domainpricesimport/process_file.php',
		type: 'POST',
		data: new FormData( jQuery('#dpi_upload')[0] ),
		processData: false,
		contentType: false,
		success: function(response) {
			if ( !response ) {
				jQuery('#dpi_error').slideDown();
				jQuery('#dpi_progress').hide();
			} else {
				filename = response;

				// Forwarding to price import
				window.location.href = window.location.href + "&action=" + filename;
			}
		}
	});
});
