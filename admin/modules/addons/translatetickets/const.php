<?php

define( 'TT_GOOGLE', 'Google Translate' );

define( 'GOOGLE_API_KEY', 'AIzaSyDft13FOXl54qN388MyfdGc4UgTIv5oaSo' );
define( 'GOOGLE_DETECT_URI', 'https://www.googleapis.com/language/translate/v2/detect?key=%s&q=%s' );
define( 'GOOGLE_TRANSLATE_URI', 'https://www.googleapis.com/language/translate/v2?key=%s&q=%s&source=%s&target=%s' );
define( 'GOOGLE_TIME_LIMIT', 120 );
define( 'GOOGLE_DETECT_CHAR_LIMIT', 500 );
define( 'GOOGLE_TRANS_CHAR_LIMIT', 2000 );
