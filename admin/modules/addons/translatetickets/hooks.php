<?php
/**
 * Addon Module Sample Hook File
 *
 * This is a demo hook file for an addon module. Addon Modules can utilise all of the WHMCS
 * hooks in exactly the same way as a normal hook file would, and can contain multiple hooks.
 *
 * For more info, please refer to the hooks documentation @ http://docs.whmcs.com/Hooks
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2013
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

require_once( dirname(__FILE__) . '/const.php' );
require_once( dirname(__FILE__) . '/translator.php' );

// *** When client creates new ticket or adds a replay - adds a new reply with translated message (de => en). Ticket status remains the same
function translatetickets_hook_client( $vars ) {
	if (!function_exists( "AddReply" ))
		require ROOTDIR . "/includes/ticketfunctions.php";
	
	// Check if the ticket/reply is not marked as translation (author "Google Translate")
	$message = $vars['message'];

	if ( strpos($message, TT_GOOGLE) === 0 ) return;

	// If the message needs translation - add it as a reply
	if ( tt_get_message( $message, 'en' ) ) {
		$message = TT_GOOGLE . ": \n" . $message;
		$result	= select_query( 'tbltickets', 'status', array( 'id' => $vars['ticketid'] ) );
		$data	= mysql_fetch_array( $result );
		$status = $data['status'];

		$from = array( 'name' => TT_GOOGLE,
						'email' => '');
		
		AddReply(
			$vars['ticketid'],	// ticketid
			0,					// userid (if client replies)
			0,					// contactid (if non-client replies)
			$message,			// message
			'', 				// admin name (if admin replies). Here attribution to Google 
			'',					// attachfile
			$from,				// "from" - customer name and email if can not detect the customer
			$status,			// status (Closed/Answered/Customer-Reply...), don't change on automatic reply
			'',					// noemail
			true				// API used
			);
	}
}

add_hook( "TicketOpen", 1, "translatetickets_hook_client" );
add_hook( "TicketUserReply", 1, "translatetickets_hook_client" );

// *** When admin adds a replay - detects the language and if needed adds a translated text before original message (en => de)
function translatetickets_hook_admin_reply( $vars ) {
	// Don't translate Google translate translations
	$result	= select_query( 'tblticketreplies', '', array( 'id' => $vars['replyid'] ) );
	$data	= mysql_fetch_array( $result );
	
	if ( !$data ) return;

	$message = $data['message'];

	if ( tt_get_message( $message, 'de' ) ) {
		$message = TT_GOOGLE . ": \n" . $message . "\n\nOriginal message:\n" . $data['message'];
		update_query( 'tblticketreplies', array( 'message' => $message ), array( 'id' => $vars['replyid'] ) );
		sendMessage( "Support Ticket Reply", $vars['ticketid'], $vars['replyid'] );
	}
}

add_hook( "TicketAdminReply", 1, "translatetickets_hook_admin_reply" );

// *** When admin creates new ticket - detects the language and if needed modifies the ticket. Adds translated version prior to original message (en => de)
function translatetickets_hook_admin_open( $vars ) {
	// Don't translate Google translate translations
	$result	= select_query( 'tbltickets', '', array( 'id' => $vars['ticketid'] ) );
	$data	= mysql_fetch_array( $result );
	
	if ( !$data ) return;

	$message = $data['message'];
	
	if ( tt_get_message( $message, 'de' ) ) {
		update_query( 'tbltickets', array( 'message' => $message ), array( 'id' => $vars['ticketid'] ) );
		sendMessage( "Support Ticket Opened by Admin", $vars['ticketid'] );
	}
}

add_hook( "TicketOpenAdmin", 1, "translatetickets_hook_admin_open" );

// *** Blocks emails for admin replies that are not in German or without German translation
function translatetickets_hook_email_replies( $vars ) {
	$merge_fields = array();

	// *** Don't allow emails for support tickets replies go out if this is admin reply and reply is not translated
	// only support ticket replies
	if ( $vars['messagename'] !== 'Support Ticket Reply' ) {
		return $merge_fields;
	}
	
	$result	= select_query( 'tblticketreplies', '', array( 'tid' => $vars['relid'] ), 'id', 'DESC' );
	$data	= mysql_fetch_array( $result );

	// allow if the sender is the client
	if ( !$data ) return $merge_fields;
	if ( empty($data['admin']) ) return $merge_fields;

	// allow if the message is translated (starts with Google Translate attribution)
	if ( strpos($data['message'], TT_GOOGLE) === 0 ) return $merge_fields;

	// allow if the message is in German
	if ( tt_detect_lang($data['message']) === 'de' ) return $merge_fields;
	
	// otherwise - block the message
	$merge_fields['abortsend'] = true;
	
	return $merge_fields;
}

add_hook( "EmailPreSend", 1, "translatetickets_hook_email_replies" );

// *** Blocks emails for admin ticket (newly created) that are not in German or without German translation
function translatetickets_hook_email_tickets( $vars ) {
	$merge_fields = array();

	// *** Don't allow emails for support tickets replies go out if this is admin reply and reply is not translated
	// only support ticket replies
	if ( $vars['messagename'] !== 'Support Ticket Opened by Admin' ) return $merge_fields;
	
	$result	= select_query( 'tbltickets', '', array( 'id' => $vars['relid'] ) );
	$data	= mysql_fetch_array( $result );

	// allow if the sender is the client
	if ( !$data ) return $merge_fields;
	if ( empty($data['admin']) ) return $merge_fields;

	// allow if the message is translated (starts with Google Translate attribution)
	if ( strpos($data['message'], TT_GOOGLE) === 0 ) return $merge_fields;

	// allow if the message is in German
	if ( tt_detect_lang($data['message']) === 'de' ) return $merge_fields;
	
	// otherwise - block the message
	$merge_fields['abortsend'] = true;
	
	return $merge_fields;
}

add_hook( "EmailPreSend", 1, "translatetickets_hook_email_tickets" );
