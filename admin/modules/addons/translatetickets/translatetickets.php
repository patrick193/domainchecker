<?php
/**
 * Module that translates client tickets and admin answers using Google Translate API
 *
 * This example addon module demonstrates all the functions an addon module can contain.
 * Please refer to the full documentation @ http://docs.whmcs.com/Addon_Modules for more details.
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2013
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

function translatetickets_config() {
    $configarray = array(
    "name" => "Translate Tickets",
    "description" => "This addon translates tickets and responses from German to English and vice versa",
    "version" => "1.0",
    "author" => "General Internet",
    "language" => "english",
	);
    
	return $configarray;
}

function translatetickets_activate() {
	return array('status'=>'success','description'=>'Module successfully activated.'); 
}

function translatetickets_deactivate() {
    return array('status'=>'success','description'=>'Module successfully deactivated.');
}
