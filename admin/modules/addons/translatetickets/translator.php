<?php
/**
 * Performs Google translate for tickets
 */

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

require_once dirname(__FILE__) . '/const.php';

// Checks the message for target language. Message is splitted into paragraphs, each paragraph is analysed and translated if needed.
// returns true if at least one paragraph was translated.
function tt_get_message(&$text, $dst_lang) {
	$result = false;
	
	// for the given text:
	// - transfer html tags back (in case WHMCS previously encoded HTML entities)
	// - check the text limit
	// - add <br /> as standard line breaks
	// - split to paragraphs and remove all tags
	$src_text = htmlspecialchars_decode($text);
	
	if ( strlen($src_text) > GOOGLE_TRANS_CHAR_LIMIT ) {
		$src_text = substr($src_text, 0, GOOGLE_TRANS_CHAR_LIMIT) . '...';
	} 

	$src_text = nl2br($src_text);
	$src_text = str_replace('<br>', '<br />', $src_text);
	$src_text = str_replace('</p>', '<br />', $src_text);
	$src_text = explode('<br />', $src_text);
	
	foreach ($src_text as $index => $value) {
		$value = strip_tags($value);
		$value = trim($value);

		if ( !empty($value) ) {
			$src_lang = tt_detect_lang($value);

			if ( $src_lang !== $dst_lang ) {
				$value = tt_translate_text($value, $src_lang, $dst_lang);
				$result = true;
			}
		}
		
		$src_text[$index] = $value;
	}
	
	$text = implode( "\n", $src_text );

	return $result;
}

// Detects message language using Google API. Text should be prepared (tags striped, line breaks converted, etc... )
function tt_detect_lang( $text ) {
	set_time_limit(GOOGLE_TIME_LIMIT);
	
	$detect_text = $text;
	
	if ( strlen($detect_text) > GOOGLE_DETECT_CHAR_LIMIT ) {
		$detect_text = substr($detect_text, 0, GOOGLE_DETECT_CHAR_LIMIT);
	} 

    $url = sprintf(GOOGLE_DETECT_URI, GOOGLE_API_KEY, urlencode($detect_text) );

    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($handle);                 
    $response = json_decode($response, true);
    curl_close($handle);

	return $response['data']['detections'][0][0]['language'];
}

// Translates message using Google API. Text should be prepared (tags striped, line breaks converted, etc... )
function tt_translate_text( $text, $src_lang, $dst_lang ) {
	set_time_limit(GOOGLE_TIME_LIMIT);

    $url = sprintf(GOOGLE_TRANSLATE_URI, GOOGLE_API_KEY, urlencode($text), $src_lang, $dst_lang );

    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($handle);                 
    $response = json_decode($response, true);
    curl_close($handle);

	return $response['data']['translations'][0]['translatedText'];
}
