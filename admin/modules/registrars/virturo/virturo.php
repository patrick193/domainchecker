<?php
/* -------------------------------------------------------------- */
/*
	*	Virturo API Connect v 1.0.0
	*   Domain Registrar Module (WHMCS)
	*	Developed by Daniel Arnhold | Living Internet GmbH
	*   2014
*/
/* -------------------------------------------------------------- */
	#error_reporting(E_ALL);
	header("Content-Type: text/html; charset=utf-8"); 
/* -------------------------------------------------------------- */

function virturo_getConfigArray() {
	$configarray = array(
	 "username" => array( "Type" => "text", "Size" => "20", "Description" => "Enter your username here", ),
	 "password" => array( "Type" => "password", "Size" => "20", "Description" => "Enter your password here", ),
	);
	
	return $configarray;
}

function virturo__sendrequest($commandStr, $params) {
	$url = 'http://login.virturo.de/api.php';

	$commandTmpArr = explode(".",$commandStr);
	$command = array('class' => $commandTmpArr[0], 'method' => $commandTmpArr[1]);

	$postData = array_merge($params, $command);		
	
	var_dump($postData);
	
	$c = curl_init();
	curl_setopt( $c, CURLOPT_URL , $url );
	curl_setopt( $c, CURLOPT_POST , 1 );
	curl_setopt( $c, CURLOPT_TIMEOUT , 100 );
	curl_setopt( $c, CURLOPT_RETURNTRANSFER , 1 );
	curl_setopt( $c, CURLOPT_POSTFIELDS , $postData );
	$result = html_entity_decode(curl_exec( $c ));
	curl_close( $c );
	
	$resultParts = explode(";",$result);
	
	foreach($resultParts as $part) {
		$kvPair = explode('=', $part,2);
		if (count($kvPair) == 2)
			$return[$kvPair[0]] = $kvPair[1];
	}
	
	return $return;
}

function virturo__getanswers ($method, $params) {
	$params = array_merge($params, virturo_getConfigArray());
	$val = virturo__sendrequest($method, $params);
	
	return ($val);
}

/* -------------------------------------------------------------- */


function virturo_GetNameservers($params) {
	return array('ns1'=>'n1.virturo.de', 'ns2'=>'ns2.virturo.de');
}

function virturo_RegisterDomain($params) {
	$tld = $params["tld"];
	$sld = $params["sld"];

	# Registrant Details
	$RegistrantFirstName = $params["firstname"];
	$RegistrantLastName = $params["lastname"];
	$RegistrantAddress1 = $params["address1"];
	$RegistrantCity = $params["city"];
	$RegistrantPostalCode = $params["postcode"];
	$RegistrantCountry = $params["country"];
	$RegistrantEmailAddress = $params["email"];
	$RegistrantPhone = $params["phonenumber"];

    /* --------- Werte formatieren ---------- */

        # Telefonnummer (eingehendes Format: 00YYXXXX)
        $rufnummer = str_replace("00", "", $RegistrantPhone);  # Rufnummer 00YYXXXX zu YYXXXX)
        $rufnummer_plain = substr($rufnummer, 2); # Rufnummer YYXXXX zu XXXX
        $ortsvorwahl = substr($rufnummer_plain, 0, 4); # Rufnummer XXXXXX cutten zu XXXX XX
        $ortsrufnummer = substr($rufnummer_plain, 4); # Rufnummer XXXXXX cutten zu XXXX XX
        $vorwahl_plain = '+'.substr($rufnummer, 0, 2); # Vorwahl +YY aus YYXXXX

        # Adresse in Straße + Hausnummer trennen
        preg_match("/^(.+) ([0-9]+.*)$/U",$RegistrantAddress1,$address_restructured);
        $strasse = $address_restructured[1];
        $hausnummer = $address_restructured[2];

    /* ------- Ende Werte formatieren ------- */

    $virturo_handle_create_vars = array(

                        'create' => 'true',
                        'type' => 'PERS',
                        'organisation' => '',
                        'firstName' => $RegistrantFirstName,
                        'middleName' => '',
                        'lastName' => $RegistrantLastName,
                        'street' => $strasse,
                        'number' => $hausnummer,
                        'city' => $RegistrantCity,
                        'postcode' => $RegistrantPostalCode,
                        'country' => $RegistrantCountry,
                        'email' => $RegistrantEmailAddress,
                        'phone0' => $vorwahl_plain,
                        'phone1' => $ortsvorwahl,
                        'phone2' => $ortsrufnummer,
                        'fax0' => $vorwahl_plain,
                        'fax1' => $ortsvorwahl,
                        'fax2' => $ortsrufnummer,
                        'protection' => 'yes'

    );

    $handle_register_domains = virturo__getanswers("HandleManagement.Create", $virturo_handle_create_vars );

    if($handle_register_domains["result"] == 'error') {
        $error = $handle_register_domains["message"];
    }

    if($handle_register_domains["result"] == 'success') { # Handle anlegen erfolgreich, Handle existiert bereits?!
        $client_handle_fetch = explode(' ', $handle_register_domains["description"]);
        $client_handle = $client_handle_fetch[2];   # Handle ID aus Description Ausgabe auslesen

            // Domain Registrieren...

            $virturo_domain_reg_vars = array(

                'tld' => $tld,
                'sld' => $sld,
                'ownerId' => $client_handle,
                'adminId' => $client_handle,
                'techId' => 'STTHC0094',            # Standardhandle für Tech-C, Thomas Steden (STTHC0094)
                'zoneId' => 'STTHC0094',            # Standardhandle für Zone-C, Thomas Steden (STTHC0094)
                'ip' => '4.4.4.4',
                'useStandardNameservers' => 'true'

            );
            $register_domain_process = virturo__getanswers("DomainManagement.Create", $virturo_domain_reg_vars );

            if($register_domain_process["result"] == 'error') {
                $error = $register_domain_process["message"];
            }

    }

	$values["error"] = $error;
	return $values;
}

function virturo_TransferDomain($params) {
	$tld = $params["tld"];
	$sld = $params["sld"];
	$transfersecret = $params["transfersecret"];

	# Registrant Details
	$RegistrantFirstName = $params["firstname"];
	$RegistrantLastName = $params["lastname"];
	$RegistrantAddress1 = $params["address1"];
	$RegistrantCity = $params["city"];
	$RegistrantPostalCode = $params["postcode"];
	$RegistrantCountry = $params["country"];
	$RegistrantEmailAddress = $params["email"];
	$RegistrantPhone = $params["phonenumber"];

    if($transfersecret == 'EMPTY' OR $transfersecret == '') {
        $error = 'Es wurde kein Authcode übergeben, Transfer fehlgeschlagen.';
    } else {

        /* --------- Werte formatieren ---------- */

        # Telefonnummer (eingehendes Format: 00YYXXXX)
        $rufnummer = str_replace("00", "", $RegistrantPhone);  # Rufnummer 00YYXXXX zu YYXXXX)
        $rufnummer_plain = substr($rufnummer, 2); # Rufnummer YYXXXX zu XXXX
        $ortsvorwahl = substr($rufnummer_plain, 0, 4); # Rufnummer XXXXXX cutten zu XXXX XX
        $ortsrufnummer = substr($rufnummer_plain, 4); # Rufnummer XXXXXX cutten zu XXXX XX
        $vorwahl_plain = '+'.substr($rufnummer, 0, 2); # Vorwahl +YY aus YYXXXX

        # Adresse in Straße + Hausnummer trennen
        preg_match("/^(.+) ([0-9]+.*)$/U", $RegistrantAddress1, $address_restructured);
        $strasse = $address_restructured[1];
        $hausnummer = $address_restructured[2];

        /* ------- Ende Werte formatieren ------- */

        $virt_handle_transfer_vars = array(

            'create' => 'true',
            'type' => 'PERS',
            'organisation' => '',
            'firstName' => $RegistrantFirstName,
            'middleName' => '',
            'lastName' => $RegistrantLastName,
            'street' => $strasse,
            'number' => $hausnummer,
            'city' => $RegistrantCity,
            'postcode' => $RegistrantPostalCode,
            'country' => $RegistrantCountry,
            'email' => $RegistrantEmailAddress,
            'phone0' => $vorwahl_plain,
            'phone1' => $ortsvorwahl,
            'phone2' => $ortsrufnummer,
            'fax0' => $vorwahl_plain,
            'fax1' => $ortsvorwahl,
            'fax2' => $ortsrufnummer,
            'protection' => 'yes'

        );

        $handle_transfer_domains = virturo__getanswers("HandleManagement.Create", $virt_handle_transfer_vars);

        if ($handle_transfer_domains["result"] == 'error') {
            $error = $handle_transfer_domains["message"];
        }

        if($handle_transfer_domains["result"] == 'success') { # Handle anlegen erfolgreich, Handle existiert bereits?!
            $client_handle_fetch = explode(' ', $handle_transfer_domains["description"]);
            $client_handle = $client_handle_fetch[2];   # Handle ID aus Description Ausgabe auslesen

            // Domain in Providerwechsel geben...

            $virt_domain_tra_vars = array(

                'tld' => $tld,
                'sld' => $sld,
                'authcode' => $transfersecret,
                'ownerId' => $client_handle,
                'adminId' => $client_handle,
                'techId' => 'STTHC0094',            # Standardhandle für Tech-C, Thomas Steden (STTHC0094)
                'zoneId' => 'STTHC0094',            # Standardhandle für Zone-C, Thomas Steden (STTHC0094)
                'ip' => '4.4.4.4',
                'useStandardNameservers' => 'true'

            );
            $transfer_domain_process = virturo__getanswers("DomainManagement.TransferIn", $virt_domain_tra_vars );

            if($transfer_domain_process["result"] == 'error') {
                $error = $transfer_domain_process["message"];
            }

        }

    }

	$values["error"] = $error;
	return $values;
}

?>