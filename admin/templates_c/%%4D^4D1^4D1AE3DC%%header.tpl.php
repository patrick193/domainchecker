<?php /* Smarty version 2.6.28, created on 2015-04-15 10:43:15
         compiled from classic/header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'file_exists', 'classic/header.tpl', 22, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?php echo $this->_tpl_vars['charset']; ?>
" />
<title><?php echo $this->_tpl_vars['companyname']; ?>
 - <?php echo $this->_tpl_vars['pagetitle']; ?>
<?php if ($this->_tpl_vars['kbarticle']['title']): ?> - <?php echo $this->_tpl_vars['kbarticle']['title']; ?>
<?php endif; ?></title>
<?php if ($this->_tpl_vars['systemurl']): ?><base href="<?php echo $this->_tpl_vars['systemurl']; ?>
" />
<?php endif; ?><link rel="stylesheet" type="text/css" href="templates/<?php echo $this->_tpl_vars['template']; ?>
/style.css" />
<script type="text/javascript" src="includes/jscript/jquery.js"></script>
<?php echo $this->_tpl_vars['headoutput']; ?>

<?php if ($this->_tpl_vars['livehelpjs']): ?><?php echo $this->_tpl_vars['livehelpjs']; ?>

<?php endif; ?></head>
<body>

<?php echo $this->_tpl_vars['headeroutput']; ?>


<div class="wrapper">

<img src="templates/<?php echo $this->_tpl_vars['template']; ?>
/header.jpg" width="730" height="118" alt="" />

<table class="topnavbar"><tr class="topnavbar"><td><a href="index.php"><?php echo $this->_tpl_vars['LANG']['globalsystemname']; ?>
</a></td><td><a href="clientarea.php"><?php echo $this->_tpl_vars['LANG']['clientareatitle']; ?>
</a></td><td><a href="announcements.php"><?php echo $this->_tpl_vars['LANG']['announcementstitle']; ?>
</a></td><td><a href="knowledgebase.php"><?php echo $this->_tpl_vars['LANG']['knowledgebasetitle']; ?>
</a></td><td><a href="supporttickets.php"><?php echo $this->_tpl_vars['LANG']['supportticketspagetitle']; ?>
</a></td><td><a href="downloads.php"><?php echo $this->_tpl_vars['LANG']['downloadstitle']; ?>
</a></td><?php if ($this->_tpl_vars['loggedin']): ?><td><a href="logout.php"><?php echo $this->_tpl_vars['LANG']['logouttitle']; ?>
</a></td><?php endif; ?></tr></table>

<p><?php if (((is_array($_tmp="templates/".($this->_tpl_vars['template'])."/images/".($this->_tpl_vars['filename']).".png")) ? $this->_run_mod_handler('file_exists', true, $_tmp) : file_exists($_tmp))): ?><img src="templates/<?php echo $this->_tpl_vars['template']; ?>
/images/<?php echo $this->_tpl_vars['filename']; ?>
.png" align="right" alt="" /><?php endif; ?>
<span class="heading"><?php echo $this->_tpl_vars['pagetitle']; ?>
</span><br />
<?php echo $this->_tpl_vars['LANG']['globalyouarehere']; ?>
: <?php echo $this->_tpl_vars['breadcrumbnav']; ?>
</p>

<?php if ($this->_tpl_vars['loggedin']): ?>
<p align="center" class="clientarealinks">
<a href="clientarea.php"><img src="images/clientarea.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['clientareanavhome']; ?>
</a>
<a href="clientarea.php?action=details"><img src="images/details.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['clientareanavdetails']; ?>
</a>
<a href="clientarea.php?action=products"><img src="images/products.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['clientareaproducts']; ?>
</a>
<?php if ($this->_tpl_vars['condlinks']['domainreg'] || $this->_tpl_vars['condlinks']['domaintrans']): ?><a href="clientarea.php?action=domains"><img src="images/domains.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['clientareanavdomains']; ?>
</a><?php endif; ?>
<?php if ($this->_tpl_vars['condlinks']['pmaddon']): ?><a href="index.php?m=project_management"><img src="images/hosting.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['clientareaprojects']; ?>
</a><?php endif; ?>
<a href="clientarea.php?action=quotes"><img src="images/pdf.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['quotestitle']; ?>
</a>
<a href="clientarea.php?action=invoices"><img src="images/invoices.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['invoices']; ?>
</a>
<a href="supporttickets.php"><img src="images/supporttickets.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['clientareanavsupporttickets']; ?>
</a>
<?php if ($this->_tpl_vars['condlinks']['affiliates']): ?><a href="affiliates.php"><img src="images/affiliates.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['affiliatestitle']; ?>
</a><?php endif; ?>
<a href="clientarea.php?action=emails"><img src="images/emails.gif" border="0" hspace="5" align="absmiddle" alt="" /><?php echo $this->_tpl_vars['LANG']['clientareaemails']; ?>
</a>
</p>
<?php endif; ?>