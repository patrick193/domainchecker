<?php /* Smarty version 2.6.28, created on 2015-04-24 14:24:48
         compiled from emailtpl:emailmessage */ ?>
<p><a href="<?php echo $this->_tpl_vars['company_domain']; ?>
" target="_blank"><img src="<?php echo $this->_tpl_vars['company_logo_url']; ?>
" alt="<?php echo $this->_tpl_vars['company_name']; ?>
" border="0" /></a></p>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>This message is to confirm that your domain purchase has been successful. The details of the domain purchase are below:</p>
<p>Registration Date: <?php echo $this->_tpl_vars['domain_reg_date']; ?>
<br /> Domain: <?php echo $this->_tpl_vars['domain_name']; ?>
<br /> Registration Period: <?php echo $this->_tpl_vars['domain_reg_period']; ?>
<br /> Amount: <?php echo $this->_tpl_vars['domain_first_payment_amount']; ?>
<br /> Next Due Date: <?php echo $this->_tpl_vars['domain_next_due_date']; ?>
</p>
<p>You may login to your client area at <?php echo $this->_tpl_vars['whmcs_url']; ?>
 to manage your new domain.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>