<?php /* Smarty version 2.6.28, created on 2015-04-23 18:29:17
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>Recently a request was submitted to reset your password for our client area. If you did not request this, please ignore this email. It will expire and become useless in 2 hours time.</p>
<p>To reset your password, please visit the url below:<br /><a href="<?php echo $this->_tpl_vars['pw_reset_url']; ?>
"><?php echo $this->_tpl_vars['pw_reset_url']; ?>
</a></p>
<p>When you visit the link above, you will have the opportunity to choose a new password.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>