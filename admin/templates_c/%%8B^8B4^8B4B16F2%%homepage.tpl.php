<?php /* Smarty version 2.6.28, created on 2015-06-15 08:52:00
         compiled from /var/www/web_domains/admin/templates/bamat/homepage.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'dc_domain_check_form', '/var/www/web_domains/admin/templates/bamat/homepage.tpl', 9, false),array('modifier', 'strip_tags', '/var/www/web_domains/admin/templates/bamat/homepage.tpl', 66, false),array('modifier', 'truncate', '/var/www/web_domains/admin/templates/bamat/homepage.tpl', 66, false),)), $this); ?>
<?php if ($this->_tpl_vars['condlinks']['domainreg'] || $this->_tpl_vars['condlinks']['domaintrans'] || $this->_tpl_vars['condlinks']['domainown']): ?>
    <div class="styled_title">
        <h1><?php echo $this->_tpl_vars['LANG']['domaincheckerchoosedomain']; ?>
</h1>
    </div>
    <br />
    <p><?php echo $this->_tpl_vars['LANG']['domaincheckerenterdomain']; ?>
</p>
    <br />

	<?php echo smarty_function_dc_domain_check_form(array(), $this);?>

<?php endif; ?>

		</div>
	</div>
</div>

<div class="container-fluid page-section-grey">
	<div class="container">
		<div class="row">

		<div class="col-md-6">
			<div class="internalpadding">
				<div class="styled_title">
					<h2><?php echo $this->_tpl_vars['LANG']['navservicesorder']; ?>
</h2>
				</div>
				<p class="get-in-box-desc"><?php echo $this->_tpl_vars['LANG']['clientareahomeorder']; ?>
<br /><br /></p>
				<form method="post" action="cart.php">
				<p class="text-center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareahomeorderbtn']; ?>
 &raquo;" class="btn" /></p>
				</form>
			</div>
		</div>

		<div class="col-md-6">
			<div class="internalpadding">
				<div class="styled_title"><h2><?php echo $this->_tpl_vars['LANG']['manageyouraccount']; ?>
</h2></div>
				<p class="get-in-box-desc"><?php echo $this->_tpl_vars['LANG']['clientareahomelogin']; ?>
<br /><br /></p>
				<form method="post" action="clientarea.php">
				<p class="text-center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareahomeloginbtn']; ?>
 &raquo;" class="btn" /></p>
				</form>
			</div>
		</div>

</div>

<div class="row">

<?php if ($this->_tpl_vars['twitterusername']): ?>
<div class="styled_title">
    <h2><?php echo $this->_tpl_vars['LANG']['twitterlatesttweets']; ?>
</h2>
</div>
<div id="twitterfeed">
    <p><img src="images/loading.gif"></p>
</div>
<?php echo '<script language="javascript">
jQuery(document).ready(function(){
  jQuery.post("announcements.php", { action: "twitterfeed", numtweets: 3 },
    function(data){
      jQuery("#twitterfeed").html(data);
    });
});
</script>'; ?>

<?php elseif ($this->_tpl_vars['announcements']): ?>
<div class="styled_title">
    <h2><?php echo $this->_tpl_vars['LANG']['latestannouncements']; ?>
</h2>
</div>
<?php $_from = $this->_tpl_vars['announcements']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['announcement']):
?>
<p><?php echo $this->_tpl_vars['announcement']['date']; ?>
 - <a href="<?php if ($this->_tpl_vars['seofriendlyurls']): ?>announcements/<?php echo $this->_tpl_vars['announcement']['id']; ?>
/<?php echo $this->_tpl_vars['announcement']['urlfriendlytitle']; ?>
.html<?php else: ?>announcements.php?id=<?php echo $this->_tpl_vars['announcement']['id']; ?>
<?php endif; ?>"><b><?php echo $this->_tpl_vars['announcement']['title']; ?>
</b></a><br /><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['announcement']['text'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 100, "...") : smarty_modifier_truncate($_tmp, 100, "...")); ?>
</p>
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>

</div>