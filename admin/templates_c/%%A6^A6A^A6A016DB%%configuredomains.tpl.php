<?php /* Smarty version 2.6.28, created on 2015-04-24 16:04:48
         compiled from /home/generali/public_html/admin/templates/orderforms/comparison/configuredomains.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strtolower', '/home/generali/public_html/admin/templates/orderforms/comparison/configuredomains.tpl', 39, false),)), $this); ?>
<script type="text/javascript" src="includes/jscript/jqueryui.js"></script>
<script type="text/javascript" src="templates/orderforms/<?php echo $this->_tpl_vars['carttpl']; ?>
/js/main.js"></script>
<link rel="stylesheet" type="text/css" href="templates/orderforms/<?php echo $this->_tpl_vars['carttpl']; ?>
/style.css" />
<link rel="stylesheet" type="text/css" href="templates/orderforms/<?php echo $this->_tpl_vars['carttpl']; ?>
/uistyle.css" />

<div id="order-comparison">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "orderforms/comparison/comparisonsteps.tpl", 'smarty_include_vars' => array('step' => 2)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="cartcontainer">

<?php echo $this->_tpl_vars['LANG']['cartdomainsconfiginfo']; ?>
<br /><br />

<?php if ($this->_tpl_vars['errormessage']): ?><div class="errorbox"><?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>
<ul><?php echo $this->_tpl_vars['errormessage']; ?>
</ul></div><br /><?php endif; ?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?a=confdomains">
<input type="hidden" name="update" value="true" />

<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['domain']):
?>
<h2><?php echo $this->_tpl_vars['domain']['domain']; ?>
</h2>
<div class="domainconfig">
<table width="100%" cellspacing="0" cellpadding="0" class="configtable">
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['hosting']; ?>
:</td><td class="fieldarea"><?php if ($this->_tpl_vars['domain']['hosting']): ?><span style="color:#009900;">[<?php echo $this->_tpl_vars['LANG']['cartdomainshashosting']; ?>
]</span><?php else: ?><a href="cart.php" style="color:#cc0000;">[<?php echo $this->_tpl_vars['LANG']['cartdomainsnohosting']; ?>
]</a><br /><?php endif; ?></td></tr>
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['orderregperiod']; ?>
:</td><td class="fieldarea"><?php echo $this->_tpl_vars['domain']['regperiod']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>
</td></tr>
<?php if ($this->_tpl_vars['domain']['eppenabled']): ?><tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['domaineppcode']; ?>
:</td><td class="fieldarea"><input type="text" name="epp[<?php echo $this->_tpl_vars['num']; ?>
]" size="20" value="<?php echo $this->_tpl_vars['domain']['eppvalue']; ?>
" /> <?php echo $this->_tpl_vars['LANG']['domaineppcodedesc']; ?>
</td></tr><?php endif; ?>
<?php if ($this->_tpl_vars['domain']['dnsmanagement'] || $this->_tpl_vars['domain']['emailforwarding'] || $this->_tpl_vars['domain']['idprotection']): ?><tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['cartaddons']; ?>
:</td><td class="fieldarea">
<?php if ($this->_tpl_vars['domain']['dnsmanagement']): ?><label><input type="checkbox" name="dnsmanagement[<?php echo $this->_tpl_vars['num']; ?>
]"<?php if ($this->_tpl_vars['domain']['dnsmanagementselected']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['domaindnsmanagement']; ?>
 (<?php echo $this->_tpl_vars['domain']['dnsmanagementprice']; ?>
)</label><br /><?php endif; ?>
<?php if ($this->_tpl_vars['domain']['emailforwarding']): ?><label><input type="checkbox" name="emailforwarding[<?php echo $this->_tpl_vars['num']; ?>
]"<?php if ($this->_tpl_vars['domain']['emailforwardingselected']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['domainemailforwarding']; ?>
 (<?php echo $this->_tpl_vars['domain']['emailforwardingprice']; ?>
)</label><br /><?php endif; ?>
<?php if ($this->_tpl_vars['domain']['idprotection']): ?><label><input type="checkbox" name="idprotection[<?php echo $this->_tpl_vars['num']; ?>
]"<?php if ($this->_tpl_vars['domain']['idprotectionselected']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['domainidprotection']; ?>
 (<?php echo $this->_tpl_vars['domain']['idprotectionprice']; ?>
)</label><br /><?php endif; ?>
</td></tr><?php endif; ?>
<?php $_from = $this->_tpl_vars['domain']['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domainfieldname'] => $this->_tpl_vars['domainfield']):
?>
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['domainfieldname']; ?>
:</td><td class="fieldarea"><?php echo $this->_tpl_vars['domainfield']; ?>
</td></tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>
<?php endforeach; endif; unset($_from); ?>

<?php if ($this->_tpl_vars['atleastonenohosting']): ?>
<h2><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['domainnameservers'])) ? $this->_run_mod_handler('strtolower', true, $_tmp) : strtolower($_tmp)); ?>
</h2>
<div class="nameservers">
<table width="100%" cellspacing="0" cellpadding="0" class="configtable">
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['cartnameserverchoice']; ?>
:</td><td class="fieldarea"><label><input type="radio" name="customns" id="usedefaultns" checked /> <?php echo $this->_tpl_vars['LANG']['cartnameserverchoicedefault']; ?>
</label><br /><label><input type="radio" name="customns" id="usecustomns" /> <?php echo $this->_tpl_vars['LANG']['cartnameserverchoicecustom']; ?>
</label></td></tr>
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['domainnameserver1']; ?>
:</td><td class="fieldarea"><input type="text" name="domainns1" size="40" value="<?php echo $this->_tpl_vars['domainns1']; ?>
" /></td></tr>
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['domainnameserver2']; ?>
:</td><td class="fieldarea"><input type="text" name="domainns2" size="40" value="<?php echo $this->_tpl_vars['domainns2']; ?>
" /></td></tr>
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['domainnameserver3']; ?>
:</td><td class="fieldarea"><input type="text" name="domainns3" size="40" value="<?php echo $this->_tpl_vars['domainns3']; ?>
" /></td></tr>
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['domainnameserver4']; ?>
:</td><td class="fieldarea"><input type="text" name="domainns4" size="40" value="<?php echo $this->_tpl_vars['domainns4']; ?>
" /></td></tr>
<tr><td class="fieldlabel"><?php echo $this->_tpl_vars['LANG']['domainnameserver5']; ?>
:</td><td class="fieldarea"><input type="text" name="domainns5" size="40" value="<?php echo $this->_tpl_vars['domainns5']; ?>
" /></td></tr>
</table>
</div>
<?php endif; ?>

<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['updatecart']; ?>
" class="cartbutton green" /></p>

</div>

</form>

</div>