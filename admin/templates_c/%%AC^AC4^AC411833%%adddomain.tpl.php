<?php /* Smarty version 2.6.28, created on 2015-04-15 16:57:09
         compiled from /home/generali/public_html/admin/templates/orderforms/bamatcart/adddomain.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', '/home/generali/public_html/admin/templates/orderforms/bamatcart/adddomain.tpl', 18, false),array('function', 'dc_domain_check_form', '/home/generali/public_html/admin/templates/orderforms/bamatcart/adddomain.tpl', 21, false),)), $this); ?>
<div id="order-comparison">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "orderforms/bamatcart/comparisonsteps.tpl", 'smarty_include_vars' => array('step' => 1)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> 

<h1><?php if ($this->_tpl_vars['domain'] == 'register'): ?><?php echo $this->_tpl_vars['LANG']['registerdomain']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['transferdomain']; ?>
<?php endif; ?></h1>

<?php if (! $this->_tpl_vars['loggedin'] && $this->_tpl_vars['currencies']): ?>
<div class="currencychooser">
<?php $_from = $this->_tpl_vars['currencies']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['curr']):
?>
<a href="cart.php?a=add&domain=<?php echo $this->_tpl_vars['domain']; ?>
&currency=<?php echo $this->_tpl_vars['curr']['id']; ?>
"><img src="images/flags/<?php if ($this->_tpl_vars['curr']['code'] == 'AUD'): ?>au<?php elseif ($this->_tpl_vars['curr']['code'] == 'CAD'): ?>ca<?php elseif ($this->_tpl_vars['curr']['code'] == 'EUR'): ?>eu<?php elseif ($this->_tpl_vars['curr']['code'] == 'GBP'): ?>gb<?php elseif ($this->_tpl_vars['curr']['code'] == 'INR'): ?>in<?php elseif ($this->_tpl_vars['curr']['code'] == 'JPY'): ?>jp<?php elseif ($this->_tpl_vars['curr']['code'] == 'USD'): ?>us<?php elseif ($this->_tpl_vars['curr']['code'] == 'ZAR'): ?>za<?php else: ?>na<?php endif; ?>.png" border="0" alt="" /> <?php echo $this->_tpl_vars['curr']['code']; ?>
</a>
<?php endforeach; endif; unset($_from); ?>
</div>
<div class="clear"></div>
<?php endif; ?>

<p class="domainregtitle"><?php if ($this->_tpl_vars['domain'] == 'register'): ?><?php echo $this->_tpl_vars['LANG']['registerdomaindesc']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['transferdomaindesc']; ?>
<?php endif; ?></p>

<?php if ($this->_tpl_vars['errormessage']): ?><div class="alert alert-danger"><?php echo ((is_array($_tmp=$this->_tpl_vars['errormessage'])) ? $this->_run_mod_handler('replace', true, $_tmp, '<li>', ' &nbsp;#&nbsp; ') : smarty_modifier_replace($_tmp, '<li>', ' &nbsp;#&nbsp; ')); ?>
 &nbsp;#&nbsp; </div><br /><?php endif; ?>

<div class="dc_embedded">
	<?php echo smarty_function_dc_domain_check_form(array(), $this);?>

</div>
			
<div id="loading" class="loading"><img src="images/loading.gif" border="0" alt="Loading..." /></div>

<form method="post" action="cart.php?a=add&domain=<?php echo $this->_tpl_vars['domain']; ?>
">

<div id="domainresults"></div>

</form>

</div>