<?php /* Smarty version 2.6.28, created on 2015-04-24 16:05:14
         compiled from /home/generali/public_html/admin/templates/orderforms/comparison/viewcart.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strtolower', '/home/generali/public_html/admin/templates/orderforms/comparison/viewcart.tpl', 48, false),array('modifier', 'sprintf2', '/home/generali/public_html/admin/templates/orderforms/comparison/viewcart.tpl', 153, false),)), $this); ?>
<script type="text/javascript" src="includes/jscript/jqueryui.js"></script>
<script type="text/javascript" src="templates/orderforms/<?php echo $this->_tpl_vars['carttpl']; ?>
/js/main.js"></script>
<script type="text/javascript" src="includes/jscript/statesdropdown.js"></script>
<script type="text/javascript" src="includes/jscript/pwstrength.js"></script>
<script type="text/javascript" src="includes/jscript/creditcard.js"></script>

<link rel="stylesheet" type="text/css" href="templates/orderforms/<?php echo $this->_tpl_vars['carttpl']; ?>
/style.css" />
<link rel="stylesheet" type="text/css" href="templates/orderforms/<?php echo $this->_tpl_vars['carttpl']; ?>
/uistyle.css" />

<?php echo '<script language="javascript">
function removeItem(type,num) {
    var response = confirm("'; ?>
<?php echo $this->_tpl_vars['LANG']['cartremoveitemconfirm']; ?>
<?php echo '");
    if (response) {
        window.location = \'cart.php?a=remove&r=\'+type+\'&i=\'+num;
    }
}
function emptyCart(type,num) {
    var response = confirm("'; ?>
<?php echo $this->_tpl_vars['LANG']['cartemptyconfirm']; ?>
<?php echo '");
    if (response) {
        window.location = \'cart.php?a=empty\';
    }
}
</script>'; ?>

<script>
window.langPasswordStrength = "<?php echo $this->_tpl_vars['LANG']['pwstrength']; ?>
";
window.langPasswordWeak = "<?php echo $this->_tpl_vars['LANG']['pwstrengthweak']; ?>
";
window.langPasswordModerate = "<?php echo $this->_tpl_vars['LANG']['pwstrengthmoderate']; ?>
";
window.langPasswordStrong = "<?php echo $this->_tpl_vars['LANG']['pwstrengthstrong']; ?>
";
</script>

<div id="order-comparison">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "orderforms/comparison/comparisonsteps.tpl", 'smarty_include_vars' => array('step' => 3)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="cartcontainer">

<?php if ($this->_tpl_vars['errormessage']): ?><div class="errorbox"><?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>
<ul><?php echo $this->_tpl_vars['errormessage']; ?>
</ul></div><?php elseif ($this->_tpl_vars['promotioncode'] && $this->_tpl_vars['rawdiscount'] == "0.00"): ?><div class="errorbox"><?php echo $this->_tpl_vars['LANG']['promoappliedbutnodiscount']; ?>
</div><?php endif; ?>

<?php if ($this->_tpl_vars['bundlewarnings']): ?>
<div class="errorbox">
<strong><?php echo $this->_tpl_vars['LANG']['bundlereqsnotmet']; ?>
</strong><br />
<?php $_from = $this->_tpl_vars['bundlewarnings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['warning']):
?>
<?php echo $this->_tpl_vars['warning']; ?>
<br />
<?php endforeach; endif; unset($_from); ?>
</div>
<?php endif; ?>

<h2><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['ordersummary'])) ? $this->_run_mod_handler('strtolower', true, $_tmp) : strtolower($_tmp)); ?>
</h2>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?a=view">

<table class="carttable">
<tr class="carttableheading"><th width="70%"><?php echo $this->_tpl_vars['LANG']['orderdesc']; ?>
</th><th width="30%"><?php echo $this->_tpl_vars['LANG']['orderprice']; ?>
</th></tr>

<?php $_from = $this->_tpl_vars['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['product']):
?>
<tr class="carttableproduct"><td>
<strong><em><?php echo $this->_tpl_vars['product']['productinfo']['groupname']; ?>
</em> - <?php echo $this->_tpl_vars['product']['productinfo']['name']; ?>
</strong><?php if ($this->_tpl_vars['product']['domain']): ?> (<?php echo $this->_tpl_vars['product']['domain']; ?>
)<?php endif; ?><br />
<?php if ($this->_tpl_vars['product']['configoptions']): ?>
<?php $_from = $this->_tpl_vars['product']['configoptions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['confnum'] => $this->_tpl_vars['configoption']):
?>&nbsp;&raquo; <?php echo $this->_tpl_vars['configoption']['name']; ?>
: <?php if ($this->_tpl_vars['configoption']['type'] == 1 || $this->_tpl_vars['configoption']['type'] == 2): ?><?php echo $this->_tpl_vars['configoption']['option']; ?>
<?php elseif ($this->_tpl_vars['configoption']['type'] == 3): ?><?php if ($this->_tpl_vars['configoption']['qty']): ?><?php echo $this->_tpl_vars['LANG']['yes']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['no']; ?>
<?php endif; ?><?php elseif ($this->_tpl_vars['configoption']['type'] == 4): ?><?php echo $this->_tpl_vars['configoption']['qty']; ?>
 x <?php echo $this->_tpl_vars['configoption']['option']; ?>
<?php endif; ?><br /><?php endforeach; endif; unset($_from); ?>
<?php endif; ?>
<a href="<?php echo $_SERVER['PHP_SELF']; ?>
?a=confproduct&i=<?php echo $this->_tpl_vars['num']; ?>
" class="cartedit">[<?php echo $this->_tpl_vars['LANG']['carteditproductconfig']; ?>
]</a> <a href="#" onclick="removeItem('p','<?php echo $this->_tpl_vars['num']; ?>
');return false" class="cartremove">[<?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
]</a>
<?php if ($this->_tpl_vars['product']['allowqty']): ?>
<div align="right"><?php echo $this->_tpl_vars['LANG']['quantity']; ?>
: <input type="text" name="qty[<?php echo $this->_tpl_vars['num']; ?>
]" size="3" value="<?php echo $this->_tpl_vars['product']['qty']; ?>
" /> <input type="submit" value="<?php echo $this->_tpl_vars['LANG']['cartqtyupdate']; ?>
" /></div>
<?php endif; ?>
</td><td class="textcenter"><strong><?php echo $this->_tpl_vars['product']['pricingtext']; ?>
<?php if ($this->_tpl_vars['product']['proratadate']): ?><br />(<?php echo $this->_tpl_vars['LANG']['orderprorata']; ?>
 <?php echo $this->_tpl_vars['product']['proratadate']; ?>
)<?php endif; ?></strong></td></tr>
<?php $_from = $this->_tpl_vars['product']['addons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['addonnum'] => $this->_tpl_vars['addon']):
?>
<tr class="carttableproduct"><td><strong><?php echo $this->_tpl_vars['LANG']['orderaddon']; ?>
</strong> - <?php echo $this->_tpl_vars['addon']['name']; ?>
</td><td align="center"><strong><?php echo $this->_tpl_vars['addon']['pricingtext']; ?>
</strong></td></tr>
<?php endforeach; endif; unset($_from); ?>
<?php endforeach; endif; unset($_from); ?>

<?php $_from = $this->_tpl_vars['addons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['addon']):
?>
<tr class="carttableproduct"><td>
<strong><?php echo $this->_tpl_vars['addon']['name']; ?>
</strong><br />
<?php echo $this->_tpl_vars['addon']['productname']; ?>
<?php if ($this->_tpl_vars['addon']['domainname']): ?> - <?php echo $this->_tpl_vars['addon']['domainname']; ?>
<?php endif; ?><br />
<a href="#" onclick="removeItem('a','<?php echo $this->_tpl_vars['num']; ?>
');return false" class="cartremove">[<?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
]</a>
</td><td class="textcenter"><strong><?php echo $this->_tpl_vars['addon']['pricingtext']; ?>
</strong></td></tr>
<?php endforeach; endif; unset($_from); ?>

<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['domain']):
?>
<tr class="carttableproduct"><td>
<strong><?php if ($this->_tpl_vars['domain']['type'] == 'register'): ?><?php echo $this->_tpl_vars['LANG']['orderdomainregistration']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['orderdomaintransfer']; ?>
<?php endif; ?></strong> - <?php echo $this->_tpl_vars['domain']['domain']; ?>
 - <?php echo $this->_tpl_vars['domain']['regperiod']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>
<br />
<?php if ($this->_tpl_vars['domain']['dnsmanagement']): ?>&nbsp;&raquo; <?php echo $this->_tpl_vars['LANG']['domaindnsmanagement']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['domain']['emailforwarding']): ?>&nbsp;&raquo; <?php echo $this->_tpl_vars['LANG']['domainemailforwarding']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['domain']['idprotection']): ?>&nbsp;&raquo; <?php echo $this->_tpl_vars['LANG']['domainidprotection']; ?>
<br /><?php endif; ?>
<a href="<?php echo $_SERVER['PHP_SELF']; ?>
?a=confdomains" class="cartedit">[<?php echo $this->_tpl_vars['LANG']['cartconfigdomainextras']; ?>
]</a> <a href="#" onclick="removeItem('d','<?php echo $this->_tpl_vars['num']; ?>
');return false" class="cartremove">[<?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
]</a>
</td><td class="textcenter"><strong><?php echo $this->_tpl_vars['domain']['price']; ?>
</strong></td></tr>
<?php endforeach; endif; unset($_from); ?>

<?php $_from = $this->_tpl_vars['renewals']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['domain']):
?>
<tr class="carttableproduct"><td>
<strong><?php echo $this->_tpl_vars['LANG']['domainrenewal']; ?>
</strong> - <?php echo $this->_tpl_vars['domain']['domain']; ?>
 - <?php echo $this->_tpl_vars['domain']['regperiod']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>
<br />
<?php if ($this->_tpl_vars['domain']['dnsmanagement']): ?>&nbsp;&raquo; <?php echo $this->_tpl_vars['LANG']['domaindnsmanagement']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['domain']['emailforwarding']): ?>&nbsp;&raquo; <?php echo $this->_tpl_vars['LANG']['domainemailforwarding']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['domain']['idprotection']): ?>&nbsp;&raquo; <?php echo $this->_tpl_vars['LANG']['domainidprotection']; ?>
<br /><?php endif; ?>
<a href="#" onclick="removeItem('r','<?php echo $this->_tpl_vars['num']; ?>
');return false" class="cartremove">[<?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
]</a>
</td><td class="textcenter"><strong><?php echo $this->_tpl_vars['domain']['price']; ?>
</strong></td></tr>
<?php endforeach; endif; unset($_from); ?>

<?php if ($this->_tpl_vars['cartitems'] == 0): ?>
<tr class="clientareatableactive"><td colspan="2" class="textcenter">
<br />
<?php echo $this->_tpl_vars['LANG']['cartempty']; ?>

<br /><br />
</td></tr>
<?php endif; ?>

<tr class="summary"><td class="textright"><?php echo $this->_tpl_vars['LANG']['ordersubtotal']; ?>
: &nbsp;</td><td class="textcenter"><?php echo $this->_tpl_vars['subtotal']; ?>
</td></tr>
<?php if ($this->_tpl_vars['promotioncode']): ?>
<tr class="summary"><td class="textright"><?php echo $this->_tpl_vars['promotiondescription']; ?>
: &nbsp;</td><td class="textcenter"><?php echo $this->_tpl_vars['discount']; ?>
</td></tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['taxrate']): ?>
<tr class="summary"><td class="textright"><?php echo $this->_tpl_vars['taxname']; ?>
 @ <?php echo $this->_tpl_vars['taxrate']; ?>
%: &nbsp;</td><td class="textcenter"><?php echo $this->_tpl_vars['taxtotal']; ?>
</td></tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['taxrate2']): ?>
<tr class="summary"><td class="textright"><?php echo $this->_tpl_vars['taxname2']; ?>
 @ <?php echo $this->_tpl_vars['taxrate2']; ?>
%: &nbsp;</td><td class="textcenter"><?php echo $this->_tpl_vars['taxtotal2']; ?>
</td></tr>
<?php endif; ?>
</table>

</form>

<div class="checkoutbuttonsleft">
<input type="button" value="<?php echo $this->_tpl_vars['LANG']['emptycart']; ?>
" onclick="emptyCart();return false" class="cartbutton red" /> <input type="button" value="<?php echo $this->_tpl_vars['LANG']['continueshopping']; ?>
" onclick="window.location='cart.php'" class="cartbutton" />
</div>
<div class="checkoutbuttonsright">
<div class="totalduetoday"><?php echo $this->_tpl_vars['LANG']['ordertotalduetoday']; ?>
: <?php echo $this->_tpl_vars['total']; ?>
</div>
<?php if ($this->_tpl_vars['totalrecurringmonthly'] || $this->_tpl_vars['totalrecurringquarterly'] || $this->_tpl_vars['totalrecurringsemiannually'] || $this->_tpl_vars['totalrecurringannually'] || $this->_tpl_vars['totalrecurringbiennially'] || $this->_tpl_vars['totalrecurringtriennially']): ?>
<div class="totalrecurring"><?php echo $this->_tpl_vars['LANG']['ordertotalrecurring']; ?>
: <?php if ($this->_tpl_vars['totalrecurringmonthly']): ?><?php echo $this->_tpl_vars['totalrecurringmonthly']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermmonthly']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['totalrecurringquarterly']): ?><?php echo $this->_tpl_vars['totalrecurringquarterly']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermquarterly']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['totalrecurringsemiannually']): ?><?php echo $this->_tpl_vars['totalrecurringsemiannually']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermsemiannually']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['totalrecurringannually']): ?><?php echo $this->_tpl_vars['totalrecurringannually']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermannually']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['totalrecurringbiennially']): ?><?php echo $this->_tpl_vars['totalrecurringbiennially']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermbiennially']; ?>
<br /><?php endif; ?>
<?php if ($this->_tpl_vars['totalrecurringtriennially']): ?><?php echo $this->_tpl_vars['totalrecurringtriennially']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermtriennially']; ?>
<br /><?php endif; ?></div>
<?php endif; ?>
<div class="promo"><?php if ($this->_tpl_vars['promotioncode']): ?><?php echo $this->_tpl_vars['LANG']['orderpromotioncode']; ?>
: <?php echo $this->_tpl_vars['promotioncode']; ?>
 (<a href="<?php echo $_SERVER['PHP_SELF']; ?>
?a=removepromo">Remove Promo Code</a>)<?php else: ?><input type="text" id="promocode" size="20" value="<?php echo $this->_tpl_vars['LANG']['orderpromotioncode']; ?>
" onfocus="if(this.value=='<?php echo $this->_tpl_vars['LANG']['orderpromotioncode']; ?>
')this.value=''" /> <input type="submit" value="<?php echo $this->_tpl_vars['LANG']['go']; ?>
" onclick="applypromo()" class="cartbutton" /><?php endif; ?></div>
</div>

<div class="clear"></div>

<?php $_from = $this->_tpl_vars['gatewaysoutput']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['gatewayoutput']):
?>
<div class="gatewaycheckout"><?php echo $this->_tpl_vars['gatewayoutput']; ?>
</div>
<?php endforeach; endif; unset($_from); ?>

<div class="viewcartcol1">

<h2><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['yourdetails'])) ? $this->_run_mod_handler('strtolower', true, $_tmp) : strtolower($_tmp)); ?>
</h2>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?a=checkout" id="orderfrm">
<input type="hidden" name="submit" value="true" />

<input type="hidden" name="custtype" id="custtype" value="<?php echo $this->_tpl_vars['custtype']; ?>
" />

<div id="loginfrm"<?php if ($this->_tpl_vars['custtype'] == 'existing' && ! $this->_tpl_vars['loggedin']): ?><?php else: ?> style="display:none;"<?php endif; ?>>
<p><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['newcustomersignup'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, '<a href="#" onclick="showloginform();return false;">', '</a>') : smarty_modifier_sprintf2($_tmp, '<a href="#" onclick="showloginform();return false;">', '</a>')); ?>
</a></p>
<table width="100%" cellpadding="2">
<tr><td width="100"><?php echo $this->_tpl_vars['LANG']['loginemail']; ?>
:</td><td><input type="text" name="loginemail" style="width:80%;" value="<?php echo $this->_tpl_vars['username']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['loginpassword']; ?>
:</td><td><input type="password" name="loginpw" style="width:50%;" /></td></tr>
</table>
</div>

<div id="signupfrm"<?php if ($this->_tpl_vars['custtype'] == 'existing' && ! $this->_tpl_vars['loggedin']): ?> style="display:none;"<?php endif; ?>>

<?php if (! $this->_tpl_vars['loggedin']): ?><p><?php echo $this->_tpl_vars['LANG']['alreadyregistered']; ?>
 <a href="<?php echo $_SERVER['PHP_SELF']; ?>
?a=login" onclick="showloginform();return false;"><?php echo $this->_tpl_vars['LANG']['clickheretologin']; ?>
...</a></p><?php endif; ?>

<table width="100%" cellpadding="2">
<tr><td width="100"><?php echo $this->_tpl_vars['LANG']['clientareafirstname']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['firstname']; ?>
<?php else: ?><input type="text" name="firstname" style="width:80%;" value="<?php echo $this->_tpl_vars['clientsdetails']['firstname']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientarealastname']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['lastname']; ?>
<?php else: ?><input type="text" name="lastname" style="width:80%;" value="<?php echo $this->_tpl_vars['clientsdetails']['lastname']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareacompanyname']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['companyname']; ?>
<?php else: ?><input type="text" name="companyname" style="width:80%;" value="<?php echo $this->_tpl_vars['clientsdetails']['companyname']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareaemail']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['email']; ?>
<?php else: ?><input type="text" name="email" style="width:90%;" value="<?php echo $this->_tpl_vars['clientsdetails']['email']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareaaddress1']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['address1']; ?>
<?php else: ?><input type="text" name="address1" style="width:80%;" value="<?php echo $this->_tpl_vars['clientsdetails']['address1']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareaaddress2']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['address2']; ?>
<?php else: ?><input type="text" name="address2" style="width:80%;" value="<?php echo $this->_tpl_vars['clientsdetails']['address2']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareacity']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['city']; ?>
<?php else: ?><input type="text" name="city" style="width:80%;" value="<?php echo $this->_tpl_vars['clientsdetails']['city']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareastate']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['state']; ?>
<?php else: ?><input type="text" name="state" style="width:80%;" value="<?php echo $this->_tpl_vars['clientsdetails']['state']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareapostcode']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['postcode']; ?>
<?php else: ?><input type="text" name="postcode" size="15" value="<?php echo $this->_tpl_vars['clientsdetails']['postcode']; ?>
" /><?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareacountry']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['country']; ?>
<?php else: ?><?php echo $this->_tpl_vars['clientcountrydropdown']; ?>
<?php endif; ?></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareaphonenumber']; ?>
</td><td><?php if ($this->_tpl_vars['loggedin']): ?><?php echo $this->_tpl_vars['clientsdetails']['phonenumber']; ?>
<?php else: ?><input type="text" name="phonenumber" size="20" value="<?php echo $this->_tpl_vars['clientsdetails']['phonenumber']; ?>
" /><?php endif; ?></td></tr>
<?php if ($this->_tpl_vars['securityquestions'] && ! $this->_tpl_vars['loggedin']): ?>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareasecurityquestion']; ?>
</td><td><select name="securityqid">
<?php $_from = $this->_tpl_vars['securityquestions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['question']):
?>
    <option value="<?php echo $this->_tpl_vars['question']['id']; ?>
"<?php if ($this->_tpl_vars['question']['id'] == $this->_tpl_vars['securityqid']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['question']['question']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareasecurityanswer']; ?>
</td><td><input type="password" name="securityqans" value="<?php echo $this->_tpl_vars['securityqans']; ?>
" size="30"></td></tr>
<?php endif; ?>
<?php $_from = $this->_tpl_vars['customfields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['customfield']):
?>
<tr><td><?php echo $this->_tpl_vars['customfield']['name']; ?>
</td><td><?php echo $this->_tpl_vars['customfield']['input']; ?>
 <?php echo $this->_tpl_vars['customfield']['description']; ?>
</td></tr>
<?php endforeach; endif; unset($_from); ?>
<?php if (! $this->_tpl_vars['loggedin']): ?>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareapassword']; ?>
</td><td><input type="password" name="password" id="newpw" size="20" value="<?php echo $this->_tpl_vars['password']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareaconfirmpassword']; ?>
</td><td><input type="password" name="password2" size="20" value="<?php echo $this->_tpl_vars['password2']; ?>
" /></td></tr>
<tr><td colspan="2" align="center"><script language="javascript">showStrengthBar();</script></td></tr>
<?php endif; ?>
</table>

<?php if ($this->_tpl_vars['taxenabled'] && ! $this->_tpl_vars['loggedin']): ?>
<p align="center"><?php echo $this->_tpl_vars['LANG']['carttaxupdateselections']; ?>
 <input type="submit" value="<?php echo $this->_tpl_vars['LANG']['carttaxupdateselectionsupdate']; ?>
" name="updateonly" class="cartbutton" /></p>
<?php endif; ?>

</div>

<?php if ($this->_tpl_vars['domainsinorder']): ?>
<br />
<h2><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['domainregistrantinfo'])) ? $this->_run_mod_handler('strtolower', true, $_tmp) : strtolower($_tmp)); ?>
</h2>
<p><select name="contact" id="domaincontact" onchange="domaincontactchange()">
<option value=""><?php echo $this->_tpl_vars['LANG']['usedefaultcontact']; ?>
</option>
<?php $_from = $this->_tpl_vars['domaincontacts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domcontact']):
?>
<option value="<?php echo $this->_tpl_vars['domcontact']['id']; ?>
"<?php if ($this->_tpl_vars['contact'] == $this->_tpl_vars['domcontact']['id']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['domcontact']['name']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
<option value="addingnew"<?php if ($this->_tpl_vars['contact'] == 'addingnew'): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['clientareanavaddcontact']; ?>
...</option>
</select><br /></p>
<div id="domaincontactfields"<?php if ($this->_tpl_vars['contact'] == 'addingnew'): ?> style="display:block"<?php endif; ?>>
<table width="100%" cellpadding="2">
<tr><td width="100"><?php echo $this->_tpl_vars['LANG']['clientareafirstname']; ?>
</td><td><input type="text" name="domaincontactfirstname" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['firstname']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientarealastname']; ?>
</td><td><input type="text" name="domaincontactlastname" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['lastname']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareacompanyname']; ?>
</td><td><input type="text" name="domaincontactcompanyname" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['companyname']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareaemail']; ?>
</td><td><input type="text" name="domaincontactemail" style="width:90%;" value="<?php echo $this->_tpl_vars['domaincontact']['email']; ?>
" /></td></tr>
<tr><td width="100"><?php echo $this->_tpl_vars['LANG']['clientareaaddress1']; ?>
</td><td><input type="text" name="domaincontactaddress1" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['address1']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareaaddress2']; ?>
</td><td><input type="text" name="domaincontactaddress2" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['address2']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareacity']; ?>
</td><td><input type="text" name="domaincontactcity" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['city']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareastate']; ?>
</td><td><input type="text" name="domaincontactstate" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['state']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareapostcode']; ?>
</td><td><input type="text" name="domaincontactpostcode" size="15" value="<?php echo $this->_tpl_vars['domaincontact']['postcode']; ?>
" /></td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareacountry']; ?>
</td><td><?php echo $this->_tpl_vars['domaincontactcountrydropdown']; ?>
</td></tr>
<tr><td><?php echo $this->_tpl_vars['LANG']['clientareaphonenumber']; ?>
</td><td><input type="text" name="domaincontactphonenumber" size="20" value="<?php echo $this->_tpl_vars['domaincontact']['phonenumber']; ?>
" /></td></tr>
</table>
</div>
<?php endif; ?>

</div>

<div class="viewcartcol2">

<h2><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['orderpaymentmethod'])) ? $this->_run_mod_handler('strtolower', true, $_tmp) : strtolower($_tmp)); ?>
</h2>
<p class="paymentmethods"><?php $_from = $this->_tpl_vars['gateways']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['gateway']):
?><label><input type="radio" name="paymentmethod" value="<?php echo $this->_tpl_vars['gateway']['sysname']; ?>
" onclick="<?php if ($this->_tpl_vars['gateway']['type'] == 'CC'): ?>showCCForm()<?php else: ?>hideCCForm()<?php endif; ?>"<?php if ($this->_tpl_vars['selectedgateway'] == $this->_tpl_vars['gateway']['sysname']): ?> checked<?php endif; ?> /><?php echo $this->_tpl_vars['gateway']['name']; ?>
</label><br /><?php endforeach; endif; unset($_from); ?></p>

<div id="ccinputform"<?php if ($this->_tpl_vars['selectedgatewaytype'] != 'CC'): ?> style="display:none;"<?php endif; ?>>
<table width="100%" cellpadding="2">
<?php if ($this->_tpl_vars['clientsdetails']['cclastfour']): ?><tr><td colspan="2"><label><input type="radio" name="ccinfo" value="useexisting" id="useexisting" onclick="useExistingCC()"<?php if ($this->_tpl_vars['clientsdetails']['cclastfour']): ?> checked<?php else: ?> disabled<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['creditcarduseexisting']; ?>
<?php if ($this->_tpl_vars['clientsdetails']['cclastfour']): ?> (<?php echo $this->_tpl_vars['clientsdetails']['cclastfour']; ?>
)<?php endif; ?></label><br />
<label><input type="radio" name="ccinfo" value="new" id="new" onclick="enterNewCC()"<?php if (! $this->_tpl_vars['clientsdetails']['cclastfour'] || $this->_tpl_vars['ccinfo'] == 'new'): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['creditcardenternewcard']; ?>
</label></td></tr><?php else: ?><input type="hidden" name="ccinfo" value="new" /><?php endif; ?>
<tr class="newccinfo"<?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>><td width="100"><?php echo $this->_tpl_vars['LANG']['creditcardcardtype']; ?>
</td><td><select name="cctype" id="cctype">
<?php $_from = $this->_tpl_vars['acceptedcctypes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['cardtype']):
?>
<option<?php if ($this->_tpl_vars['cctype'] == $this->_tpl_vars['cardtype']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['cardtype']; ?>
</option>
<?php endforeach; endif; unset($_from); ?>
</select></td></tr>
<tr class="newccinfo"<?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>><td><?php echo $this->_tpl_vars['LANG']['creditcardcardnumber']; ?>
</td><td><input type="text" name="ccnumber" value="<?php echo $this->_tpl_vars['ccnumber']; ?>
" style="width:80%;" autocomplete="off" /></td></tr>
<tr class="newccinfo"<?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>><td><?php echo $this->_tpl_vars['LANG']['creditcardcardexpires']; ?>
</td><td><select name="ccexpirymonth" id="ccexpirymonth" class="newccinfo"><?php $_from = $this->_tpl_vars['months']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['month']):
?>
<option<?php if ($this->_tpl_vars['ccexpirymonth'] == $this->_tpl_vars['month']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['month']; ?>
</option>
<?php endforeach; endif; unset($_from); ?></select> / <select name="ccexpiryyear" class="newccinfo">
<?php $_from = $this->_tpl_vars['expiryyears']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year']):
?>
<option<?php if ($this->_tpl_vars['ccexpiryyear'] == $this->_tpl_vars['year']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['year']; ?>
</option>
<?php endforeach; endif; unset($_from); ?></select></td></tr>
<?php if ($this->_tpl_vars['showccissuestart']): ?>
<tr class="newccinfo"<?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>><td><?php echo $this->_tpl_vars['LANG']['creditcardcardstart']; ?>
</td><td><select name="ccstartmonth" id="ccstartmonth" class="newccinfo"><?php $_from = $this->_tpl_vars['months']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['month']):
?>
<option<?php if ($this->_tpl_vars['ccstartmonth'] == $this->_tpl_vars['month']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['month']; ?>
</option>
<?php endforeach; endif; unset($_from); ?></select> / <select name="ccstartyear" class="newccinfo">
<?php $_from = $this->_tpl_vars['startyears']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year']):
?>
<option<?php if ($this->_tpl_vars['ccstartyear'] == $this->_tpl_vars['year']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['year']; ?>
</option>
<?php endforeach; endif; unset($_from); ?></select></td></tr>
<tr class="newccinfo"<?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>><td><?php echo $this->_tpl_vars['LANG']['creditcardcardissuenum']; ?>
</td><td><input type="text" name="ccissuenum" value="<?php echo $this->_tpl_vars['ccissuenum']; ?>
" size="5" maxlength="3" /></td></tr>
<?php endif; ?>
<tr><td width="100"><?php echo $this->_tpl_vars['LANG']['creditcardcvvnumber']; ?>
</td><td><input type="text" name="cccvv" id="cccvv" value="<?php echo $this->_tpl_vars['cccvv']; ?>
" size="5" autocomplete="off" /></td></tr>
<tr><td></td><td><a href="#" onclick="window.open('images/ccv.gif','','width=280,height=200,scrollbars=no,top=100,left=100');return false"><?php echo $this->_tpl_vars['LANG']['creditcardcvvwhere']; ?>
</a></td></tr>
<?php if ($this->_tpl_vars['shownostore']): ?><tr><td><input type="checkbox" name="nostore" id="nostore" /></td><td><label for="nostore"><?php echo $this->_tpl_vars['LANG']['creditcardnostore']; ?>
</label></td></tr><?php endif; ?>
</table>
</div>

<br />

<?php if ($this->_tpl_vars['shownotesfield']): ?>
<h2><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['ordernotes'])) ? $this->_run_mod_handler('strtolower', true, $_tmp) : strtolower($_tmp)); ?>
</h2>
<p align="center"><textarea name="notes" rows="3" style="width:95%" onFocus="if(this.value=='<?php echo $this->_tpl_vars['LANG']['ordernotesdescription']; ?>
'){this.value='';}" onBlur="if (this.value==''){this.value='<?php echo $this->_tpl_vars['LANG']['ordernotesdescription']; ?>
';}"><?php echo $this->_tpl_vars['notes']; ?>
</textarea></p>
<?php endif; ?>

<br />

<?php if ($this->_tpl_vars['accepttos']): ?>
<p align="center"><label><input type="checkbox" name="accepttos" id="accepttos" /> <?php echo $this->_tpl_vars['LANG']['ordertosagreement']; ?>
 <a href="<?php echo $this->_tpl_vars['tosurl']; ?>
" target="_blank"><?php echo $this->_tpl_vars['LANG']['ordertos']; ?>
</a></label><p>
<?php endif; ?>

<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['completeorder']; ?>
 &raquo;"<?php if ($this->_tpl_vars['cartitems'] == 0): ?> disabled<?php endif; ?> onclick="this.value='<?php echo $this->_tpl_vars['LANG']['pleasewait']; ?>
'" class="cartbutton green" /></p>

</div>

<div class="clear"></div>

<div class="checkoutsecure"><img align="left" src="images/padlock.gif" border="0" vspace="5" alt="Secure Transaction" style="padding-right: 10px;" /> <?php echo $this->_tpl_vars['LANG']['ordersecure']; ?>
 (<strong><?php echo $this->_tpl_vars['ipaddress']; ?>
</strong>) <?php echo $this->_tpl_vars['LANG']['ordersecure2']; ?>
</div>

</div>

</form>

</div>