<?php /* Smarty version 2.6.28, created on 2015-04-15 17:07:19
         compiled from bamat/header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'dc_is_current_page', 'bamat/header.tpl', 40, false),)), $this); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=<?php echo $this->_tpl_vars['charset']; ?>
" />
    <title><?php if ($this->_tpl_vars['kbarticle']['title']): ?><?php echo $this->_tpl_vars['kbarticle']['title']; ?>
 - <?php endif; ?><?php echo $this->_tpl_vars['pagetitle']; ?>
 - <?php echo $this->_tpl_vars['companyname']; ?>
</title>

    <?php if ($this->_tpl_vars['systemurl']): ?><base href="<?php echo $this->_tpl_vars['systemurl']; ?>
" /><?php endif; ?>
	<script type="text/javascript" src="includes/jscript/jquery.js"></script>
    <?php if ($this->_tpl_vars['livehelpjs']): ?><?php echo $this->_tpl_vars['livehelpjs']; ?>
<?php endif; ?>
    <link href="templates/<?php echo $this->_tpl_vars['template']; ?>
/css/bootstrap.css" rel="stylesheet">
    <link href="templates/<?php echo $this->_tpl_vars['template']; ?>
/css/whmcs.css" rel="stylesheet">
    <link href="templates/<?php echo $this->_tpl_vars['template']; ?>
/css/bamat.css" rel="stylesheet">

	<style type='text/css'>		
		@font-face <?php echo ' { '; ?>
 font-family: 'entypo-fontello'; font-weight: normal; font-style: normal;
			src: url('//<?php echo $_SERVER['SERVER_NAME']; ?>
/templates/<?php echo $this->_tpl_vars['template']; ?>
/fonts/entypo-fontello.eot?v=3');
			src: url('//<?php echo $_SERVER['SERVER_NAME']; ?>
/templates/<?php echo $this->_tpl_vars['template']; ?>
/fonts/entypo-fontello.eot?v=3#iefix') format('embedded-opentype'), 
				url('//<?php echo $_SERVER['SERVER_NAME']; ?>
/templates/<?php echo $this->_tpl_vars['template']; ?>
/fonts/entypo-fontello.woff?v=3') format('woff'), 
				url('//<?php echo $_SERVER['SERVER_NAME']; ?>
/templates/<?php echo $this->_tpl_vars['template']; ?>
/fonts/entypo-fontello.ttf?v=3') format('truetype'), 
				url('//<?php echo $_SERVER['SERVER_NAME']; ?>
/templates/<?php echo $this->_tpl_vars['template']; ?>
/fonts/entypo-fontello.svg?v=3#entypo-fontello') format('svg');
			<?php echo ' } '; ?>


	</style>

	<link rel="icon" href="templates/<?php echo $this->_tpl_vars['template']; ?>
/img/bamat-icon.ico" type="image/x-icon">
	
    <script src="templates/<?php echo $this->_tpl_vars['template']; ?>
/js/whmcs.js"></script>
    <script src="templates/<?php echo $this->_tpl_vars['template']; ?>
/js/bamat.js"></script>

    <?php echo $this->_tpl_vars['headoutput']; ?>


  </head>

  <body>

<?php echo $this->_tpl_vars['headeroutput']; ?>


<header>

<?php echo smarty_function_dc_is_current_page(array('page' => 'home'), $this);?>

	<div class="container-fluid social-wrap">
		<div class="container">
			<div class="row top-menu-wrap">
				<ul class="social-bookmarks">
					<li class="social-link social-link-facebook">
						<a target="_blank" href="https://www.facebook.com/pages/General-Internet/483679735067038" title="Facebook"></a>
					</li>
					<li class="social-link social-link-twitter">
						<a target="_blank" href="https://twitter.com/Best_SeoHosting" title="Twitter"></a>
					</li>
					<li class="social-link social-link-google">
						<a target="_blank" href="https://plus.google.com/109933878477951881560/about" title="Google"></a>
					</li>
				</ul>
				
				<ul class="top-menu">
					<li><a href="http://best-seo-hosting.com/home/impressum/">Impressum</a></li>
					<li><a href="http://best-seo-hosting.com/home/disclaimer/">Disclaimer</a></li>
					<li class="last"><a href="http://best-seo-hosting.com/home/kontakt/">Kontakt</a></li>
				</ul>

				<div class="phone"><span>Tel.: +41 41 630-02.30</span></div>
			</div>
		</div>
	</div>
		
	<div class="container-fluid main-menu-wrap">
		<div class="container">
			<div class="row main-menu-row">
				<div class="logo pull-left">
					<a href="index.php">
						<img src="templates/<?php echo $this->_tpl_vars['template']; ?>
/img/bamat-logo.jpg" alt="Best Seo Hosting - der Turbo für SEO-Spezialisten" height="78" width="243">
					</a>
				</div>

				<a id="menu-toggle" class="toggle" href="#"></a>
				
				<div class="main-menu-box pull-right">
					<div class="menu-toggle-close-wrap">
						<a id="menu-toggle-close" class="toggle" href="#"></a>
					</div>
					
					<ul class="main-menu">
						<?php if ($this->_tpl_vars['loggedin']): ?>
							<?php $this->assign('home_page', 'clientarea.php'); ?>
						<?php else: ?>
							<?php $this->assign('home_page', 'index.php'); ?>
						<?php endif; ?>
						<li <?php echo smarty_function_dc_is_current_page(array('page' => $this->_tpl_vars['home_page']), $this);?>
><a id="Menu-Home" href="<?php echo $this->_tpl_vars['home_page']; ?>
"><?php echo $this->_tpl_vars['LANG']['hometitle']; ?>
</a></li>
						
						<?php if ($this->_tpl_vars['loggedin']): ?>
							<li <?php echo smarty_function_dc_is_current_page(array('page' => 'clientarea.php?action=products|index.php?m=project_management|cart.php|cart.php?gid=addons'), $this);?>
>
								<a id="Menu-Services" href="#"><?php echo $this->_tpl_vars['LANG']['navservices']; ?>
</a>
								<ul class="sub-menu">
									<li><a id="Menu-Services-My_Services" href="clientarea.php?action=products"><?php echo $this->_tpl_vars['LANG']['clientareanavservices']; ?>
</a></li>
									<?php if ($this->_tpl_vars['condlinks']['pmaddon']): ?><li><a id="Menu-Services-My_Projects" href="index.php?m=project_management"><?php echo $this->_tpl_vars['LANG']['clientareaprojects']; ?>
</a></li><?php endif; ?>
									<li class="divider"></li>
									<li><a id="Menu-Services-Order_New_Services" href="cart.php"><?php echo $this->_tpl_vars['LANG']['navservicesorder']; ?>
</a></li>
									<li><a id="Menu-Services-View_Available_Addons" href="cart.php?gid=addons"><?php echo $this->_tpl_vars['LANG']['clientareaviewaddons']; ?>
</a></li>
								</ul>
							</li>

							<?php if ($this->_tpl_vars['condlinks']['domainreg'] || $this->_tpl_vars['condlinks']['domaintrans']): ?>
							<li <?php echo smarty_function_dc_is_current_page(array('page' => "clientarea.php?action"), $this);?>
|domain_check.php'}>
								<a id="Menu-Domains" href="#"><?php echo $this->_tpl_vars['LANG']['navdomains']; ?>
</a>
								<ul class="sub-menu">
									<li><a id="Menu-Domains-My_Domains" href="clientarea.php?action=domains"><?php echo $this->_tpl_vars['LANG']['clientareanavdomains']; ?>
</a></li>
									<li class="divider"></li>
									<li><a id="Menu-Domains-Renew_Domains" href="cart.php?gid=renewals"><?php echo $this->_tpl_vars['LANG']['navrenewdomains']; ?>
</a></li>
									<?php if ($this->_tpl_vars['condlinks']['domainreg']): ?><li><a id="Menu-Domains-Register_a_New_Domain" href="cart.php?a=add&domain=register"><?php echo $this->_tpl_vars['LANG']['navregisterdomain']; ?>
</a></li><?php endif; ?>
									<?php if ($this->_tpl_vars['condlinks']['domaintrans']): ?><li><a id="Menu-Domains-Transfer_Domains_to_Us" href="cart.php?a=add&domain=transfer"><?php echo $this->_tpl_vars['LANG']['navtransferdomain']; ?>
</a></li><?php endif; ?>
									<?php if ($this->_tpl_vars['enomnewtldsenabled']): ?><li><a id="Menu-Domains-Preregister_New_TLDs" href="<?php echo $this->_tpl_vars['enomnewtldslink']; ?>
">Preregister New TLDs</a></li><?php endif; ?>
									<li class="divider"></li>
									<li><a id="Menu-Domains-Whois_Lookup" href="domain_check.php"><?php echo $this->_tpl_vars['LANG']['navwhoislookup']; ?>
</a></li>
								</ul>
							</li>
							<?php endif; ?>
							
							<li <?php echo smarty_function_dc_is_current_page(array('page' => 'clientarea.php?action=invoices|clientarea.php?action=quotes|clientarea.php?action=addfunds|clientarea.php?action=masspay&all=true|clientarea.php?action=creditcard'), $this);?>
>
								<a id="Menu-Billing" href="#"><?php echo $this->_tpl_vars['LANG']['navbilling']; ?>
s</a>
								<ul class="sub-menu">
									<li><a id="Menu-Billing-My_Invoices" href="clientarea.php?action=invoices"><?php echo $this->_tpl_vars['LANG']['invoices']; ?>
</a></li>
									<li><a id="Menu-Billing-My_Quotes" href="clientarea.php?action=quotes"><?php echo $this->_tpl_vars['LANG']['quotestitle']; ?>
</a></li>
									<li class="divider"></li>
									<?php if ($this->_tpl_vars['condlinks']['addfunds']): ?><li><a id="Menu-Billing-Add_Funds" href="clientarea.php?action=addfunds"><?php echo $this->_tpl_vars['LANG']['addfunds']; ?>
</a></li><?php endif; ?>
									<?php if ($this->_tpl_vars['condlinks']['masspay']): ?><li><a id="Menu-Billing-Mass_Payment" href="clientarea.php?action=masspay&all=true"><?php echo $this->_tpl_vars['LANG']['masspaytitle']; ?>
</a></li><?php endif; ?>
									<?php if ($this->_tpl_vars['condlinks']['updatecc']): ?><li><a id="Menu-Billing-Manage_Credit_Card" href="clientarea.php?action=creditcard"><?php echo $this->_tpl_vars['LANG']['navmanagecc']; ?>
</a></li><?php endif; ?>
								</ul>
							</li>
							
							<li <?php echo smarty_function_dc_is_current_page(array('page' => 'supporttickets.php|knowledgebase.php|downloads.php|serverstatus.php'), $this);?>
>
								<a id="Menu-Support"href="#"><?php echo $this->_tpl_vars['LANG']['navsupport']; ?>
</a>
								<ul class="sub-menu">
									<li><a id="Menu-Support-Tickets" href="supporttickets.php"><?php echo $this->_tpl_vars['LANG']['navtickets']; ?>
</a></li>
									<li><a id="Menu-Support-Knowledgebase" href="knowledgebase.php"><?php echo $this->_tpl_vars['LANG']['knowledgebasetitle']; ?>
</a></li>
									<li><a id="Menu-Support-Downloads" href="downloads.php"><?php echo $this->_tpl_vars['LANG']['downloadstitle']; ?>
</a></li>
									<li><a id="Menu-Support-Network_Status" href="serverstatus.php"><?php echo $this->_tpl_vars['LANG']['networkstatustitle']; ?>
</a></li>
								</ul>
							</li>
							
							<li <?php echo smarty_function_dc_is_current_page(array('page' => 'submitticket.php'), $this);?>
><a id="Menu-Open_Ticket" href="submitticket.php"><?php echo $this->_tpl_vars['LANG']['navopenticket']; ?>
</a></li>
							
							<?php if ($this->_tpl_vars['condlinks']['affiliates']): ?><li <?php echo smarty_function_dc_is_current_page(array('page' => 'affiliates.php'), $this);?>
><a id="Menu-Affiliates" href="affiliates.php"><?php echo $this->_tpl_vars['LANG']['affiliatestitle']; ?>
</a></li><?php endif; ?>
							
							<?php if ($this->_tpl_vars['livehelp']): ?><li><a id="Menu-Live_Chat" href="#" class="LiveHelpButton">Live Chat - <span class="LiveHelpTextStatus">Offline</span></a></li><?php endif; ?>
							
							<li <?php echo smarty_function_dc_is_current_page(array('page' => 'clientarea.php?action=details|clientarea.php?action=creditcard|clientarea.php?action=contacts|clientarea.php?action=addfunds|clientarea.php?action=emails|clientarea.php?action=changepw|logout.php'), $this);?>
>
								<a href="#" id="Menu-Hello_User"><?php echo $this->_tpl_vars['LANG']['hello']; ?>
, <?php echo $this->_tpl_vars['loggedinuser']['firstname']; ?>
!</a>
								<ul class="sub-menu">
									<li><a id="Menu-Hello_User-Edit_Account_Details" href="clientarea.php?action=details"><?php echo $this->_tpl_vars['LANG']['editaccountdetails']; ?>
</a></li>
									<?php if ($this->_tpl_vars['condlinks']['updatecc']): ?><li><a id="Menu-Hello_User-Contacts_Sub-Accounts" href="clientarea.php?action=creditcard"><?php echo $this->_tpl_vars['LANG']['navmanagecc']; ?>
</a></li><?php endif; ?>
									<li><a href="clientarea.php?action=contacts"><?php echo $this->_tpl_vars['LANG']['clientareanavcontacts']; ?>
</a></li>
									<?php if ($this->_tpl_vars['condlinks']['addfunds']): ?><li><a id="Menu-Hello_User-Add_Funds" href="clientarea.php?action=addfunds"><?php echo $this->_tpl_vars['LANG']['addfunds']; ?>
</a></li><?php endif; ?>
									<li><a id="Menu-Hello_User-Email_History" href="clientarea.php?action=emails"><?php echo $this->_tpl_vars['LANG']['navemailssent']; ?>
</a></li>
									<li><a id="Menu-Hello_User-Change_Password" href="clientarea.php?action=changepw"><?php echo $this->_tpl_vars['LANG']['clientareanavchangepw']; ?>
</a></li>
									<li class="divider"></li>
									<li><a id="Menu-Hello_User-Logout" href="logout.php"><?php echo $this->_tpl_vars['LANG']['logouttitle']; ?>
</a></li>
								</ul>
							</li>

						<?php else: ?>	
							<li <?php echo smarty_function_dc_is_current_page(array('page' => "announcements.php"), $this);?>
><a id="Menu-Annoucements" href="announcements.php"><?php echo $this->_tpl_vars['LANG']['announcementstitle']; ?>
</a></li>
							<li <?php echo smarty_function_dc_is_current_page(array('page' => "knowledgebase.php"), $this);?>
><a id="Menu-Knowledgebase" href="knowledgebase.php"><?php echo $this->_tpl_vars['LANG']['knowledgebasetitle']; ?>
</a></li>
							<li <?php echo smarty_function_dc_is_current_page(array('page' => "serverstatus.php"), $this);?>
><a id="Menu-Network_Status" href="serverstatus.php"><?php echo $this->_tpl_vars['LANG']['networkstatustitle']; ?>
</a></li>
							<li <?php echo smarty_function_dc_is_current_page(array('page' => "affiliates.php"), $this);?>
><a id="Menu-Affiliates" href="affiliates.php"><?php echo $this->_tpl_vars['LANG']['affiliatestitle']; ?>
</a></li>
							<li <?php echo smarty_function_dc_is_current_page(array('page' => "contact.php"), $this);?>
><a id="Menu-Contact_Us" href="http://best-seo-hosting.com/home/kontakt/"><?php echo $this->_tpl_vars['LANG']['contactus']; ?>
</a></li>
							<?php if ($this->_tpl_vars['livehelp']): ?><li><a id="Menu-Live_Chat" href="#" class="LiveHelpButton">Live Chat - <span class="LiveHelpTextStatus">Offline</span></a></li><?php endif; ?>

							<li <?php echo smarty_function_dc_is_current_page(array('page' => 'clientarea.php|register.php|pwreset.php'), $this);?>
>
								<a id="Menu-Account" href="#"><?php echo $this->_tpl_vars['LANG']['account']; ?>
</a>
								<ul class="sub-menu">
									<li><a id="Menu-Account-Login" href="clientarea.php"><?php echo $this->_tpl_vars['LANG']['login']; ?>
</a></li>
									<li><a id="Menu-Account-Register" href="register.php"><?php echo $this->_tpl_vars['LANG']['register']; ?>
</a></li>
									<li class="divider"></li>
									<li><a id="Menu-Account-Forgot_Password" href="pwreset.php"><?php echo $this->_tpl_vars['LANG']['forgotpw']; ?>
</a></li>
								</ul>
							</li>

						<?php endif; ?>
						
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="whmcsheader">
    <div class="whmcscontainer">
        <div id="whmcstxtlogo"><a href="index.php"><?php echo $this->_tpl_vars['companyname']; ?>
</a></div>
    </div>
</div>

<div class="content">
	<div class="container-fluid">
		<div class="container">
			<div class="row">

<?php if ($this->_tpl_vars['pagetitle'] == $this->_tpl_vars['LANG']['carttitle']): ?><div id="whmcsorderfrm"><?php endif; ?>
