<?php /* Smarty version 2.6.28, created on 2015-04-15 16:37:31
         compiled from /home/generali/public_html/admin/templates/orderforms/bamatcart/domainrenewals.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'sprintf2', '/home/generali/public_html/admin/templates/orderforms/bamatcart/domainrenewals.tpl', 43, false),)), $this); ?>
<div id="order-comparison">

<h1><?php echo $this->_tpl_vars['LANG']['domainrenewals']; ?>
</h1>

<p><?php echo $this->_tpl_vars['LANG']['domainrenewdesc']; ?>
</p>

<form method="post" action="cart.php?a=add&renewals=true">

<div class="center80">

<table class="table table-striped table-framed">
	<thead>
		<tr>
			<th class="textcenter" style="width: 20px"></th>
			<th class="textcenter"><?php echo $this->_tpl_vars['LANG']['orderdomain']; ?>
</th>
			<th class="textcenter"><?php echo $this->_tpl_vars['LANG']['domainstatus']; ?>
</th>
			<th class="textcenter"><?php echo $this->_tpl_vars['LANG']['domaindaysuntilexpiry']; ?>
</th>
			<th class="textcenter"></th>
		</tr>
	</thead>

	<tbody>
		<?php $_from = $this->_tpl_vars['renewals']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['renewal']):
?>
		<tr class="carttablerow">
			<td><?php if (! $this->_tpl_vars['renewal']['pastgraceperiod']): ?><input type="checkbox" name="renewalids[]" value="<?php echo $this->_tpl_vars['renewal']['id']; ?>
" /><?php endif; ?></td>
			<td><?php echo $this->_tpl_vars['renewal']['domain']; ?>
</td>
			<td><?php echo $this->_tpl_vars['renewal']['status']; ?>
</td>
			<td>
				<?php if ($this->_tpl_vars['renewal']['daysuntilexpiry'] > 30): ?>
				<span class="textgreen"><?php echo $this->_tpl_vars['renewal']['daysuntilexpiry']; ?>
 <?php echo $this->_tpl_vars['LANG']['domainrenewalsdays']; ?>
</span>
				<?php elseif ($this->_tpl_vars['renewal']['daysuntilexpiry'] > 0): ?>
				<span class="textred"><?php echo $this->_tpl_vars['renewal']['daysuntilexpiry']; ?>
 <?php echo $this->_tpl_vars['LANG']['domainrenewalsdays']; ?>
</span>
				<?php else: ?>
				<span class="textblack"><?php echo $this->_tpl_vars['renewal']['daysuntilexpiry']*-1; ?>
 <?php echo $this->_tpl_vars['LANG']['domainrenewalsdaysago']; ?>
</span>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['renewal']['ingraceperiod']): ?>
				<br />
				<span class="textred"><?php echo $this->_tpl_vars['LANG']['domainrenewalsingraceperiod']; ?>
<span>
				<?php endif; ?>
			</td>
			<td>
				<?php if ($this->_tpl_vars['renewal']['beforerenewlimit']): ?>
				<span class="textred"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['domainrenewalsbeforerenewlimit'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, $this->_tpl_vars['renewal']['beforerenewlimitdays']) : smarty_modifier_sprintf2($_tmp, $this->_tpl_vars['renewal']['beforerenewlimitdays'])); ?>
<span>
				<?php elseif ($this->_tpl_vars['renewal']['pastgraceperiod']): ?>
				<span class="textred"><?php echo $this->_tpl_vars['LANG']['domainrenewalspastgraceperiod']; ?>
<span>
				<?php else: ?>
				<select name="renewalperiod[<?php echo $this->_tpl_vars['renewal']['id']; ?>
]">
					<?php $_from = $this->_tpl_vars['renewal']['renewaloptions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['renewaloption']):
?>
					<option value="<?php echo $this->_tpl_vars['renewaloption']['period']; ?>
"><?php echo $this->_tpl_vars['renewaloption']['period']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>
 @ <?php echo $this->_tpl_vars['renewaloption']['price']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; else: ?>
		
		<tr class="carttablerow">
			<td colspan="5"><?php echo $this->_tpl_vars['LANG']['domainrenewalsnoneavailable']; ?>
</td>
		</tr>
		
		<?php endif; unset($_from); ?>
	</tbody>
</table>
</div>

<p class="text-center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['ordernowbutton']; ?>
 &raquo;" class="btn" /></p>

</form>

</div>