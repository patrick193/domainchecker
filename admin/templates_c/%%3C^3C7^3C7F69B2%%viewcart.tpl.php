<?php /* Smarty version 2.6.28, created on 2015-04-09 10:36:41
         compiled from /home/generali/public_html/admin/templates/orderforms/bamatcart/viewcart.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'sprintf2', '/home/generali/public_html/admin/templates/orderforms/bamatcart/viewcart.tpl', 269, false),array('function', 'dc_get_session_id', '/home/generali/public_html/admin/templates/orderforms/bamatcart/viewcart.tpl', 659, false),)), $this); ?>
<script type="text/javascript" src="includes/jscript/pwstrength.js"></script>
<script type="text/javascript" src="includes/jscript/creditcard.js"></script>

<?php echo '
<script>
function removeItem(type,num) {
    var response = confirm("'; ?>
<?php echo $this->_tpl_vars['LANG']['cartremoveitemconfirm']; ?>
<?php echo '");
    if (response) {
        window.location = \'cart.php?a=remove&r=\'+type+\'&i=\'+num;
    }
}

function emptyCart(type,num) {
    var response = confirm("'; ?>
<?php echo $this->_tpl_vars['LANG']['cartemptyconfirm']; ?>
<?php echo '");
    if (response) {
        window.location = \'cart.php?a=empty\';
    }
}
</script>
'; ?>


<script>
window.langPasswordStrength = "<?php echo $this->_tpl_vars['LANG']['pwstrength']; ?>
";
window.langPasswordWeak = "<?php echo $this->_tpl_vars['LANG']['pwstrengthweak']; ?>
";
window.langPasswordModerate = "<?php echo $this->_tpl_vars['LANG']['pwstrengthmoderate']; ?>
";
window.langPasswordStrong = "<?php echo $this->_tpl_vars['LANG']['pwstrengthstrong']; ?>
";
</script>

<div id="order-comparison">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "orderforms/bamatcart/comparisonsteps.tpl", 'smarty_include_vars' => array('step' => 3)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="col-md-12">

<?php if ($this->_tpl_vars['errormessage']): ?>
	<div class="alert alert-danger cart_errors">
		<?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>

		<ul><?php echo $this->_tpl_vars['errormessage']; ?>
</ul>
	</div>
<?php elseif ($this->_tpl_vars['promotioncode'] && $this->_tpl_vars['rawdiscount'] == "0.00"): ?>
	<div class="alert alert-danger cart_errors">
		<?php echo $this->_tpl_vars['LANG']['promoappliedbutnodiscount']; ?>

	</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['bundlewarnings']): ?>
<div class="alert alert-danger">
	<strong><?php echo $this->_tpl_vars['LANG']['bundlereqsnotmet']; ?>
</strong><br />
	
	<?php $_from = $this->_tpl_vars['bundlewarnings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['warning']):
?>
		<?php echo $this->_tpl_vars['warning']; ?>
<br />
	<?php endforeach; endif; unset($_from); ?>
</div>
<?php endif; ?>

<h3><?php echo $this->_tpl_vars['LANG']['ordersummary']; ?>
</h3>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?a=view" id="bcart_form">

<table class="table table-framed" id="cart_view">
<thead>
	<tr>
		<th><?php echo $this->_tpl_vars['LANG']['orderdesc']; ?>
</th>
		<th><?php echo $this->_tpl_vars['LANG']['orderprice']; ?>
</th>
	</tr>
</thead>

<tbody>
	<?php $_from = $this->_tpl_vars['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['product']):
?>
		<tr class="bcart_product">
			<td>
				<div class="bcart_prod_title">
					<span class="bcart_prod_group"><?php echo $this->_tpl_vars['product']['productinfo']['groupname']; ?>
 - </span><span class="bcart_prod_name"><?php echo $this->_tpl_vars['product']['productinfo']['name']; ?>
</span> <?php if ($this->_tpl_vars['product']['domain']): ?><span class="bcart_prod_domain">(<?php echo $this->_tpl_vars['product']['domain']; ?>
)</span><?php endif; ?>
				</div>

				<div class="bcart_padding">
					<?php if ($this->_tpl_vars['product']['configoptions']): ?>
						<ul class="bcart_prod_options">
						<?php $_from = $this->_tpl_vars['product']['configoptions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['confnum'] => $this->_tpl_vars['configoption']):
?>
							<li>
							<?php echo $this->_tpl_vars['configoption']['name']; ?>
: <?php if ($this->_tpl_vars['configoption']['type'] == 1 || $this->_tpl_vars['configoption']['type'] == 2): ?><?php echo $this->_tpl_vars['configoption']['option']; ?>
<?php elseif ($this->_tpl_vars['configoption']['type'] == 3): ?><?php if ($this->_tpl_vars['configoption']['qty']): ?><?php echo $this->_tpl_vars['LANG']['yes']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['no']; ?>
<?php endif; ?><?php elseif ($this->_tpl_vars['configoption']['type'] == 4): ?><?php echo $this->_tpl_vars['configoption']['qty']; ?>
 x <?php echo $this->_tpl_vars['configoption']['option']; ?>
<?php endif; ?>
							</li>
						<?php endforeach; endif; unset($_from); ?>
						</ul>
					<?php endif; ?>
					
					<?php if ($this->_tpl_vars['product']['allowqty']): ?>
						<div class="bcart_qty_wrap"><?php echo $this->_tpl_vars['LANG']['quantity']; ?>
: <span class="glyphicon glyphicon-arrow-down text-success qty_btn" data-qty-id="<?php echo $this->_tpl_vars['num']; ?>
" data-qty-type="0"></span> <input class="cart_qty text-center" type="text" disabled="disabled" name="qty[<?php echo $this->_tpl_vars['num']; ?>
]" id="qty_<?php echo $this->_tpl_vars['num']; ?>
" value="<?php echo $this->_tpl_vars['product']['qty']; ?>
" /> <span class="glyphicon glyphicon-arrow-up text-success qty_btn" data-qty-id="<?php echo $this->_tpl_vars['num']; ?>
" data-qty-type="1"></span></div>
					<?php endif; ?>

					<a href="<?php echo $_SERVER['PHP_SELF']; ?>
?a=confproduct&i=<?php echo $this->_tpl_vars['num']; ?>
" class="cartedit text-warning"><?php echo $this->_tpl_vars['LANG']['carteditproductconfig']; ?>
</a>
					<a href="#" onclick="removeItem('p','<?php echo $this->_tpl_vars['num']; ?>
');return false" class="cartremove text-danger text-right" title="<?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
"><?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
</a>
				</div>
			</td>
		
			<td id="pricing_<?php echo $this->_tpl_vars['num']; ?>
" class="text-center bcart_td_price">
				<strong><?php echo $this->_tpl_vars['product']['pricingtext']; ?>
<?php if ($this->_tpl_vars['product']['proratadate']): ?><br />(<?php echo $this->_tpl_vars['LANG']['orderprorata']; ?>
 <?php echo $this->_tpl_vars['product']['proratadate']; ?>
)<?php endif; ?></strong>
			</td>

		</tr>

		<?php $_from = $this->_tpl_vars['product']['addons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['addonnum'] => $this->_tpl_vars['addon']):
?>
		<tr class="carttableproduct">
			<td>
				<div class="bcart_prod_subtitle bcart_padding">
					<span class="bcart_prod_group"><?php echo $this->_tpl_vars['LANG']['orderaddon']; ?>
 - </span><span class="bcart_prod_name"><?php echo $this->_tpl_vars['addon']['name']; ?>
</span>
				</div>
			</td>
			
			<td class="text-center bcart_td_price">
				<strong><?php echo $this->_tpl_vars['addon']['pricingtext']; ?>
</strong>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	<?php endforeach; endif; unset($_from); ?>

	<?php $_from = $this->_tpl_vars['addons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['addon']):
?>
	<tr class="carttableproduct">
		<td>
			<strong><?php echo $this->_tpl_vars['addon']['name']; ?>
</strong>
			<br />
			<?php echo $this->_tpl_vars['addon']['productname']; ?>
<?php if ($this->_tpl_vars['addon']['domainname']): ?> - <?php echo $this->_tpl_vars['addon']['domainname']; ?>
<?php endif; ?>
			<br />
			<a href="#" onclick="removeItem('a','<?php echo $this->_tpl_vars['num']; ?>
');return false" class="cartremove text-danger"><?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
</a>
		</td>
		
		<td class="text-center bcart_td_price">
			<strong><?php echo $this->_tpl_vars['addon']['pricingtext']; ?>
</strong>
		</td>
		
		<td></td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>

	<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['domain']):
?>
	<tr class="bcart_product">
		<td>
			<div class="bcart_prod_title">
				<span class="bcart_prod_group"><?php if ($this->_tpl_vars['domain']['type'] == 'register'): ?><?php echo $this->_tpl_vars['LANG']['orderdomainregistration']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['orderdomaintransfer']; ?>
<?php endif; ?> - </span><span class="bcart_prod_name"><?php echo $this->_tpl_vars['domain']['domain']; ?>
</span><span class="bcart_prod_group"> - <?php echo $this->_tpl_vars['domain']['regperiod']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>
</span>
			</div>

			<div class="bcart_padding">
				<?php if ($this->_tpl_vars['domain']['dnsmanagement'] || $this->_tpl_vars['domain']['emailforwarding'] || $this->_tpl_vars['domain']['idprotection']): ?>
				<ul class="bcart_prod_options">
					<?php if ($this->_tpl_vars['domain']['dnsmanagement']): ?><li><?php echo $this->_tpl_vars['LANG']['domaindnsmanagement']; ?>
</li><?php endif; ?>
					<?php if ($this->_tpl_vars['domain']['emailforwarding']): ?><li><?php echo $this->_tpl_vars['LANG']['domainemailforwarding']; ?>
</li><?php endif; ?>
					<?php if ($this->_tpl_vars['domain']['idprotection']): ?><li><?php echo $this->_tpl_vars['LANG']['domainidprotection']; ?>
</li><?php endif; ?>
					</ul>
				<?php endif; ?>

				<a href="<?php echo $_SERVER['PHP_SELF']; ?>
?a=confdomains" class="cartedit text-warning"><?php echo $this->_tpl_vars['LANG']['cartconfigdomainextras']; ?>
</a>
				<a href="#" onclick="removeItem('d','<?php echo $this->_tpl_vars['num']; ?>
');return false" class="cartremove text-danger"><?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
</a>
			</div>
		</td>
		
		<td class="text-center bcart_td_price">
			<strong><?php echo $this->_tpl_vars['domain_prices'][$this->_tpl_vars['domain']['domain']]['price_text']; ?>
</strong>
			<br />
			<em><?php echo $this->_tpl_vars['domain']['price']; ?>
</em>
		</td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>

	<?php $_from = $this->_tpl_vars['renewals']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['domain']):
?>
	<tr class="carttableproduct">
		<td>
			<div class="bcart_prod_title">
				<span class="bcart_prod_group"><?php echo $this->_tpl_vars['LANG']['domainrenewal']; ?>
 - </span><span class="bcart_prod_name"><?php echo $this->_tpl_vars['domain']['domain']; ?>
</span><span class="bcart_prod_group"> - <?php echo $this->_tpl_vars['domain']['regperiod']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>
</span>
			</div>

			<div class="bcart_padding">
				<?php if ($this->_tpl_vars['domain']['dnsmanagement'] || $this->_tpl_vars['domain']['emailforwarding'] || $this->_tpl_vars['domain']['idprotection']): ?>
				<ul class="bcart_prod_options">
					<?php if ($this->_tpl_vars['domain']['dnsmanagement']): ?><li><?php echo $this->_tpl_vars['LANG']['domaindnsmanagement']; ?>
</li><?php endif; ?>
					<?php if ($this->_tpl_vars['domain']['emailforwarding']): ?><li><?php echo $this->_tpl_vars['LANG']['domainemailforwarding']; ?>
</li><?php endif; ?>
					<?php if ($this->_tpl_vars['domain']['idprotection']): ?><li><?php echo $this->_tpl_vars['LANG']['domainidprotection']; ?>
</li><?php endif; ?>
					</ul>
				<?php endif; ?>

				<a href="#" onclick="removeItem('r','<?php echo $this->_tpl_vars['num']; ?>
');return false" class="cartremove text-danger"><?php echo $this->_tpl_vars['LANG']['cartremove']; ?>
</a>
			</div>
		</td>
		
		<td class="text-center bcart_td_price">
			<strong><?php echo $this->_tpl_vars['domain']['price']; ?>
</strong>
		</td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>

	<?php if ($this->_tpl_vars['cartitems'] == 0): ?>
	<tr class="clientareatableactive">
		<td colspan="2" class="text-center">
			<br />
			<?php echo $this->_tpl_vars['LANG']['cartempty']; ?>

			<br /><br />
		</td>
	</tr>
	<?php endif; ?>
</tbody>

<tbody id="summary_rows">
	<tr class="summary">
		<td><?php echo $this->_tpl_vars['LANG']['ordersubtotal']; ?>
: &nbsp;</td>
		<td><?php echo $this->_tpl_vars['subtotal']; ?>
</td>
	</tr>
	
	<?php if ($this->_tpl_vars['promotioncode']): ?>
	<tr class="summary">
		<td><?php echo $this->_tpl_vars['promotiondescription']; ?>
: &nbsp;</td>
		<td><?php echo $this->_tpl_vars['discount']; ?>
</td>
	</tr>
	<?php endif; ?>
	
	<?php if ($this->_tpl_vars['taxrate']): ?>
	<tr class="summary">
		<td><?php echo $this->_tpl_vars['taxname']; ?>
 @ <?php echo $this->_tpl_vars['taxrate']; ?>
%: &nbsp;</td>
		<td><?php echo $this->_tpl_vars['taxtotal']; ?>
</td>
	</tr>
	<?php endif; ?>

	<?php if ($this->_tpl_vars['taxrate2']): ?>
	<tr class="summary">
		<td><?php echo $this->_tpl_vars['taxname2']; ?>
 @ <?php echo $this->_tpl_vars['taxrate2']; ?>
%: &nbsp;</td>
		<td><?php echo $this->_tpl_vars['taxtotal2']; ?>
</td>
	</tr>
	<?php endif; ?>
</tbody>
</table>

</form>

<div class="checkoutbuttonsleft">
	<input type="button" value="<?php echo $this->_tpl_vars['LANG']['emptycart']; ?>
" onclick="emptyCart();return false" class="btn btn-danger" />
	<input type="button" value="<?php echo $this->_tpl_vars['LANG']['continueshopping']; ?>
" onclick="window.location='cart.php'" class="btn btn-primary" />
</div>

<div class="checkoutbuttonsright">
	<div class="totalduetoday"><?php echo $this->_tpl_vars['LANG']['ordertotalduetoday']; ?>
: <span><?php echo $this->_tpl_vars['total']; ?>
</span></div>
	
	<?php if ($this->_tpl_vars['totalrecurringmonthly'] || $this->_tpl_vars['totalrecurringquarterly'] || $this->_tpl_vars['totalrecurringsemiannually'] || $this->_tpl_vars['totalrecurringannually'] || $this->_tpl_vars['totalrecurringbiennially'] || $this->_tpl_vars['totalrecurringtriennially']): ?>
	<div class="totalrecurring"><p><?php echo $this->_tpl_vars['LANG']['ordertotalrecurring']; ?>
: <?php if ($this->_tpl_vars['totalrecurringmonthly']): ?><span><?php echo $this->_tpl_vars['totalrecurringmonthly']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermmonthly']; ?>
</span><br /><?php endif; ?>
		<?php if ($this->_tpl_vars['totalrecurringquarterly']): ?><span><?php echo $this->_tpl_vars['totalrecurringquarterly']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermquarterly']; ?>
</span><br /><?php endif; ?>
		<?php if ($this->_tpl_vars['totalrecurringsemiannually']): ?><span><?php echo $this->_tpl_vars['totalrecurringsemiannually']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermsemiannually']; ?>
</span><br /><?php endif; ?>
		<?php if ($this->_tpl_vars['totalrecurringannually']): ?><span><?php echo $this->_tpl_vars['totalrecurringannually']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermannually']; ?>
</span><br /><?php endif; ?>
		<?php if ($this->_tpl_vars['totalrecurringbiennially']): ?><span><?php echo $this->_tpl_vars['totalrecurringbiennially']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermbiennially']; ?>
</span><br /><?php endif; ?>
		<?php if ($this->_tpl_vars['totalrecurringtriennially']): ?><span><?php echo $this->_tpl_vars['totalrecurringtriennially']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderpaymenttermtriennially']; ?>
</span><?php endif; ?></p>
	</div>
	<?php endif; ?>

	<div class="promo">
		<?php if ($this->_tpl_vars['promotioncode']): ?><?php echo $this->_tpl_vars['LANG']['orderpromotioncode']; ?>
: <?php echo $this->_tpl_vars['promotioncode']; ?>
 (<a href="<?php echo $_SERVER['PHP_SELF']; ?>
?a=removepromo">Remove Promo Code</a>)<?php else: ?><input type="text" id="promocode" size="20" value="<?php echo $this->_tpl_vars['LANG']['orderpromotioncode']; ?>
" onfocus="if(this.value=='<?php echo $this->_tpl_vars['LANG']['orderpromotioncode']; ?>
')this.value=''" /> <input type="submit" value="<?php echo $this->_tpl_vars['LANG']['go']; ?>
" onclick="applypromo()" class="btn" /><?php endif; ?>
	</div>
</div>

<div class="clear"></div>

<?php $_from = $this->_tpl_vars['gatewaysoutput']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['gatewayoutput']):
?>
<div class="gatewaycheckout"><?php echo $this->_tpl_vars['gatewayoutput']; ?>
</div>
<?php endforeach; endif; unset($_from); ?>

<h3><?php echo $this->_tpl_vars['LANG']['yourdetails']; ?>
</h3>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?a=checkout" id="orderfrm">
	<input type="hidden" name="submit" value="true" />
	<input type="hidden" name="custtype" id="custtype" value="<?php echo $this->_tpl_vars['custtype']; ?>
" />

	<!-- Login form -->
	<div class="logincontainer" id="loginfrm" <?php if ($this->_tpl_vars['custtype'] == 'existing' && ! $this->_tpl_vars['loggedin']): ?><?php else: ?> style="display:none;"<?php endif; ?>>
		<p><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['newcustomersignup'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, '<a href="#" onclick="showloginform();return false;">', '</a>') : smarty_modifier_sprintf2($_tmp, '<a href="#" onclick="showloginform();return false;">', '</a>')); ?>
</a></p>

		<fieldset class="control-group text-center">

			<div class="control-group">
				<label class="control-label" for="loginemail"><?php echo $this->_tpl_vars['LANG']['loginemail']; ?>
:</label>
				<div class="controls">
					<input  type="text" name="loginemail" id="loginemail"  value="<?php echo $this->_tpl_vars['username']; ?>
" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="loginpw"><?php echo $this->_tpl_vars['LANG']['loginpassword']; ?>
:</label>
				<div class="controls">
					<input type="password" name="loginpw" id="loginpw">
				</div>
			</div>
		</fieldset>

	</div>

	<div id="signupfrm"<?php if ($this->_tpl_vars['custtype'] == 'existing' && ! $this->_tpl_vars['loggedin']): ?> style="display:none;"<?php endif; ?>>

	<?php if (! $this->_tpl_vars['loggedin']): ?><p><?php echo $this->_tpl_vars['LANG']['alreadyregistered']; ?>
 <a href="<?php echo $_SERVER['PHP_SELF']; ?>
?a=login" onclick="showloginform();return false;"><?php echo $this->_tpl_vars['LANG']['clickheretologin']; ?>
...</a></p><?php endif; ?>

	<?php if ($this->_tpl_vars['loggedin']): ?>
		<table class="table table-striped table-framed table-domain-cart">
		<tbody>
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareafirstname']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['firstname']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientarealastname']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['lastname']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareacompanyname']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['companyname']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareaemail']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['email']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareaaddress1']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['address1']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareaaddress2']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['address2']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareacity']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['city']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareastate']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['state']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareapostcode']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['postcode']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareacountry']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['country']; ?>
</td>
			</tr>
			
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['clientareaphonenumber']; ?>
</th>
				<td><?php echo $this->_tpl_vars['clientsdetails']['phonenumber']; ?>
</td>
			</tr>
			
			<?php $_from = $this->_tpl_vars['customfields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['customfield']):
?>
			<tr>
				<td><?php echo $this->_tpl_vars['customfield']['name']; ?>
</td>
				<td><?php echo $this->_tpl_vars['customfield']['input']; ?>
 <?php echo $this->_tpl_vars['customfield']['description']; ?>
</td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
		</tbody>	
		</table>	
	<?php else: ?>
		<div class="col-md-6">
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareafirstname']; ?>
</label>
				<input type="text" name="firstname" value="<?php echo $this->_tpl_vars['clientsdetails']['firstname']; ?>
" />
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientarealastname']; ?>
</label>
				<input type="text" name="lastname" value="<?php echo $this->_tpl_vars['clientsdetails']['lastname']; ?>
" />
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareacompanyname']; ?>
</label>
				<input type="text" name="companyname" value="<?php echo $this->_tpl_vars['clientsdetails']['companyname']; ?>
" />
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareaemail']; ?>
</label>
				<input type="text" name="email" value="<?php echo $this->_tpl_vars['clientsdetails']['email']; ?>
" />
			</div>
			
			<?php if ($this->_tpl_vars['securityquestions']): ?>
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareasecurityquestion']; ?>
</label>
				<select name="securityqid">
					<?php $_from = $this->_tpl_vars['securityquestions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['question']):
?>
					<option value="<?php echo $this->_tpl_vars['question']['id']; ?>
"<?php if ($this->_tpl_vars['question']['id'] == $this->_tpl_vars['securityqid']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['question']['question']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareasecurityanswer']; ?>
</label>
				<input type="password" name="securityqans" value="<?php echo $this->_tpl_vars['securityqans']; ?>
" size="30">
			</div>
			<?php endif; ?>
			
			<?php $_from = $this->_tpl_vars['customfields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['customfield']):
?>
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['customfield']['name']; ?>
</label>
				<?php echo $this->_tpl_vars['customfield']['input']; ?>

				<span class="bcart_reg_custom_desc"><?php echo $this->_tpl_vars['customfield']['description']; ?>
</span>
			</div>
			<?php endforeach; endif; unset($_from); ?>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareapassword']; ?>
</label>
				<input type="password" name="password" id="newpw" size="20" value="<?php echo $this->_tpl_vars['password']; ?>
" />
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareaconfirmpassword']; ?>
</label>
				<input type="password" name="password2" size="20" value="<?php echo $this->_tpl_vars['password2']; ?>
" />
			</div>
			
			<div class="controls">
				<label class="control-label">&nbsp;</label>
				<div class="text-center"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pwstrength.tpl", 'smarty_include_vars' => array('id' => 'newpw')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
			</div>
			
		</div>

		<div class="col-md-6">
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareaphonenumber']; ?>
</label>
				<?php if ($this->_tpl_vars['loggedin']): ?><span><?php echo $this->_tpl_vars['clientsdetails']['phonenumber']; ?>
</span><?php else: ?><input type="text" name="phonenumber" size="20" value="<?php echo $this->_tpl_vars['clientsdetails']['phonenumber']; ?>
" /><?php endif; ?>
			</div>

			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareaaddress1']; ?>
</label>
				<?php if ($this->_tpl_vars['loggedin']): ?><span><?php echo $this->_tpl_vars['clientsdetails']['address1']; ?>
</span><?php else: ?><input type="text" name="address1" value="<?php echo $this->_tpl_vars['clientsdetails']['address1']; ?>
" /><?php endif; ?>
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareaaddress2']; ?>
</label>
				<?php if ($this->_tpl_vars['loggedin']): ?><span><?php echo $this->_tpl_vars['clientsdetails']['address2']; ?>
</span><?php else: ?><input type="text" name="address2" value="<?php echo $this->_tpl_vars['clientsdetails']['address2']; ?>
" /><?php endif; ?>
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareacity']; ?>
</label>
				<?php if ($this->_tpl_vars['loggedin']): ?><span><?php echo $this->_tpl_vars['clientsdetails']['city']; ?>
</span><?php else: ?><input type="text" name="city" value="<?php echo $this->_tpl_vars['clientsdetails']['city']; ?>
" /><?php endif; ?>
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareastate']; ?>
</label>
				<?php if ($this->_tpl_vars['loggedin']): ?><span><?php echo $this->_tpl_vars['clientsdetails']['state']; ?>
</span><?php else: ?><input type="text" name="state" value="<?php echo $this->_tpl_vars['clientsdetails']['state']; ?>
" /><?php endif; ?>
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareapostcode']; ?>
</label>
				<?php if ($this->_tpl_vars['loggedin']): ?><span><?php echo $this->_tpl_vars['clientsdetails']['postcode']; ?>
</span><?php else: ?><input type="text" name="postcode" size="15" value="<?php echo $this->_tpl_vars['clientsdetails']['postcode']; ?>
" /><?php endif; ?>
			</div>
			
			<div class="controls">
				<label class="control-label"><?php echo $this->_tpl_vars['LANG']['clientareacountry']; ?>
</label>
				<?php if ($this->_tpl_vars['loggedin']): ?><span><?php echo $this->_tpl_vars['clientsdetails']['country']; ?>
</span><?php else: ?><?php echo $this->_tpl_vars['clientcountrydropdown']; ?>
<?php endif; ?></td>
			</div>

		</div>

		<div class="clearfix"></div>
	<?php endif; ?>

	<?php if ($this->_tpl_vars['taxenabled'] && ! $this->_tpl_vars['loggedin']): ?>
	<p class="text-center"><?php echo $this->_tpl_vars['LANG']['carttaxupdateselections']; ?>
</p>
	<p class="text-center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['carttaxupdateselectionsupdate']; ?>
" name="updateonly" class="btn" /></p>
	<?php endif; ?>

	</div>

	<?php if ($this->_tpl_vars['domainsinorder']): ?>
	<input type="hidden" name="contact" id="domaincontact" value="">
	
	<div id="domaincontactfields"<?php if ($this->_tpl_vars['contact'] == 'addingnew'): ?> style="display:block"<?php endif; ?>>
	
	<table class="table table-striped table-framed table-domain-cart">
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareafirstname']; ?>
</th>
			<td><input type="text" name="domaincontactfirstname" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['firstname']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientarealastname']; ?>
</th>
			<td><input type="text" name="domaincontactlastname" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['lastname']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareacompanyname']; ?>
</th>
			<td><input type="text" name="domaincontactcompanyname" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['companyname']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareaemail']; ?>
</th>
			<td><input type="text" name="domaincontactemail" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['email']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareaaddress1']; ?>
</th>
			<td><input type="text" name="domaincontactaddress1" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['address1']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareaaddress2']; ?>
</th>
			<td><input type="text" name="domaincontactaddress2" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['address2']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareacity']; ?>
</th>
			<td><input type="text" name="domaincontactcity" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['city']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareastate']; ?>
</th>
			<td><input type="text" name="domaincontactstate" style="width:80%;" value="<?php echo $this->_tpl_vars['domaincontact']['state']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareapostcode']; ?>
</th>
			<td><input type="text" name="domaincontactpostcode" size="15" value="<?php echo $this->_tpl_vars['domaincontact']['postcode']; ?>
" /></td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareacountry']; ?>
</th>
			<td><?php echo $this->_tpl_vars['domaincontactcountrydropdown']; ?>
</td>
		</tr>
		
		<tr>
			<th><?php echo $this->_tpl_vars['LANG']['clientareaphonenumber']; ?>
</th>
			<td><input type="text" name="domaincontactphonenumber" size="20" value="<?php echo $this->_tpl_vars['domaincontact']['phonenumber']; ?>
" /></td>
		</tr>
	</table>
	</div>
<?php endif; ?>


<div class="viewcartcol2">

<h3><?php echo $this->_tpl_vars['LANG']['orderpaymentmethod']; ?>
</h3>
<p class="paymentmethods"><?php $_from = $this->_tpl_vars['gateways']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['gateway']):
?><label><input type="radio" name="paymentmethod" value="<?php echo $this->_tpl_vars['gateway']['sysname']; ?>
" onclick="<?php if ($this->_tpl_vars['gateway']['type'] == 'CC'): ?>showCCForm()<?php else: ?>hideCCForm()<?php endif; ?>"<?php if ($this->_tpl_vars['selectedgateway'] == $this->_tpl_vars['gateway']['sysname']): ?> checked<?php endif; ?> /><?php echo $this->_tpl_vars['gateway']['name']; ?>
</label><br /><?php endforeach; endif; unset($_from); ?></p>

<div id="ccinputform"<?php if ($this->_tpl_vars['selectedgatewaytype'] != 'CC'): ?> style="display:none;"<?php endif; ?>>

<?php if (! $this->_tpl_vars['clientsdetails']['cclastfour']): ?><input type="hidden" name="ccinfo" value="new" /><?php endif; ?>

<table>
</tbody>
	<?php if ($this->_tpl_vars['clientsdetails']['cclastfour']): ?>
		<tr>
			<td colspan="2">
				<label><input type="radio" name="ccinfo" value="useexisting" id="useexisting" onclick="useExistingCC()"<?php if ($this->_tpl_vars['clientsdetails']['cclastfour']): ?> checked<?php else: ?> disabled<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['creditcarduseexisting']; ?>
<?php if ($this->_tpl_vars['clientsdetails']['cclastfour']): ?> (<?php echo $this->_tpl_vars['clientsdetails']['cclastfour']; ?>
)<?php endif; ?></label>
				<br />
				<label><input type="radio" name="ccinfo" value="new" id="new" onclick="enterNewCC()"<?php if (! $this->_tpl_vars['clientsdetails']['cclastfour'] || $this->_tpl_vars['ccinfo'] == 'new'): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['creditcardenternewcard']; ?>
</label>
			</td>
		</tr>
	<?php endif; ?>
	
	<tr class="newccinfo" <?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>>
		<td><?php echo $this->_tpl_vars['LANG']['creditcardcardtype']; ?>
</td>
		<td>
			<select name="cctype" id="cctype">
				<?php $_from = $this->_tpl_vars['acceptedcctypes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['cardtype']):
?>
					<option<?php if ($this->_tpl_vars['cctype'] == $this->_tpl_vars['cardtype']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['cardtype']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	
	<tr class="newccinfo" <?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>>
		<td><?php echo $this->_tpl_vars['LANG']['creditcardcardnumber']; ?>
</td>
		<td><input type="text" name="ccnumber" value="<?php echo $this->_tpl_vars['ccnumber']; ?>
" autocomplete="off" /></td>
	</tr>
	
	<tr class="newccinfo" <?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>>
		<td><?php echo $this->_tpl_vars['LANG']['creditcardcardexpires']; ?>
</td>
		<td>
			<select name="ccexpirymonth" id="ccexpirymonth" class="newccinfo">
				<?php $_from = $this->_tpl_vars['months']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['month']):
?>
					<option<?php if ($this->_tpl_vars['ccexpirymonth'] == $this->_tpl_vars['month']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['month']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
			&nbsp;/&nbsp;
			<select name="ccexpiryyear" class="newccinfo">
				<?php $_from = $this->_tpl_vars['expiryyears']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year']):
?>
					<option<?php if ($this->_tpl_vars['ccexpiryyear'] == $this->_tpl_vars['year']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['year']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	
	<?php if ($this->_tpl_vars['showccissuestart']): ?>
	<tr class="newccinfo" <?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>>
		<td><?php echo $this->_tpl_vars['LANG']['creditcardcardstart']; ?>
</td>
		<td>
			<select name="ccstartmonth" id="ccstartmonth" class="newccinfo">
				<?php $_from = $this->_tpl_vars['months']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['month']):
?>
					<option<?php if ($this->_tpl_vars['ccstartmonth'] == $this->_tpl_vars['month']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['month']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
			&nbsp;/&nbsp;
			<select name="ccstartyear" class="newccinfo">
				<?php $_from = $this->_tpl_vars['startyears']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['year']):
?>
					<option<?php if ($this->_tpl_vars['ccstartyear'] == $this->_tpl_vars['year']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['year']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	
	<tr class="newccinfo" <?php if ($this->_tpl_vars['clientsdetails']['cclastfour'] && $this->_tpl_vars['ccinfo'] != 'new'): ?> style="display:none;"<?php endif; ?>>
		<td><?php echo $this->_tpl_vars['LANG']['creditcardcardissuenum']; ?>
</td>
		<td><input type="text" name="ccissuenum" value="<?php echo $this->_tpl_vars['ccissuenum']; ?>
" size="5" maxlength="3" /></td>
	</tr>
	<?php endif; ?>
	
	<tr>
		<td><?php echo $this->_tpl_vars['LANG']['creditcardcvvnumber']; ?>
</td>
		<td><input type="text" name="cccvv" id="cccvv" value="<?php echo $this->_tpl_vars['cccvv']; ?>
" size="5" autocomplete="off" />
		</td>
	</tr>
	
	<tr>
		<td></td>
		<td><a href="#" onclick="window.open('images/ccv.gif','','width=280,height=200,scrollbars=no,top=100,left=100');return false"><?php echo $this->_tpl_vars['LANG']['creditcardcvvwhere']; ?>
</a></td>
	</tr>
	
	<?php if ($this->_tpl_vars['shownostore']): ?>
	<tr>
		<td><input type="checkbox" name="nostore" id="nostore" /></td><td><label for="nostore"><?php echo $this->_tpl_vars['LANG']['creditcardnostore']; ?>
</label></td>
	</tr>
	<?php endif; ?>
</tbody>
</table>
</div>

<?php if ($this->_tpl_vars['shownotesfield']): ?>
<h3><?php echo $this->_tpl_vars['LANG']['ordernotes']; ?>
</h3>
<p class="text-center"><textarea name="notes" rows="3" style="width:95%" onFocus="if(this.value=='<?php echo $this->_tpl_vars['LANG']['ordernotesdescription']; ?>
'){this.value='';}" onBlur="if (this.value==''){this.value='<?php echo $this->_tpl_vars['LANG']['ordernotesdescription']; ?>
';}"><?php echo $this->_tpl_vars['notes']; ?>
</textarea></p>
<?php endif; ?>

<br />

<?php if ($this->_tpl_vars['accepttos']): ?>
<p class="text-center"><label><input type="checkbox" name="accepttos" id="accepttos" /> <?php echo $this->_tpl_vars['LANG']['ordertosagreement']; ?>
 <a href="<?php echo $this->_tpl_vars['tosurl']; ?>
" target="_blank"><?php echo $this->_tpl_vars['LANG']['ordertos']; ?>
</a></label><p>
<?php endif; ?>

<p class="text-center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['completeorder']; ?>
 &raquo;"<?php if ($this->_tpl_vars['cartitems'] == 0): ?> disabled<?php endif; ?> onclick="this.value='<?php echo $this->_tpl_vars['LANG']['pleasewait']; ?>
'" class="btn btn-success" /></p>

</div>

<div class="clear"></div>

<div class="checkoutsecure"><img class="text-left" src="images/padlock.gif"alt="Secure Transaction" style="border: 0; padding: 5px 10px 5px 0;" /> <?php echo $this->_tpl_vars['LANG']['ordersecure']; ?>
 (<strong><?php echo $this->_tpl_vars['ipaddress']; ?>
</strong>) <?php echo $this->_tpl_vars['LANG']['ordersecure2']; ?>
</div>

</div>

</form>

</div>

<div class="hidden" id="sid"><?php echo smarty_function_dc_get_session_id(array(), $this);?>
</div>