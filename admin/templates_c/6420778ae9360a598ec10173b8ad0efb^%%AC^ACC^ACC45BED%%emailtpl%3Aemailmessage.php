<?php /* Smarty version 2.6.28, created on 2015-04-29 16:20:27
         compiled from emailtpl:emailmessage */ ?>
<p><a href="<?php echo $this->_tpl_vars['company_domain']; ?>
" target="_blank"><img src="<?php echo $this->_tpl_vars['company_logo_url']; ?>
" alt="<?php echo $this->_tpl_vars['company_name']; ?>
" border="0" /></a></p>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>Thank you for your domain transfer order. Your order has been received and we have now initiated the transfer process. The details of the domain purchase are below:</p>
<p>Domain: <?php echo $this->_tpl_vars['domain_name']; ?>
<br />Registration Length: <?php echo $this->_tpl_vars['domain_reg_period']; ?>
<br />Transfer Price: <?php echo $this->_tpl_vars['domain_first_payment_amount']; ?>
<br />Renewal Price: <?php echo $this->_tpl_vars['domain_recurring_amount']; ?>
<br />Next Due Date: <?php echo $this->_tpl_vars['domain_next_due_date']; ?>
</p>
<p>You may login to your client area at <?php echo $this->_tpl_vars['whmcs_url']; ?>
 to manage your domain.<br /><br />Please provide the EPP/Auth-code for your domain-transfer at <?php echo $this->_tpl_vars['whmcs_url']; ?>
/dc_change_epp.php?domainepp=<?php echo $this->_tpl_vars['domain_name']; ?>
</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>