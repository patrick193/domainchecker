<?php /* Smarty version 2.6.28, created on 2015-04-15 16:55:09
         compiled from /home/generali/public_html/admin/templates/orderforms/bamatcart/addons.tpl */ ?>
<div id="order-comparison">

<h1><?php echo $this->_tpl_vars['LANG']['cartproductaddons']; ?>
</h1>

<p><?php echo $this->_tpl_vars['LANG']['cartfollowingaddonsavailable']; ?>
</p>

<?php $_from = $this->_tpl_vars['addons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['addon']):
?>
<div class="addoncontainer">
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?a=add">
<input type="hidden" name="aid" value="<?php echo $this->_tpl_vars['addon']['id']; ?>
" />
<div class="addon">
<div class="title"><?php echo $this->_tpl_vars['addon']['name']; ?>
</div>
<div class="pricing"><?php if ($this->_tpl_vars['addon']['free']): ?>
<?php echo $this->_tpl_vars['LANG']['orderfree']; ?>

<?php else: ?>
<?php echo $this->_tpl_vars['addon']['recurringamount']; ?>
 <?php echo $this->_tpl_vars['addon']['billingcycle']; ?>

<?php if ($this->_tpl_vars['addon']['setupfee']): ?><br /><span class="setup"><?php echo $this->_tpl_vars['addon']['setupfee']; ?>
 <?php echo $this->_tpl_vars['LANG']['ordersetupfee']; ?>
</span><?php endif; ?>
<?php endif; ?></div>
<div class="clear"></div>
<?php echo $this->_tpl_vars['addon']['description']; ?>

<div class="product">
<select name="productid">
<?php $_from = $this->_tpl_vars['addon']['productids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['product']):
?>
<option value="<?php echo $this->_tpl_vars['product']['id']; ?>
"><?php echo $this->_tpl_vars['product']['product']; ?>
<?php if ($this->_tpl_vars['product']['domain']): ?> - <?php echo $this->_tpl_vars['product']['domain']; ?>
<?php endif; ?></option>
<?php endforeach; endif; unset($_from); ?>
</select> <input type="submit" value="<?php echo $this->_tpl_vars['LANG']['ordernowbutton']; ?>
" class="cartbutton green" />
</div>
</div>
</form>
</div>
<?php if (!(!($this->_tpl_vars['num'] % 2))): ?>
<div class="clear"></div>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
<div class="clear"></div>

<?php if ($this->_tpl_vars['noaddons']): ?>
<div class="alert alert-danger"><?php echo $this->_tpl_vars['LANG']['cartproductaddonsnone']; ?>
</div>
<?php endif; ?>

</div>