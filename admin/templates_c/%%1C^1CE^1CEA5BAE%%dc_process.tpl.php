<?php /* Smarty version 2.6.28, created on 2015-06-22 13:57:07
         compiled from /var/www/web_domains/admin/templates/bamat/dc_process.tpl */ ?>
<form method="post" action="<?php echo $this->_tpl_vars['systemsslurl']; ?>
domain_check.php">

<?php if ($this->_tpl_vars['dc_debug']): ?><span style="display: none;"><?php echo $this->_tpl_vars['tld']['dc_run_time']; ?>
</span><?php endif; ?>

<table class="table table-framed" id="dc_table">
    <thead>
        <tr class="dc_tr_btn">
            <th colspan="5" class="dc_th_price"><?php echo $this->_tpl_vars['LANG']['domain_check']['tablepricenotice']; ?>
</th>
            <th><input type="submit" value="<?php if ($this->_tpl_vars['dc_is_product']): ?><?php echo $this->_tpl_vars['LANG']['updatecart']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['domain_check']['domainproceed']; ?>
<?php endif; ?>" class="btn btn-danger" /></th>
        </tr>

        <tr class="dc_tr_head">
            <th><?php echo $this->_tpl_vars['LANG']['domain_check']['tabledomain']; ?>
</th>
            <th><?php echo $this->_tpl_vars['LANG']['domain_check']['tablestatus']; ?>
</th>
            <th><?php echo $this->_tpl_vars['LANG']['domain_check']['tablesetupprice']; ?>
</th>
            <th><?php echo $this->_tpl_vars['LANG']['domain_check']['tableprice']; ?>
</th>
            <th><?php echo $this->_tpl_vars['LANG']['domain_check']['tablecategory']; ?>
</th>
            <th id="dc_check_all"><label><input type="checkbox" /> <?php echo $this->_tpl_vars['LANG']['domain_check']['tablecheckall']; ?>
</label></th>
        </tr>
    </thead>

    <tbody>
	
	<?php $_from = $this->_tpl_vars['tlds']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tld']):
?>
	
		<tr class="<?php if ($this->_tpl_vars['tld']['status'] === false): ?>dc_fail<?php elseif ($this->_tpl_vars['tld']['status'] === true): ?>dc_success<?php else: ?>dc_not_checked<?php endif; ?>">
			<td class="dc_td_name"><?php echo $this->_tpl_vars['domain']; ?>
<?php echo $this->_tpl_vars['tld']['tld']; ?>
<?php if ($this->_tpl_vars['dc_debug']): ?><span style="display: none;"><?php echo $this->_tpl_vars['tld']['result']; ?>
</span><?php endif; ?></td>
			<td class="dc_td_status"><div class="dc_status_sign"></div> <?php if ($this->_tpl_vars['tld']['status'] === false): ?><?php echo $this->_tpl_vars['LANG']['domain_check']['statustaken']; ?>
<?php elseif ($this->_tpl_vars['tld']['status'] === true): ?><?php echo $this->_tpl_vars['LANG']['domain_check']['statusfree']; ?>
<?php else: ?>Can not check<?php endif; ?></td>
			<td class="dc_td_setup"><span><?php echo $this->_tpl_vars['LANG']['ordersetupfee']; ?>
: </span><?php if (! $this->_tpl_vars['tld']['status']): ?><?php echo $this->_tpl_vars['tld']['trans_price']; ?>
<?php else: ?><?php echo $this->_tpl_vars['tld']['reg_price']; ?>
<?php endif; ?></td>
			<td class="dc_td_price"><?php echo $this->_tpl_vars['tld']['year_price']; ?>
*</td>
			<td class="dc_td_info"><?php echo $this->_tpl_vars['tld']['description']; ?>
</td>
			<td class="dc_td_check">
				<label><input type="checkbox" name="domains[]" value="<?php echo $this->_tpl_vars['domain']; ?>
<?php echo $this->_tpl_vars['tld']['tld']; ?>
"/> <?php if (! $this->_tpl_vars['tld']['status']): ?><?php echo $this->_tpl_vars['LANG']['domain_check']['actiontransfer']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['domain_check']['actionregister']; ?>
<?php endif; ?></label>

				<input type="hidden" name="domainsregperiod[<?php echo $this->_tpl_vars['domain']; ?>
<?php echo $this->_tpl_vars['tld']['tld']; ?>
]" value="1" />
				<input type="hidden" name="domainsaction[<?php echo $this->_tpl_vars['domain']; ?>
<?php echo $this->_tpl_vars['tld']['tld']; ?>
]" value="<?php if (! $this->_tpl_vars['tld']['status']): ?>transfer<?php else: ?>register<?php endif; ?>" />
				
			</td>
		</tr>
	<?php endforeach; endif; unset($_from); ?>
	
    </tbody>

    <tfoot>
        <tr class="dc_tr_btn">
            <th colspan="5" class="dc_th_price"><?php echo $this->_tpl_vars['LANG']['domain_check']['tablepricenotice']; ?>
</th>
            <th><input type="submit" value="<?php if ($this->_tpl_vars['dc_is_product']): ?><?php echo $this->_tpl_vars['LANG']['updatecart']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['domain_check']['domainproceed']; ?>
<?php endif; ?>" class="btn btn-danger" /></th>
        </tr>
    </tfoot>
</table>

<?php if ($this->_tpl_vars['dc_is_product']): ?>
	<input type="hidden" name="dc_is_product" value="<?php echo $this->_tpl_vars['dc_is_product']; ?>
" />
	<input type="hidden" name="dc_pid" value="<?php echo $this->_tpl_vars['dc_get_vars']['pid']; ?>
" />
<?php endif; ?>

</form>

<?php echo '
<script language="javascript">
// "Check All" in domain results table
jQuery(\'body\').on( \'change\', \'#dc_check_all input[type="checkbox"]\', function() {
	jQuery(\'#dc_table\').find(\'.dc_td_check input[type="checkbox"]\').prop( \'checked\', jQuery(\'#dc_check_all [type="checkbox"]\').is(\':checked\') );
});

// Make the whole row clickable
jQuery(\'body\').on( \'click\', \'#dc_table tr\', function(e) {
	if (e.target.nodeName == \'INPUT\' || e.target.nodeName == \'LABEL\') {
		return;
	}
	
	var check = jQuery(\'.dc_td_check input[type="checkbox"]\', this);

	if ( check.length > 0 ) {
		if ( check.is(\':checked\') ) {
			check.prop(\'checked\', false);
		} else {
			check.prop(\'checked\', true);
		}
	}
});
</script>
'; ?>
 