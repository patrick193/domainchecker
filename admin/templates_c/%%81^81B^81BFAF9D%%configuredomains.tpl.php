<?php /* Smarty version 2.6.28, created on 2015-04-09 10:36:36
         compiled from /home/generali/public_html/admin/templates/orderforms/bamatcart/configuredomains.tpl */ ?>
<div id="order-comparison">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "orderforms/bamatcart/comparisonsteps.tpl", 'smarty_include_vars' => array('step' => 2)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="col-md-2"></div>

<div class="col-md-8">
	<?php echo $this->_tpl_vars['LANG']['cartdomainsconfiginfo']; ?>

	<br />
	<?php if ($this->_tpl_vars['errormessage']): ?><div class="alert alert-danger"><?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>
<ul><?php echo $this->_tpl_vars['errormessage']; ?>
</ul></div><br /><?php endif; ?>
	
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?a=confdomains" id="conf_domain_form">
	<input type="hidden" name="update" value="true" />

	<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['domain']):
?>
	<h3><?php echo $this->_tpl_vars['domain']['domain']; ?>
</h3>
	<div class="domainconfig">
	<table class="table table-striped table-framed table-domain-cart">
		<tbody>
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['orderdomain']; ?>
:</td>
				<td><?php if ($this->_tpl_vars['domain']['eppenabled']): ?><?php echo $this->_tpl_vars['LANG']['ordertransferdomain']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['orderregisterdomain']; ?>
<?php endif; ?></td>
			</tr>

			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['hosting']; ?>
:</td>
				<td><?php if ($this->_tpl_vars['domain']['hosting']): ?><span class="text-success"><span class="glyphicon glyphicon-ok"></span> <?php echo $this->_tpl_vars['LANG']['cartdomainshashosting']; ?>
</span><?php else: ?><a href="cart.php" style="color:#cc0000;"><span class="glyphicon glyphicon-wrench"></span> <?php echo $this->_tpl_vars['LANG']['cartdomainsnohosting']; ?>
</a><br /><?php endif; ?></td>
			</tr>
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['orderregperiod']; ?>
:</td>
				<td>
					<?php echo $this->_tpl_vars['domain']['regperiod']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>

					<?php if ($this->_tpl_vars['domain']['eppenabled']): ?><input type="hidden" name="epp[<?php echo $this->_tpl_vars['num']; ?>
]" value="<?php echo $this->_tpl_vars['domain']['eppvalue']; ?>
" /><?php endif; ?>
				</td>
			</tr>
			
			<?php if ($this->_tpl_vars['domain']['dnsmanagement'] || $this->_tpl_vars['domain']['emailforwarding'] || $this->_tpl_vars['domain']['idprotection']): ?>
			<tr>
				<th><?php echo $this->_tpl_vars['LANG']['cartaddons']; ?>
:</th>
				<td>
					<?php if ($this->_tpl_vars['domain']['dnsmanagement']): ?><label><input type="checkbox" name="dnsmanagement[<?php echo $this->_tpl_vars['num']; ?>
]"<?php if ($this->_tpl_vars['domain']['dnsmanagementselected']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['domaindnsmanagement']; ?>
 (<?php echo $this->_tpl_vars['domain']['dnsmanagementprice']; ?>
)</label><?php endif; ?>
					<?php if ($this->_tpl_vars['domain']['emailforwarding']): ?><label><input type="checkbox" name="emailforwarding[<?php echo $this->_tpl_vars['num']; ?>
]"<?php if ($this->_tpl_vars['domain']['emailforwardingselected']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['domainemailforwarding']; ?>
 (<?php echo $this->_tpl_vars['domain']['emailforwardingprice']; ?>
)</label><?php endif; ?>
					<?php if ($this->_tpl_vars['domain']['idprotection']): ?><label><input type="checkbox" name="idprotection[<?php echo $this->_tpl_vars['num']; ?>
]"<?php if ($this->_tpl_vars['domain']['idprotectionselected']): ?> checked<?php endif; ?> /> <?php echo $this->_tpl_vars['LANG']['domainidprotection']; ?>
 (<?php echo $this->_tpl_vars['domain']['idprotectionprice']; ?>
)</label><?php endif; ?>
				</td>
			</tr>
			<?php endif; ?>
			
			<?php $_from = $this->_tpl_vars['domain']['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domainfieldname'] => $this->_tpl_vars['domainfield']):
?>
			<tr>
				<th><?php echo $this->_tpl_vars['domainfieldname']; ?>
:</th>
				<td><?php echo $this->_tpl_vars['domainfield']; ?>
</td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
		</tbody>
	</table>
	
	</div>
	<?php endforeach; endif; unset($_from); ?>

	<p class="text-center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['updatecart']; ?>
" class="btn" /></p>

	</form>
</div>

<div class="col-md-2"></div>
<div class="clear"></div>
</div>