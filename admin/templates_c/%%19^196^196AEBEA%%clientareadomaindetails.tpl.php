<?php /* Smarty version 2.6.28, created on 2015-04-27 17:34:24
         compiled from /home/generali/public_html/admin/templates/classic/clientareadomaindetails.tpl */ ?>
<?php echo '<script language="javascript">
function usedefaultns() {
    jQuery(".domnsinputs").attr("disabled", true);
}
function usecustomns() {
    jQuery(".domnsinputs").removeAttr("disabled");
}
jQuery(document).ready(function(){
'; ?>
<?php if ($this->_tpl_vars['defaultns']): ?>usedefaultns();
<?php endif; ?>
<?php echo '});
</script>'; ?>


<p class="heading2"><?php echo $this->_tpl_vars['LANG']['clientareanavdomains']; ?>
</p>

<table cellspacing="1" cellpadding="0" class="frame"><tr><td>

<table width="100%" cellpadding="2">
<tr><td class="fieldarea" width="150"><?php echo $this->_tpl_vars['LANG']['clientareahostingregdate']; ?>
:</td><td><?php echo $this->_tpl_vars['registrationdate']; ?>
</td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['clientareahostingdomain']; ?>
:</td><td><a href="http://<?php echo $this->_tpl_vars['domain']; ?>
" target="_blank"><?php echo $this->_tpl_vars['domain']; ?>
</a></td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['orderpaymentmethod']; ?>
:</td><td><?php echo $this->_tpl_vars['paymentmethod']; ?>
</td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['firstpaymentamount']; ?>
:</td><td><?php echo $this->_tpl_vars['firstpaymentamount']; ?>
</td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['recurringamount']; ?>
:</td><td><?php echo $this->_tpl_vars['recurringamount']; ?>
</td></tr>
<?php if ($this->_tpl_vars['recreatesubscriptionbutton']): ?><tr><td></td><td><?php echo $this->_tpl_vars['recreatesubscriptionbutton']; ?>
</td></tr><?php endif; ?>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['clientareahostingnextduedate']; ?>
:</td><td><?php echo $this->_tpl_vars['nextduedate']; ?>
</td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['clientarearegistrationperiod']; ?>
:</td><td><?php echo $this->_tpl_vars['registrationperiod']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderyears']; ?>
</td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['clientareastatus']; ?>
:</td><td><?php echo $this->_tpl_vars['status']; ?>
</td></tr>
</table>

</td></tr></table>

<br />

<div align="center"><?php echo $this->_tpl_vars['moduleclientarea']; ?>
</div>

<?php if ($this->_tpl_vars['status'] == $this->_tpl_vars['LANG']['clientareaactive']): ?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails">
<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<p><strong>&nbsp;&raquo;&nbsp;&nbsp;<?php echo $this->_tpl_vars['LANG']['domainsautorenew']; ?>
</strong></p>
<?php if ($this->_tpl_vars['donotrenew']): ?><div class="errorbox"><?php echo $this->_tpl_vars['LANG']['domainsautorenewdisabledwarning']; ?>
</div><br><?php endif; ?>
<p><?php echo $this->_tpl_vars['LANG']['domainsautorenewstatus']; ?>
: <?php if ($this->_tpl_vars['donotrenew']): ?><?php echo $this->_tpl_vars['LANG']['domainsautorenewdisabled']; ?>
 &nbsp;&nbsp;&nbsp; <input type="hidden" name="autorenew" value="enable"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domainsautorenewenable']; ?>
" class="buttongo" /><?php else: ?><?php echo $this->_tpl_vars['LANG']['domainsautorenewenabled']; ?>
 &nbsp;&nbsp;&nbsp; <input type="hidden" name="autorenew" value="disable"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domainsautorenewdisable']; ?>
" class="buttonwarn" /><?php endif; ?></p>
</form>

<?php if ($this->_tpl_vars['managens']): ?>

<p><strong>&nbsp;&raquo;&nbsp;&nbsp;<?php echo $this->_tpl_vars['LANG']['domainnameservers']; ?>
</strong></p>

<?php if ($this->_tpl_vars['error']): ?><div class="errorbox"><?php echo $this->_tpl_vars['error']; ?>
</div><br /><?php endif; ?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails">
<input type="hidden" name="sub" value="savens">
<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
">

<p><input type="radio" name="nschoice" value="default" id="nschoicedefault" onclick="usedefaultns()"<?php if ($this->_tpl_vars['defaultns']): ?> checked<?php endif; ?> /> <label for="nschoicedefault"><?php echo $this->_tpl_vars['LANG']['nschoicedefault']; ?>
</label><br />
<input type="radio" name="nschoice" value="custom" id="nschoicecustom" onclick="usecustomns()"<?php if (! $this->_tpl_vars['defaultns']): ?> checked<?php endif; ?> /> <label for="nschoicecustom"><?php echo $this->_tpl_vars['LANG']['nschoicecustom']; ?>
</label></p>

<table cellspacing="1" cellpadding="0" class="frame"><tr><td>
<table width="100%" cellpadding="2">
<tr><td class="fieldarea" width="150"><?php echo $this->_tpl_vars['LANG']['domainnameserver1']; ?>
:</td><td><input type="text" name="ns1" value="<?php echo $this->_tpl_vars['ns1']; ?>
" size="40" class="domnsinputs" /></td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['domainnameserver2']; ?>
:</td><td><input type="text" name="ns2" value="<?php echo $this->_tpl_vars['ns2']; ?>
" size="40" class="domnsinputs" /></td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['domainnameserver3']; ?>
:</td><td><input type="text" name="ns3" value="<?php echo $this->_tpl_vars['ns3']; ?>
" size="40" class="domnsinputs" /></td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['domainnameserver4']; ?>
:</td><td><input type="text" name="ns4" value="<?php echo $this->_tpl_vars['ns4']; ?>
" size="40" class="domnsinputs" /></td></tr>
<tr><td class="fieldarea"><?php echo $this->_tpl_vars['LANG']['domainnameserver5']; ?>
:</td><td><input type="text" name="ns5" value="<?php echo $this->_tpl_vars['ns5']; ?>
" size="40" class="domnsinputs" /></td></tr>
</table>
</td></tr></table>
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareasavechanges']; ?>
" class="buttongo" /></p>
</form>

<?php endif; ?>

<?php if ($this->_tpl_vars['lockstatus']): ?>
<?php if ($this->_tpl_vars['tld'] != "co.uk" && $this->_tpl_vars['tld'] != "org.uk" && $this->_tpl_vars['tld'] != "ltd.uk" && $this->_tpl_vars['tld'] != "plc.uk" && $this->_tpl_vars['tld'] != "me.uk"): ?>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails">
<input type="hidden" name="sub" value="savereglock">
<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<p><strong>&nbsp;&raquo;&nbsp;&nbsp;<?php echo $this->_tpl_vars['LANG']['domainregistrarlock']; ?>
</strong></p>
<table cellspacing="1" cellpadding="0" class="frame"><tr><td>
<table width="100%" cellpadding="2">
<tr><td class="fieldarea" width="150"><?php echo $this->_tpl_vars['LANG']['domainregistrarlock']; ?>
:</td><td><input type="checkbox" name="reglock"<?php if ($this->_tpl_vars['lockstatus'] == 'locked'): ?> checked<?php endif; ?>> <?php echo $this->_tpl_vars['LANG']['domainregistrarlockdesc']; ?>
</td></tr>
</table>
</td></tr></table>
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['clientareasavechanges']; ?>
" class="buttongo" /></p>
</form>
<?php endif; ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['releasedomain']): ?>
<p><strong>&nbsp;&raquo;&nbsp;&nbsp;<?php echo $this->_tpl_vars['LANG']['domainrelease']; ?>
</strong></p>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindetails">
<input type="hidden" name="sub" value="releasedomain">
<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<table cellspacing="1" cellpadding="0" class="frame"><tr><td>
<table width="100%" cellpadding="2">
<tr><td class="fieldarea" width="150"><?php echo $this->_tpl_vars['LANG']['domainreleasetag']; ?>
:</td><td><input type="text" name="transtag" size="20" /> <?php echo $this->_tpl_vars['LANG']['domainreleasedescription']; ?>
</td></tr>
</table>
</td></tr></table>
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domainrelease']; ?>
" class="buttonwarn" /></p>
</form>
<?php endif; ?>

<?php endif; ?>

<p><strong>&nbsp;&raquo;&nbsp;&nbsp;<?php echo $this->_tpl_vars['LANG']['domainmanagementtools']; ?>
</strong></p>

<table align="center"><tr>
<?php if ($this->_tpl_vars['renew']): ?><td><form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domainrenew">
<input type="hidden" name="domainid" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domainrenew']; ?>
" class="button" /></p>
</form></td><?php endif; ?>
<?php if ($this->_tpl_vars['managecontacts']): ?><td><form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaincontacts">
<input type="hidden" name="domainid" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domaincontactinfo']; ?>
" class="button" /></p>
</form></td><?php endif; ?>
<?php if ($this->_tpl_vars['emailforwarding']): ?><td><form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domainemailforwarding">
<input type="hidden" name="domainid" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domainemailforwarding']; ?>
" class="button" /></p>
</form></td><?php endif; ?>
<?php if ($this->_tpl_vars['dnsmanagement']): ?><td><form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaindns">
<input type="hidden" name="domainid" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domaindnsmanagement']; ?>
" class="button" /></p>
</form></td><?php endif; ?>
<?php if ($this->_tpl_vars['getepp']): ?><td><form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domaingetepp">
<input type="hidden" name="domainid" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domaingeteppcode']; ?>
" class="button" /></p>
</form></td><?php endif; ?>
<?php if ($this->_tpl_vars['registerns']): ?><td><form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?action=domainregisterns">
<input type="hidden" name="domainid" value="<?php echo $this->_tpl_vars['domainid']; ?>
">
<p align="center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['domainregisterns']; ?>
" class="button" /></p>
</form></td><?php endif; ?>
</tr></table>

<p align="center"><input type="button" value="<?php echo $this->_tpl_vars['LANG']['clientareabacklink']; ?>
" onclick="window.location='clientarea.php?action=domains'" class="button" /></p>