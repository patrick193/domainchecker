<?php /* Smarty version 2.6.28, created on 2015-04-10 12:31:10
         compiled from /home/generali/public_html/admin/templates/bamat/knowledgebasearticle.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['knowledgebasetitle'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script language="javascript">
function addBookmark() {
    if (window.sidebar) {
        window.sidebar.addPanel('<?php echo $this->_tpl_vars['companyname']; ?>
 - <?php echo $this->_tpl_vars['kbarticle']['title']; ?>
', location.href,"");
    } else if( document.all ) {
        window.external.AddFavorite( location.href, '<?php echo $this->_tpl_vars['companyname']; ?>
 - <?php echo $this->_tpl_vars['kbarticle']['title']; ?>
');
    } else if( window.opera && window.print ) {
        return true;
    }
}
</script>

<p class="breadcrumbnav"><?php echo $this->_tpl_vars['breadcrumbnav']; ?>
</p> 

<br />

<h2><?php echo $this->_tpl_vars['kbarticle']['title']; ?>
</h2>

<br />

<blockquote>
<br /><br />
<?php echo $this->_tpl_vars['kbarticle']['text']; ?>

<br /><br />
</blockquote>

<form method="post" action="knowledgebase.php?action=displayarticle&amp;id=<?php echo $this->_tpl_vars['kbarticle']['id']; ?>
&amp;useful=vote">
<p>
<?php if ($this->_tpl_vars['kbarticle']['voted']): ?>
<strong><?php echo $this->_tpl_vars['LANG']['knowledgebaserating']; ?>
</strong> <?php echo $this->_tpl_vars['kbarticle']['useful']; ?>
 <?php echo $this->_tpl_vars['LANG']['knowledgebaseratingtext']; ?>
 (<?php echo $this->_tpl_vars['kbarticle']['votes']; ?>
 <?php echo $this->_tpl_vars['LANG']['knowledgebasevotes']; ?>
)
<?php else: ?>
<strong><?php echo $this->_tpl_vars['LANG']['knowledgebasehelpful']; ?>
</strong> <select name="vote"><option value="yes"><?php echo $this->_tpl_vars['LANG']['knowledgebaseyes']; ?>
</option><option value="no"><?php echo $this->_tpl_vars['LANG']['knowledgebaseno']; ?>
</option></select> <input type="submit" value="<?php echo $this->_tpl_vars['LANG']['knowledgebasevote']; ?>
" class="btn" />
<?php endif; ?>
</p>
</form>

<p><img src="images/addtofavouritesicon.gif" class="text-center" alt="<?php echo $this->_tpl_vars['LANG']['knowledgebasefavorites']; ?>
" /> <a href="#" onClick="addBookmark();return false"><?php echo $this->_tpl_vars['LANG']['knowledgebasefavorites']; ?>
</a> &nbsp;&nbsp; <img src="images/print.gif" class="text-center" alt="<?php echo $this->_tpl_vars['LANG']['knowledgebaseprint']; ?>
" /> <a href="#" onclick="window.print();return false"><?php echo $this->_tpl_vars['LANG']['knowledgebaseprint']; ?>
</a></p>

<?php if ($this->_tpl_vars['kbarticles']): ?>

<div class="kbalsoread"><?php echo $this->_tpl_vars['LANG']['knowledgebasealsoread']; ?>
</div>

<?php $_from = $this->_tpl_vars['kbarticles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['kbarticle']):
?>
<div class="kbarticle">
<img src="images/article.gif" class="text-center" alt="" /> <strong><a href="<?php if ($this->_tpl_vars['seofriendlyurls']): ?>knowledgebase/<?php echo $this->_tpl_vars['kbarticle']['id']; ?>
/<?php echo $this->_tpl_vars['kbarticle']['urlfriendlytitle']; ?>
.html<?php else: ?>knowledgebase.php?action=displayarticle&amp;id=<?php echo $this->_tpl_vars['kbarticle']['id']; ?>
<?php endif; ?>"><?php echo $this->_tpl_vars['kbarticle']['title']; ?>
</a></strong> <span class="kbviews">(<?php echo $this->_tpl_vars['LANG']['knowledgebaseviews']; ?>
: <?php echo $this->_tpl_vars['kbarticle']['views']; ?>
)</span>
</div>
<?php endforeach; endif; unset($_from); ?>

<?php endif; ?>

<br />