<?php /* Smarty version 2.6.28, created on 2015-04-09 10:37:24
         compiled from /home/generali/public_html/admin/templates/orderforms/bamatcart/configureproductdomain.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'sprintf2', '/home/generali/public_html/admin/templates/orderforms/bamatcart/configureproductdomain.tpl', 20, false),array('modifier', 'substr', '/home/generali/public_html/admin/templates/orderforms/bamatcart/configureproductdomain.tpl', 93, false),array('function', 'dc_domain_check_form', '/home/generali/public_html/admin/templates/orderforms/bamatcart/configureproductdomain.tpl', 35, false),)), $this); ?>
<div id="order-comparison">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "orderforms/bamatcart/comparisonsteps.tpl", 'smarty_include_vars' => array('step' => 1)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<p><?php echo $this->_tpl_vars['LANG']['cartproductdomaindesc']; ?>
</p>

<?php $this->assign('show_domainoptions', 0); ?>
<?php if ($this->_tpl_vars['incartdomains'] || $this->_tpl_vars['owndomainenabled'] || $this->_tpl_vars['subdomains']): ?>
	<?php $this->assign('show_domainoptions', 1); ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['show_domainoptions'] == 1): ?>
	<div class="domainoptions">
	<div><label><input type="radio" name="domainoption" value="regtransfer" /> <?php echo $this->_tpl_vars['LANG']['domain_check']['cartregisterortransfer']; ?>
</label></div>
	<?php if ($this->_tpl_vars['incartdomains']): ?>
	<div><label><input type="radio" name="domainoption" value="incart" /> <?php echo $this->_tpl_vars['LANG']['cartproductdomainuseincart']; ?>
</label></div>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['owndomainenabled']): ?>
	<div><label><input type="radio" name="domainoption" value="owndomain" /> <?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['cartexistingdomainchoice'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, $this->_tpl_vars['companyname']) : smarty_modifier_sprintf2($_tmp, $this->_tpl_vars['companyname'])); ?>
</label></div>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['subdomains']): ?>
	<div><label><input type="radio" name="domainoption" value="subdomain" /> <?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['cartsubdomainchoice'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, $this->_tpl_vars['companyname']) : smarty_modifier_sprintf2($_tmp, $this->_tpl_vars['companyname'])); ?>
</label></div>
	<?php endif; ?>
	</div>
<?php else: ?>
	<input type="hidden" name="domainoption" value="regtransfer" />
<?php endif; ?>


<br />

<div class="wrap_domain">
	<div class="domainreginput" id="regtransfer_wrap">
		<?php echo smarty_function_dc_domain_check_form(array('productdomain' => true), $this);?>

	</div>

	<form class="dc_form dc_add_form" id="checkdomain_form">

	<div class="dc_wrap">
		<div class="domainreginput" id="domainincart">
			<div class="dc_wrap_elements">
				<select id="incartsld">
					<?php $_from = $this->_tpl_vars['incartdomains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['incartdomain']):
?>
					<option value="<?php echo $this->_tpl_vars['incartdomain']; ?>
"><?php echo $this->_tpl_vars['incartdomain']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>

			</div>

			<input type="submit" value="<?php echo $this->_tpl_vars['LANG']['ordercontinuebutton']; ?>
" class="btn" />
		</div>
	
		<div class="domainreginput" id="domainregister">
			<div class="dc_wrap_elements">
				<span>www.</span>&nbsp;
				
				<input type="text" id="registersld" size="30" value="<?php echo $this->_tpl_vars['sld']; ?>
" />
				
				<select id="registertld">
				<?php $_from = $this->_tpl_vars['registertlds']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['listtld']):
?>
					<option value="<?php echo $this->_tpl_vars['listtld']; ?>
"<?php if ($this->_tpl_vars['listtld'] == $this->_tpl_vars['tld']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['listtld']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>

			<input type="submit" value="<?php echo $this->_tpl_vars['LANG']['ordercontinuebutton']; ?>
" class="btn" />
		</div>
		
		<div class="domainreginput" id="domaintransfer">
			<div class="dc_wrap_elements">
				<span>www.</span>
			
				<input type="text" id="transfersld" size="30" value="<?php echo $this->_tpl_vars['sld']; ?>
" />
				<select id="transfertld">
					<?php $_from = $this->_tpl_vars['transfertlds']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['listtld']):
?>
					<option value="<?php echo $this->_tpl_vars['listtld']; ?>
"<?php if ($this->_tpl_vars['listtld'] == $this->_tpl_vars['tld']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['listtld']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>

			<input type="submit" value="<?php echo $this->_tpl_vars['LANG']['ordercontinuebutton']; ?>
" class="btn" />
		</div>
		
		<div class="domainreginput" id="domainowndomain">
			<div class="dc_wrap_elements">
				<span>www.</span>

				<input type="text" id="owndomainsld" size="30" value="<?php echo $this->_tpl_vars['sld']; ?>
" />

				<span>&nbsp;.&nbsp;</span>

				<input type="text" id="owndomaintld" size="5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['tld'])) ? $this->_run_mod_handler('substr', true, $_tmp, 1) : substr($_tmp, 1)); ?>
" />

			</div>

			<input type="submit" value="<?php echo $this->_tpl_vars['LANG']['ordercontinuebutton']; ?>
" class="btn" />
		</div>
		
		<div class="domainreginput" id="domainsubdomain">
			<div class="dc_wrap_elements">
				<span>http://</span>

				<input type="text" id="subdomainsld" size="30" value="<?php echo $this->_tpl_vars['sld']; ?>
" />
				
				<select id="subdomaintld">
					<?php $_from = $this->_tpl_vars['subdomains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['subid'] => $this->_tpl_vars['subdomain']):
?>
					<option value="<?php echo $this->_tpl_vars['subid']; ?>
"><?php echo $this->_tpl_vars['subdomain']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</div>

			<input type="submit" value="<?php echo $this->_tpl_vars['LANG']['ordercontinuebutton']; ?>
" class="btn" />
		</div>
		
	</div>
	
	</form>
</div>

<?php if ($this->_tpl_vars['freedomaintlds']): ?><p>* <em><?php echo $this->_tpl_vars['LANG']['orderfreedomainregistration']; ?>
 <?php echo $this->_tpl_vars['LANG']['orderfreedomainappliesto']; ?>
: <?php echo $this->_tpl_vars['freedomaintlds']; ?>
</em></p><?php endif; ?>

<div class="clear"></div>

<div id="loading3" class="loading"><img src="images/loading.gif" border="0" alt="Loading..." /></div>

<form method="post" action="cart.php?a=add&pid=<?php echo $this->_tpl_vars['pid']; ?>
" id="domainfrm">
<div id="domainresults"></div>
</form>

</div>