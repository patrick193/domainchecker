<?php /* Smarty version 2.6.28, created on 2015-06-15 08:52:01
         compiled from /var/www/web_domains/admin/templates/bamat/dc_search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'sprintf2', '/var/www/web_domains/admin/templates/bamat/dc_search.tpl', 58, false),)), $this); ?>
<?php if ($this->_tpl_vars['dc_error'] == 1): ?>
<div class="alert alert-danger textcenter">
    <?php echo $this->_tpl_vars['LANG']['captchaverifyincorrect']; ?>

</div>
<?php endif; ?>

<div class="dc_search">
	<div class="searchdomains">
		<form name="domainsearchform" action="<?php echo $this->_tpl_vars['systemsslurl']; ?>
<?php echo $this->_tpl_vars['dc_target_url']; ?>
" method="get" class="dc_form">
			<?php if ($this->_tpl_vars['dc_is_product']): ?>
				<?php $_from = $this->_tpl_vars['dc_get_vars']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['var_name'] => $this->_tpl_vars['var_value']):
?>
					<input type="hidden" name="<?php echo $this->_tpl_vars['var_name']; ?>
" value="<?php echo $this->_tpl_vars['var_value']; ?>
" />
				<?php endforeach; endif; unset($_from); ?>
			<?php endif; ?>

			<label for="dc_dom"><?php echo $this->_tpl_vars['LANG']['domain_check']['editsearch']; ?>
:</label>
			
			<table class="dc_form_elements">
			<tbody>
				<tr>
					<td><input type="text" class="text-input" maxlength="67" name="dc_dom" id="dc_dom" placeholder="Domainname" value="<?php echo $this->_tpl_vars['dc_dom']; ?>
"></td>

					<td>
						<select class="searchselect" name="dc_cat">
							<?php $_from = $this->_tpl_vars['dc_categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['dc_cat']):
?>
								<?php if ($this->_tpl_vars['dc_cat']['id'] == $this->_tpl_vars['dc_curr_cat']): ?>
									<option value="<?php echo $this->_tpl_vars['dc_cat']['id']; ?>
" selected="selected"><?php echo $this->_tpl_vars['dc_cat']['name']; ?>
</option>
								<?php else: ?>
									<option value="<?php echo $this->_tpl_vars['dc_cat']['id']; ?>
"><?php echo $this->_tpl_vars['dc_cat']['name']; ?>
</option>
								<?php endif; ?>
							<?php endforeach; endif; unset($_from); ?>
						</select>
					</td>
				</tr>

				<?php if ($this->_tpl_vars['use_captcha']): ?>
				<tr class="captcha_row">
					<td class="text-center"><img src="get_captcha.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"/></td>
					<td><input type="text" class="text-input" maxlength="67" name="dc_captcha" id="dc_captcha" placeholder="" value=""></td>
				</tr>

				<tr class="captcha_desc_row text-center">
					<td colspan="2" class="captcha_desc"><?php echo $this->_tpl_vars['LANG']['captchaverify']; ?>
</td>
				</tr>

				<?php endif; ?>
			</tbody>
			</table>
				
				<div class="btn_wrap"><input type="submit" title="<?php echo $this->_tpl_vars['LANG']['domain_check']['check']; ?>
" class="btn btn-large" id="btnPruefen" name="btnPruefen" value="<?php echo $this->_tpl_vars['LANG']['domain_check']['check']; ?>
"></div>
			</div>

			
		</form>
	</div>

	<div class="dc_user_info">
		<p><?php echo $this->_tpl_vars['LANG']['domain_check']['clientcountry']; ?>
: <strong><?php echo $this->_tpl_vars['dc_country']; ?>
</strong>. <?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['domain_check']['clientcurrency'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, "<strong>".($this->_tpl_vars['dc_cur_name'])."</strong>") : smarty_modifier_sprintf2($_tmp, "<strong>".($this->_tpl_vars['dc_cur_name'])."</strong>")); ?>
 <?php if (! $this->_tpl_vars['logged_in']): ?>(<em><a href="<?php echo $this->_tpl_vars['dc_curr_url']; ?>
" class="dashed_bottom"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['domain_check']['currencyswitch'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, $this->_tpl_vars['dc_switch_curr']) : smarty_modifier_sprintf2($_tmp, $this->_tpl_vars['dc_switch_curr'])); ?>
?</a></em>)<?php endif; ?>
		</p>
	</div>
	
</div>