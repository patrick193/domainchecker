<?php /* Smarty version 2.6.28, created on 2015-04-17 17:53:50
         compiled from /home/generali/public_html/admin/templates/bamat/viewticket.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', '/home/generali/public_html/admin/templates/bamat/viewticket.tpl', 7, false),array('function', 'tt_check_google_translate', '/home/generali/public_html/admin/templates/bamat/viewticket.tpl', 119, false),)), $this); ?>
<?php if ($this->_tpl_vars['error']): ?>

<p><?php echo $this->_tpl_vars['LANG']['supportticketinvalid']; ?>
</p>

<?php else: ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['LANG']['supportticketsviewticket'])) ? $this->_run_mod_handler('cat', true, $_tmp, ' #') : smarty_modifier_cat($_tmp, ' #')))) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['tid']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['tid'])))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['errormessage']): ?>
<div class="alert alert-danger">
    <p class="bold"><?php echo $this->_tpl_vars['LANG']['clientareaerrors']; ?>
</p>
    <ul>
        <?php echo $this->_tpl_vars['errormessage']; ?>

    </ul>
</div>
<?php endif; ?>

<h2><?php echo $this->_tpl_vars['subject']; ?>
</h2>

<table class="table table-framed table-centered">
    <thead>
        <tr class="dc_tr_head">
            <th><?php echo $this->_tpl_vars['LANG']['supportticketsubmitted']; ?>
</th>
            <th><?php echo $this->_tpl_vars['LANG']['supportticketsdepartment']; ?>
</th>
            <th><?php echo $this->_tpl_vars['LANG']['supportticketspriority']; ?>
</th>
            <th><?php echo $this->_tpl_vars['LANG']['supportticketsstatus']; ?>
</th>
			
			<?php if ($this->_tpl_vars['customfields']): ?>
				<?php $_from = $this->_tpl_vars['customfields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['customfield']):
?>
				<th><?php echo $this->_tpl_vars['customfield']['name']; ?>
</th>
				<?php endforeach; endif; unset($_from); ?>
			<?php endif; ?>
        </tr>
    </thead>

    <tbody>
		<tr>
			<td class="dc_td_status"><?php echo $this->_tpl_vars['date']; ?>
</td>
			<td class="dc_td_status"><?php echo $this->_tpl_vars['department']; ?>
</td>
			<td class="dc_td_status"><?php echo $this->_tpl_vars['urgency']; ?>
</td>
			<td class="dc_td_status"><?php echo $this->_tpl_vars['status']; ?>
</td>

			<?php if ($this->_tpl_vars['customfields']): ?>
				<?php $_from = $this->_tpl_vars['customfields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['customfield']):
?>
				<td class="dc_td_status"><?php echo $this->_tpl_vars['customfield']['value']; ?>
</td>
				<?php endforeach; endif; unset($_from); ?>
			<?php endif; ?>
		</tr>
		
    </tbody>
</table>

<br>

<p class="ticket-buttons">
	<input type="button" value="<?php echo $this->_tpl_vars['LANG']['clientareabacklink']; ?>
" class="btn" onclick="window.location='supporttickets.php'" id="ticket-back" />
	<?php if ($this->_tpl_vars['showclosebutton']): ?>
		<input type="button" value="<?php echo $this->_tpl_vars['LANG']['supportticketsclose']; ?>
" class="btn btn-danger" onclick="window.location='<?php echo $_SERVER['PHP_SELF']; ?>
?tid=<?php echo $this->_tpl_vars['tid']; ?>
&amp;c=<?php echo $this->_tpl_vars['c']; ?>
&amp;closeticket=true'" id="ticket-close-top" />
	<?php endif; ?>
	<input type="button" value="<?php echo $this->_tpl_vars['LANG']['supportticketsreply']; ?>
" class="btn btn-primary" id="ticket-reply-top" />
</p>

<div id="replyform-top">
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
?tid=<?php echo $this->_tpl_vars['tid']; ?>
&amp;c=<?php echo $this->_tpl_vars['c']; ?>
&amp;postreply=true" enctype="multipart/form-data" class="form-horizontal" id="submitreply" style="display: none;">

    <fieldset class="control-group">

        <div class="row">
            <div class="col-md-6">
                <div class="control-group">
                    <label class="control-label bold" for="name"><?php echo $this->_tpl_vars['LANG']['supportticketsclientname']; ?>
</label>
                    <div class="controls">
                        <?php if ($this->_tpl_vars['loggedin']): ?><input class="input-xlarge disabled" type="text" id="name" value="<?php echo $this->_tpl_vars['clientname']; ?>
" disabled="disabled" /><?php else: ?><input class="input-xlarge" type="text" name="replyname" id="name" value="<?php echo $this->_tpl_vars['replyname']; ?>
" /><?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="control-group">
                    <label class="control-label bold" for="email"><?php echo $this->_tpl_vars['LANG']['supportticketsclientemail']; ?>
</label>
                    <div class="controls">
                        <?php if ($this->_tpl_vars['loggedin']): ?><input class="input-xlarge disabled" type="text" id="email" value="<?php echo $this->_tpl_vars['email']; ?>
" disabled="disabled" /><?php else: ?><input class="input-xlarge" type="text" name="replyemail" id="email" value="<?php echo $this->_tpl_vars['replyemail']; ?>
" /><?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
				<div class="control-group">
					<label class="control-label bold" for="message"><?php echo $this->_tpl_vars['LANG']['contactmessage']; ?>
</label>
					<div class="controls">
						<textarea name="replymessage" id="message" rows="12" class="fullwidth"><?php echo $this->_tpl_vars['replymessage']; ?>
</textarea>
					</div>
				</div>
			</div>
			
            <div class="col-md-12">
				<div class="control-group">
					<label class="control-label bold" for="attachments"><?php echo $this->_tpl_vars['LANG']['supportticketsticketattachments']; ?>
:</label>
					<div class="controls">
						<input type="file" name="attachments[]" style="width:70%;" /><br />
						<div id="fileuploads"></div>
						<a href="#" onclick="extraTicketAttachment();return false"><img src="images/add.gif" class="text-center" border="0" /> <?php echo $this->_tpl_vars['LANG']['addmore']; ?>
</a><br />
						(<?php echo $this->_tpl_vars['LANG']['supportticketsallowedextensions']; ?>
: <?php echo $this->_tpl_vars['allowedfiletypes']; ?>
)
					</div>
				</div>
			</div>
		</div>

    </fieldset>

    <p class="text-center"><input type="submit" value="<?php echo $this->_tpl_vars['LANG']['supportticketsticketsubmit']; ?>
" class="btn btn-primary" /></p>

</form>
</div>

<?php $_from = $this->_tpl_vars['descreplies']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['reply']):
?>

<?php echo smarty_function_tt_check_google_translate(array('reply' => $this->_tpl_vars['reply']), $this);?>

<br>

<div class="ticket-msg <?php if ($this->_tpl_vars['reply']['admin'] && ! $this->_tpl_vars['is_google_translate']): ?>ticket-msg-admin<?php else: ?>ticket-msg-client<?php endif; ?>" id="ticket-msg-<?php echo $this->_tpl_vars['reply']['id']; ?>
">
	<div class="ticket-msg-author-wrap">
		<div class="ticket-msg-author">
			<?php echo $this->_tpl_vars['reply']['name']; ?>

		</div>
        <?php if (! $this->_tpl_vars['is_google_translate']): ?>
		<div class="ticket-msg-position">
			<?php if ($this->_tpl_vars['reply']['admin']): ?>
				<?php echo $this->_tpl_vars['LANG']['supportticketsstaff']; ?>

			<?php elseif ($this->_tpl_vars['reply']['contactid']): ?>
				<?php echo $this->_tpl_vars['LANG']['supportticketscontact']; ?>

			<?php elseif ($this->_tpl_vars['reply']['userid']): ?>
				<?php echo $this->_tpl_vars['LANG']['supportticketsclient']; ?>

			<?php else: ?>
				<?php echo $this->_tpl_vars['reply']['email']; ?>

			<?php endif; ?>
		</div>
		<?php endif; ?>

		<div class="ticket-msg-date"><?php echo $this->_tpl_vars['reply']['date']; ?>
</div>
	</div>
	
	<div class="ticket-text-wrap">
	
		<div class="ticket-text">
		<?php echo $this->_tpl_vars['reply']['message']; ?>

		</div>

		<?php if ($this->_tpl_vars['reply']['attachments']): ?>
		<div class="ticket-attachments">
			<div class="ticket-attachments-title"><?php echo $this->_tpl_vars['LANG']['supportticketsticketattachments']; ?>
:</div>

			<div class="ticket-attachments-files">
				<?php $_from = $this->_tpl_vars['reply']['attachments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['attachment']):
?>
				<a href="dl.php?type=<?php if ($this->_tpl_vars['reply']['id']): ?>ar&id=<?php echo $this->_tpl_vars['reply']['id']; ?>
<?php else: ?>a&id=<?php echo $this->_tpl_vars['id']; ?>
<?php endif; ?>&i=<?php echo $this->_tpl_vars['num']; ?>
"><span class="glyphicon glyphicon-file"></span> <?php echo $this->_tpl_vars['attachment']; ?>
</a><br />
				<?php endforeach; endif; unset($_from); ?>
			</div>
		</div>
		<?php endif; ?>

        <?php if ($this->_tpl_vars['reply']['id'] && $this->_tpl_vars['reply']['admin'] && $this->_tpl_vars['ratingenabled'] && ! $this->_tpl_vars['is_google_translate']): ?>
		<div class="ticket-rating">
			<?php if ($this->_tpl_vars['reply']['rating']): ?>
				<span class="ticket-rating-caption"><?php echo $this->_tpl_vars['LANG']['ticketreatinggiven']; ?>
: </span>
				
				<span class="ticket-rating-star">
					<?php $_from = $this->_tpl_vars['ratings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rating']):
?>
					<span class="ticket-rating-img ticket-rating-<?php if ($this->_tpl_vars['reply']['rating'] >= $this->_tpl_vars['rating']): ?>pos<?php else: ?>neg<?php endif; ?>">&nbsp;</span>
					<?php endforeach; endif; unset($_from); ?>
				</span>
			<?php else: ?>
				<span class="ticket-rating-caption"><?php echo $this->_tpl_vars['LANG']['ticketratingquestion']; ?>
 </span>
				<span class="ticket-rating-caption ticket-rating-mark-neg" data-reply-id="<?php echo $this->_tpl_vars['reply']['id']; ?>
" data-ticket-id="<?php echo $this->_tpl_vars['tid']; ?>
" data-ticket-c="<?php echo $this->_tpl_vars['c']; ?>
"><?php echo $this->_tpl_vars['LANG']['ticketratingpoor']; ?>
</span>

				<span class="ticket-rating-stars-active" data-reply-id="<?php echo $this->_tpl_vars['reply']['id']; ?>
">
					<?php $_from = $this->_tpl_vars['ratings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rating']):
?>
					<span class="ticket-rating-img-active ticket-rating-neg" id="ticket-rating-<?php echo $this->_tpl_vars['reply']['id']; ?>
-<?php echo $this->_tpl_vars['rating']; ?>
" data-reply-id="<?php echo $this->_tpl_vars['reply']['id']; ?>
" data-rating="<?php echo $this->_tpl_vars['rating']; ?>
" data-ticket-id="<?php echo $this->_tpl_vars['tid']; ?>
" data-ticket-c="<?php echo $this->_tpl_vars['c']; ?>
">&nbsp;</span>
					<?php endforeach; endif; unset($_from); ?>
				</span>

				<span class="ticket-rating-caption ticket-rating-mark-pos" data-reply-id="<?php echo $this->_tpl_vars['reply']['id']; ?>
" data-ticket-id="<?php echo $this->_tpl_vars['tid']; ?>
" data-ticket-c="<?php echo $this->_tpl_vars['c']; ?>
"><?php echo $this->_tpl_vars['LANG']['ticketratingexcellent']; ?>
</span>

			<?php endif; ?>
		</div>
		<?php endif; ?>
	</div>
	
</div>
<div class="clear"></div>
<?php endforeach; endif; unset($_from); ?>
	
<br>
<br>

<p class="ticket-buttons">
	<input type="button" value="<?php echo $this->_tpl_vars['LANG']['clientareabacklink']; ?>
" class="btn" onclick="window.location='supporttickets.php'" id="ticket-back" />
	<?php if ($this->_tpl_vars['showclosebutton']): ?>
		<input type="button" value="<?php echo $this->_tpl_vars['LANG']['supportticketsclose']; ?>
" class="btn btn-danger" onclick="window.location='<?php echo $_SERVER['PHP_SELF']; ?>
?tid=<?php echo $this->_tpl_vars['tid']; ?>
&amp;c=<?php echo $this->_tpl_vars['c']; ?>
&amp;closeticket=true'" id="ticket-close-bottom" />
	<?php endif; ?>
	<input type="button" value="<?php echo $this->_tpl_vars['LANG']['supportticketsreply']; ?>
" class="btn btn-primary" id="ticket-reply-bottom" />
</p>

<div id="replyform-bottom">
</div>

<?php endif; ?>