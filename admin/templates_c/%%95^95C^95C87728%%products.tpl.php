<?php /* Smarty version 2.6.28, created on 2015-04-21 09:14:49
         compiled from /home/generali/public_html/admin/templates/orderforms/bamatcart/products.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'dc_set_user_info', '/home/generali/public_html/admin/templates/orderforms/bamatcart/products.tpl', 14, false),array('function', 'dc_prepare_product_features', '/home/generali/public_html/admin/templates/orderforms/bamatcart/products.tpl', 25, false),array('modifier', 'sprintf2', '/home/generali/public_html/admin/templates/orderforms/bamatcart/products.tpl', 16, false),)), $this); ?>
<div id="order-comparison">

<h1><?php echo $this->_tpl_vars['LANG']['cartbrowse']; ?>
</h1>

<!-- Product group name -->
<?php $_from = $this->_tpl_vars['productgroups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['productgroup']):
?>
<?php if ($this->_tpl_vars['gid'] == $this->_tpl_vars['productgroup']['gid']): ?>
<h2 class="dc_product_group">Webhosting</h2>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

<!-- Currency switcher -->
<?php if (! $this->_tpl_vars['loggedin'] && $this->_tpl_vars['currencies']): ?>
<?php echo smarty_function_dc_set_user_info(array(), $this);?>

<div class="dc_currency_switcher">
	<p><?php echo $this->_tpl_vars['LANG']['domain_check']['clientcountry']; ?>
: <strong><?php echo $this->_tpl_vars['dc_country']; ?>
</strong>. <?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['domain_check']['clientcurrency'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, "<strong>".($this->_tpl_vars['dc_cur_name'])."</strong>") : smarty_modifier_sprintf2($_tmp, "<strong>".($this->_tpl_vars['dc_cur_name'])."</strong>")); ?>
 (<em><a href="cart.php?gid=<?php echo $this->_tpl_vars['gid']; ?>
&currency=<?php echo $this->_tpl_vars['dc_switch_curr_id']; ?>
" class="dashed_bottom"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['domain_check']['currencyswitch'])) ? $this->_run_mod_handler('sprintf2', true, $_tmp, $this->_tpl_vars['dc_switch_curr']) : smarty_modifier_sprintf2($_tmp, $this->_tpl_vars['dc_switch_curr'])); ?>
?</a></em>)</p>
</div>

<div class="clear"></div>
<?php endif; ?>

<br>

<!-- First category of features is considered "Top Features" -->
<?php echo smarty_function_dc_prepare_product_features(array('products' => $this->_tpl_vars['products']), $this);?>


<div class="product-row product-row-0">

<?php if (count ( $this->_tpl_vars['dc_features'] )): ?>
	<?php if (count ( $this->_tpl_vars['dc_features']['0'] )): ?>
		<?php if (count ( $this->_tpl_vars['dc_features']['0']['features'] )): ?>
		<div class="productfeature-wrap">
			<div class="productfeature">
				<div class="productfeature-head">
					<img src="http://admin.web-domains.local.com/templates/orderforms/bamatcart/images/schwarm-schatten.jpg" alt="features">
				</div>
					
				<?php $this->assign('findex', 0); ?>
				
				<?php $_from = $this->_tpl_vars['dc_features']['0']['features']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['feature']):
?>
					<div class="productfeature-title productfeature-title-<?php echo $this->_tpl_vars['findex']; ?>
"><span class="productfeature-cell-text"><?php echo $this->_tpl_vars['feature']['feature']; ?>
</span></div>
					<?php $this->assign('findex', $this->_tpl_vars['findex']+1); ?>
				<?php endforeach; endif; unset($_from); ?>

				<div class="productfeature-footer">
					<a class="product-button" href="#detailed_features">Alle Pakete im Vergleich</a>
				</div>
			</div>

			<div class="productcol-footer-img">&nbsp;</div>
		</div>
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>

<!-- "Top Features" for products -->
<?php $this->assign('pindex', 0); ?>

<?php $_from = $this->_tpl_vars['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['product']):
?>
<div class="productcol-wrap productcol-wrap-<?php echo $this->_tpl_vars['pindex']; ?>
">
	<div class="productcol">
		<div class="productcol-head">
			<div class="productcol-head-top">
				<h3><?php echo $this->_tpl_vars['product']['name']; ?>
</h3>
			</div>

			<div class="productcol-head-circle-wrap">
				<div class="productcol-head-circle">
					<span><?php echo $this->_tpl_vars['currency']['code']; ?>
</span>
					<?php echo $this->_tpl_vars['dc_prices'][$this->_tpl_vars['num']]; ?>

					<span>/ Monat</span>
				</div>
			</div>

			<div class="productcol-head-bottom">
			</div>
		</div>

		<?php $this->assign('findex', 0); ?>
		<?php $_from = $this->_tpl_vars['dc_features']['0']['features']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['feature']):
?>
		<div class="productcol-feature productcol-feature-<?php echo $this->_tpl_vars['findex']; ?>
">
			<span class="productcol-feature-cell-text"><span class="productcol-feature-cell-title"><?php echo $this->_tpl_vars['feature']['feature']; ?>
:</span> <?php echo $this->_tpl_vars['feature']['values'][$this->_tpl_vars['num']]; ?>
</span>
		</div>
		<?php $this->assign('findex', $this->_tpl_vars['findex']+1); ?>
		<?php endforeach; else: ?>
		<div class="productcol-description">
			<?php echo $this->_tpl_vars['product']['description']; ?>

		</div>
		<?php endif; unset($_from); ?>

		<div class="productcol-footer">
			<a class="product-button" href="//<?php echo $_SERVER['HTTP_HOST']; ?>
<?php echo $_SERVER['PHP_SELF']; ?>
?a=add&<?php if ($this->_tpl_vars['product']['bid']): ?>bid=<?php echo $this->_tpl_vars['product']['bid']; ?>
<?php else: ?>pid=<?php echo $this->_tpl_vars['product']['pid']; ?>
<?php endif; ?>">Bestellen</a>
		</div>
	</div>

	<div class="productcol-footer-img">&nbsp;</div>
</div>

<?php $this->assign('pindex', $this->_tpl_vars['pindex']+1); ?>
<?php endforeach; endif; unset($_from); ?>
</div>

<!-- Detailed categories with the full list of features -->
<?php $this->assign('cindex', 0); ?>

<div class="dc_feature_tabs" id="detailed_features">

<?php $_from = $this->_tpl_vars['dc_features']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['category']):
?>
	<?php if ($this->_tpl_vars['category']['cat'] != ' '): ?>
	<div class="dc_feature_tab dc_feature_tab-<?php echo $this->_tpl_vars['cindex']; ?>
 <?php if ($this->_tpl_vars['cindex'] == 0): ?>active<?php else: ?>inactive<?php endif; ?>">

		<div class="dc_feature_tab_title" data-tab-index="<?php echo $this->_tpl_vars['cindex']; ?>
">
			<span class="dc_tab_icon dc_tab_icon_plus">+</span>
			<span class="dc_tab_icon dc_tab_icon_minus">-</span>
			<?php echo $this->_tpl_vars['category']['cat']; ?>
</div>
		
		<div class="dc_feature_tab_content">
			<ul class="dc_feature_table dc_feature_captions">
				<li class="dc_feature_item-0"><?php echo $this->_tpl_vars['category']['cat']; ?>
</li>

				<?php $this->assign('findex', 1); ?>
				<?php $_from = $this->_tpl_vars['category']['features']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['feature']):
?>
				<li class="dc_feature_item-<?php echo $this->_tpl_vars['findex']; ?>
"><?php echo $this->_tpl_vars['feature']['feature']; ?>
</li>
				<?php $this->assign('findex', $this->_tpl_vars['findex']+1); ?>
				<?php endforeach; endif; unset($_from); ?>
			</ul>
			
			<?php $_from = $this->_tpl_vars['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['product']):
?>
			<ul class="dc_feature_table">
				<li class="dc_feature_item-0"><?php echo $this->_tpl_vars['product']['name']; ?>
</li>
				
				<?php $this->assign('findex', 1); ?>
				<?php $_from = $this->_tpl_vars['category']['features']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['feature']):
?>
				<li class="dc_feature_item-<?php echo $this->_tpl_vars['findex']; ?>
"><span class="feature_title"><?php echo $this->_tpl_vars['feature']['feature']; ?>
: </span><?php echo $this->_tpl_vars['feature']['values'][$this->_tpl_vars['num']]; ?>
</li>
				<?php $this->assign('findex', $this->_tpl_vars['findex']+1); ?>
				<?php endforeach; endif; unset($_from); ?>
			</ul>
			<?php endforeach; endif; unset($_from); ?>

			<div class="clear"></div>
		</div>

		<?php $this->assign('cindex', $this->_tpl_vars['cindex']+1); ?>
	</div>
	<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

</div>

<div class="clear"></div>

</div>