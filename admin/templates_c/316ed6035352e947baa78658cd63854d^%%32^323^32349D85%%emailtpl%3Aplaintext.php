<?php /* Smarty version 2.6.28, created on 2015-04-23 18:29:38
         compiled from emailtpl:plaintext */ ?>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>As you requested, your password for our client area has now been reset.</p>
<p>If it was not at your request, then please contact support immediately.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>