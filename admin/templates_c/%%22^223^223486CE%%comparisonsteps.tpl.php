<?php /* Smarty version 2.6.28, created on 2015-04-09 10:36:36
         compiled from orderforms/bamatcart/comparisonsteps.tpl */ ?>
<?php if ($this->_tpl_vars['step'] == 1): ?>
	<?php $this->assign('progress', 33); ?>
<?php elseif ($this->_tpl_vars['step'] == 2): ?>
	<?php $this->assign('progress', 66); ?>
<?php else: ?>
	<?php $this->assign('progress', 100); ?>
<?php endif; ?>

<div class="bamatsteps">
	<div class="progress reverse">
		<div class="progress-bar progress-bar-striped active" role="progressbar" style="width: <?php echo $this->_tpl_vars['progress']; ?>
%"></div>
	</div>

	<!-- <hr> -->
	
	<div class="stepwrap <?php if ($this->_tpl_vars['step'] == 1): ?>current<?php endif; ?>" id="step_domain">
		<div class="stepbox">
			<span class="glyphicon glyphicon-screenshot"></span>
		</div>
		<span class="step_label">1. <?php echo $this->_tpl_vars['LANG']['cartproductdomainchoose']; ?>
</span>
	</div>

	<div class="stepwrap <?php if ($this->_tpl_vars['step'] == 2): ?>current<?php endif; ?>" id="step_options">
		<div class="stepbox">
			<span class="glyphicon glyphicon-check"></span>
		</div>
		<span class="step_label">2. <?php echo $this->_tpl_vars['LANG']['cartproductchooseoptions']; ?>
</span>
	</div>

	<div class="stepwrap <?php if ($this->_tpl_vars['step'] == 3): ?>current<?php endif; ?>" id="step_checkout">
		<div class="stepbox">
			<span class="glyphicon glyphicon-shopping-cart"></span>
		</div>
		<span class="step_label">3. <?php echo $this->_tpl_vars['LANG']['cartreviewcheckout']; ?>
</span>
	</div>
	
</div>