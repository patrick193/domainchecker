<?php /* Smarty version 2.6.28, created on 2015-04-23 18:29:38
         compiled from emailtpl:emailmessage */ ?>
<p><a href="<?php echo $this->_tpl_vars['company_domain']; ?>
" target="_blank"><img src="<?php echo $this->_tpl_vars['company_logo_url']; ?>
" alt="<?php echo $this->_tpl_vars['company_name']; ?>
" border="0" /></a></p>
<p>Dear <?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>As you requested, your password for our client area has now been reset.</p>
<p>If it was not at your request, then please contact support immediately.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>