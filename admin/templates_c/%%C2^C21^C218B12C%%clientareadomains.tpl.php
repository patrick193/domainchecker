<?php /* Smarty version 2.6.28, created on 2015-04-24 18:46:07
         compiled from /home/generali/public_html/admin/templates/bamat/clientareadomains.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/pageheader.tpl", 'smarty_include_vars' => array('title' => $this->_tpl_vars['LANG']['clientareanavdomains'],'desc' => $this->_tpl_vars['LANG']['clientareadomainsintro'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="searchbox">
    <form method="post" action="clientarea.php?action=domains">
        <div class="input-append">
            <input type="text" name="q" value="<?php if ($this->_tpl_vars['q']): ?><?php echo $this->_tpl_vars['q']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['searchenterdomain']; ?>
<?php endif; ?>" class="input-medium appendedInputButton" onfocus="if(this.value=='<?php echo $this->_tpl_vars['LANG']['searchenterdomain']; ?>
')this.value=''" /><button type="submit" class="btn"><?php echo $this->_tpl_vars['LANG']['searchfilter']; ?>
</button>
        </div>
    </form>
</div>

<div class="resultsbox">
<p><?php echo $this->_tpl_vars['numitems']; ?>
 <?php echo $this->_tpl_vars['LANG']['recordsfound']; ?>
, <?php echo $this->_tpl_vars['LANG']['page']; ?>
 <?php echo $this->_tpl_vars['pagenumber']; ?>
 <?php echo $this->_tpl_vars['LANG']['pageof']; ?>
 <?php echo $this->_tpl_vars['totalpages']; ?>
</p>
</div>

<div class="clear"></div>
<?php echo '
<script>
$(document).ready(function() {
    $(".setbulkaction").click(function(event) {
      event.preventDefault();
      $("#bulkaction").val($(this).attr(\'id\'));
      $("#bulkactionform").submit();
    });
});
</script>
'; ?>

<form method="post" id="bulkactionform" action="clientarea.php?action=bulkdomain">
<input id="bulkaction" name="update" type="hidden" />

<table class="table table-striped table-framed">
    <thead>
        <tr>
            <th class="textcenter"><input type="checkbox" onclick="toggleCheckboxes('domids')" /></th>
            <th<?php if ($this->_tpl_vars['orderby'] == 'domain'): ?> class="headerSort<?php echo $this->_tpl_vars['sort']; ?>
"<?php endif; ?>><a href="clientarea.php?action=domains<?php if ($this->_tpl_vars['q']): ?>&q=<?php echo $this->_tpl_vars['q']; ?>
<?php endif; ?>&orderby=domain"><?php echo $this->_tpl_vars['LANG']['clientareahostingdomain']; ?>
</a></th>
            <th<?php if ($this->_tpl_vars['orderby'] == 'regdate'): ?> class="headerSort<?php echo $this->_tpl_vars['sort']; ?>
"<?php endif; ?>><a href="clientarea.php?action=domains<?php if ($this->_tpl_vars['q']): ?>&q=<?php echo $this->_tpl_vars['q']; ?>
<?php endif; ?>&orderby=regdate"><?php echo $this->_tpl_vars['LANG']['clientareahostingregdate']; ?>
</a></th>
            <th<?php if ($this->_tpl_vars['orderby'] == 'nextduedate'): ?> class="headerSort<?php echo $this->_tpl_vars['sort']; ?>
"<?php endif; ?>><a href="clientarea.php?action=domains<?php if ($this->_tpl_vars['q']): ?>&q=<?php echo $this->_tpl_vars['q']; ?>
<?php endif; ?>&orderby=nextduedate"><?php echo $this->_tpl_vars['LANG']['clientareahostingnextduedate']; ?>
</a></th>
            <th<?php if ($this->_tpl_vars['orderby'] == 'status'): ?> class="headerSort<?php echo $this->_tpl_vars['sort']; ?>
"<?php endif; ?>><a href="clientarea.php?action=domains<?php if ($this->_tpl_vars['q']): ?>&q=<?php echo $this->_tpl_vars['q']; ?>
<?php endif; ?>&orderby=status"><?php echo $this->_tpl_vars['LANG']['clientareastatus']; ?>
</a></th>
            <th<?php if ($this->_tpl_vars['orderby'] == 'autorenew'): ?> class="headerSort<?php echo $this->_tpl_vars['sort']; ?>
"<?php endif; ?>><a href="clientarea.php?action=domains<?php if ($this->_tpl_vars['q']): ?>&q=<?php echo $this->_tpl_vars['q']; ?>
<?php endif; ?>&orderby=autorenew"><?php echo $this->_tpl_vars['LANG']['domainsautorenew']; ?>
</a></th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['num'] => $this->_tpl_vars['domain']):
?>
        <tr>
            <td class="textcenter"><input type="checkbox" name="domids[]" class="domids" value="<?php echo $this->_tpl_vars['domain']['id']; ?>
" /></td>
            <td><a href="http://<?php echo $this->_tpl_vars['domain']['domain']; ?>
/" target="_blank"><?php echo $this->_tpl_vars['domain']['domain']; ?>
</a></td>
            <td><?php echo $this->_tpl_vars['domain']['registrationdate']; ?>
</td>
            <td><?php echo $this->_tpl_vars['domain']['nextduedate']; ?>
</td>
            <td><span class="label <?php echo $this->_tpl_vars['domain']['rawstatus']; ?>
"><?php echo $this->_tpl_vars['domain']['statustext']; ?>
</span></td>
            <td><?php if ($this->_tpl_vars['domain']['autorenew']): ?><?php echo $this->_tpl_vars['LANG']['domainsautorenewenabled']; ?>
<?php else: ?><?php echo $this->_tpl_vars['LANG']['domainsautorenewdisabled']; ?>
<?php endif; ?></td>
            <td class="cell-menu-wrap">
                <div class="btn-group">
					<a class="btn" href="clientarea.php?action=domaindetails&id=<?php echo $this->_tpl_vars['domain']['id']; ?>
"> <span class="glyphicon glyphicon-wrench"></span> <?php echo $this->_tpl_vars['LANG']['managedomain']; ?>
</a>
					<?php if ($this->_tpl_vars['domain']['rawstatus'] == 'active'): ?>
					<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu cell-menu" role="menu">
						<li><a href="clientarea.php?action=domaincontacts&domainid=<?php echo $this->_tpl_vars['domain']['id']; ?>
"><span class="glyphicon glyphicon-user"></span> <?php echo $this->_tpl_vars['LANG']['domaincontactinfoedit']; ?>
</a></li>
						<li><a href="clientarea.php?action=domaindetails&id=<?php echo $this->_tpl_vars['domain']['id']; ?>
#tab2"><span class="glyphicon glyphicon-globe"></span> <?php echo $this->_tpl_vars['LANG']['domainautorenewstatus']; ?>
</a></li>
						<li class="divider"></li>
						<li><a href="clientarea.php?action=domaindetails&id=<?php echo $this->_tpl_vars['domain']['id']; ?>
"><span class="glyphicon glyphicon-pencil"></span> <?php echo $this->_tpl_vars['LANG']['managedomain']; ?>
</a></li>
					</ul>
                <?php endif; ?>
                </div>
            </td>
        </tr>
<?php endforeach; else: ?>
        <tr>
            <td colspan="7" class="textcenter"><?php echo $this->_tpl_vars['LANG']['norecordsfound']; ?>
</td>
        </tr>
<?php endif; unset($_from); ?>
    </tbody>
</table>

	<div class="btn-group">
		<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-folder-open"></span> <?php echo $this->_tpl_vars['LANG']['withselected']; ?>
 <span class="caret"></span></button>
		<ul class="dropdown-menu cell-menu" role="menu">
			<li><a href="#" id="autorenew" class="setbulkaction"><span class="glyphicon glyphicon-refresh"></span> <?php echo $this->_tpl_vars['LANG']['domainautorenewstatus']; ?>
</a></li>
			<li><a href="#" id="reglock" class="setbulkaction"><span class="glyphicon glyphicon-lock"></span> <?php echo $this->_tpl_vars['LANG']['domainreglockstatus']; ?>
</a></li>
			<li><a href="#" id="contactinfo" class="setbulkaction"><span class="glyphicon glyphicon-user"></span> <?php echo $this->_tpl_vars['LANG']['domaincontactinfoedit']; ?>
</a></li>
			<?php if ($this->_tpl_vars['allowrenew']): ?><li><a href="#" id="renew" class="setbulkaction"><span class="glyphicon glyphicon-repeat"></span> <?php echo $this->_tpl_vars['LANG']['domainmassrenew']; ?>
</a></li><?php endif; ?>
		</ul>
	</div>
</form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['template'])."/clientarearecordslimit.tpl", 'smarty_include_vars' => array('clientareaaction' => $this->_tpl_vars['clientareaaction'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<nav>
	<ul class="pagination">
		<li <?php if (! $this->_tpl_vars['prevpage']): ?>class="disabled"<?php endif; ?>><a href="<?php if ($this->_tpl_vars['prevpage']): ?>clientarea.php?action=domains<?php if ($this->_tpl_vars['q']): ?>&q=<?php echo $this->_tpl_vars['q']; ?>
<?php endif; ?>&amp;page=<?php echo $this->_tpl_vars['prevpage']; ?>
<?php else: ?>javascript:return false;<?php endif; ?>"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
		<li <?php if (! $this->_tpl_vars['nextpage']): ?>class="disabled"<?php endif; ?>><a href="<?php if ($this->_tpl_vars['nextpage']): ?>clientarea.php?action=domains<?php if ($this->_tpl_vars['q']): ?>&q=<?php echo $this->_tpl_vars['q']; ?>
<?php endif; ?>&amp;page=<?php echo $this->_tpl_vars['nextpage']; ?>
<?php else: ?>javascript:return false;<?php endif; ?>"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
	</ul>
</nav>

</form>

<br />
<br />