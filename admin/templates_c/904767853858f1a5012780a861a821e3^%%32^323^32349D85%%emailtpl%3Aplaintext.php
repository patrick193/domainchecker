<?php /* Smarty version 2.6.28, created on 2015-04-24 11:15:02
         compiled from emailtpl:plaintext */ ?>
<p><?php echo $this->_tpl_vars['client_name']; ?>
,</p>
<p>Your email to our support system could not be accepted because it was not recognized as coming from an email address belonging to one of our customers. If you need assistance, please email from the address you registered with us that you use to login to our client area.</p>
<p><?php echo $this->_tpl_vars['signature']; ?>
</p>