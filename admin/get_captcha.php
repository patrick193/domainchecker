<?php
$sid = htmlspecialchars($_GET['sid']);
session_id( $sid );
session_start();

// generating random code

$chars = array_merge( range('A', 'Z'), range('1', '9') );
shuffle($chars);
$chars = array_slice($chars, 0, 5);

$captcha_text = implode('', $chars);

$captcha_img	= imagecreatefrompng( "images/cback.png" );
$color			= imagecolorallocate( $captcha_img, 0, 0, 0 );
$px				= (imagesx($captcha_img) - 7.5 * strlen($captcha_text)) / 2;
$py				= imagesy($captcha_img) / 2 - 5;
imagestring( $captcha_img, 5, $px, 10, $captcha_text, $color );

$_SESSION['ctext'] = md5($captcha_text);

header('Expires: 0');
header( "Content-type: image/png" );
header( "Pragma: no-cache" );
imagepng( $captcha_img );
imagedestroy( $captcha_img );
?>