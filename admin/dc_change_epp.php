<?php
define( 'CLIENTAREA', true );
require_once('init.php');

// If this is a request from email to change the EPP - find the domain and redirect to domain management page
if ( !empty($_GET['domainepp']) ) {
	$result	= select_query( 'tbldomains', '', array('domain' => $_GET['domainepp']) );
	$data	= mysql_fetch_array( $result );

	if ( $data ) {
		header( "Location: clientarea.php?action=domaindetails&id=" . $data['id'] . "#tab7" );
		exit();
	}
}

$ca = new WHMCS_ClientArea();

$ca->setPageTitle($_LANG["dc_epp_page"]);

$ca->addToBreadCrumb('index.php',$whmcs->get_lang('globalsystemname'));
$ca->addToBreadCrumb('domain_check.php', $_LANG["dc_epp_page"]);

$ca->initPage();

$dc_id			= $_POST['dc_id'];
$dc_epp_code	= $_POST['dc_epp_code'];
$user_id		= $_SESSION['uid'];
$error			= '';
$alert_class	= 'alert-danger';

// Checking if all the input is available, domain is transfered, domain has needed status, user has access
if ( empty($dc_id) || empty($dc_epp_code) || empty($user_id) ) {
	$error = $_LANG["dc_epp_no_data"];
} else {
	$result	= select_query( 'tbldomains', '', array('id' => $dc_id) );

	$data	= mysql_fetch_array( $result );
	
	if ( !$data ) {
		$error = $_LANG["dc_epp_no_domain"];
	} elseif ( $data['userid'] !== $user_id ) {
		$error = $_LANG["dc_epp_no_user"];
	} elseif ( $data['type'] !== 'Transfer' ) {
		$error = $_LANG["dc_epp_no_status"];
	} elseif ( $data['status'] !== 'Pending' && $data['status'] !== 'Pending Transfer' ) {
		$error = $_LANG["dc_epp_no_status"];
	}
}

// Checking order and modifying EPP code
if ( empty($error) ) {
	$result	= select_query( 'tblorders', '', array('id' => $data['orderid']) );

	$order_data	= mysql_fetch_array( $result );

	if ( !$order_data ) {
		$error = $_LANG["dc_epp_no_order"];
	} else {
		$domain_data = unserialize($order_data['transfersecret']);
		$domain_data[ $data['domain'] ] = $dc_epp_code;
		$domain_data = serialize($domain_data);
		update_query( 'tblorders', array('transfersecret' => $domain_data), array('id' => $data['orderid']) );
		
		$error			= $_LANG["dc_epp_success"];
		$alert_class	= 'alert-success';
	}
}

$ca->assign('error', $error);
$ca->assign('alert_class', $alert_class);
$ca->assign('domainid', $dc_id);

$templatefile = 'dc_change_epp';

$ca->setTemplate($templatefile);
$ca->output();

?>