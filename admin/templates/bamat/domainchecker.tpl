{include file="$template/pageheader.tpl" title=$LANG.domaintitle desc=$LANG.domaincheckerintro}

{include_php file="templates/$template/dc_check/dc_form.php"}


{if $inccode}
<div class="alert alert-danger textcenter">
    {$LANG.captchaverifyincorrect}
</div>
{/if}

<p>{$LANG.domaincheckerenterdomain}</p>

{if $bulkdomainsearchenabled}<p class="text-right"><a href="domainchecker.php?search=bulkregister">{$LANG.domainbulksearch}</a> | <a href="domainchecker.php?search=bulktransfer">{$LANG.domainbulktransfersearch}</a></p>{/if}

<div class="wrap_domain">
	<form method="post" action="domainchecker.php" class="dc_form">

		{if $captcha}
			<div class="captcha_wrap">
				<div class="captcha">
					{if $captcha eq "recaptcha"}
						{$recaptchahtml}
					{else}
						<img src="includes/verifyimage.php" class="dc_captcha" class="text-center" /> <input type="text" name="code" class="dc_captcha_input" maxlength="5" />
					{/if}
				</div>
				<div class="captchaverify">{$LANG.captchaverify}</div>
			</div>
		{/if}

		<div class="dc_wrap">
			<input name="domain" type="text" value="{if $domain}{$domain}{else}{$LANG.domaincheckerdomainexample}{/if}" onfocus="if(this.value=='{$LANG.domaincheckerdomainexample}')this.value=''" onblur="if(this.value=='')this.value='{$LANG.domaincheckerdomainexample}'" />

			<div class="button-row">
				<input type="submit" value="{$LANG.checkavailability}" class="btn btn-primary btn-large" />
				{if $condlinks.domaintrans}<input type="submit" name="transfer" value="{$LANG.domainstransfer}" class="btn btn-success btn-large" />{/if}
				<input type="submit" name="hosting" value="{$LANG.domaincheckerhostingonly}" class="btn btn-large" />
				<input type="button" value="{$LANG.searchmultipletlds} &raquo;" class="btn " onclick="jQuery('#tlds').slideToggle()" />
			</div>
		</div>

		<div class="domcheckertldselect" id="tlds" style="display: none">
			{foreach from=$tldslist key=num item=listtld}
				<div class="col-md-3"><label class="full"><input type="checkbox" name="tlds[]" value="{$listtld}"{if in_array($listtld,$tlds) || !$tlds && $num==1} checked{/if}> {$listtld}</label></div>
			{/foreach}
		</div>

	</form>
</div>

{if $lookup}

{if $invalidtld}
    <p class="fontsize3 domcheckererror textcenter">{$invalidtld|strtoupper} {$LANG.domaincheckerinvalidtld}</p>
{elseif $available}
    <p class="fontsize3 domcheckersuccess textcenter">{$LANG.domainavailable1} <strong>{$sld}{$ext}</strong> {$LANG.domainavailable2}</p>
{elseif $invalid}
    <p class="fontsize3 domcheckererror textcenter">{$LANG.ordererrordomaininvalid}</p>
{elseif $error}
    <p class="fontsize3 domcheckererror textcenter">{$LANG.domainerror}</p>
{else}
    <p class="fontsize3 domcheckererror textcenter">{$LANG.domainunavailable1} <strong>{$sld}{$ext}</strong> {$LANG.domainunavailable2}</p>
{/if}

{if !$invalid}

<br />

<div class="center80">

<form method="post" action="{$systemsslurl}cart.php?a=add&domain=register">

<table class="table table-framed dc_table">
    <thead>
        <tr class="dc_tr_btn">
            <th colspan="2"></th>
            <th><input type="submit" value="{$LANG.ordernowbutton} &raquo;" class="btn btn-danger" /></th>
            <th></th>
        </tr>

        <tr class="dc_tr_head">
            <th>{$LANG.domainname}</th>
            <th>{$LANG.domainstatus}</th>
            <th>{$LANG.domainmoreinfo}</th>
            <th><input type="checkbox" id="dc_check_all" /> {$LANG.all}</th>
        </tr>
    </thead>
    <tbody>
{foreach from=$availabilityresults key=num item=result}
        <tr class="{if $result.status eq "available"}dc_success{else}dc_fail{/if}">
            <td class="dc_td_name">{$result.domain}</td>
            <td class="dc_td_status"><div class="dc_status_sign"></div> {if $result.status eq "available"}{$LANG.domainavailable}{else}{$LANG.domainunavailable}{/if}</td>
            <td class="dc_td_info">{if $result.status eq "unavailable"}<a href="http://{$result.domain}" target="_blank">WWW</a> <a href="#" onclick="popupWindow('whois.php?domain={$result.domain}','whois',650,420);return false">WHOIS</a>{else}<select name="domainsregperiod[{$result.domain}]">{foreach key=period item=regoption from=$result.regoptions}<option value="{$period}">{$period} {$LANG.orderyears} @ {$regoption.register}</option>{/foreach}</select>{/if}</td>
            <td class="dc_td_check">{if $result.status eq "available"}<input type="checkbox" name="domains[]" value="{$result.domain}" {if $num eq "0" && $available}checked {/if}/><input type="hidden" name="domainsregperiod[{$result.domain}]" value="{$result.period}" />{else}X{/if}</td>
        </tr>
{/foreach}
    </tbody>

    <tfoot>
        <tr class="dc_tr_btn">
            <th colspan="2"></th>
            <th><input type="submit" value="{$LANG.ordernowbutton} &raquo;" class="btn btn-danger" /></th>
            <th></th>
        </tr>
    </tfoot>
</table>

</form>

</div>

{/if}

{else}

{include file="$template/subheader.tpl" title=$LANG.domainspricing}

<div class="center80">

<table class="table table-striped table-framed">
    <thead>
        <tr>
            <th class="textcenter">{$LANG.domaintld}</th>
            <th class="textcenter">{$LANG.domainminyears}</th>
            <th class="textcenter">{$LANG.domainsregister}</th>
            <th class="textcenter">{$LANG.domainstransfer}</th>
            <th class="textcenter">{$LANG.domainsrenew}</th>
        </tr>
    </thead>
    <tbody>
{foreach from=$tldpricelist item=tldpricelist}
        <tr>
            <td>{$tldpricelist.tld}</td>
            <td class="textcenter">{$tldpricelist.period}</td>
            <td class="textcenter">{if $tldpricelist.register}{$tldpricelist.register}{else}{$LANG.domainregnotavailable}{/if}</td>
            <td class="textcenter">{if $tldpricelist.transfer}{$tldpricelist.transfer}{else}{$LANG.domainregnotavailable}{/if}</td>
            <td class="textcenter">{if $tldpricelist.renew}{$tldpricelist.renew}{else}{$LANG.domainregnotavailable}{/if}</td>
        </tr>
{/foreach}
    </tbody>
</table>

{if !$loggedin && $currencies}
<form method="post" action="domainchecker.php">
<p class="text-right">{$LANG.choosecurrency}: <select name="currency" onchange="submit()">{foreach from=$currencies item=curr}
<option value="{$curr.id}"{if $curr.id eq $currency.id} selected{/if}>{$curr.code}</option>
{/foreach}</select> <input type="submit" class="btn" value="{$LANG.go}" /></p>
</form>
{/if}

</div>

{/if}

<br /><br />

{literal}
<script language="javascript">
// "Check All" in domain results table
jQuery('body').on( 'change', '#dc_check_all', function() {
	jQuery('.dc_table').find('.dc_td_check input[type="checkbox"]').prop( 'checked', jQuery('#dc_check_all').is(':checked') );
});
</script>
{/literal}
