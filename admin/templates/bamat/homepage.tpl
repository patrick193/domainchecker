{if $condlinks.domainreg || $condlinks.domaintrans || $condlinks.domainown}
    <div class="styled_title">
        <h1>{$LANG.domaincheckerchoosedomain}</h1>
    </div>
    <br />
    <p>{$LANG.domaincheckerenterdomain}</p>
    <br />

	{dc_domain_check_form}
{/if}

		</div>
	</div>
</div>

<div class="container-fluid page-section-grey">
	<div class="container">
		<div class="row">

		<div class="col-md-6">
			<div class="internalpadding">
				<div class="styled_title">
					<h2>{$LANG.navservicesorder}</h2>
				</div>
				<p class="get-in-box-desc">{$LANG.clientareahomeorder}<br /><br /></p>
				<form method="post" action="cart.php">
				<p class="text-center"><input type="submit" value="{$LANG.clientareahomeorderbtn} &raquo;" class="btn" /></p>
				</form>
			</div>
		</div>

		<div class="col-md-6">
			<div class="internalpadding">
				<div class="styled_title"><h2>{$LANG.manageyouraccount}</h2></div>
				<p class="get-in-box-desc">{$LANG.clientareahomelogin}<br /><br /></p>
				<form method="post" action="clientarea.php">
				<p class="text-center"><input type="submit" value="{$LANG.clientareahomeloginbtn} &raquo;" class="btn" /></p>
				</form>
			</div>
		</div>

</div>

<div class="row">

{if $twitterusername}
<div class="styled_title">
    <h2>{$LANG.twitterlatesttweets}</h2>
</div>
<div id="twitterfeed">
    <p><img src="images/loading.gif"></p>
</div>
{literal}<script language="javascript">
jQuery(document).ready(function(){
  jQuery.post("announcements.php", { action: "twitterfeed", numtweets: 3 },
    function(data){
      jQuery("#twitterfeed").html(data);
    });
});
</script>{/literal}
{elseif $announcements}
<div class="styled_title">
    <h2>{$LANG.latestannouncements}</h2>
</div>
{foreach from=$announcements item=announcement}
<p>{$announcement.date} - <a href="{if $seofriendlyurls}announcements/{$announcement.id}/{$announcement.urlfriendlytitle}.html{else}announcements.php?id={$announcement.id}{/if}"><b>{$announcement.title}</b></a><br />{$announcement.text|strip_tags|truncate:100:"..."}</p>
{/foreach}
{/if}

</div>