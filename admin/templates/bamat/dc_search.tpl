{if $dc_error eq 1}
<div class="alert alert-danger textcenter">
    {$LANG.captchaverifyincorrect}
</div>
{/if}

<div class="dc_search">
	<div class="searchdomains">
		<form name="domainsearchform" action="{$systemsslurl}{$dc_target_url}" method="get" class="dc_form">
			{if $dc_is_product}
				{foreach key=var_name item=var_value from=$dc_get_vars}
					<input type="hidden" name="{$var_name}" value="{$var_value}" />
				{/foreach}
			{/if}

			<label for="dc_dom">{$LANG.domain_check.editsearch}:</label>
			
			<table class="dc_form_elements">
			<tbody>
				<tr>
					<td><input type="text" class="text-input" maxlength="67" name="dc_dom" id="dc_dom" placeholder="Domainname" value="{$dc_dom}"></td>

					<td>
						<select class="searchselect" name="dc_cat">
							{foreach from=$dc_categories item=dc_cat}
								{if $dc_cat.id eq $dc_curr_cat}
									<option value="{$dc_cat.id}" selected="selected">{$dc_cat.name}</option>
								{else}
									<option value="{$dc_cat.id}">{$dc_cat.name}</option>
								{/if}
							{/foreach}
						</select>
					</td>
				</tr>

				{if $use_captcha}
				<tr class="captcha_row">
					<td class="text-center"><img src="get_captcha.php?sid={$sid}"/></td>
					<td><input type="text" class="text-input" maxlength="67" name="dc_captcha" id="dc_captcha" placeholder="" value=""></td>
				</tr>

				<tr class="captcha_desc_row text-center">
					<td colspan="2" class="captcha_desc">{$LANG.captchaverify}</td>
				</tr>

				{/if}
			</tbody>
			</table>
				
				<div class="btn_wrap"><input type="submit" title="{$LANG.domain_check.check}" class="btn btn-large" id="btnPruefen" name="btnPruefen" value="{$LANG.domain_check.check}"></div>
			</div>

			
		</form>
	</div>

	<div class="dc_user_info">
		<p>{$LANG.domain_check.clientcountry}: <strong>{$dc_country}</strong>. {$LANG.domain_check.clientcurrency|sprintf2:"<strong>`$dc_cur_name`</strong>"} {if !$logged_in}(<em><a href="{$dc_curr_url}" class="dashed_bottom">{$LANG.domain_check.currencyswitch|sprintf2:$dc_switch_curr}?</a></em>){/if}
		</p>
	</div>
	
</div>
