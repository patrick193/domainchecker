{if $dc_action=="register"}

	<input type="hidden" name="domainoption" value="register" />

	{if $tld.status}

		<div class="alert alert-success">{$LANG.cartcongratsdomainavailable|sprintf2:"<strong>`$domain`</strong>"} <strong>{$tld.out_price}</strong>{$LANG.domainaddonsperyear}</div>
		<input type="hidden" name="domains[]" value="{$domain}" />
		<input type="hidden" name="domainsregperiod[{$domain}]" value="1" />

		{assign var='continueok' value=true}

	{else}

		<div class="alert alert-warning">{$LANG.cartdomaintaken|sprintf2:"<strong>`$domain`</strong>"}</div>

	{/if}

{elseif $dc_action=="transfer"}

	<input type="hidden" name="domainoption" value="transfer" />

	{if $tld.status}

		<div class="alert alert-warning">{$LANG.carttransfernotregistered|sprintf2:"<strong>`$domain`</strong>"}</div>

	{else}

		<div class="alert alert-success">{$LANG.carttransferpossible|sprintf2:"<strong>`$domain`</strong>":"<strong>`$tld.out_price`</strong>"}</div>
		<input type="hidden" name="domains[]" value="{$domain}" />
		<input type="hidden" name="domainsregperiod[{$domain}]" value="{$transferterm}" />

		{assign var='continueok' value=true}

	{/if}
{/if}

<p class="dc_price_info"><em>{$LANG.domain_check.tablepricenotice}</em></p>
<p class="text-center"><input type="submit" value="{$LANG.ordercontinuebutton}" class="btn"{if !$continueok} style="display:none;"{/if} /></p>

{literal}
<script language="javascript">
jQuery(document).ready(function(){
	jQuery("input.cartbutton:button,input.cartbutton:submit").button();
});
</script>
{/literal}

