{include file="$template/pageheader.tpl" title=$LANG.clientareaemails desc=$LANG.emailstagline}

<p>{$numitems} {$LANG.recordsfound}, {$LANG.page} {$pagenumber} {$LANG.pageof} {$totalpages}</p>

<br />

<table class="table table-striped table-framed">
    <thead>
        <tr>
            <th{if $orderby eq "date"} class="headerSort{$sort}"{/if}><a href="clientarea.php?action=emails&orderby=date">{$LANG.clientareaemailsdate}</a></th>
            <th{if $orderby eq "subject"} class="headerSort{$sort}"{/if}><a href="clientarea.php?action=emails&orderby=subject">{$LANG.clientareaemailssubject}</a></th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
{foreach from=$emails item=email}
        <tr>
            <td>{$email.date}</td>
            <td>{$email.subject}</td>
            <td class="textcenter"><input type="button" class="btn btn-info" value="{$LANG.emailviewmessage}" onclick="popupWindow('viewemail.php?id={$email.id}','emlmsg',650,400)" /></td>
        </tr>
{foreachelse}
        <tr>
            <td colspan="3" class="textcenter">{$LANG.norecordsfound}</td>
        </tr>
{/foreach}
    </tbody>
</table>

<nav>
	<ul class="pagination">
		<li {if !$prevpage}class="disabled"{/if}><a href="{if $prevpage}clientarea.php?action=emails&amp;page={$prevpage}{else}javascript:return false;{/if}"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
		<li {if !$nextpage}class="disabled"{/if}><a href="{if $nextpage}clientarea.php?action=emails&amp;page={$nextpage}{else}javascript:return false;{/if}"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
	</ul>
</nav>
