{if $error}

<p>{$LANG.supportticketinvalid}</p>

{else}

{include file="$template/pageheader.tpl" title=$LANG.supportticketsviewticket|cat:' #'|cat:$tid}

{if $errormessage}
<div class="alert alert-danger">
    <p class="bold">{$LANG.clientareaerrors}</p>
    <ul>
        {$errormessage}
    </ul>
</div>
{/if}

<h2>{$subject}</h2>

<table class="table table-framed table-centered">
    <thead>
        <tr class="dc_tr_head">
            <th>{$LANG.supportticketsubmitted}</th>
            <th>{$LANG.supportticketsdepartment}</th>
            <th>{$LANG.supportticketspriority}</th>
            <th>{$LANG.supportticketsstatus}</th>
			
			{if $customfields}
				{foreach from=$customfields item=customfield}
				<th>{$customfield.name}</th>
				{/foreach}
			{/if}
        </tr>
    </thead>

    <tbody>
		<tr>
			<td class="dc_td_status">{$date}</td>
			<td class="dc_td_status">{$department}</td>
			<td class="dc_td_status">{$urgency}</td>
			<td class="dc_td_status">{$status}</td>

			{if $customfields}
				{foreach from=$customfields item=customfield}
				<td class="dc_td_status">{$customfield.value}</td>
				{/foreach}
			{/if}
		</tr>
		
    </tbody>
</table>

<br>

<p class="ticket-buttons">
	<input type="button" value="{$LANG.clientareabacklink}" class="btn" onclick="window.location='supporttickets.php'" id="ticket-back" />
	{if $showclosebutton}
		<input type="button" value="{$LANG.supportticketsclose}" class="btn btn-danger" onclick="window.location='{$smarty.server.PHP_SELF}?tid={$tid}&amp;c={$c}&amp;closeticket=true'" id="ticket-close-top" />
	{/if}
	<input type="button" value="{$LANG.supportticketsreply}" class="btn btn-primary" id="ticket-reply-top" />
</p>

<div id="replyform-top">
<form method="post" action="{$smarty.server.PHP_SELF}?tid={$tid}&amp;c={$c}&amp;postreply=true" enctype="multipart/form-data" class="form-horizontal" id="submitreply" style="display: none;">

    <fieldset class="control-group">

        <div class="row">
            <div class="col-md-6">
                <div class="control-group">
                    <label class="control-label bold" for="name">{$LANG.supportticketsclientname}</label>
                    <div class="controls">
                        {if $loggedin}<input class="input-xlarge disabled" type="text" id="name" value="{$clientname}" disabled="disabled" />{else}<input class="input-xlarge" type="text" name="replyname" id="name" value="{$replyname}" />{/if}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="control-group">
                    <label class="control-label bold" for="email">{$LANG.supportticketsclientemail}</label>
                    <div class="controls">
                        {if $loggedin}<input class="input-xlarge disabled" type="text" id="email" value="{$email}" disabled="disabled" />{else}<input class="input-xlarge" type="text" name="replyemail" id="email" value="{$replyemail}" />{/if}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
				<div class="control-group">
					<label class="control-label bold" for="message">{$LANG.contactmessage}</label>
					<div class="controls">
						<textarea name="replymessage" id="message" rows="12" class="fullwidth">{$replymessage}</textarea>
					</div>
				</div>
			</div>
			
            <div class="col-md-12">
				<div class="control-group">
					<label class="control-label bold" for="attachments">{$LANG.supportticketsticketattachments}:</label>
					<div class="controls">
						<input type="file" name="attachments[]" style="width:70%;" /><br />
						<div id="fileuploads"></div>
						<a href="#" onclick="extraTicketAttachment();return false"><img src="images/add.gif" class="text-center" border="0" /> {$LANG.addmore}</a><br />
						({$LANG.supportticketsallowedextensions}: {$allowedfiletypes})
					</div>
				</div>
			</div>
		</div>

    </fieldset>

    <p class="text-center"><input type="submit" value="{$LANG.supportticketsticketsubmit}" class="btn btn-primary" /></p>

</form>
</div>

{foreach from=$descreplies key=num item=reply}

{tt_check_google_translate reply=$reply}
<br>

<div class="ticket-msg {if $reply.admin && !$is_google_translate}ticket-msg-admin{else}ticket-msg-client{/if}" id="ticket-msg-{$reply.id}">
	<div class="ticket-msg-author-wrap">
		<div class="ticket-msg-author">
			{$reply.name}
		</div>
        {if !$is_google_translate}
		<div class="ticket-msg-position">
			{if $reply.admin}
				{$LANG.supportticketsstaff}
			{elseif $reply.contactid}
				{$LANG.supportticketscontact}
			{elseif $reply.userid}
				{$LANG.supportticketsclient}
			{else}
				{$reply.email}
			{/if}
		</div>
		{/if}

		<div class="ticket-msg-date">{$reply.date}</div>
	</div>
	
	<div class="ticket-text-wrap">
	
		<div class="ticket-text">
		{$reply.message}
		</div>

		{if $reply.attachments}
		<div class="ticket-attachments">
			<div class="ticket-attachments-title">{$LANG.supportticketsticketattachments}:</div>

			<div class="ticket-attachments-files">
				{foreach from=$reply.attachments key=num item=attachment}
				<a href="dl.php?type={if $reply.id}ar&id={$reply.id}{else}a&id={$id}{/if}&i={$num}"><span class="glyphicon glyphicon-file"></span> {$attachment}</a><br />
				{/foreach}
			</div>
		</div>
		{/if}

        {if $reply.id && $reply.admin && $ratingenabled && !$is_google_translate}
		<div class="ticket-rating">
			{if $reply.rating}
				<span class="ticket-rating-caption">{$LANG.ticketreatinggiven}: </span>
				
				<span class="ticket-rating-star">
					{foreach from=$ratings item=rating}
					<span class="ticket-rating-img ticket-rating-{if $reply.rating>=$rating}pos{else}neg{/if}">&nbsp;</span>
					{/foreach}
				</span>
			{else}
				<span class="ticket-rating-caption">{$LANG.ticketratingquestion} </span>
				<span class="ticket-rating-caption ticket-rating-mark-neg" data-reply-id="{$reply.id}" data-ticket-id="{$tid}" data-ticket-c="{$c}">{$LANG.ticketratingpoor}</span>

				<span class="ticket-rating-stars-active" data-reply-id="{$reply.id}">
					{foreach from=$ratings item=rating}
					<span class="ticket-rating-img-active ticket-rating-neg" id="ticket-rating-{$reply.id}-{$rating}" data-reply-id="{$reply.id}" data-rating="{$rating}" data-ticket-id="{$tid}" data-ticket-c="{$c}">&nbsp;</span>
					{/foreach}
				</span>

				<span class="ticket-rating-caption ticket-rating-mark-pos" data-reply-id="{$reply.id}" data-ticket-id="{$tid}" data-ticket-c="{$c}">{$LANG.ticketratingexcellent}</span>

			{/if}
		</div>
		{/if}
	</div>
	
</div>
<div class="clear"></div>
{/foreach}
	
<br>
<br>

<p class="ticket-buttons">
	<input type="button" value="{$LANG.clientareabacklink}" class="btn" onclick="window.location='supporttickets.php'" id="ticket-back" />
	{if $showclosebutton}
		<input type="button" value="{$LANG.supportticketsclose}" class="btn btn-danger" onclick="window.location='{$smarty.server.PHP_SELF}?tid={$tid}&amp;c={$c}&amp;closeticket=true'" id="ticket-close-bottom" />
	{/if}
	<input type="button" value="{$LANG.supportticketsreply}" class="btn btn-primary" id="ticket-reply-bottom" />
</p>

<div id="replyform-bottom">
</div>

{/if}