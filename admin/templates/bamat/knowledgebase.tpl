{include file="$template/pageheader.tpl" title=$LANG.knowledgebasetitle}

<div class="textcenter">
	<form method="post" action="knowledgebase.php?action=search" class="form-inline">
		<fieldset class="control-group">
			<input class="bigfield" name="search" type="text" value="{$LANG.kbquestionsearchere}" onfocus="this.value=(this.value=='{$LANG.kbquestionsearchere}') ? '' : this.value;" onblur="this.value=(this.value=='') ? '{$LANG.kbquestionsearchere}' : this.value;"/>
			<input type="submit" class="btn btn-large btn-primary" value="{$LANG.knowledgebasesearch}" />
		</fieldset>
	</form>
</div>

{include file="$template/subheader.tpl" title=$LANG.knowledgebasecategories}

<div class="row">
<div class="control-group">
{foreach name=kbasecats from=$kbcats item=kbcat}
	<div class="internalpadding">
		<h4><span class="glyphicon glyphicon-folder-open"></span> <a href="{if $seofriendlyurls}knowledgebase/{$kbcat.id}/{$kbcat.urlfriendlyname}{else}knowledgebase.php?action=displaycat&amp;catid={$kbcat.id}{/if}">{$kbcat.name}</a> ({$kbcat.numarticles})</h4><p>{$kbcat.description}</p>
	</div>
{/foreach}
	<div class="clear"></div>
</div>
</div>

{include file="$template/subheader.tpl" title=$LANG.knowledgebasepopular}

{foreach from=$kbmostviews item=kbarticle}
<div class="row">
    <h4><span class="glyphicon glyphicon-file"></span> <a href="{if $seofriendlyurls}knowledgebase/{$kbarticle.id}/{$kbarticle.urlfriendlytitle}.html{else}knowledgebase.php?action=displayarticle&amp;id={$kbarticle.id}{/if}">{$kbarticle.title}</a></h4>
    <p>{$kbarticle.article|truncate:100:"..."}</p>
</div>
{/foreach}

<br />