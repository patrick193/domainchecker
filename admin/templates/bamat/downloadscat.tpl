{include file="$template/pageheader.tpl" title=$LANG.downloadstitle}

<div class="searchbox">
    <form method="post" action="downloads.php?action=search">
        <div class="input-append">
            <input type="text" name="search" value="{if $search}{$search}{else}{$LANG.downloadssearch}{/if}" class="input-medium appendedInputButton" onfocus="if(this.value=='{$LANG.downloadssearch}')this.value=''" /><button type="submit" class="btn btn-warning">{$LANG.search}</button>
        </div>
    </form>
</div>

<p>{$LANG.downloadsintrotext}</p>

<br />

{if $dlcats}

{include file="$template/subheader.tpl" title=$LANG.downloadscategories}

<div class="row">
<div class="control-group">
{foreach from=$dlcats item=dlcat}
	<div class="internalpadding">
		<h4><span class="glyphicon glyphicon-download"></span> 
		<a href="{if $seofriendlyurls}downloads/{$dlcat.id}/{$dlcat.urlfriendlyname}{else}downloads.php?action=displaycat&amp;catid={$dlcat.id}{/if}">{$dlcat.name}</a> ({$dlcat.numarticles})</h4>
		<p>{$dlcat.description}</p>
	</div>
{/foreach}
</div>
</div>

{/if}
{include file="$template/subheader.tpl" title=$LANG.downloadsfiles}
{if $downloads}

{foreach from=$downloads item=download}
<div class="row">
    <h4>{$download.type} <a href="{$download.link}" {if $download.clientsonly}title="{$LANG.loginrequired}"{/if}>{$download.title}{if $download.clientsonly} <span class="glyphicon glyphicon-lock"></span>{/if}</a></h4>
    <p>{$download.description}</p>
    <small class="lighttext">{$LANG.downloadsfilesize}: {$download.filesize}</small>	
</div>
{/foreach}

{else}

<p class="textcenter fontsize3">{$LANG.downloadsnone}</p>

{/if}

<br />
<br />
<br />