{if $id eq ''}
	{assign var=id value='password'}
{/if}
{literal}
<script>
jQuery(document).ready(function(){
    jQuery("#{/literal}{$id}{literal}").keyup(function () {
        var pw = jQuery("#{/literal}{$id}{literal}").val();
        var pwlength=(pw.length);
        if(pwlength>5)pwlength=5;
        var numnumeric=pw.replace(/[0-9]/g,"");
        var numeric=(pw.length-numnumeric.length);
        if(numeric>3)numeric=3;
        var symbols=pw.replace(/\W/g,"");
        var numsymbols=(pw.length-symbols.length);
        if(numsymbols>3)numsymbols=3;
        var numupper=pw.replace(/[A-Z]/g,"");
        var upper=(pw.length-numupper.length);
        if(upper>3)upper=3;
        var pwstrength=((pwlength*10)-20)+(numeric*10)+(numsymbols*15)+(upper*10);
        if(pwstrength<0){pwstrength=0}
        if(pwstrength>100){pwstrength=100}
        jQuery("#pwstrengthbox").removeClass("weak");
        jQuery("#pwstrengthbox").removeClass("moderate");
        jQuery("#pwstrengthbox").removeClass("strong");
		
		if ( pwstrength == 0 ) {
			jQuery("#pwstrengthbox").html("{/literal}{$LANG.pwstrengthenter}{literal}");
		} else if ( pwstrength < 30 ) {
			jQuery("#pwstrengthbox").html("{/literal}{$LANG.pwstrengthweak}{literal}");
			jQuery("#pwstrengthbox").addClass("weak");
		} else if ( pwstrength < 75 ) {
			jQuery("#pwstrengthbox").html("{/literal}{$LANG.pwstrengthmoderate}{literal}");
			jQuery("#pwstrengthbox").addClass("moderate");
		} else {
			jQuery("#pwstrengthbox").html("{/literal}{$LANG.pwstrengthstrong}{literal}");
			jQuery("#pwstrengthbox").addClass("strong");
		}
    });
});
</script>
{/literal}
<div id="pwstrengthbox">{$LANG.pwstrengthenter}</div>