function adjustDropDownWrapWidth() {
	jQuery('.cell-menu-wrap').each(function () {
		var width = jQuery( '.btn-group > a', jQuery(this) ).outerWidth();
		width = width + jQuery( '.btn-group > button', jQuery(this) ).outerWidth();
		width = width + jQuery(this).outerWidth() - jQuery(this).width();
		width = width + 4;
		
		jQuery(this).css( 'min-width', width);
		jQuery(this).css( 'width', width);
	});
}

jQuery(document).ready(function(){
	// Fix header on scroll
	jQuery(window).scroll(function (event) {
		if ( jQuery(window).width() <= 1024 ) return;
		
		var scroll = $(window).scrollTop();

		if ( scroll != 0 ) {
			jQuery('.main-menu > li > a').css('line-height', '44px');
			
			jQuery('.logo img').attr('height', 34);
			jQuery('.logo img').attr('width', 106);
		} else {
			jQuery('.main-menu > li > a').css('line-height', '88px');
			jQuery('.logo img').attr('height', 78);
			jQuery('.logo img').attr('width', 243);
		}
	});
	
	jQuery(window).resize(function (event) {
		if ( jQuery(window).width() <= 1024 ) {
			jQuery('.main-menu > li > a').css('line-height', '44px');
			
			jQuery('.logo img').attr('height', 34);
			jQuery('.logo img').attr('width', 106);
		}

		if ( jQuery(window).width() <= 768 ) {
			jQuery('.main-menu-box').css('display', 'none');
		} else {
			jQuery('.main-menu-box').css('display', 'block');
		}
		
	});

	// Menu routines
	jQuery('#menu-toggle').click(function (event) {
		jQuery('.main-menu-box').css('display', 'block');
	});

	jQuery('#menu-toggle-close').click(function (event) {
		jQuery('.main-menu-box').css('display', 'none');
	});

	jQuery('a').each(function () {
		if ( jQuery(this).attr('href') == '#' ) {
			jQuery(this).click( function(e) {
				e.preventDefault();
			});
		}
	});
	
	// Auto width for table-cells with menu buttons
	adjustDropDownWrapWidth();
	jQuery( 'select[name="language"]' ).change(function () {
		adjustDropDownWrapWidth();
	});
});
