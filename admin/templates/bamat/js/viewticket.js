function showRating(replyID, rating) {
	for ( i = 1; i <= rating; i++) {
		jQuery('#ticket-rating-' + replyID + '-' + i).removeClass('ticket-rating-neg');
		jQuery('#ticket-rating-' + replyID + '-' + i).addClass('ticket-rating-pos');
	}
	
	for ( i = 5; i > rating; i--) {
		jQuery('#ticket-rating-' + replyID + '-' + i).removeClass('ticket-rating-pos');
		jQuery('#ticket-rating-' + replyID + '-' + i).addClass('ticket-rating-neg');
	}
}

function postRating(ticketID, c, replyID, rating) {
	window.location = 'viewticket.php?tid=' + ticketID + '&c=' + c + '&rating=rate' + replyID + '_' + rating;
}

jQuery(document).ready(function(){
	// On rating stars hover - show rating from first till selected
	jQuery('.ticket-rating-img-active').mouseover( function() {
		showRating( jQuery(this).attr('data-reply-id'), jQuery(this).attr('data-rating') );
	});
	
	// On rating stars leave 
	jQuery('.ticket-rating-stars-active').mouseleave( function() {
		showRating( jQuery(this).attr('data-reply-id'), 0 );
	});

	// On hover over "Poor" caption
	jQuery('.ticket-rating-mark-neg').mouseover( function() {
		showRating( jQuery(this).attr('data-reply-id'), 1 );
	});
	
	jQuery('.ticket-rating-mark-neg').mouseleave( function() {
		showRating( jQuery(this).attr('data-reply-id'), 0 );
	});

	// On hover over "Excellent" caption
	jQuery('.ticket-rating-mark-pos').mouseover( function() {
		showRating( jQuery(this).attr('data-reply-id'), 5 );
	});

	jQuery('.ticket-rating-mark-pos').mouseleave( function() {
		showRating( jQuery(this).attr('data-reply-id'), 0 );
	});
	
	// Post rating
	jQuery('.ticket-rating-mark-pos').click( function() {
		postRating(
			jQuery(this).attr('data-ticket-id'),
			jQuery(this).attr('data-ticket-c'),
			jQuery(this).attr('data-reply-id'),
			5 );
	});
	
	jQuery('.ticket-rating-mark-neg').click( function() {
		postRating(
			jQuery(this).attr('data-ticket-id'),
			jQuery(this).attr('data-ticket-c'),
			jQuery(this).attr('data-reply-id'),
			1 );
	});

	jQuery('.ticket-rating-img-active').click( function() {
		postRating(
			jQuery(this).attr('data-ticket-id'),
			jQuery(this).attr('data-ticket-c'),
			jQuery(this).attr('data-reply-id'),
			jQuery(this).attr('data-rating') );
	});
	
	// Show/hide repry form
	jQuery('#ticket-reply-top').click( function() {
		if ( jQuery('#submitreply').is(':visible') ) {
			jQuery('#submitreply').slideUp();
		} else {
			jQuery('#submitreply').prependTo('#replyform-top');
			jQuery('#submitreply').slideDown();
		}
	});
	
	jQuery('#ticket-reply-bottom').click( function() {
		console.log('bottom');
		if ( jQuery('#submitreply').is(':visible') ) {
			jQuery('#submitreply').slideUp();
		} else {
			jQuery('#submitreply').prependTo('#replyform-bottom');
			jQuery('#submitreply').slideDown();
		}
	});
});
