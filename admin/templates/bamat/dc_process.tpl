<form method="post" action="{$systemsslurl}domain_check.php">

{if $dc_debug}<span style="display: none;">{$tld.dc_run_time}</span>{/if}

<table class="table table-framed" id="dc_table">
    <thead>
        <tr class="dc_tr_btn">
            <th colspan="5" class="dc_th_price">{$LANG.domain_check.tablepricenotice}</th>
            <th><input type="submit" value="{if $dc_is_product}{$LANG.updatecart}{else}{$LANG.domain_check.domainproceed}{/if}" class="btn btn-danger" /></th>
        </tr>

        <tr class="dc_tr_head">
            <th>{$LANG.domain_check.tabledomain}</th>
            <th>{$LANG.domain_check.tablestatus}</th>
            <th>{$LANG.domain_check.tablesetupprice}</th>
            <th>{$LANG.domain_check.tableprice}</th>
            <th>{$LANG.domain_check.tablecategory}</th>
            <th id="dc_check_all"><label><input type="checkbox" /> {$LANG.domain_check.tablecheckall}</label></th>
        </tr>
    </thead>

    <tbody>
	
	{foreach from=$tlds item=tld}
	
		<tr class="{if $tld.status === false}dc_fail{elseif $tld.status === true}dc_success{else}dc_not_checked{/if}">
			<td class="dc_td_name">{$domain}{$tld.tld}{if $dc_debug}<span style="display: none;">{$tld.result}</span>{/if}</td>
			<td class="dc_td_status"><div class="dc_status_sign"></div> {if $tld.status === false}{$LANG.domain_check.statustaken}{elseif $tld.status === true}{$LANG.domain_check.statusfree}{else}Can not check{/if}</td>
			<td class="dc_td_setup"><span>{$LANG.ordersetupfee}: </span>{if !$tld.status}{$tld.trans_price}{else}{$tld.reg_price}{/if}</td>
			<td class="dc_td_price">{$tld.year_price}*</td>
			<td class="dc_td_info">{$tld.description}</td>
			<td class="dc_td_check">
				<label><input type="checkbox" name="domains[]" value="{$domain}{$tld.tld}"/> {if !$tld.status}{$LANG.domain_check.actiontransfer}{else}{$LANG.domain_check.actionregister}{/if}</label>

				<input type="hidden" name="domainsregperiod[{$domain}{$tld.tld}]" value="1" />
				<input type="hidden" name="domainsaction[{$domain}{$tld.tld}]" value="{if !$tld.status}transfer{else}register{/if}" />
				
			</td>
		</tr>
	{/foreach}
	
    </tbody>

    <tfoot>
        <tr class="dc_tr_btn">
            <th colspan="5" class="dc_th_price">{$LANG.domain_check.tablepricenotice}</th>
            <th><input type="submit" value="{if $dc_is_product}{$LANG.updatecart}{else}{$LANG.domain_check.domainproceed}{/if}" class="btn btn-danger" /></th>
        </tr>
    </tfoot>
</table>

{if $dc_is_product}
	<input type="hidden" name="dc_is_product" value="{$dc_is_product}" />
	<input type="hidden" name="dc_pid" value="{$dc_get_vars.pid}" />
{/if}

</form>

{literal}
<script language="javascript">
// "Check All" in domain results table
jQuery('body').on( 'change', '#dc_check_all input[type="checkbox"]', function() {
	jQuery('#dc_table').find('.dc_td_check input[type="checkbox"]').prop( 'checked', jQuery('#dc_check_all [type="checkbox"]').is(':checked') );
});

// Make the whole row clickable
jQuery('body').on( 'click', '#dc_table tr', function(e) {
	if (e.target.nodeName == 'INPUT' || e.target.nodeName == 'LABEL') {
		return;
	}
	
	var check = jQuery('.dc_td_check input[type="checkbox"]', this);

	if ( check.length > 0 ) {
		if ( check.is(':checked') ) {
			check.prop('checked', false);
		} else {
			check.prop('checked', true);
		}
	}
});
</script>
{/literal} 