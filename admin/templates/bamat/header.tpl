<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset={$charset}" />
    <title>{if $kbarticle.title}{$kbarticle.title} - {/if}{$pagetitle} - {$companyname}</title>

    {if $systemurl}<base href="{$systemurl}" />{/if}
	<script type="text/javascript" src="includes/jscript/jquery.js"></script>
    {if $livehelpjs}{$livehelpjs}{/if}
    <link href="templates/{$template}/css/bootstrap.css" rel="stylesheet">
    <link href="templates/{$template}/css/whmcs.css" rel="stylesheet">
    <link href="templates/{$template}/css/bamat.css" rel="stylesheet">

	<style type='text/css'>		
		@font-face {literal} { {/literal} font-family: 'entypo-fontello'; font-weight: normal; font-style: normal;
			src: url('//{$smarty.server.SERVER_NAME}/templates/{$template}/fonts/entypo-fontello.eot?v=3');
			src: url('//{$smarty.server.SERVER_NAME}/templates/{$template}/fonts/entypo-fontello.eot?v=3#iefix') format('embedded-opentype'), 
				url('//{$smarty.server.SERVER_NAME}/templates/{$template}/fonts/entypo-fontello.woff?v=3') format('woff'), 
				url('//{$smarty.server.SERVER_NAME}/templates/{$template}/fonts/entypo-fontello.ttf?v=3') format('truetype'), 
				url('//{$smarty.server.SERVER_NAME}/templates/{$template}/fonts/entypo-fontello.svg?v=3#entypo-fontello') format('svg');
			{literal} } {/literal}

	</style>

	<link rel="icon" href="templates/{$template}/img/bamat-icon.ico" type="image/x-icon">
	
    <script src="templates/{$template}/js/whmcs.js"></script>
    <script src="templates/{$template}/js/bamat.js"></script>

    {$headoutput}

  </head>

  <body>

{$headeroutput}

<header>

{dc_is_current_page page="home"}
	<div class="container-fluid social-wrap">
		<div class="container">
			<div class="row top-menu-wrap">
				<ul class="social-bookmarks">
					<li class="social-link social-link-facebook">
						<a target="_blank" href="https://www.facebook.com/pages/General-Internet/483679735067038" title="Facebook"></a>
					</li>
					<li class="social-link social-link-twitter">
						<a target="_blank" href="https://twitter.com/Best_SeoHosting" title="Twitter"></a>
					</li>
					<li class="social-link social-link-google">
						<a target="_blank" href="https://plus.google.com/109933878477951881560/about" title="Google"></a>
					</li>
				</ul>
				
				<ul class="top-menu">
					<li><a href="http://best-seo-hosting.com/home/impressum/">Impressum</a></li>
					<li><a href="http://best-seo-hosting.com/home/disclaimer/">Disclaimer</a></li>
					<li class="last"><a href="http://best-seo-hosting.com/home/kontakt/">Kontakt</a></li>
				</ul>

				<div class="phone"><span>Tel.: +41 41 630-02.30</span></div>
			</div>
		</div>
	</div>
		
	<div class="container-fluid main-menu-wrap">
		<div class="container">
			<div class="row main-menu-row">
				<div class="logo pull-left">
					<a href="index.php">
						<img src="templates/{$template}/img/bamat-logo.jpg" alt="Best Seo Hosting - der Turbo für SEO-Spezialisten" height="78" width="243">
					</a>
				</div>

				<a id="menu-toggle" class="toggle" href="#"></a>
				
				<div class="main-menu-box pull-right">
					<div class="menu-toggle-close-wrap">
						<a id="menu-toggle-close" class="toggle" href="#"></a>
					</div>
					
					<ul class="main-menu">
						{if $loggedin}
							{assign var=home_page value='clientarea.php'}
						{else}
							{assign var=home_page value='index.php'}
						{/if}
						<li {dc_is_current_page page=$home_page}><a id="Menu-Home" href="{$home_page}">{$LANG.hometitle}</a></li>
						
						{if $loggedin}
							<li {dc_is_current_page page='clientarea.php?action=products|index.php?m=project_management|cart.php|cart.php?gid=addons'}>
								<a id="Menu-Services" href="#">{$LANG.navservices}</a>
								<ul class="sub-menu">
									<li><a id="Menu-Services-My_Services" href="clientarea.php?action=products">{$LANG.clientareanavservices}</a></li>
									{if $condlinks.pmaddon}<li><a id="Menu-Services-My_Projects" href="index.php?m=project_management">{$LANG.clientareaprojects}</a></li>{/if}
									<li class="divider"></li>
									<li><a id="Menu-Services-Order_New_Services" href="cart.php">{$LANG.navservicesorder}</a></li>
									<li><a id="Menu-Services-View_Available_Addons" href="cart.php?gid=addons">{$LANG.clientareaviewaddons}</a></li>
								</ul>
							</li>

							{if $condlinks.domainreg || $condlinks.domaintrans}
							<li {dc_is_current_page page='clientarea.php?action=domains|cart.php?gid=renewals|cart.php?a=add&domain=register|cart.php?a=add&domain=transfer|{$enomnewtldslink}|domain_check.php'}>
								<a id="Menu-Domains" href="#">{$LANG.navdomains}</a>
								<ul class="sub-menu">
									<li><a id="Menu-Domains-My_Domains" href="clientarea.php?action=domains">{$LANG.clientareanavdomains}</a></li>
									<li class="divider"></li>
									<li><a id="Menu-Domains-Renew_Domains" href="cart.php?gid=renewals">{$LANG.navrenewdomains}</a></li>
									{if $condlinks.domainreg}<li><a id="Menu-Domains-Register_a_New_Domain" href="cart.php?a=add&domain=register">{$LANG.navregisterdomain}</a></li>{/if}
									{if $condlinks.domaintrans}<li><a id="Menu-Domains-Transfer_Domains_to_Us" href="cart.php?a=add&domain=transfer">{$LANG.navtransferdomain}</a></li>{/if}
									{if $enomnewtldsenabled}<li><a id="Menu-Domains-Preregister_New_TLDs" href="{$enomnewtldslink}">Preregister New TLDs</a></li>{/if}
									<li class="divider"></li>
									<li><a id="Menu-Domains-Whois_Lookup" href="domain_check.php">{$LANG.navwhoislookup}</a></li>
								</ul>
							</li>
							{/if}
							
							<li {dc_is_current_page page='clientarea.php?action=invoices|clientarea.php?action=quotes|clientarea.php?action=addfunds|clientarea.php?action=masspay&all=true|clientarea.php?action=creditcard'}>
								<a id="Menu-Billing" href="#">{$LANG.navbilling}s</a>
								<ul class="sub-menu">
									<li><a id="Menu-Billing-My_Invoices" href="clientarea.php?action=invoices">{$LANG.invoices}</a></li>
									<li><a id="Menu-Billing-My_Quotes" href="clientarea.php?action=quotes">{$LANG.quotestitle}</a></li>
									<li class="divider"></li>
									{if $condlinks.addfunds}<li><a id="Menu-Billing-Add_Funds" href="clientarea.php?action=addfunds">{$LANG.addfunds}</a></li>{/if}
									{if $condlinks.masspay}<li><a id="Menu-Billing-Mass_Payment" href="clientarea.php?action=masspay&all=true">{$LANG.masspaytitle}</a></li>{/if}
									{if $condlinks.updatecc}<li><a id="Menu-Billing-Manage_Credit_Card" href="clientarea.php?action=creditcard">{$LANG.navmanagecc}</a></li>{/if}
								</ul>
							</li>
							
							<li {dc_is_current_page page='supporttickets.php|knowledgebase.php|downloads.php|serverstatus.php'}>
								<a id="Menu-Support"href="#">{$LANG.navsupport}</a>
								<ul class="sub-menu">
									<li><a id="Menu-Support-Tickets" href="supporttickets.php">{$LANG.navtickets}</a></li>
									<li><a id="Menu-Support-Knowledgebase" href="knowledgebase.php">{$LANG.knowledgebasetitle}</a></li>
									<li><a id="Menu-Support-Downloads" href="downloads.php">{$LANG.downloadstitle}</a></li>
									<li><a id="Menu-Support-Network_Status" href="serverstatus.php">{$LANG.networkstatustitle}</a></li>
								</ul>
							</li>
							
							<li {dc_is_current_page page='submitticket.php'}><a id="Menu-Open_Ticket" href="submitticket.php">{$LANG.navopenticket}</a></li>
							
							{if $condlinks.affiliates}<li {dc_is_current_page page='affiliates.php'}><a id="Menu-Affiliates" href="affiliates.php">{$LANG.affiliatestitle}</a></li>{/if}
							
							{if $livehelp}<li><a id="Menu-Live_Chat" href="#" class="LiveHelpButton">Live Chat - <span class="LiveHelpTextStatus">Offline</span></a></li>{/if}
							
							<li {dc_is_current_page page='clientarea.php?action=details|clientarea.php?action=creditcard|clientarea.php?action=contacts|clientarea.php?action=addfunds|clientarea.php?action=emails|clientarea.php?action=changepw|logout.php'}>
								<a href="#" id="Menu-Hello_User">{$LANG.hello}, {$loggedinuser.firstname}!</a>
								<ul class="sub-menu">
									<li><a id="Menu-Hello_User-Edit_Account_Details" href="clientarea.php?action=details">{$LANG.editaccountdetails}</a></li>
									{if $condlinks.updatecc}<li><a id="Menu-Hello_User-Contacts_Sub-Accounts" href="clientarea.php?action=creditcard">{$LANG.navmanagecc}</a></li>{/if}
									<li><a href="clientarea.php?action=contacts">{$LANG.clientareanavcontacts}</a></li>
									{if $condlinks.addfunds}<li><a id="Menu-Hello_User-Add_Funds" href="clientarea.php?action=addfunds">{$LANG.addfunds}</a></li>{/if}
									<li><a id="Menu-Hello_User-Email_History" href="clientarea.php?action=emails">{$LANG.navemailssent}</a></li>
									<li><a id="Menu-Hello_User-Change_Password" href="clientarea.php?action=changepw">{$LANG.clientareanavchangepw}</a></li>
									<li class="divider"></li>
									<li><a id="Menu-Hello_User-Logout" href="logout.php">{$LANG.logouttitle}</a></li>
								</ul>
							</li>

						{else}	
							<li {dc_is_current_page page="announcements.php"}><a id="Menu-Annoucements" href="announcements.php">{$LANG.announcementstitle}</a></li>
							<li {dc_is_current_page page="knowledgebase.php"}><a id="Menu-Knowledgebase" href="knowledgebase.php">{$LANG.knowledgebasetitle}</a></li>
							<li {dc_is_current_page page="serverstatus.php"}><a id="Menu-Network_Status" href="serverstatus.php">{$LANG.networkstatustitle}</a></li>
							<li {dc_is_current_page page="affiliates.php"}><a id="Menu-Affiliates" href="affiliates.php">{$LANG.affiliatestitle}</a></li>
							<li {dc_is_current_page page="contact.php"}><a id="Menu-Contact_Us" href="http://best-seo-hosting.com/home/kontakt/">{$LANG.contactus}</a></li>
							{if $livehelp}<li><a id="Menu-Live_Chat" href="#" class="LiveHelpButton">Live Chat - <span class="LiveHelpTextStatus">Offline</span></a></li>{/if}

							<li {dc_is_current_page page='clientarea.php|register.php|pwreset.php'}>
								<a id="Menu-Account" href="#">{$LANG.account}</a>
								<ul class="sub-menu">
									<li><a id="Menu-Account-Login" href="clientarea.php">{$LANG.login}</a></li>
									<li><a id="Menu-Account-Register" href="register.php">{$LANG.register}</a></li>
									<li class="divider"></li>
									<li><a id="Menu-Account-Forgot_Password" href="pwreset.php">{$LANG.forgotpw}</a></li>
								</ul>
							</li>

						{/if}
						
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="whmcsheader">
    <div class="whmcscontainer">
        <div id="whmcstxtlogo"><a href="index.php">{$companyname}</a></div>
    </div>
</div>

<div class="content">
	<div class="container-fluid">
		<div class="container">
			<div class="row">

{if $pagetitle eq $LANG.carttitle}<div id="whmcsorderfrm">{/if}

