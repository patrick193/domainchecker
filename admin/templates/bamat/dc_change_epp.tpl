{include file="$template/pageheader.tpl" title=$LANG.domaingeteppcode}

<div class="alert {$alert_class}">
    <p>{$error}</p>
</div>

<form method="get" action="clientarea.php">
	<input type="hidden" name="id" value="{$domainid}" />
	<input type="hidden" name="action" value="domaindetails" />

	<p><input type="submit" value="{$LANG.clientareabacklink}" class="btn" /></p>
</form>
