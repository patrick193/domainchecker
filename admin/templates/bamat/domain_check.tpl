{include file="$template/pageheader.tpl" title=$LANG.domaintitle}

<div id="domain_check">
	<div class="wrap">
	</div>
</div>
		
<div id="dc_preloader">
	<div class="progress reverse">
		<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">&nbsp;</div>
	</div>

	{if $dc_do_search}
		<div id="dc_statustext">{$LANG.domain_check.waitforsearch}</div>
	{else}
		<div id="dc_statustext">{$LANG.domain_check.preparepage}</div>
	{/if}
</div>

<script type="text/javascript">
{$dc_ajax}
</script>
