{include file="$template/pageheader.tpl" title=$LANG.clientareaproducts desc=$LANG.clientareaproductsintro}

<div class="searchbox">
    <form method="post" action="clientarea.php?action=products">
        <div class="input-append">
            <input type="text" name="q" value="{if $q}{$q}{else}{$LANG.searchenterdomain}{/if}" class="input-medium appendedInputButton" onfocus="if(this.value=='{$LANG.searchenterdomain}')this.value=''" /><button type="submit" class="btn">{$LANG.searchfilter}</button>
        </div>
    </form>
</div>

<div class="resultsbox">
<p>{$numitems} {$LANG.recordsfound}, {$LANG.page} {$pagenumber} {$LANG.pageof} {$totalpages}</p>
</div>

<table class="table table-striped table-framed">
    <thead>
        <tr>
            <th{if $orderby eq "product"} class="headerSort{$sort}"{/if}><a href="clientarea.php?action=products{if $q}&q={$q}{/if}&orderby=product">{$LANG.orderproduct}</a></th>
            <th{if $orderby eq "price"} class="headerSort{$sort}"{/if}><a href="clientarea.php?action=products{if $q}&q={$q}{/if}&orderby=price">{$LANG.orderprice}</a></th>
            <th{if $orderby eq "billingcycle"} class="headerSort{$sort}"{/if}><a href="clientarea.php?action=products{if $q}&q={$q}{/if}&orderby=billingcycle">{$LANG.orderbillingcycle}</a></th>
            <th{if $orderby eq "nextduedate"} class="headerSort{$sort}"{/if}><a href="clientarea.php?action=products{if $q}&q={$q}{/if}&orderby=nextduedate">{$LANG.clientareahostingnextduedate}</a></th>
            <th{if $orderby eq "status"} class="headerSort{$sort}"{/if}><a href="clientarea.php?action=products{if $q}&q={$q}{/if}&orderby=status">{$LANG.clientareastatus}</a></th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>

{foreach from=$services item=service}
        <tr>
            <td><strong>{$service.group} - {$service.product}</strong>{if $service.domain}<br /><a href="http://{$service.domain}" target="_blank">{$service.domain}</a>{/if}</td>
            <td>{$service.amount}</td>
            <td>{$service.billingcycle}</td>
            <td>{$service.nextduedate}</td>
            <td><span class="label {$service.rawstatus}">{$service.statustext}</span></td>
            <td class="cell-menu-wrap">
                <div class="btn-group">
					<a class="btn" href="clientarea.php?action=productdetails&id={$service.id}"> <span class="glyphicon glyphicon-list-alt"></span> {$LANG.clientareaviewdetails}</a>
					{if $service.rawstatus == "active" && ($service.downloads || $service.addons || $service.packagesupgrade || $service.showcancelbutton)}
					<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu cell-menu" role="menu">
						{if $service.downloads} <li><a href="clientarea.php?action=productdetails&id={$service.id}#tab3"><span class="glyphicon glyphicon-download"></span> {$LANG.downloadstitle}</a></li>{/if}
						{if $service.addons} <li><a href="clientarea.php?action=productdetails&id={$service.id}#tab4"><span class="glyphicon glyphicon-th-large"></span> {$LANG.clientareahostingaddons}</a></li>{/if}
						{if $service.packagesupgrade} <li><a href="upgrade.php?type=package&id={$service.id}#tab3"><span class="glyphicon glyphicon-resize-vertical"></span> {$LANG.upgradedowngradepackage}</a></li>{/if}
						{if ($service.addons || $service.downloads || $service.packagesupgrade) && $service.showcancelbutton} <li class="divider"></li>{/if}
						{if $service.showcancelbutton} <li><a href="clientarea.php?action=cancel&id={$service.id}"><span class="glyphicon glyphicon-off"></span> {$LANG.clientareacancelrequestbutton}</a></li>{/if}
					</ul>

					{/if}
                </div>
            </td>
        </tr>
{foreachelse}
        <tr>
            <td colspan="6" class="textcenter">{$LANG.norecordsfound}</td>
        </tr>
{/foreach}
    </tbody>
</table>

{include file="$template/clientarearecordslimit.tpl" clientareaaction=$clientareaaction}

<nav>
	<ul class="pagination">
		<li {if !$prevpage}class="disabled"{/if}><a href="{if $prevpage}clientarea.php?action=products{if $q}&q={$q}{/if}&amp;page={$prevpage}{else}javascript:return false;{/if}"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
		<li {if !$nextpage}class="disabled"{/if}><a href="{if $nextpage}clientarea.php?action=products{if $q}&q={$q}{/if}&amp;page={$nextpage}{else}javascript:return false;{/if}"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
	</ul>
</nav>

