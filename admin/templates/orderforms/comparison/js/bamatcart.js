function adjustProductHeights() {
	// Same height for product blocks
	var max_height = 0;
	
	jQuery( '.prodtablecol' ).each(	function() {
		if ( jQuery(this).outerHeight() > max_height )
			max_height = jQuery(this).outerHeight()
	});
	
	jQuery( '.prodtablecol' ).each(	function() {
		if ( jQuery(this).outerHeight() < max_height ) {
			var diff = max_height - jQuery(this).outerHeight();
			var spacer = jQuery( '.prod-col-desc', jQuery(this) ).last();
			
			spacer.height( spacer.outerHeight() + diff ); // 20 = padding-top + padding-bottom
			
		}
	});
}

jQuery(document).ready(function(){
	adjustProductHeights();
	
	jQuery(window).resize( function() {
		adjustProductHeights();
	});
});
