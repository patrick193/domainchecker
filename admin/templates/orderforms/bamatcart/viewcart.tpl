<script type="text/javascript" src="includes/jscript/pwstrength.js"></script>
<script type="text/javascript" src="includes/jscript/creditcard.js"></script>

{literal}
<script>
function removeItem(type,num) {
    var response = confirm("{/literal}{$LANG.cartremoveitemconfirm}{literal}");
    if (response) {
        window.location = 'cart.php?a=remove&r='+type+'&i='+num;
    }
}

function emptyCart(type,num) {
    var response = confirm("{/literal}{$LANG.cartemptyconfirm}{literal}");
    if (response) {
        window.location = 'cart.php?a=empty';
    }
}
</script>
{/literal}

<script>
window.langPasswordStrength = "{$LANG.pwstrength}";
window.langPasswordWeak = "{$LANG.pwstrengthweak}";
window.langPasswordModerate = "{$LANG.pwstrengthmoderate}";
window.langPasswordStrong = "{$LANG.pwstrengthstrong}";
</script>

<div id="order-comparison">

{include file="orderforms/bamatcart/comparisonsteps.tpl" step=3}

<div class="col-md-12">

{if $errormessage}
	<div class="alert alert-danger cart_errors">
		{$LANG.clientareaerrors}
		<ul>{$errormessage}</ul>
	</div>
{elseif $promotioncode && $rawdiscount eq "0.00"}
	<div class="alert alert-danger cart_errors">
		{$LANG.promoappliedbutnodiscount}
	</div>
{/if}

{if $bundlewarnings}
<div class="alert alert-danger">
	<strong>{$LANG.bundlereqsnotmet}</strong><br />
	
	{foreach from=$bundlewarnings item=warning}
		{$warning}<br />
	{/foreach}
</div>
{/if}

<h3>{$LANG.ordersummary}</h3>

<form method="post" action="{$smarty.server.PHP_SELF}?a=view" id="bcart_form">

<table class="table table-framed" id="cart_view">
<thead>
	<tr>
		<th>{$LANG.orderdesc}</th>
		<th>{$LANG.orderprice}</th>
	</tr>
</thead>

<tbody>
	{foreach key=num item=product from=$products}
		<tr class="bcart_product">
			<td>
				<div class="bcart_prod_title">
					<span class="bcart_prod_group">{$product.productinfo.groupname} - </span><span class="bcart_prod_name">{$product.productinfo.name}</span> {if $product.domain}<span class="bcart_prod_domain">({$product.domain})</span>{/if}
				</div>

				<div class="bcart_padding">
					{if $product.configoptions}
						<ul class="bcart_prod_options">
						{foreach key=confnum item=configoption from=$product.configoptions}
							<li>
							{$configoption.name}: {if $configoption.type eq 1 || $configoption.type eq 2}{$configoption.option}{elseif $configoption.type eq 3}{if $configoption.qty}{$LANG.yes}{else}{$LANG.no}{/if}{elseif $configoption.type eq 4}{$configoption.qty} x {$configoption.option}{/if}
							</li>
						{/foreach}
						</ul>
					{/if}
					
					{if $product.allowqty}
						<div class="bcart_qty_wrap">{$LANG.quantity}: <span class="glyphicon glyphicon-arrow-down text-success qty_btn" data-qty-id="{$num}" data-qty-type="0"></span> <input class="cart_qty text-center" type="text" disabled="disabled" name="qty[{$num}]" id="qty_{$num}" value="{$product.qty}" /> <span class="glyphicon glyphicon-arrow-up text-success qty_btn" data-qty-id="{$num}" data-qty-type="1"></span></div>
					{/if}

					<a href="{$smarty.server.PHP_SELF}?a=confproduct&i={$num}" class="cartedit text-warning">{$LANG.carteditproductconfig}</a>
					<a href="#" onclick="removeItem('p','{$num}');return false" class="cartremove text-danger text-right" title="{$LANG.cartremove}">{$LANG.cartremove}</a>
				</div>
			</td>
		
			<td id="pricing_{$num}" class="text-center bcart_td_price">
				<strong>{$product.pricingtext}{if $product.proratadate}<br />({$LANG.orderprorata} {$product.proratadate}){/if}</strong>
			</td>

		</tr>

		{foreach key=addonnum item=addon from=$product.addons}
		<tr class="carttableproduct">
			<td>
				<div class="bcart_prod_subtitle bcart_padding">
					<span class="bcart_prod_group">{$LANG.orderaddon} - </span><span class="bcart_prod_name">{$addon.name}</span>
				</div>
			</td>
			
			<td class="text-center bcart_td_price">
				<strong>{$addon.pricingtext}</strong>
			</td>
		</tr>
		{/foreach}
	{/foreach}

	{foreach key=num item=addon from=$addons}
	<tr class="carttableproduct">
		<td>
			<strong>{$addon.name}</strong>
			<br />
			{$addon.productname}{if $addon.domainname} - {$addon.domainname}{/if}
			<br />
			<a href="#" onclick="removeItem('a','{$num}');return false" class="cartremove text-danger">{$LANG.cartremove}</a>
		</td>
		
		<td class="text-center bcart_td_price">
			<strong>{$addon.pricingtext}</strong>
		</td>
		
		<td></td>
	</tr>
	{/foreach}

	{foreach key=num item=domain from=$domains}
	<tr class="bcart_product">
		<td>
			<div class="bcart_prod_title">
				<span class="bcart_prod_group">{if $domain.type eq "register"}{$LANG.orderdomainregistration}{else}{$LANG.orderdomaintransfer}{/if} - </span><span class="bcart_prod_name">{$domain.domain}</span><span class="bcart_prod_group"> - {$domain.regperiod} {$LANG.orderyears}</span>
			</div>

			<div class="bcart_padding">
				{if $domain.dnsmanagement || $domain.emailforwarding || $domain.idprotection}
				<ul class="bcart_prod_options">
					{if $domain.dnsmanagement}<li>{$LANG.domaindnsmanagement}</li>{/if}
					{if $domain.emailforwarding}<li>{$LANG.domainemailforwarding}</li>{/if}
					{if $domain.idprotection}<li>{$LANG.domainidprotection}</li>{/if}
					</ul>
				{/if}

				<a href="{$smarty.server.PHP_SELF}?a=confdomains" class="cartedit text-warning">{$LANG.cartconfigdomainextras}</a>
				<a href="#" onclick="removeItem('d','{$num}');return false" class="cartremove text-danger">{$LANG.cartremove}</a>
			</div>
		</td>
		
		<td class="text-center bcart_td_price">
			<strong>{$domain_prices[$domain.domain].price_text}</strong>
			<br />
			<em>{$domain.price}</em>
		</td>
	</tr>
	{/foreach}

	{foreach key=num item=domain from=$renewals}
	<tr class="carttableproduct">
		<td>
			<div class="bcart_prod_title">
				<span class="bcart_prod_group">{$LANG.domainrenewal} - </span><span class="bcart_prod_name">{$domain.domain}</span><span class="bcart_prod_group"> - {$domain.regperiod} {$LANG.orderyears}</span>
			</div>

			<div class="bcart_padding">
				{if $domain.dnsmanagement || $domain.emailforwarding || $domain.idprotection}
				<ul class="bcart_prod_options">
					{if $domain.dnsmanagement}<li>{$LANG.domaindnsmanagement}</li>{/if}
					{if $domain.emailforwarding}<li>{$LANG.domainemailforwarding}</li>{/if}
					{if $domain.idprotection}<li>{$LANG.domainidprotection}</li>{/if}
					</ul>
				{/if}

				<a href="#" onclick="removeItem('r','{$num}');return false" class="cartremove text-danger">{$LANG.cartremove}</a>
			</div>
		</td>
		
		<td class="text-center bcart_td_price">
			<strong>{$domain.price}</strong>
		</td>
	</tr>
	{/foreach}

	{if $cartitems==0}
	<tr class="clientareatableactive">
		<td colspan="2" class="text-center">
			<br />
			{$LANG.cartempty}
			<br /><br />
		</td>
	</tr>
	{/if}
</tbody>

<tbody id="summary_rows">
	<tr class="summary">
		<td>{$LANG.ordersubtotal}: &nbsp;</td>
		<td>{$subtotal}</td>
	</tr>
	
	{if $promotioncode}
	<tr class="summary">
		<td>{$promotiondescription}: &nbsp;</td>
		<td>{$discount}</td>
	</tr>
	{/if}
	
	{if $taxrate}
	<tr class="summary">
		<td>{$taxname} @ {$taxrate}%: &nbsp;</td>
		<td>{$taxtotal}</td>
	</tr>
	{/if}

	{if $taxrate2}
	<tr class="summary">
		<td>{$taxname2} @ {$taxrate2}%: &nbsp;</td>
		<td>{$taxtotal2}</td>
	</tr>
	{/if}
</tbody>
</table>

</form>

<div class="checkoutbuttonsleft">
	<input type="button" value="{$LANG.emptycart}" onclick="emptyCart();return false" class="btn btn-danger" />
	<input type="button" value="{$LANG.continueshopping}" onclick="window.location='cart.php'" class="btn btn-primary" />
</div>

<div class="checkoutbuttonsright">
	<div class="totalduetoday">{$LANG.ordertotalduetoday}: <span>{$total}</span></div>
	
	{if $totalrecurringmonthly || $totalrecurringquarterly || $totalrecurringsemiannually || $totalrecurringannually || $totalrecurringbiennially || $totalrecurringtriennially}
	<div class="totalrecurring"><p>{$LANG.ordertotalrecurring}: {if $totalrecurringmonthly}<span>{$totalrecurringmonthly} {$LANG.orderpaymenttermmonthly}</span><br />{/if}
		{if $totalrecurringquarterly}<span>{$totalrecurringquarterly} {$LANG.orderpaymenttermquarterly}</span><br />{/if}
		{if $totalrecurringsemiannually}<span>{$totalrecurringsemiannually} {$LANG.orderpaymenttermsemiannually}</span><br />{/if}
		{if $totalrecurringannually}<span>{$totalrecurringannually} {$LANG.orderpaymenttermannually}</span><br />{/if}
		{if $totalrecurringbiennially}<span>{$totalrecurringbiennially} {$LANG.orderpaymenttermbiennially}</span><br />{/if}
		{if $totalrecurringtriennially}<span>{$totalrecurringtriennially} {$LANG.orderpaymenttermtriennially}</span>{/if}</p>
	</div>
	{/if}

	<div class="promo">
		{if $promotioncode}{$LANG.orderpromotioncode}: {$promotioncode} (<a href="{$smarty.server.PHP_SELF}?a=removepromo">Remove Promo Code</a>){else}<input type="text" id="promocode" size="20" value="{$LANG.orderpromotioncode}" onfocus="if(this.value=='{$LANG.orderpromotioncode}')this.value=''" /> <input type="submit" value="{$LANG.go}" onclick="applypromo()" class="btn" />{/if}
	</div>
</div>

<div class="clear"></div>

{foreach from=$gatewaysoutput item=gatewayoutput}
<div class="gatewaycheckout">{$gatewayoutput}</div>
{/foreach}

<h3>{$LANG.yourdetails}</h3>

<form method="post" action="{$smarty.server.PHP_SELF}?a=checkout" id="orderfrm">
	<input type="hidden" name="submit" value="true" />
	<input type="hidden" name="custtype" id="custtype" value="{$custtype}" />

	<!-- Login form -->
	<div class="logincontainer" id="loginfrm" {if $custtype eq "existing" && !$loggedin}{else} style="display:none;"{/if}>
		<p>{$LANG.newcustomersignup|sprintf2:'<a href="#" onclick="showloginform();return false;">':'</a>'}</a></p>

		<fieldset class="control-group text-center">

			<div class="control-group">
				<label class="control-label" for="loginemail">{$LANG.loginemail}:</label>
				<div class="controls">
					<input  type="text" name="loginemail" id="loginemail"  value="{$username}" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="loginpw">{$LANG.loginpassword}:</label>
				<div class="controls">
					<input type="password" name="loginpw" id="loginpw">
				</div>
			</div>
		</fieldset>

	</div>

	<div id="signupfrm"{if $custtype eq "existing" && !$loggedin} style="display:none;"{/if}>

	{if !$loggedin}<p>{$LANG.alreadyregistered} <a href="{$smarty.server.PHP_SELF}?a=login" onclick="showloginform();return false;">{$LANG.clickheretologin}...</a></p>{/if}

	{if $loggedin}
		<table class="table table-striped table-framed table-domain-cart">
		<tbody>
			<tr>
				<th>{$LANG.clientareafirstname}</th>
				<td>{$clientsdetails.firstname}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientarealastname}</th>
				<td>{$clientsdetails.lastname}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareacompanyname}</th>
				<td>{$clientsdetails.companyname}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareaemail}</th>
				<td>{$clientsdetails.email}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareaaddress1}</th>
				<td>{$clientsdetails.address1}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareaaddress2}</th>
				<td>{$clientsdetails.address2}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareacity}</th>
				<td>{$clientsdetails.city}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareastate}</th>
				<td>{$clientsdetails.state}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareapostcode}</th>
				<td>{$clientsdetails.postcode}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareacountry}</th>
				<td>{$clientsdetails.country}</td>
			</tr>
			
			<tr>
				<th>{$LANG.clientareaphonenumber}</th>
				<td>{$clientsdetails.phonenumber}</td>
			</tr>
			
			{foreach key=num item=customfield from=$customfields}
			<tr>
				<td>{$customfield.name}</td>
				<td>{$customfield.input} {$customfield.description}</td>
			</tr>
			{/foreach}
		</tbody>	
		</table>	
	{else}
		<div class="col-md-6">
			<div class="controls">
				<label class="control-label">{$LANG.clientareafirstname}</label>
				<input type="text" name="firstname" value="{$clientsdetails.firstname}" />
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientarealastname}</label>
				<input type="text" name="lastname" value="{$clientsdetails.lastname}" />
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareacompanyname}</label>
				<input type="text" name="companyname" value="{$clientsdetails.companyname}" />
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareaemail}</label>
				<input type="text" name="email" value="{$clientsdetails.email}" />
			</div>
			
			{if $securityquestions}
			<div class="controls">
				<label class="control-label">{$LANG.clientareasecurityquestion}</label>
				<select name="securityqid">
					{foreach key=num item=question from=$securityquestions}
					<option value="{$question.id}"{if $question.id eq $securityqid} selected{/if}>{$question.question}</option>
					{/foreach}
				</select>
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareasecurityanswer}</label>
				<input type="password" name="securityqans" value="{$securityqans}" size="30">
			</div>
			{/if}
			
			{foreach key=num item=customfield from=$customfields}
			<div class="controls">
				<label class="control-label">{$customfield.name}</label>
				{$customfield.input}
				<span class="bcart_reg_custom_desc">{$customfield.description}</span>
			</div>
			{/foreach}
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareapassword}</label>
				<input type="password" name="password" id="newpw" size="20" value="{$password}" />
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareaconfirmpassword}</label>
				<input type="password" name="password2" size="20" value="{$password2}" />
			</div>
			
			<div class="controls">
				<label class="control-label">&nbsp;</label>
				<div class="text-center">{include file="$template/pwstrength.tpl" id='newpw'}</div>
			</div>
			
		</div>

		<div class="col-md-6">
			<div class="controls">
				<label class="control-label">{$LANG.clientareaphonenumber}</label>
				{if $loggedin}<span>{$clientsdetails.phonenumber}</span>{else}<input type="text" name="phonenumber" size="20" value="{$clientsdetails.phonenumber}" />{/if}
			</div>

			<div class="controls">
				<label class="control-label">{$LANG.clientareaaddress1}</label>
				{if $loggedin}<span>{$clientsdetails.address1}</span>{else}<input type="text" name="address1" value="{$clientsdetails.address1}" />{/if}
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareaaddress2}</label>
				{if $loggedin}<span>{$clientsdetails.address2}</span>{else}<input type="text" name="address2" value="{$clientsdetails.address2}" />{/if}
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareacity}</label>
				{if $loggedin}<span>{$clientsdetails.city}</span>{else}<input type="text" name="city" value="{$clientsdetails.city}" />{/if}
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareastate}</label>
				{if $loggedin}<span>{$clientsdetails.state}</span>{else}<input type="text" name="state" value="{$clientsdetails.state}" />{/if}
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareapostcode}</label>
				{if $loggedin}<span>{$clientsdetails.postcode}</span>{else}<input type="text" name="postcode" size="15" value="{$clientsdetails.postcode}" />{/if}
			</div>
			
			<div class="controls">
				<label class="control-label">{$LANG.clientareacountry}</label>
				{if $loggedin}<span>{$clientsdetails.country}</span>{else}{$clientcountrydropdown}{/if}</td>
			</div>

		</div>

		<div class="clearfix"></div>
	{/if}

	{if $taxenabled && !$loggedin}
	<p class="text-center">{$LANG.carttaxupdateselections}</p>
	<p class="text-center"><input type="submit" value="{$LANG.carttaxupdateselectionsupdate}" name="updateonly" class="btn" /></p>
	{/if}

	</div>

	{if $domainsinorder}
	<input type="hidden" name="contact" id="domaincontact" value="">
	
	<div id="domaincontactfields"{if $contact eq "addingnew"} style="display:block"{/if}>
	
	<table class="table table-striped table-framed table-domain-cart">
		<tr>
			<th>{$LANG.clientareafirstname}</th>
			<td><input type="text" name="domaincontactfirstname" style="width:80%;" value="{$domaincontact.firstname}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientarealastname}</th>
			<td><input type="text" name="domaincontactlastname" style="width:80%;" value="{$domaincontact.lastname}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareacompanyname}</th>
			<td><input type="text" name="domaincontactcompanyname" style="width:80%;" value="{$domaincontact.companyname}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareaemail}</th>
			<td><input type="text" name="domaincontactemail" style="width:80%;" value="{$domaincontact.email}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareaaddress1}</th>
			<td><input type="text" name="domaincontactaddress1" style="width:80%;" value="{$domaincontact.address1}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareaaddress2}</th>
			<td><input type="text" name="domaincontactaddress2" style="width:80%;" value="{$domaincontact.address2}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareacity}</th>
			<td><input type="text" name="domaincontactcity" style="width:80%;" value="{$domaincontact.city}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareastate}</th>
			<td><input type="text" name="domaincontactstate" style="width:80%;" value="{$domaincontact.state}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareapostcode}</th>
			<td><input type="text" name="domaincontactpostcode" size="15" value="{$domaincontact.postcode}" /></td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareacountry}</th>
			<td>{$domaincontactcountrydropdown}</td>
		</tr>
		
		<tr>
			<th>{$LANG.clientareaphonenumber}</th>
			<td><input type="text" name="domaincontactphonenumber" size="20" value="{$domaincontact.phonenumber}" /></td>
		</tr>
	</table>
	</div>
{/if}


<div class="viewcartcol2">

<h3>{$LANG.orderpaymentmethod}</h3>
<p class="paymentmethods">{foreach key=num item=gateway from=$gateways}<label><input type="radio" name="paymentmethod" value="{$gateway.sysname}" onclick="{if $gateway.type eq "CC"}showCCForm(){else}hideCCForm(){/if}"{if $selectedgateway eq $gateway.sysname} checked{/if} />{$gateway.name}</label><br />{/foreach}</p>

<div id="ccinputform"{if $selectedgatewaytype neq "CC"} style="display:none;"{/if}>

{if !$clientsdetails.cclastfour}<input type="hidden" name="ccinfo" value="new" />{/if}

<table>
</tbody>
	{if $clientsdetails.cclastfour}
		<tr>
			<td colspan="2">
				<label><input type="radio" name="ccinfo" value="useexisting" id="useexisting" onclick="useExistingCC()"{if $clientsdetails.cclastfour} checked{else} disabled{/if} /> {$LANG.creditcarduseexisting}{if $clientsdetails.cclastfour} ({$clientsdetails.cclastfour}){/if}</label>
				<br />
				<label><input type="radio" name="ccinfo" value="new" id="new" onclick="enterNewCC()"{if !$clientsdetails.cclastfour || $ccinfo eq "new"} checked{/if} /> {$LANG.creditcardenternewcard}</label>
			</td>
		</tr>
	{/if}
	
	<tr class="newccinfo" {if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>
		<td>{$LANG.creditcardcardtype}</td>
		<td>
			<select name="cctype" id="cctype">
				{foreach key=num item=cardtype from=$acceptedcctypes}
					<option{if $cctype eq $cardtype} selected{/if}>{$cardtype}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	
	<tr class="newccinfo" {if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>
		<td>{$LANG.creditcardcardnumber}</td>
		<td><input type="text" name="ccnumber" value="{$ccnumber}" autocomplete="off" /></td>
	</tr>
	
	<tr class="newccinfo" {if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>
		<td>{$LANG.creditcardcardexpires}</td>
		<td>
			<select name="ccexpirymonth" id="ccexpirymonth" class="newccinfo">
				{foreach from=$months item=month}
					<option{if $ccexpirymonth eq $month} selected{/if}>{$month}</option>
				{/foreach}
			</select>
			&nbsp;/&nbsp;
			<select name="ccexpiryyear" class="newccinfo">
				{foreach from=$expiryyears item=year}
					<option{if $ccexpiryyear eq $year} selected{/if}>{$year}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	
	{if $showccissuestart}
	<tr class="newccinfo" {if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>
		<td>{$LANG.creditcardcardstart}</td>
		<td>
			<select name="ccstartmonth" id="ccstartmonth" class="newccinfo">
				{foreach from=$months item=month}
					<option{if $ccstartmonth eq $month} selected{/if}>{$month}</option>
				{/foreach}
			</select>
			&nbsp;/&nbsp;
			<select name="ccstartyear" class="newccinfo">
				{foreach from=$startyears item=year}
					<option{if $ccstartyear eq $year} selected{/if}>{$year}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	
	<tr class="newccinfo" {if $clientsdetails.cclastfour && $ccinfo neq "new"} style="display:none;"{/if}>
		<td>{$LANG.creditcardcardissuenum}</td>
		<td><input type="text" name="ccissuenum" value="{$ccissuenum}" size="5" maxlength="3" /></td>
	</tr>
	{/if}
	
	<tr>
		<td>{$LANG.creditcardcvvnumber}</td>
		<td><input type="text" name="cccvv" id="cccvv" value="{$cccvv}" size="5" autocomplete="off" />
		</td>
	</tr>
	
	<tr>
		<td></td>
		<td><a href="#" onclick="window.open('images/ccv.gif','','width=280,height=200,scrollbars=no,top=100,left=100');return false">{$LANG.creditcardcvvwhere}</a></td>
	</tr>
	
	{if $shownostore}
	<tr>
		<td><input type="checkbox" name="nostore" id="nostore" /></td><td><label for="nostore">{$LANG.creditcardnostore}</label></td>
	</tr>
	{/if}
</tbody>
</table>
</div>

{if $shownotesfield}
<h3>{$LANG.ordernotes}</h3>
<p class="text-center"><textarea name="notes" rows="3" style="width:95%" onFocus="if(this.value=='{$LANG.ordernotesdescription}'){ldelim}this.value='';{rdelim}" onBlur="if (this.value==''){ldelim}this.value='{$LANG.ordernotesdescription}';{rdelim}">{$notes}</textarea></p>
{/if}

<br />

{if $accepttos}
<p class="text-center"><label><input type="checkbox" name="accepttos" id="accepttos" /> {$LANG.ordertosagreement} <a href="{$tosurl}" target="_blank">{$LANG.ordertos}</a></label><p>
{/if}

<p class="text-center"><input type="submit" value="{$LANG.completeorder} &raquo;"{if $cartitems==0} disabled{/if} onclick="this.value='{$LANG.pleasewait}'" class="btn btn-success" /></p>

</div>

<div class="clear"></div>

<div class="checkoutsecure"><img class="text-left" src="images/padlock.gif"alt="Secure Transaction" style="border: 0; padding: 5px 10px 5px 0;" /> {$LANG.ordersecure} (<strong>{$ipaddress}</strong>) {$LANG.ordersecure2}</div>

</div>

</form>

</div>

<div class="hidden" id="sid">{dc_get_session_id}</div>