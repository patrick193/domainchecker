<div id="order-comparison">

<h1>{$LANG.cartbrowse}</h1>

<!-- Product group name -->
{foreach key=num item=productgroup from=$productgroups}
{if $gid eq $productgroup.gid}
<h2 class="dc_product_group">Webhosting</h2>
{/if}
{/foreach}

<!-- Currency switcher -->
{if !$loggedin && $currencies}
{dc_set_user_info}
<div class="dc_currency_switcher">
	<p>{$LANG.domain_check.clientcountry}: <strong>{$dc_country}</strong>. {$LANG.domain_check.clientcurrency|sprintf2:"<strong>`$dc_cur_name`</strong>"} (<em><a href="cart.php?gid={$gid}&currency={$dc_switch_curr_id}" class="dashed_bottom">{$LANG.domain_check.currencyswitch|sprintf2:$dc_switch_curr}?</a></em>)</p>
</div>

<div class="clear"></div>
{/if}

<br>

<!-- First category of features is considered "Top Features" -->
{dc_prepare_product_features products=$products}

<div class="product-row product-row-0">

{if count($dc_features)}
	{if count($dc_features.0)}
		{if count($dc_features.0.features)}
		<div class="productfeature-wrap">
			<div class="productfeature">
				<div class="productfeature-head">
					<img src="http://admin.web-domains.local.com/templates/orderforms/bamatcart/images/schwarm-schatten.jpg" alt="features">
				</div>
					
				{assign var=findex value=0}
				
				{foreach from=$dc_features.0.features item=feature}
					<div class="productfeature-title productfeature-title-{$findex}"><span class="productfeature-cell-text">{$feature.feature}</span></div>
					{assign var=findex value=$findex+1}
				{/foreach}

				<div class="productfeature-footer">
					<a class="product-button" href="#detailed_features">Alle Pakete im Vergleich</a>
				</div>
			</div>

			<div class="productcol-footer-img">&nbsp;</div>
		</div>
		{/if}
	{/if}
{/if}

<!-- "Top Features" for products -->
{assign var=pindex value=0}

{foreach key=num item=product from=$products}
<div class="productcol-wrap productcol-wrap-{$pindex}">
	<div class="productcol">
		<div class="productcol-head">
			<div class="productcol-head-top">
				<h3>{$product.name}</h3>
			</div>

			<div class="productcol-head-circle-wrap">
				<div class="productcol-head-circle">
					<span>{$currency.code}</span>
					{$dc_prices[$num]}
					<span>/ Monat</span>
				</div>
			</div>

			<div class="productcol-head-bottom">
			</div>
		</div>

		{assign var=findex value=0}
		{foreach from=$dc_features.0.features item=feature}
		<div class="productcol-feature productcol-feature-{$findex}">
			<span class="productcol-feature-cell-text"><span class="productcol-feature-cell-title">{$feature.feature}:</span> {$feature.values[$num]}</span>
		</div>
		{assign var=findex value=$findex+1}
		{foreachelse}
		<div class="productcol-description">
			{$product.description}
		</div>
		{/foreach}

		<div class="productcol-footer">
			<a class="product-button" href="//{$smarty.server.HTTP_HOST}{$smarty.server.PHP_SELF}?a=add&{if $product.bid}bid={$product.bid}{else}pid={$product.pid}{/if}">Bestellen</a>
		</div>
	</div>

	<div class="productcol-footer-img">&nbsp;</div>
</div>

{assign var=pindex value=$pindex+1}
{/foreach}
</div>

<!-- Detailed categories with the full list of features -->
{assign var=cindex value=0}

<div class="dc_feature_tabs" id="detailed_features">

{foreach from=$dc_features item=category}
	{if $category.cat neq ' '}
	<div class="dc_feature_tab dc_feature_tab-{$cindex} {if $cindex eq 0}active{else}inactive{/if}">

		<div class="dc_feature_tab_title" data-tab-index="{$cindex}">
			<span class="dc_tab_icon dc_tab_icon_plus">+</span>
			<span class="dc_tab_icon dc_tab_icon_minus">-</span>
			{$category.cat}</div>
		
		<div class="dc_feature_tab_content">
			<ul class="dc_feature_table dc_feature_captions">
				<li class="dc_feature_item-0">{$category.cat}</li>

				{assign var=findex value=1}
				{foreach item=feature from=$category.features}
				<li class="dc_feature_item-{$findex}">{$feature.feature}</li>
				{assign var=findex value=$findex+1}
				{/foreach}
			</ul>
			
			{foreach key=num item=product from=$products}
			<ul class="dc_feature_table">
				<li class="dc_feature_item-0">{$product.name}</li>
				
				{assign var=findex value=1}
				{foreach item=feature from=$category.features}
				<li class="dc_feature_item-{$findex}"><span class="feature_title">{$feature.feature}: </span>{$feature.values[$num]}</li>
				{assign var=findex value=$findex+1}
				{/foreach}
			</ul>
			{/foreach}

			<div class="clear"></div>
		</div>

		{assign var=cindex value=$cindex+1}
	</div>
	{/if}
{/foreach}

</div>

<div class="clear"></div>

</div>
