<div id="order-comparison">

{include file="orderforms/bamatcart/comparisonsteps.tpl" step=1}

<p>{$LANG.cartproductdomaindesc}</p>

{* If there is the only option for domains - don't show it *}
{assign var="show_domainoptions" value=0}
{if $incartdomains or $owndomainenabled or $subdomains}
	{assign var="show_domainoptions" value=1}
{/if}

{if $show_domainoptions eq 1}
	<div class="domainoptions">
	<div><label><input type="radio" name="domainoption" value="regtransfer" /> {$LANG.domain_check.cartregisterortransfer}</label></div>
	{if $incartdomains}
	<div><label><input type="radio" name="domainoption" value="incart" /> {$LANG.cartproductdomainuseincart}</label></div>
	{/if}
	{if $owndomainenabled}
	<div><label><input type="radio" name="domainoption" value="owndomain" /> {$LANG.cartexistingdomainchoice|sprintf2:$companyname}</label></div>
	{/if}
	{if $subdomains}
	<div><label><input type="radio" name="domainoption" value="subdomain" /> {$LANG.cartsubdomainchoice|sprintf2:$companyname}</label></div>
	{/if}
	</div>
{else}
	<input type="hidden" name="domainoption" value="regtransfer" />
{/if}


<br />

<div class="wrap_domain">
	<div class="domainreginput" id="regtransfer_wrap">
		{dc_domain_check_form productdomain=true}
	</div>

	<form class="dc_form dc_add_form" id="checkdomain_form">

	<div class="dc_wrap">
		<div class="domainreginput" id="domainincart">
			<div class="dc_wrap_elements">
				<select id="incartsld">
					{foreach key=num item=incartdomain from=$incartdomains}
					<option value="{$incartdomain}">{$incartdomain}</option>
					{/foreach}
				</select>

			</div>

			<input type="submit" value="{$LANG.ordercontinuebutton}" class="btn" />
		</div>
	
		<div class="domainreginput" id="domainregister">
			<div class="dc_wrap_elements">
				<span>www.</span>&nbsp;
				
				<input type="text" id="registersld" size="30" value="{$sld}" />
				
				<select id="registertld">
				{foreach key=num item=listtld from=$registertlds}
					<option value="{$listtld}"{if $listtld eq $tld} selected="selected"{/if}>{$listtld}</option>
				{/foreach}
				</select>
			</div>

			<input type="submit" value="{$LANG.ordercontinuebutton}" class="btn" />
		</div>
		
		<div class="domainreginput" id="domaintransfer">
			<div class="dc_wrap_elements">
				<span>www.</span>
			
				<input type="text" id="transfersld" size="30" value="{$sld}" />
				<select id="transfertld">
					{foreach key=num item=listtld from=$transfertlds}
					<option value="{$listtld}"{if $listtld eq $tld} selected="selected"{/if}>{$listtld}</option>
					{/foreach}
				</select>
			</div>

			<input type="submit" value="{$LANG.ordercontinuebutton}" class="btn" />
		</div>
		
		<div class="domainreginput" id="domainowndomain">
			<div class="dc_wrap_elements">
				<span>www.</span>

				<input type="text" id="owndomainsld" size="30" value="{$sld}" />

				<span>&nbsp;.&nbsp;</span>

				<input type="text" id="owndomaintld" size="5" value="{$tld|substr:1}" />

			</div>

			<input type="submit" value="{$LANG.ordercontinuebutton}" class="btn" />
		</div>
		
		<div class="domainreginput" id="domainsubdomain">
			<div class="dc_wrap_elements">
				<span>http://</span>

				<input type="text" id="subdomainsld" size="30" value="{$sld}" />
				
				<select id="subdomaintld">
					{foreach from=$subdomains key=subid item=subdomain}
					<option value="{$subid}">{$subdomain}</option>
					{/foreach}
				</select>
			</div>

			<input type="submit" value="{$LANG.ordercontinuebutton}" class="btn" />
		</div>
		
	</div>
	
	</form>
</div>

{if $freedomaintlds}<p>* <em>{$LANG.orderfreedomainregistration} {$LANG.orderfreedomainappliesto}: {$freedomaintlds}</em></p>{/if}

<div class="clear"></div>

<div id="loading3" class="loading"><img src="images/loading.gif" border="0" alt="Loading..." /></div>

<form method="post" action="cart.php?a=add&pid={$pid}" id="domainfrm">
<div id="domainresults"></div>
</form>

</div>