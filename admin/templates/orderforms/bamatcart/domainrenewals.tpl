<div id="order-comparison">

<h1>{$LANG.domainrenewals}</h1>

<p>{$LANG.domainrenewdesc}</p>

<form method="post" action="cart.php?a=add&renewals=true">

<div class="center80">

<table class="table table-striped table-framed">
	<thead>
		<tr>
			<th class="textcenter" style="width: 20px"></th>
			<th class="textcenter">{$LANG.orderdomain}</th>
			<th class="textcenter">{$LANG.domainstatus}</th>
			<th class="textcenter">{$LANG.domaindaysuntilexpiry}</th>
			<th class="textcenter"></th>
		</tr>
	</thead>

	<tbody>
		{foreach from=$renewals item=renewal}
		<tr class="carttablerow">
			<td>{if !$renewal.pastgraceperiod}<input type="checkbox" name="renewalids[]" value="{$renewal.id}" />{/if}</td>
			<td>{$renewal.domain}</td>
			<td>{$renewal.status}</td>
			<td>
				{if $renewal.daysuntilexpiry > 30}
				<span class="textgreen">{$renewal.daysuntilexpiry} {$LANG.domainrenewalsdays}</span>
				{elseif $renewal.daysuntilexpiry > 0}
				<span class="textred">{$renewal.daysuntilexpiry} {$LANG.domainrenewalsdays}</span>
				{else}
				<span class="textblack">{$renewal.daysuntilexpiry*-1} {$LANG.domainrenewalsdaysago}</span>
				{/if}
				{if $renewal.ingraceperiod}
				<br />
				<span class="textred">{$LANG.domainrenewalsingraceperiod}<span>
				{/if}
			</td>
			<td>
				{if $renewal.beforerenewlimit}
				<span class="textred">{$LANG.domainrenewalsbeforerenewlimit|sprintf2:$renewal.beforerenewlimitdays}<span>
				{elseif $renewal.pastgraceperiod}
				<span class="textred">{$LANG.domainrenewalspastgraceperiod}<span>
				{else}
				<select name="renewalperiod[{$renewal.id}]">
					{foreach from=$renewal.renewaloptions item=renewaloption}
					<option value="{$renewaloption.period}">{$renewaloption.period} {$LANG.orderyears} @ {$renewaloption.price}</option>
					{/foreach}
				</select>
				{/if}
			</td>
		</tr>
		{foreachelse}
		
		<tr class="carttablerow">
			<td colspan="5">{$LANG.domainrenewalsnoneavailable}</td>
		</tr>
		
		{/foreach}
	</tbody>
</table>
</div>

<p class="text-center"><input type="submit" value="{$LANG.ordernowbutton} &raquo;" class="btn" /></p>

</form>

</div>