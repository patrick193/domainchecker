<div id="order-comparison">

{include file="orderforms/bamatcart/comparisonsteps.tpl" step=2}

<div class="col-md-2"></div>

<div class="col-md-8">
	{$LANG.cartdomainsconfiginfo}
	<br />
	{if $errormessage}<div class="alert alert-danger">{$LANG.clientareaerrors}<ul>{$errormessage}</ul></div><br />{/if}
	
	<form method="post" action="{$smarty.server.PHP_SELF}?a=confdomains" id="conf_domain_form">
	<input type="hidden" name="update" value="true" />

	{foreach key=num item=domain from=$domains}
	<h3>{$domain.domain}</h3>
	<div class="domainconfig">
	<table class="table table-striped table-framed table-domain-cart">
		<tbody>
			<tr>
				<th>{$LANG.orderdomain}:</td>
				<td>{if $domain.eppenabled}{$LANG.ordertransferdomain}{else}{$LANG.orderregisterdomain}{/if}</td>
			</tr>

			<tr>
				<th>{$LANG.hosting}:</td>
				<td>{if $domain.hosting}<span class="text-success"><span class="glyphicon glyphicon-ok"></span> {$LANG.cartdomainshashosting}</span>{else}<a href="cart.php" style="color:#cc0000;"><span class="glyphicon glyphicon-wrench"></span> {$LANG.cartdomainsnohosting}</a><br />{/if}</td>
			</tr>
			<tr>
				<th>{$LANG.orderregperiod}:</td>
				<td>
					{$domain.regperiod} {$LANG.orderyears}
					{if $domain.eppenabled}<input type="hidden" name="epp[{$num}]" value="{$domain.eppvalue}" />{/if}
				</td>
			</tr>
			
			{if $domain.dnsmanagement || $domain.emailforwarding || $domain.idprotection}
			<tr>
				<th>{$LANG.cartaddons}:</th>
				<td>
					{if $domain.dnsmanagement}<label><input type="checkbox" name="dnsmanagement[{$num}]"{if $domain.dnsmanagementselected} checked{/if} /> {$LANG.domaindnsmanagement} ({$domain.dnsmanagementprice})</label>{/if}
					{if $domain.emailforwarding}<label><input type="checkbox" name="emailforwarding[{$num}]"{if $domain.emailforwardingselected} checked{/if} /> {$LANG.domainemailforwarding} ({$domain.emailforwardingprice})</label>{/if}
					{if $domain.idprotection}<label><input type="checkbox" name="idprotection[{$num}]"{if $domain.idprotectionselected} checked{/if} /> {$LANG.domainidprotection} ({$domain.idprotectionprice})</label>{/if}
				</td>
			</tr>
			{/if}
			
			{foreach key=domainfieldname item=domainfield from=$domain.fields}
			<tr>
				<th>{$domainfieldname}:</th>
				<td>{$domainfield}</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
	
	</div>
	{/foreach}

	<p class="text-center"><input type="submit" value="{$LANG.updatecart}" class="btn" /></p>

	</form>
</div>

<div class="col-md-2"></div>
<div class="clear"></div>
</div>