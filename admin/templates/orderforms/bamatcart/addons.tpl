<div id="order-comparison">

<h1>{$LANG.cartproductaddons}</h1>

<p>{$LANG.cartfollowingaddonsavailable}</p>

{foreach from=$addons key=num item=addon}
<div class="addoncontainer">
<form method="post" action="{$smarty.server.PHP_SELF}?a=add">
<input type="hidden" name="aid" value="{$addon.id}" />
<div class="addon">
<div class="title">{$addon.name}</div>
<div class="pricing">{if $addon.free}
{$LANG.orderfree}
{else}
{$addon.recurringamount} {$addon.billingcycle}
{if $addon.setupfee}<br /><span class="setup">{$addon.setupfee} {$LANG.ordersetupfee}</span>{/if}
{/if}</div>
<div class="clear"></div>
{$addon.description}
<div class="product">
<select name="productid">
{foreach from=$addon.productids item=product}
<option value="{$product.id}">{$product.product}{if $product.domain} - {$product.domain}{/if}</option>
{/foreach}
</select> <input type="submit" value="{$LANG.ordernowbutton}" class="cartbutton green" />
</div>
</div>
</form>
</div>
{if $num is not div by 2}
<div class="clear"></div>
{/if}
{/foreach}
<div class="clear"></div>

{if $noaddons}
<div class="alert alert-danger">{$LANG.cartproductaddonsnone}</div>
{/if}

</div>