{if $step eq 1}
	{assign var=progress value=33}
{elseif $step eq 2}
	{assign var=progress value=66}
{else}
	{assign var=progress value=100}
{/if}

<div class="bamatsteps">
	<div class="progress reverse">
		<div class="progress-bar progress-bar-striped active" role="progressbar" style="width: {$progress}%"></div>
	</div>

	<!-- <hr> -->
	
	<div class="stepwrap {if $step eq 1}current{/if}" id="step_domain">
		<div class="stepbox">
			<span class="glyphicon glyphicon-screenshot"></span>
		</div>
		<span class="step_label">1. {$LANG.cartproductdomainchoose}</span>
	</div>

	<div class="stepwrap {if $step eq 2}current{/if}" id="step_options">
		<div class="stepbox">
			<span class="glyphicon glyphicon-check"></span>
		</div>
		<span class="step_label">2. {$LANG.cartproductchooseoptions}</span>
	</div>

	<div class="stepwrap {if $step eq 3}current{/if}" id="step_checkout">
		<div class="stepbox">
			<span class="glyphicon glyphicon-shopping-cart"></span>
		</div>
		<span class="step_label">3. {$LANG.cartreviewcheckout}</span>
	</div>
	
</div>
