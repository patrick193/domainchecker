<div id="order-comparison">

{include file="orderforms/bamatcart/comparisonsteps.tpl" step=3}

<div class="cartcontainer">

<h2>{$LANG.cartexistingclientlogin}</h2>

<p>{$LANG.cartexistingclientlogindesc}</p>

<form action="dologin.php" method="post">

{if $incorrect}<div class="alert alert-danger">{$LANG.loginincorrect}</div><br>{/if}

<table class="text-center">
<tr><td class="text-right">{$LANG.loginemail}1:</td><td><input type="text" name="username" size="40" value="{$username}" /></td></tr>
<tr><td class="text-right">{$LANG.loginpassword}:</td><td><input type="password" name="password" size="25" /></td></tr>
</table>
<p class="text-center"><input type="submit" value="{$LANG.loginbutton}" class="cartbutton" /></p>

</form>

<p><strong>{$LANG.loginforgotten}</strong> <a href="pwreset.php" target="_blank">{$LANG.loginforgotteninstructions}</a></p>

</div>

</form>

</div>