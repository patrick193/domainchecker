<div id="order-comparison">

{include file="orderforms/bamatcart/comparisonsteps.tpl" step=3}

<div class="cartcontainer">

<h2>{$LANG.orderconfirmation}</h2>

<br />

<p>{$LANG.orderreceived}</p>

<div class="cartbox">
<p class="text-center"><strong>{$LANG.ordernumberis} {$ordernumber}</strong></p>
</div>

<p>{$LANG.orderfinalinstructions}</p>

{if $invoiceid && !$ispaid}
<div class="alert alert-danger">{$LANG.ordercompletebutnotpaid}</div><br>
<p class="text-center"><a href="viewinvoice.php?id={$invoiceid}" target="_blank">{$LANG.invoicenumber}{$invoiceid}</a></p>
{/if}

{foreach from=$addons_html item=addon_html}
<div style="margin:15px 0 15px 0;">{$addon_html}</div>
{/foreach}

{if $ispaid}
<!-- Enter any HTML code which needs to be displayed once a user has completed the checkout of their order here - for example conversion tracking and affiliate tracking scripts -->
{/if}

<br /><br />

<p class="text-center"><a href="clientarea.php">{$LANG.ordergotoclientarea}</a></p>

<br />

</div>

</div>