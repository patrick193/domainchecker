<div id="order-comparison">

{include file="orderforms/bamatcart/comparisonsteps.tpl" step=3}

<div class="cartcontainer">

<p class="totalduetoday" style="float:left">{$LANG.thereisaproblem}</p><br /><br />

<h2>{$errortitle}</h2>

<p>{$errormsg}</p>

<p class="text-center"><br /><a href="javascript:history.go(-1)">&laquo; {$LANG.problemgoback}</a></p>

</div>

</div>