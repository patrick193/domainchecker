// Same height for product cells at one line [products.tpl]
function adjustProductCells(index) {
	var titleCell = null,
		productCells = '',
		includePadding = 20;

	// get the needed objects
	if ( index == -1 ) {
		titleCell = jQuery('#order-comparison .productfeature .productfeature-head');
		productCells = '#order-comparison .productcol-wrap .productcol-head';
	} else {
		titleCell = jQuery('#order-comparison .productfeature .productfeature-title-' + index);
		productCells = '#order-comparison .productcol-wrap .productcol-feature-' + index;
	}
	
	// dropping height to auto
	jQuery(productCells).each( function() {
		jQuery(this).height('auto');
	});

	// for responsive - do nothing
	if ( jQuery(window).width() <= 620 ) return;

	// detect the maximum height of all cells in a row
	var maxHeight = titleCell.height();
		
	jQuery(productCells).each( function() {
		if ( jQuery(this).height() > maxHeight )
			maxHeight = jQuery(this).height();
	});
	
	// set the maximum height for all elements
	titleCell.height(maxHeight + includePadding);

	jQuery(productCells).each( function() {
		jQuery(this).height( maxHeight + includePadding);
	});
}
	
// Same height for all products information rows [products.tpl]
function adjustProductHeights() {
	var i = 0;
	
	while ( jQuery('#order-comparison .productfeature .productfeature-title-' + i).length > 0 ) {
		adjustProductCells(i);
		i++;
	}
}

// Adjust rows with products [products.tpl]
function adjustProductRows() {
	var productCount = jQuery('.productcol').length,
		maxProducts = productCount, // for responsive mode (until is not specified) placing all products in one row
		colWidth = '100%';
	
	if ( jQuery(window).width() > 768 ) {
		maxProducts = 3;
		colWidth = '23%';
	} else if ( jQuery(window).width() > 620 ) {
		maxProducts = 2;
		colWidth = '31%';
	}
	
	var rowCount = Math.ceil( productCount / maxProducts);
	
	if ( maxProducts == 1 ) rowCount = 1;
		
	// Getting the necessary amount of rows
	for (row = 0; row < rowCount; row++) {
		if ( jQuery('.product-row-' + row).length ) {
		} else {
			jQuery('<div class="product-row product-row-' + row + '"></div>').insertAfter( '.product-row-' + (row - 1) );
			jQuery( '.product-row-0 .productfeature-wrap' ).clone().appendTo( '.product-row-' + row );
		}
	}
		
	// Placing products to rows
	var currentRow = 0;
	
	for (i = 0; i < productCount; i++) {
		if ( (i + 1) > (maxProducts * (currentRow + 1)) ) currentRow++;
		
		if ( !jQuery('.productcol-wrap-' + i).parent().hasClass('product-row-' + currentRow) ) {
			jQuery('.productcol-wrap-' + i).appendTo('.product-row-' + currentRow);
		}
		
		jQuery('.productcol-wrap-' + i).width(colWidth);
		jQuery('.product-row-' + currentRow + ' .productfeature-wrap').width(colWidth);
	}

	// Removing empty rows
	currentRow = rowCount;
	while ( jQuery('.product-row-' + currentRow).length > 0 ) {
		jQuery('.product-row-' + currentRow).remove();
	}
}

// Detailed features - tabs [products.tpl]
function openTab(index) {
	var tab = jQuery('.dc_feature_tab-' + index);
	
	if ( tab.hasClass('active') ) {
		// close opened tab
		tab.removeClass('active');
		tab.addClass('inactive');
		
		jQuery('.glyphicon', tab).removeClass('glyphicon-minus');
		jQuery('.glyphicon', tab).addClass('glyphicon-plus');
	} else {
		// open tab
		tab.removeClass('inactive');
		tab.addClass('active');

		jQuery('.glyphicon', tab).removeClass('glyphicon-plus');
		jQuery('.glyphicon', tab).addClass('glyphicon-minus');
		
		adjustFeaturesRows(index);
	}
	
	// close all tabs except selected
	jQuery('.dc_feature_tab').each( function() {
		if ( !jQuery(this).hasClass('dc_feature_tab-' + index) ) {
			jQuery(this).removeClass('active');
			jQuery(this).addClass('inactive');

			jQuery('.glyphicon', jQuery(this)).removeClass('glyphicon-minus');
			jQuery('.glyphicon', jQuery(this)).addClass('glyphicon-plus');
		}
	});
}

// Same height for rows with features [products.tpl]
function adjustFeaturesRows(index) {
	var content = jQuery('.dc_feature_tab-' + index + ' .dc_feature_tab_content'),
		padding = 18;
	
	if (!content.length) return;

	// set height to default
	jQuery( 'li', content ).each( function() {
		jQuery(this).height('auto');
	});
	
	for (i = 0; true; i++) {
		if ( !jQuery('.dc_feature_item-' + i, content ).length ) break;
		
		var maxHeight = 0;
		
		jQuery('.dc_feature_item-' + i, content ).each( function() {
			console.log( jQuery('.dc_feature_item-' + i, content ).height() );
			if ( jQuery('.dc_feature_item-' + i, content ).height() > maxHeight )
				maxHeight = jQuery('.dc_feature_item-' + i, content ).height();
		});
		
		jQuery('.dc_feature_item-' + i, content ).each( function() {
			jQuery('.dc_feature_item-' + i, content ).height(maxHeight + padding);
		});
	}
}

// Check domain while configuring product options [configureproductdomain.tpl]
function checkdomain() {
	var domainoption = jQuery( '.domainoptions input:checked' ).val();
	var sld = jQuery( '#' + domainoption + 'sld' ).val();
	var tld = '';

	if ( domainoption == 'incart' ) var sld = jQuery( '#' + domainoption + 'sld option:selected' ).text();
	if ( domainoption == 'subdomain' )
		var tld = jQuery( '#' + domainoption + 'tld option:selected' ).text();
	else
		var tld = jQuery( '#' + domainoption + 'tld' ).val();
		
	jQuery( '#loading3' ).slideDown();
	
	jQuery.post( 'cart.php', { a: 'domainoptions', sld: sld, tld: tld, checktype: domainoption, ajax: 1 }, function(data) {
		jQuery( '#domainresults' ).html(data);
		jQuery( '#domainresults' ).slideDown();
		jQuery( '#loading3' ).slideUp();
	});
} 

jQuery(document).ready(function(){
	// *** Products page [products.tpl]
	adjustProductHeights();
	adjustProductRows();
	
	jQuery(window).resize( function() {
		adjustProductHeights();
		adjustProductRows();
	});
	
	jQuery('.dc_feature_tab_title').click( function() {
		openTab( jQuery(this).attr('data-tab-index') );
	});
	
	// *** Add a domain for a product [configureproductdomain.tpl]
	jQuery( '.domainreginput' ).hide();
	jQuery( '.domainoptions input:first' ).attr('checked', 'checked');
	jQuery( '#domain' + jQuery('.domainoptions input:first').val() ).show();
	jQuery( '#checkdomain_form' ).hide();
	jQuery( '#regtransfer_wrap' ).show();
	
    jQuery( '.domainoptions input' ).click(function(){
		console.log(jQuery(this).val());
	
		if ( jQuery(this).val() == 'regtransfer' ) {
			jQuery( '#checkdomain_form' ).hide();
			jQuery( '#regtransfer_wrap' ).show();
		} else {
			jQuery( '#checkdomain_form' ).show();
			jQuery( '#regtransfer_wrap' ).hide();
			
			jQuery( '#domainresults' ).slideUp();
			jQuery( '.domainreginput' ).hide();
			jQuery( '#domain' + jQuery(this).val() ).show();
		}
    });
	
	jQuery( '#checkdomain_form' ).submit(function(e) {
		e.preventDefault();
		checkdomain();
	});
	
	// *** Cart page [viewcart.tpl]
	// changing quantity without reloading a page
	jQuery( '.qty_btn' ).click(function(e) {
		var id = jQuery(this).attr('data-qty-id');
		
		var data = {
			'id': id,
			'type': jQuery(this).attr('data-qty-type'),
			'sid': jQuery('#sid').html(),
		};
		
		jQuery.post('templates/orderforms/bamatcart/js/cart_qty.php', data, function(response) {
			jQuery('#qty_' + id).val(response);

			jQuery.get('cart.php?a=view', function(data) {
				jQuery('#pricing_' + id).html( jQuery( '#pricing_' + id, jQuery(data) ).html() );
				jQuery('#summary_rows').html( jQuery( '#summary_rows', jQuery(data) ).html() );
				jQuery('.checkoutbuttonsright').html( jQuery( '.checkoutbuttonsright', jQuery(data) ).html() );
			});
		});
	});
});
