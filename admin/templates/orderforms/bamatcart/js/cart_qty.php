<?php
$sid = htmlspecialchars($_POST['sid']);
session_id( $sid );
session_start();

$id		= $_POST['id'];
$type	= intval($_POST['type']);

if ( isset($_SESSION['cart']['products'][$id]) && is_array( $_SESSION['cart']['products'][$id] ) ) {
	$value = $_SESSION['cart']['products'][$id]['qty'];
	
	switch ($type) {
		case 0:	if ( $value > 1) $value--;
				break;
		case 1:	$value++;
				break;
	}
}

$_SESSION['cart']['products'][$id]['qty'] = $value;

echo $value;
//echo session_id();
?>