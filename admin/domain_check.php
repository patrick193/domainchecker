<?php
define( 'CLIENTAREA', true );
require_once('init.php');
require_once('includes/cartfunctions.php');
require_once('includes/domain-check/dc_const.php');
require_once('includes/domain-check/dc_utils.php');

$dc_error = DC_ER_NONE;

// *** Get captcha configuration from WHMCS Database
$use_captcha = false;

$result = select_query( 'tblconfiguration', 'setting, value', array( 'setting' => 'CaptchaSetting' ) );
$data = mysql_fetch_assoc($result);

if ( defined('DC_CAPTCHA') && DC_CAPTCHA )
	if ( isset($data['value']) )
		$use_captcha = ( $data['value'] == 'on' ) || ( $data['value'] == 'offloggedin' && !isset($_SESSION["uid"] ) );

// *** Check captcha if needed
if ( $use_captcha && !empty($_SESSION['ctext']) && !empty($_GET['dc_dom']) ) {
	$dc_captcha = htmlspecialchars( $_GET['dc_captcha'] );
	
	if ( $_SESSION['ctext'] !== md5( strtoupper($dc_captcha) ) )
		$dc_error = DC_ER_CAPTCHA;
	
	$_SESSION['ctext'] = '';
}

// *** Getting user info
dc_user_info($dc_country, $dc_cur);

/*
// If the client is not logged in we take his location from GeoIP and currency = EUR
// otherwise we take country and currency from user's profile
if ( !empty($_SESSION["uid"]) ) {
	require_once( ROOTDIR . '/includes/clientfunctions.php' );

	$client_info = getClientsDetails( $_SESSION["uid"] );

	$dc_country = $client_info['countryname'];
	
	$cur = getCurrency($_SESSION["uid"]);
	$dc_cur = $cur['code'];
} else {
	if ( empty($dc_country) ) {
		require_once( dirname(__FILE__) . '/includes/domain-check/geoip/master/src/geoip.inc' );
		
		$gi = geoip_open(dirname(__FILE__) . '/includes/domain-check/geoip/GeoIP.dat', GEOIP_STANDARD);
		
		$gi_code = geoip_country_code_by_addr($gi, $_SERVER[REMOTE_ADDR]);
		$dc_country = geoip_country_name_by_addr($gi, $_SERVER[REMOTE_ADDR]);

		geoip_close($gi);
	}
	
	if ( $_GET['dc_cur'] != '' ) {

		if ( $_GET['dc_cur'] == 'CHF' ) {
			$dc_cur = 'CHF';
		} else {
			$dc_cur = 'EUR';
		}

	} elseif ( empty($dc_cur) ) {

		if( $gi_code == 'CH' ) {
			$dc_cur = 'CHF'; 
		} else {
			$dc_cur = 'EUR';
		}
		
	}
	
}
*/

// *** Process different requests types

// * The client requested selected domains from domain check results - add them to SESSION in order WHMCS will process it further 
if ( isset( $_POST['domains'] ) && is_array( $_POST['domains'] ) ) {
	$domains 			= $_POST['domains'];
	$domainsregperiod	= $_POST['domainsregperiod'];
	$domainsaction		= $_POST['domainsaction'];

	// This is used to prevent domain duplication in the cart
	$cart_domains = array();
	foreach ($_SESSION['cart']['domains'] as $domain)
		$cart_domains[] = $domain['domain'];

	foreach ($domains as $domain) {
		if ( in_array($domain, $cart_domains) ) continue;
	
		$new_domain = array('type' => $domainsaction[$domain], 
							'domain' => strtolower($domain), 
							'regperiod' => $domainsregperiod[$domain],
							);
		
		if ( $domainsaction[$domain] == 'transfer' )
			$new_domain['eppcode'] = 'EMPTY';
	
		$_SESSION['cart']['domains'][] = $new_domain;
		$cart_domains[] = strtolower($domain);
	}
	
	if ( $_POST['dc_is_product'] ) {
		// If this is a product registration - we connect a domain with a product 
		$cart_domains = array();
		foreach ($_SESSION['cart']['domains'] as $domain)
			$cart_domains[] = $domain['domain'];
			
		$prodarray = array( "pid" => $_POST['dc_pid'], "domain" => $cart_domains[0], "noconfig" => false );
		$_SESSION["cart"]["products"][] = $prodarray;

		$_SESSION["cart"]["newproduct"] = true;
		$newprodnum = count( $_SESSION["cart"]["products"] ) - 1;
		header( "Location: cart.php?a=confproduct&i=" . $newprodnum );
	} else {
		// otherwise it is domain registration/transfer
		header( "Location: cart.php?a=confdomains");
	}

	exit();
}

// * The client requires to check domain by Category
$dc_dom = dc_prepare_sld($_GET['dc_dom']);

// Prepare the ajax query based on the current search state
// if no search string - show only search form
// otherwise show search results
if ( empty($dc_dom) || $dc_error == DC_ER_CAPTCHA ) {
	$dc_ajax_file = 'dc_search.php';
} else {
	$dc_ajax_file = 'dc_process.php';
}

$dc_ajax_src = htmlspecialchars($_SERVER['REQUEST_URI']); // current url which is used to correctly generate currency switch on the search form
$dc_ajax_logged_in = !empty($_SESSION["uid"]); // currency switch is not available for logged in users

if ( !empty($dc_dom) ) $dc_ajax_dom = "'dc_dom': '$dc_dom',"; // include the domain search string into the query if it present
if ( !empty($_GET['dc_cat']) ) $dc_ajax_cat .= "'dc_cat': '" . htmlspecialchars($_GET['dc_cat']) . "',"; // include selected search Category into the query if it present
if ( defined('DC_PRODUCTDOMAIN') ) $dc_ajax_is_product .= "'dc_is_product': true,"; // if this is a request from product domain selection page (configureproductdomain.tpl) - redirect the search form there

$dc_ajax = "
	jQuery(document).ready( function() {
		var data = {
			'dc_src': '$dc_ajax_src',
			'dc_country': '$dc_country',
			'dc_cur': '$dc_cur',
			'logged_in': '$dc_ajax_logged_in',
			'use_captcha': '$use_captcha',
			'dc_error': '$dc_error',
			$dc_ajax_dom
			$dc_ajax_cat
			$dc_ajax_is_product
		};
		
		jQuery.post('includes/domain-check/$dc_ajax_file?" . $_SERVER['QUERY_STRING'] . "', data, function(response) {
			jQuery('#domain_check .wrap').html(response);
			jQuery('#dc_preloader').fadeOut();
			jQuery('#domain_check').fadeIn();
			
			jQuery('#dc_table').focus();
		});
	});";


	
if ( defined('DC_EMBEDDED') ) {
	// Using smarty to display
	global $whmcs;
	global $_LANG;

	$dc_smarty = new Smarty();
	$dc_smarty->caching = 0;
	$dc_smarty->compile_dir = $whmcs->get_template_compiledir_name();

	$dc_smarty->assign( "LANG", $_LANG );

	$dc_smarty->assign('dc_ajax', $dc_ajax);
	$dc_smarty->assign('dc_do_search', !empty($dc_dom) );
	
	echo $dc_smarty->fetch( ROOTDIR . '/templates/' . $whmcs->get_config('Template') . '/domain_check.tpl' );
} else {
	// Using page display mode
	$ca = new WHMCS_ClientArea();

	$ca->setPageTitle($_LANG["domaintitle"]);
	 
	$ca->addToBreadCrumb('index.php',$whmcs->get_lang('globalsystemname'));
	$ca->addToBreadCrumb('domain_check.php', $_LANG["domaintitle"]);
	 
	$ca->initPage();
	
	$ca->assign('dc_ajax', $dc_ajax);
	$ca->assign('dc_do_search', !empty($dc_dom) );

	$templatefile = 'domain_check';

	$ca->setTemplate($templatefile);
	$ca->output();
}
	
?>
