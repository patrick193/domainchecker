<?php
/**
 * WHMCS Language File
 * German (de)
 *
 * Please Note: These language files are overwritten during software updates
 * and therefore editing of these files directly is not advised. Instead we
 * recommend that you use overrides to customise the text displayed in a way
 * which will be safely preserved through the upgrade process.
 *
 * For instructions on overrides, please visit:
 *   http://docs.whmcs.com/Language_Overrides
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2014
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */

if (!defined("WHMCS")) die("This file cannot be accessed directly");

$_LANG['isocode'] = "de";

$_LANG['domaintitle'] = "Domainauswahl";
$_LANG['updatecart'] = "jetzt bestellen";
$_LANG['cartproductdomaindesc'] = "Jetzt benötigen wir zur Ausführung Ihres Auftrages noch die Domain, die wir mit dem gewählten Produkt verknüpfen. Sie können entweder eine bestehende Domain zu uns transferieren, oder sich aus unseren über 600 Top-Level-Domains Domains eine neue individuelle Domain generieren.<br /><br />Ihre bestehende Domain mitsamt Endung (z.B. .de/.com) tragen Sie bitte in das erste Feld.";
$_LANG['domaincheckerchoosedomain'] = "Domainauswahl";

// -------------------------------------
// Language strings for domain check
// -------------------------------------
$_LANG['domain_check']['preparepage'] = "Bitte warten Sie, während die Seite geladen wird.";
$_LANG['domain_check']['waitforsearch'] = "Ihre Wunschdomain wird nun gesucht. Wir prüfen in jedem relevanten Land und bei den einzelnen Registrierungsstellen, ob Ihre Wunschdomain frei ist.<br /><br />Das kann einen kleinen Augenblick dauern.<br />Bitte haben Sie Geduld und warten Sie, bis die Seite endgültig geladen ist.";
$_LANG['domain_check']['editsearch'] = "Wunschdomain";
$_LANG['domain_check']['check'] = "Prüfen";
$_LANG['domain_check']['clientcountry'] = "Herkunftsland";
$_LANG['domain_check']['clientcurrency'] = "Alle Preise in %s";
$_LANG['domain_check']['currencyswitch'] = "auf %s umschalten";
$_LANG['domain_check']['EUR'] = "Euro";
$_LANG['domain_check']['CHF'] = "Schweizer Franken";
$_LANG['domain_check']['tablepricenotice'] = "* Domain Jahrespreis, ggf. zzgl. Einrichtungsgebühren";
$_LANG['domain_check']['tabledomain'] = "Domain-Name";
$_LANG['domain_check']['tablestatus'] = "Status";
$_LANG['domain_check']['tablesetupprice'] = "Setup Preis";
$_LANG['domain_check']['tableprice'] = "Preis / Jahr";
$_LANG['domain_check']['tablecategory'] = "Land / Bezeichnung";
$_LANG['domain_check']['tablecheckall'] = "Alle Auswählen";
$_LANG['domain_check']['statusfree'] = "frei";
$_LANG['domain_check']['statustaken'] = "vergeben";
$_LANG['domain_check']['actionregister'] = "registrieren";
$_LANG['domain_check']['actiontransfer'] = "umziehen";
$_LANG['domain_check']['domainproceed'] = "Domains registrieren";
$_LANG['domain_check']['cartregisterortransfer'] = "Registrieren Sie einen neuen Domain oder übertragen Sie Ihre Domain von einem anderen Registrar";

// -------------------------------------
// Change EPP/Auth-code
// -------------------------------------
$_LANG['dc_epp_tab']			= "EPP/Authcode"; 
$_LANG['dc_epp_label']			= "Bitte geben Sie den EPP/Authcode ein.";
$_LANG['dc_epp_desc']			= "Wir benötigen zum Providerwechsel einen sogenannten Authcode. Diesen erhalten Sie von Ihrem alten Provider.";
$_LANG['dc_epp_submit'] 		= "Senden"; 
$_LANG['dc_epp_page']			= "Change EPP Code";
$_LANG['dc_epp_no_data']		= "Not enough data";
$_LANG['dc_epp_no_domain']		= "Domain not found";
$_LANG['dc_epp_no_user']		= "You are not allowed to manage this domain";
$_LANG['dc_epp_no_status']		= "You can not change EPP code for this domain";
$_LANG['dc_epp_no_order']		= "Order not found";
$_LANG['dc_epp_success']		= "EPP Code changed successfully";
