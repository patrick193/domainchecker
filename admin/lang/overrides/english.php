<?php
/**
 * WHMCS Language File
 * English (en)
 *
 * Please Note: These language files are overwritten during software updates
 * and therefore editing of these files directly is not advised. Instead we
 * recommend that you use overrides to customise the text displayed in a way
 * which will be safely preserved through the upgrade process.
 *
 * For instructions on overrides, please visit:
 *   http://docs.whmcs.com/Language_Overrides
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2014
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */

if (!defined("WHMCS")) die("This file cannot be accessed directly");

$_LANG['isocode'] = 'en';
$_LANG['updatecart'] = "buy now";

// -------------------------------------
// Language strings for domain check
// -------------------------------------
$_LANG['domain_check']['preparepage'] = "Please wait while the page is loading.";
$_LANG['domain_check']['waitforsearch'] = "Your domain name is searched. We are checking in each relevant country and at the individual registries if the domain you desire is free.<br /><br />This may take a while.<br />Please be patient and wait until the page is finally loaded.";
$_LANG['domain_check']['editsearch'] = "Search for";
$_LANG['domain_check']['check'] = "Check";
$_LANG['domain_check']['clientcountry'] = "Your Location";
$_LANG['domain_check']['clientcurrency'] = "All prices in %s";
$_LANG['domain_check']['currencyswitch'] = "switch to %s";
$_LANG['domain_check']['EUR'] = "Euro";
$_LANG['domain_check']['CHF'] = "Swiss Franc";
$_LANG['domain_check']['tablepricenotice'] = "* Year price for a domain, plus setup fees";
$_LANG['domain_check']['tabledomain'] = "Domain name";
$_LANG['domain_check']['tablestatus'] = "Status";
$_LANG['domain_check']['tablesetupprice'] = "Setup Price";
$_LANG['domain_check']['tableprice'] = "Price / Year";
$_LANG['domain_check']['tablecategory'] = "Country / Location";
$_LANG['domain_check']['tablecheckall'] = "Check all";
$_LANG['domain_check']['statusfree'] = "free";
$_LANG['domain_check']['statustaken'] = "unavailable";
$_LANG['domain_check']['actionregister'] = "register";
$_LANG['domain_check']['actiontransfer'] = "transfer";
$_LANG['domain_check']['domainproceed'] = "Register domains";
$_LANG['domain_check']['cartregisterortransfer'] = "Register a new domain or transfer your domain from another registrar";

// -------------------------------------
// Change EPP/Auth-code
// -------------------------------------
$_LANG['dc_epp_tab']			= "EPP Code";
$_LANG['dc_epp_label']			= "You must enter the EPP code for";
$_LANG['dc_epp_desc']			= "This needs to be obtained from the current registrar for authorisation";
$_LANG['dc_epp_submit']			= "Submit";
$_LANG['dc_epp_page']			= "Change EPP Code";
$_LANG['dc_epp_no_data']		= "Not enough data";
$_LANG['dc_epp_no_domain']		= "Domain not found";
$_LANG['dc_epp_no_user']		= "You are not allowed to manage this domain";
$_LANG['dc_epp_no_status']		= "You can not change EPP code for this domain";
$_LANG['dc_epp_no_order']		= "Order not found";
$_LANG['dc_epp_success']		= "EPP Code changed successfully";
