<?php
/**
 * WHMCS Language File
 * Russian (ru)
 *
 * Please Note: These language files are overwritten during software updates
 * and therefore editing of these files directly is not advised. Instead we
 * recommend that you use overrides to customise the text displayed in a way
 * which will be safely preserved through the upgrade process.
 *
 * For instructions on overrides, please visit:
 *   http://docs.whmcs.com/Language_Overrides
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2014
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */

if (!defined("WHMCS")) die("This file cannot be accessed directly");

$_LANG['isocode'] = "ru";

// -------------------------------------
// Language strings for domain check
// -------------------------------------
$_LANG['domain_check']['preparepage'] = "Пожалуйста, дождитесь загрузки страницы.";
$_LANG['domain_check']['waitforsearch'] = "Осуществляется поиск Вашего доменного имени. Мы проверяем свободен ли Ваш домен в каждой соответствующей стране и индивидуальных реестрах.<br /><br />Это может занять какое-то время.<br />Подождите, пожалуйста, полной загрузки страницы.";
$_LANG['domain_check']['editsearch'] = "Искать";
$_LANG['domain_check']['check'] = "Проверить";
$_LANG['domain_check']['clientcountry'] = "Ваше расположение";
$_LANG['domain_check']['clientcurrency'] = "Цены указаны в %s";
$_LANG['domain_check']['currencyswitch'] = "показать в %s";
$_LANG['domain_check']['EUR'] = "Евро";
$_LANG['domain_check']['CHF'] = "Швейцарский Франк";
$_LANG['domain_check']['tablepricenotice'] = "* Цена за домен на год, плюс стоимость установки";
$_LANG['domain_check']['tabledomain'] = "Доменное имя";
$_LANG['domain_check']['tablestatus'] = "Состояние";
$_LANG['domain_check']['tablesetupprice'] = "Установка";
$_LANG['domain_check']['tableprice'] = "Цена / Год";
$_LANG['domain_check']['tablecategory'] = "Страна / Расположение";
$_LANG['domain_check']['tablecheckall'] = "Отметить все";
$_LANG['domain_check']['statusfree'] = "свободен";
$_LANG['domain_check']['statustaken'] = "занят";
$_LANG['domain_check']['actionregister'] = "регистрировать";
$_LANG['domain_check']['actiontransfer'] = "перенести";
$_LANG['domain_check']['domainproceed'] = "Зарегистрировать домены";
$_LANG['domain_check']['cartregisterortransfer'] = "Зарегистрировать новый домен или перенести свой домен от другого регистратора";

// -------------------------------------
// Change EPP/Auth-code
// -------------------------------------
$_LANG['dc_epp_tab']			= "Код EPP";
$_LANG['dc_epp_label']			= "Введите код EPP для";
$_LANG['dc_epp_desc']			= "Этот код необходимо получить у текущего регистратора для авторизации";
$_LANG['dc_epp_submit']			= "Отправить";
$_LANG['dc_epp_page']			= "Изменить код EPP";
$_LANG['dc_epp_no_data']		= "Недостаточно данных";
$_LANG['dc_epp_no_domain']		= "Домен не найден";
$_LANG['dc_epp_no_user']		= "У Вас нет прав для управления этим доменом";
$_LANG['dc_epp_no_status']		= "Вы не можете изменить код EPP для этого домена";
$_LANG['dc_epp_no_order']		= "Заказ не найден";
$_LANG['dc_epp_success']		= "Код EPP успешно изменен";
