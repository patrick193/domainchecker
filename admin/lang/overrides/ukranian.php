<?php
/**
 * WHMCS Language File
 * Ukranian (uk-ua)
 *
 * Please Note: These language files are overwritten during software updates
 * and therefore editing of these files directly is not advised. Instead we
 * recommend that you use overrides to customise the text displayed in a way
 * which will be safely preserved through the upgrade process.
 *
 * For instructions on overrides, please visit:
 *   http://docs.whmcs.com/Language_Overrides
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2014
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */

if (!defined("WHMCS")) die("This file cannot be accessed directly");

$_LANG['isocode'] = "ua";

// -------------------------------------
// Language strings for domain check
// -------------------------------------
$_LANG['domain_check']['preparepage'] = "Будь ласка, дочекайтеся завантаження сторінки.";
$_LANG['domain_check']['waitforsearch'] = "Здійснюється пошук Вашого доменного імені. Ми перевіряемо чи вільний Ваш домен в кожній відповідній країні та індивідуальних реєстрах.<br /><br />Це може зайняти деякий час.<br />Дочекайтеся, будь ласка, повного заватаження сторінки.";
$_LANG['domain_check']['editsearch'] = "Шукати";
$_LANG['domain_check']['check'] = "Перевірити";
$_LANG['domain_check']['clientcountry'] = "Ваше розташування";
$_LANG['domain_check']['clientcurrency'] = "Ціни вказані у %s";
$_LANG['domain_check']['currencyswitch'] = "показати у %s";
$_LANG['domain_check']['EUR'] = "Євро";
$_LANG['domain_check']['CHF'] = "Швейцарський Франк";
$_LANG['domain_check']['tablepricenotice'] = "* Ціна за домен на рік, плюс вартість встановлення";
$_LANG['domain_check']['tabledomain'] = "Доменне ім'я";
$_LANG['domain_check']['tablestatus'] = "Стан";
$_LANG['domain_check']['tablesetupprice'] = "Встановлення";
$_LANG['domain_check']['tableprice'] = "Ціна / Рік";
$_LANG['domain_check']['tablecategory'] = "Країна / Розташування";
$_LANG['domain_check']['tablecheckall'] = "Відмітити все";
$_LANG['domain_check']['statusfree'] = "вільний";
$_LANG['domain_check']['statustaken'] = "зайнятий";
$_LANG['domain_check']['actionregister'] = "реєструвати";
$_LANG['domain_check']['actiontransfer'] = "перенести";
$_LANG['domain_check']['domainproceed'] = "Зареєструвати домени";
$_LANG['domain_check']['cartregisterortransfer'] = "Зареєструвати новий домен або перенести свій домен від іншого реєстратора";

// -------------------------------------
// Change EPP/Auth-code
// -------------------------------------
$_LANG['dc_epp_tab']			= "Код EPP";
$_LANG['dc_epp_label']			= "Введіть код EPP для";
$_LANG['dc_epp_desc']			= "Цей код необхідно отримати у поточного реєстратора для авторизації";
$_LANG['dc_epp_submit']			= "Відправити";
$_LANG['dc_epp_page']			= "Змінити код EPP";
$_LANG['dc_epp_no_data']		= "Недостатньо даних";
$_LANG['dc_epp_no_domain']		= "Домен не знайдено";
$_LANG['dc_epp_no_user']		= "У Вас немає прав для керуванням цім доменом";
$_LANG['dc_epp_no_status']		= "Ви не можете змінити код EPP для цього домену";
$_LANG['dc_epp_no_order']		= "Замовлення не знайдено";
$_LANG['dc_epp_success']		= "Код EPP успішно змінено";
