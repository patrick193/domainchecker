<?php
/* -------------------------------------------------------------- */
/*
	*	Virturo API Connect v 1.0.0
	*   Domain Registrar Module (WHMCS)
	*	Developed by Daniel Arnhold | Living Internet GmbH
	*   2014
*/
/* -------------------------------------------------------------- */
	error_reporting(E_ALL);
	header("Content-Type: text/html; charset=utf-8"); 
/* -------------------------------------------------------------- */

function virturo_getConfigArray() {
	/*$configarray = array(
	 "Username" => array( "Type" => "text", "Size" => "20", "Description" => "Enter your username here", ),
	 "Password" => array( "Type" => "password", "Size" => "20", "Description" => "Enter your password here", ),
	 "TestMode" => array( "Type" => "yesno", ),
	);
	*/
	
	$configarray = array(
		"username" => "da0c1498bad9241810b1a9065affb6c70714e4bf",
		"password" => "f2665d2413e05aa577820da217e4374d259c5ac6",
                "TestMode"       => "yes"
	);
	
	return $configarray;
}

function virturo__sendrequest($commandStr, $params) {
	$url = 'http://login.virturo.de/api.php';

	$commandTmpArr = explode(".",$commandStr);
	$command = array('class' => $commandTmpArr[0], 'method' => $commandTmpArr[1]);

	$postData = array_merge($params, $command);		
	
	var_dump($postData);
	
	$c = curl_init();
	curl_setopt( $c, CURLOPT_URL , $url );
	curl_setopt( $c, CURLOPT_POST , 1 );
	curl_setopt( $c, CURLOPT_TIMEOUT , 100 );
	curl_setopt( $c, CURLOPT_RETURNTRANSFER , 1 );
	curl_setopt( $c, CURLOPT_POSTFIELDS , $postData );
	$result = html_entity_decode(curl_exec( $c ));
	curl_close( $c );
	
	$resultParts = explode(";",$result);
	
	foreach($resultParts as $part) {
		$kvPair = explode('=', $part,2);
		if (count($kvPair) == 2)
			$return[$kvPair[0]] = $kvPair[1];
	}
	
	return $return;
}

function virturo__getanswers ($method, $params) {
	$params = array_merge($params, virturo_getConfigArray());
	$val = virturo__sendrequest($method, $params);
	
	return ($val);
}

/* -------------------------------------------------------------- */

$doof = virturo__getanswers("DomainManagement.Start", array('tld'=>'de', 'sld'=>'1und1') );
echo $doof["result"].'<br>';
echo $doof["message"];




function virturo_GetNameservers($params) {
	return array('ns1'=>'n1.virturo.de', 'ns2'=>'ns2.virturo.de');
}

function virturo_RegisterDomain($params) {
	$username = $params["Username"];
	$password = $params["Password"];
	$testmode = $params["TestMode"];
	$tld = $params["tld"];
	$sld = $params["sld"];
	$regperiod = $params["regperiod"];
	$nameserver1 = $params["ns1"];
	$nameserver2 = $params["ns2"];
	# Registrant Details
	$RegistrantFirstName = $params["firstname"];
	$RegistrantLastName = $params["lastname"];
	$RegistrantAddress1 = $params["address1"];
	$RegistrantAddress2 = $params["address2"];
	$RegistrantCity = $params["city"];
	$RegistrantStateProvince = $params["state"];
	$RegistrantPostalCode = $params["postcode"];
	$RegistrantCountry = $params["country"];
	$RegistrantEmailAddress = $params["email"];
	$RegistrantPhone = $params["phonenumber"];
	# Admin Details
	$AdminFirstName = $params["adminfirstname"];
	$AdminLastName = $params["adminlastname"];
	$AdminAddress1 = $params["adminaddress1"];
	$AdminAddress2 = $params["adminaddress2"];
	$AdminCity = $params["admincity"];
	$AdminStateProvince = $params["adminstate"];
	$AdminPostalCode = $params["adminpostcode"];
	$AdminCountry = $params["admincountry"];
	$AdminEmailAddress = $params["adminemail"];
	$AdminPhone = $params["adminphonenumber"];
	# Put your code to register domain here
	# If error, return the error message in the value below
	$values["error"] = $error;
	return $values;
}

function virturo_TransferDomain($params) {
	$username = $params["Username"];
	$password = $params["Password"];
	$testmode = $params["TestMode"];
	$tld = $params["tld"];
	$sld = $params["sld"];
	$regperiod = $params["regperiod"];
	$transfersecret = $params["transfersecret"];
	$nameserver1 = $params["ns1"];
	$nameserver2 = $params["ns2"];
	# Registrant Details
	$RegistrantFirstName = $params["firstname"];
	$RegistrantLastName = $params["lastname"];
	$RegistrantAddress1 = $params["address1"];
	$RegistrantAddress2 = $params["address2"];
	$RegistrantCity = $params["city"];
	$RegistrantStateProvince = $params["state"];
	$RegistrantPostalCode = $params["postcode"];
	$RegistrantCountry = $params["country"];
	$RegistrantEmailAddress = $params["email"];
	$RegistrantPhone = $params["phonenumber"];
	# Admin Details
	$AdminFirstName = $params["adminfirstname"];
	$AdminLastName = $params["adminlastname"];
	$AdminAddress1 = $params["adminaddress1"];
	$AdminAddress2 = $params["adminaddress2"];
	$AdminCity = $params["admincity"];
	$AdminStateProvince = $params["adminstate"];
	$AdminPostalCode = $params["adminpostcode"];
	$AdminCountry = $params["admincountry"];
	$AdminEmailAddress = $params["adminemail"];
	$AdminPhone = $params["adminphonenumber"];
	# Put your code to transfer domain here
	# If error, return the error message in the value below
	$values["error"] = $error;
	return $values;
}

function virturo_RenewDomain($params) {
	$username = $params["Username"];
	$password = $params["Password"];
	$testmode = $params["TestMode"];
	$tld = $params["tld"];
	$sld = $params["sld"];
	$regperiod = $params["regperiod"];
	# Put your code to renew domain here
	# If error, return the error message in the value below
	$values["error"] = $error;
	return $values;
}


?>